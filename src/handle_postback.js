const call_send_API = require('./call_send_API'),
  saon = require('./sender_action_on'),
  sol = require('./mongodb/leaf/show_one_leaf.js'),
  cufi = require('./mongodb/user_fb_info/create_user_fb_info.js'),
  logger = require('./utils/logger/logger.js'),
  soufi = require('./mongodb/user_fb_info/show_one_user_fb_info'),
  us = require('./mongodb/user/update_user_by_psid.js'),
  console_logger = logger.getLogger(),

  plugin_url = process.env.PLUGIN_URL || "https://legal-chatbot-ai-plugin.herokuapp.com",
  plugin_port = process.env.PLUGIN_PORT || '';

// Handles messaging_postbacks events
exports.handlePostback = function handlePostback(
  sender_psid,
  received_postback
) {
  // Get the payload for the postback
  let payload = received_postback.payload;
  saon.sender_action_on(sender_psid, function (err, result) { });

  let soubp = require('./mongodb/user/show_one_user_by_psid.js');
  soubp.show_one_user_by_psid(sender_psid.toString(), function (err, result) {
    if (result) {
      var is_active = result.IS_ACTIVE,
        status = result.STATUS,
        live_chat = result.LIVE_CHAT;
      if (is_active == 1) {
        if (live_chat == 0) {
          saon.sender_action_on(sender_psid, function (err, result) { });
          let cua = require('../src/mongodb/user_activity/create_user_activity.js');
          cua.create_user_activity(sender_psid.toString(), payload, function (err, result) {
            if (err) {
              console_logger.error(err);
            } else {
              if (result) {
                console_logger.info('create user activity successfully');
              } else {
                console_logger.info('create user activity unsuccessfully');
              }
            }
          });
          if (payload == 'WEATHER') {
            let hwp = require('./utils/weather/handle_weather_payload.js');
            hwp.handle_weather_payload(function (err, result) {
              if (err) {
                console_logger.error(err);
              } else {
                if (result) {
                  saon.sender_action_on(sender_psid, function (err, result) { });
                  call_send_API.callSendAPI(sender_psid, result);
                  setTimeout(() => {
                    saon.sender_action_on(sender_psid, function (err, result) { });
                    sol.show_one_leaf('DEFAULT', function (err, result) {
                      if (result) {
                        result.map(e => {
                          call_send_API.callSendAPI(sender_psid, e.VALUE);
                        });
                      }
                    });
                  }, 4000);
                }
              }
            });
          }
          else if (payload == 'GET_STARTED') {
            soufi.show_one_user_fb_info(sender_psid.toString(), function (err, result) {
              call_send_API.callSendAPI(sender_psid, { "text": "Xin chào bạn " + result.FB_NAME + "!" })
            })
            setTimeout(() => {
              sol.show_one_leaf('GET_STARTED', function (err, result) {
                if (err) {
                  console_logger.error(err);
                } else {
                  saon.sender_action_on(sender_psid, function (err, result) { });
                  result.map((e, i) => {
                    (function a(e, i) {
                      setTimeout(() => {
                        saon.sender_action_on(sender_psid, function (err, result) { });
                        call_send_API.callSendAPI(sender_psid, e.VALUE);
                      }, (2000 * i))
                    })(e, i);
                  });
                  if (status == 0) {
                    saon.sender_action_on(sender_psid, function (err, result) { });
                    setTimeout(() => {
                      sol.show_one_leaf('LOGIN_FORM', function (err, result) {
                        if (err) {
                          console_logger.error(err);
                        } else {
                          saon.sender_action_on(sender_psid, function (err, result) { });
                          let ed = require('./utils/crypt/encrypt_data.js');
                          result[0].VALUE.attachment.payload.elements[0].buttons[0].url = plugin_url + plugin_port + "/login?data=" + ed.encrypt_data(sender_psid.toString());
                          call_send_API.callSendAPI(sender_psid, result[0].VALUE);
                        }
                      });
                    }, 4000)
                  } else {
                    saon.sender_action_on(sender_psid, function (err, result) { });
                    setTimeout(() => {
                      sol.show_one_leaf('HOME', function (err, result) {
                        if (err) {
                          console_logger.error(err);
                        } else {
                          result.map((e, i) => {
                            (function a(e, i) {
                              setTimeout(() => {
                                saon.sender_action_on(sender_psid, function (err, result) { });
                                call_send_API.callSendAPI(sender_psid, e.VALUE);
                              }, (2000 * i))
                            })(e, i);
                          });
                        }
                      });
                    }, 4000)
                  }
                }
              });
            }, 2000)
          }
          else if (payload == 'ACTIVATE/INACTIVATE_BOT') {
            saon.sender_action_on(sender_psid, function (err, result) { });
            us.update_user_by_psid(sender_psid, { LIVE_CHAT: 1 }, function (err, result) {
              if (result) {
                console.log('ok')
                let inactivate_bot = {
                  "text": "Bạn đã tắt chức năng chat với bot. Xin mời bật lên để thực hiện thao tác với chatbot."
                }
                call_send_API.callSendAPI(sender_psid, inactivate_bot);
              }
            })

          }
          else {
            if (status == 0) {
              saon.sender_action_on(sender_psid, function (err, result) { });
              sol.show_one_leaf('LOGIN_FORM', function (err, result) {
                if (err) {
                  console_logger.error(err);
                } else {
                  let ed = require('./utils/crypt/encrypt_data.js');
                  result[0].VALUE.attachment.payload.elements[0].buttons[0].url = plugin_url + plugin_port + "/login?data=" + ed.encrypt_data(sender_psid.toString());
                  call_send_API.callSendAPI(sender_psid, result[0].VALUE);
                }
              });
            } else {
              saon.sender_action_on(sender_psid, function (err, result) { });
              sol.show_one_leaf(payload, function (err, result) {
                if (err) {
                  console_logger.error(err);
                } else {
                  if (result) {
                    result.map((e, i) => {
                      (function a(e, i) {
                        setTimeout(() => {
                          saon.sender_action_on(sender_psid, function (err, result) { });
                          call_send_API.callSendAPI(sender_psid, e.VALUE);
                        }, (2000 * i))
                      })(e, i);
                    });
                    setTimeout(() => {
                      saon.sender_action_on(sender_psid, function (err, result) { });
                      sol.show_one_leaf('DEFAULT', function (err, result) {
                        if (result) {
                          call_send_API.callSendAPI(sender_psid, result[0].VALUE);
                        }
                      });
                    }, 4000);
                  }
                  else {
                    saon.sender_action_on(sender_psid, function (err, result) { });
                    sol.show_one_leaf('DEFAULT', function (err, result) {
                      if (err) {
                        console_logger.error(err);
                      } else {
                        if (result) {
                          call_send_API.callSendAPI(sender_psid, result[0].VALUE);
                        }
                      }
                    });
                  }
                }
              });
            }
          }
        }
        else {
          if (payload == 'ACTIVATE/INACTIVATE_BOT') {
            saon.sender_action_on(sender_psid, function (err, result) { });
            us.update_user_by_psid(sender_psid, { LIVE_CHAT: 1 }, function (err, result) {
              if (result) {
                let activate_bot = {
                  "text": "Bạn đã bật chức năng chat với bot. Xin thực hiện thao tác với chatbot."
                };
                call_send_API.callSendAPI(sender_psid, activate_bot);
              }
            })

          }
        }
      }
    } else {
      saon.sender_action_on(sender_psid, function (err, result) { });
      let cu = require('./mongodb/user/create_user.js');
      cu.create_user(sender_psid, function (err, result) {
        if (err) {
          console_logger.error(err);
        } else {
          if (result) {
            cufi.create_user_fb_info(sender_psid, function (err, result) {
              soufi.show_one_user_fb_info(sender_psid.toString(), function (err, result) {
                call_send_API.callSendAPI(sender_psid, { "text": "Xin chào bạn " + result.FB_NAME + "!" })
                saon.sender_action_on(sender_psid, function (err, result) { });
                sol.show_one_leaf('GET_STARTED', function (err, result) {
                  if (err) {
                    console_logger.error(err);
                  } else {
                    result.map((e, i) => {
                      (function a(e, i) {
                        setTimeout(() => {
                          saon.sender_action_on(sender_psid, function (err, result) { });
                          call_send_API.callSendAPI(sender_psid, e.VALUE);
                        }, (2000 * i))
                      })(e, i);
                    });
                  }
                });
              })
            })
          }
        }
      });
    }
  });
}
