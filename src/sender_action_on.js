const request = require('request'), logger = require('./utils/logger/logger.js'),
  console_logger = logger.getLogger();

// Sends response messages via the Send API
exports.sender_action_on = function sender_action_on(sender_psid) {
  // Construct the message body
  let request_body = {
    "recipient": {
      "id": sender_psid
    },
    "sender_action": "typing_on"
  }

  // Send the HTTP request to the Messenger Platform
  request({
    "uri": "https://graph.facebook.com/v2.6/me/messages",
    "qs": {
      "access_token": "EAAIMSYvpR4cBAB3SbJICX5ddNoRmQjedVnIdr4RMKEABnIY0TxHP9V2twrvIFsnscZCSh5T7iFEXup0RUBMPZCI9j4DFh5VdSq9R6NEvOM8RgusEbzshk1ZC4gUTjkorSBVrjT2ZAqYBTiNqN6JFTzXJ1zDAZAnW75VsVyZAOZAXgZDZD"
    },
    "method": "POST",
    "json": request_body
  }, (err, res, body) => {
    if (!err) {
      console_logger.info('typing_on!');
    } else {
      console_logger.error("Unable to typing_on:" + err);
    }
  });
}