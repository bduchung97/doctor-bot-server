exports.post_admin_logout = function post_admin_logout(req, res) {
  let data = req.body,
    session_token = data.session_token,
    soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
  soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
    if (err) {
      res.status(500).send({ msg_code: '006' });
    } else {
      if (result) {
        let das = require('../../mongodb/admin_session/delete_admin_session.js');
        das.delete_admin_session(result._id, function (err, result) {
          if (err) {
            res.status(500).send({ msg_code: '006' });
          } else {
            if (result) {
              res.status(200).send({ msg_code: '001' });
            } else {
              res.status(500).send({ msg_code: '007' });
            }
          }
        });
      } else {
        res.status(400).send({ msg_code: '005' });
      }
    }
  });
}
