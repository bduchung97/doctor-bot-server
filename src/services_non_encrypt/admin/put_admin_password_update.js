const logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger();
exports.put_admin_password_update = function put_admin_password_update(req, res) {
  let data = req.body,
    session_token = data.session_token,
    soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
  soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
    if (err) {
      console_logger.error(err);
      res.status(500).send({ msg_code: '006' });
    } else {
      if (result) {
        let req_data = data.data,
          admin_ID = req_data.admin_ID,
          old_password = req_data.old_password,
          new_password = req_data.new_password,
          soa = require('../../mongodb/admin/show_one_admin.js');
        soa.show_one_admin(admin_ID, function (err, result) {
          if (err) {
            console_logger.error(err);
            res.status(500).send({ msg_code: '006' });
          }
          else {
            console.log(result)
            if (result) {
              let password = result.PASSWORD,
                cp = require('../../utils/crypt/compare_password.js'),
                hp = require('../../utils/crypt/hash_password.js');
              if (cp.compare_password(old_password, password) == true) {
                let ua = require('../../mongodb/admin/update_admin.js');
                ua.update_admin(admin_ID, { PASSWORD: hp.hash_password(new_password) },
                  function (err, result) {
                    if (err) {
                      console_logger.error(err);
                      res.status(500).send({ msg_code: '006' });
                    } else {
                      if (result) {
                        console_logger.info('Update password successfully');
                        res.status(200).send({ msg_code: '001' });
                      } else {
                        res.status(500).send({ msg_code: '007' });
                      }
                    }
                  });
              }
              else {
                console_logger.info('password invalid');
                res.status(200).send({ msg_code: '002' });
              }
            } else {
              console_logger.info("no admin exist");
              res.status(200).send({ msg_code: '002' }); 
            }
          }
        })
      } else {
        console_logger.info("check token failed");
        res.status(400).send({ msg_code: '005' });
      }
    }
  });
}
