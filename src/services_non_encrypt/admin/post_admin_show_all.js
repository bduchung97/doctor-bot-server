exports.post_admin_show_all = function post_admin_show_all(req, res) {
  let data = req.body,
    session_token = data.session_token,
    soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
  soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
    if (err) {
      res.status(500).send({ msg_code: '006' });
    } else {
      if (result) {
        let saa = require('../../mongodb/admin/show_all_admins.js');
        saa.show_all_admins(
          function (err, result) {
            if (err) {
              res.status(500).send({ msg_code: '006' });
            } else {
              if (result) {
                res.status(200).send({ msg_code: '001', admins_data: result });
              } else {
                res.status(500).send({ msg_code: '007' });
              }
            }
          });
      } else {
        res.status(400).send({ msg_code: '005' });
      }
    }
  });
}
