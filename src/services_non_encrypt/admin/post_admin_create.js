exports.post_admin_create = function post_admin_create(req, res) {
  let data = req.body;
  let session_token = data.session_token,
    soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
  soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
    if (err) {
      res.status(500).send({ msg_code: '006' });
    } else {
      if (result) {
        let admin_info = data.data,
          fullname = admin_info.fullname,
          username = admin_info.username,
          password = admin_info.password,
          email = admin_info.email,
          phone_number = admin_info.phone_number,
          permission = admin_info.permission,
          admin_id = admin_info.admin_id,
          ca = require('../../mongodb/admin/create_admin.js');
        ca.create_admin(
          fullname,
          username,
          password,
          email,
          phone_number,
          permission,
          admin_id,
          function (err, result) {
            if (err) {
              res.status(500).send({ msg_code: '006' });
            } else {
              if (result) {
                res.status(200).send({ msg_code: '001' });
              } else {
                res.status(500).send({ msg_code: '007' });
              }
            }
          });
      } else {
        res.status(400).send({ msg_code: '005' });
      }
    }
  });
}
