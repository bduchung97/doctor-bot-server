const logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger();
exports.post_admin_login = function post_admin_login(req, res) {
  let data = req.body,
    username = data.data.username,
    password = data.data.password,
    soabu = require('../../mongodb/admin/show_one_admin_by_username.js');
  soabu.show_one_admin_by_username(username, function (err, result) {
    if (err) {
      console_logger.error(err);
      res.status(500).send({ msg_code: '006' });
    } else {
      if (result) {
        var cp = require('../../utils/crypt/compare_password.js'),
          admin_data = result,
          admin_id = admin_data._id;
        if (cp.compare_password(password, admin_data.PASSWORD) == true) {
          let soasbai = require('../../mongodb/admin_session/show_one_admin_session_by_admin_id.js');
          soasbai.show_one_admin_session_by_admin_id(admin_id, function (err, result) {
            if (err) {
              console_logger.error(err);
              res.status(500).send({ msg_code: '006' });
            } else {
              if (result) {
                let das = require('../../mongodb/admin_session/delete_admin_session.js');
                das.delete_admin_session(result._id, function (err, result) {
                  if (err) {
                    console_logger.error(err);
                    res.status(500).send({ msg_code: '006' });
                  } else {
                    if (result) {
                      let cas = require('../../mongodb/admin_session/create_admin_session.js');
                      cas.create_admin_session(admin_id, function (err, result) {
                        if (err) {
                          console_logger.error(err);
                          res.status(500).send({ msg_code: '006' });
                        } else {
                          if (result) {
                            var admin_session_id = result;
                            let soas = require('../../mongodb/admin_session/show_one_admin_session');
                            soas.show_one_admin_session(admin_session_id, function (err, result) {
                              if (err) {
                                res.status(500).send({ msg_code: '006' });
                              }
                              else {
                                if (result) {
                                  console_logger.info('login successfully');
                                  res.status(200).send(
                                    {
                                      msg_code: '001',
                                      data: {
                                        admin_data: admin_data, 
                                        session_token: result.TOKEN
                                      }
                                    });
                                } else {
                                  res.status(500).send({ msg_code: '007' });
                                }
                              }
                            });
                          } else {
                            res.status(500).send({ msg_code: '007' });
                          }
                        }
                      });
                      let cash = require('../../mongodb/admin_session_history/create_admin_session_history.js');
                      cash.create_admin_session_history(admin_id, function (err, result) { });
                    } else {
                      res.status(500).send({ msg_code: '007' });
                    }
                  }
                });
              } else {
                let cas = require('../../mongodb/admin_session/create_admin_session.js');
                cas.create_admin_session(admin_id, function (err, result) {
                  if (err) {
                    console_logger.error(err);
                    res.status(500).send({ msg_code: '006' });
                  } else {
                    if (result) {
                      var admin_session_id = result;
                      let soas = require('../../mongodb/admin_session/show_one_admin_session');
                      soas.show_one_admin_session(admin_session_id, function (err, result) {
                        if (err) {
                          res.status(500).send({ msg_code: '006' });
                        }
                        else {
                          if (result) {
                            console_logger.info('login successfully');
                            res.status(200).send(
                              {
                                msg_code: '001',
                                data: {
                                  admin_data: admin_data, 
                                  session_token: result.TOKEN
                                }
                              });
                          } else {
                            res.status(500).send({ msg_code: '007' });
                          }
                        }
                      });
                    } else {
                      res.status(500).send({ msg_code: '007' });
                    }
                  }
                });
                let cash = require('../../mongodb/admin_session_history/create_admin_session_history.js');
                cash.create_admin_session_history(admin_id, function (err, result) { });
              }
            }
          });
        } else {
          res.status(200).send({ msg_code: '002' });
        }
      } else {
        res.status(200).send({ msg_code: '002' });
      }
    }
  });
}
