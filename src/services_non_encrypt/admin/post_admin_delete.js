let logger = require('../../utils/logger/logger.js'),
  error_logger = logger.getLogger('err'),
  another_logger = logger.getLogger();
exports.post_admin_delete = function post_admin_delete(req, res) {
  let data = req.body,
    session_token = data.session_token,
    soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
  soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
    if (err) {
      error_logger.error(err);
      res.status(500).send({ msg_code: '006' });
    } else {
      if (result) {
        another_logger.info('check token successfully');
        let req_data = data.data,
          admin_ID = req_data.admin_id,
          updated_data = { IS_ACTIVE: 0, UPDATED_AT: new Date(), UPDATED_BY: admin_ID },
          ua = require('../../mongodb/admin/update_admin.js');
        ua.update_admin(admin_ID, updated_data,
          function (err, result) {
            if (err) {
              error_logger.error(err);
              res.status(500).send({ msg_code: '006' });
            } else {
              if (result) {
                res.status(200).send({ msg_code: '001' });
              } else {
                res.status(500).send({ msg_code: '007' });
              }
            }
          });
      } else {
        res.status(400).send({ msg_code: '005' });
      }
    }
  });
}
