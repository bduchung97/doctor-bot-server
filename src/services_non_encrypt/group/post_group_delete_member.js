exports.post_group_delete_member = function post_group_delete_member(req, res) {
  let data = req.body,
    session_token = data.session_token,
    soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
  soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
    if (err) {
      res.status(500).send({ msg_code: '006' });
    } else {
      if (result) {
        let req_data = data.data,
          group_ID = req_data.group_ID,
          user_ID = req_data.user_ID,
          dm = require('../../mongodb/group/delete_member.js');
        dm.delete_member(group_ID, user_ID, function (err, result) {
          if (err) {
            res.status(500).send({ msg_code: '006' });
          } else {
            if (result) {
              res.status(200).send({ msg_code: '001' });
            } else {
              res.status(500).send({ msg_code: '007' });
            }
          }
        });
      } else {
        res.status(400).send({ msg_code: '005' });
      }
    }
  });
}
