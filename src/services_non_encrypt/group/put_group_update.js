exports.put_group_update = function put_group_update(req, res) {
  let data = req.body,
    session_token = data.session_token,
    soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
  soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
    if (err) {
      res.status(500).send({ msg_code: '006' });
    } else {
      if (result) {
        let req_data = data.data,
          group_ID = req_data.group_ID,
          new_group_name = req_data.new_group_name,
          sogbn = require('../../mongodb/group/show_one_group_by_name.js');
        sogbn.show_one_group_by_name(new_group_name, function (err, result) {
          if (err) {
            res.status(500).send({ msg_code: '006' });
          } else {
            if (result) {
              res.status(200).send({ msg_code: '002' });
            } else {
              let ug = require('../../mongodb/group/update_group.js');
              ug.update_group(group_ID, { NAME: new_group_name }, function (err, result) {
                if (err) {
                  res.status(500).send({ msg_code: '006' });
                } else {
                  if (result) {
                    res.status(200).send({ msg_code: '001' });
                  } else {
                    res.status(500).send({ msg_code: '007' });
                  }
                }
              });
            }
          }
        });
      } else {
        res.status(400).send({ msg_code: '005' });
      }
    }
  });
}
