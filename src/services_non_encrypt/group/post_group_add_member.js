exports.post_group_add_member = function post_group_add_member(req, res) {
  let
    data = req.body,
    session_token = data.session_token,
    soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
  soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
    if (err) {
      res.status(500).send({ msg_code: '006' });
    } else {
      if (result) {
        let req_data = data.data,
          group_ID = req_data.group_ID,
          user_ID = req_data.user_ID,
          PSID = req_data.PSID,
          au = require('../../mongodb/group/add_user.js');
        au.add_user(group_ID, user_ID, PSID, function (err, result) {
          if (err) {
            res.status(500).send({ msg_code: '006' });
          } else {
            if (result) {
              res.status(200).send({ msg_code: '001' });
            } else {
              res.status(200).send({ msg_code: '002' });
            }
          }
        });
      } else {
        res.status(400).send({ msg_code: '005' });
      }
    }
  });
}
