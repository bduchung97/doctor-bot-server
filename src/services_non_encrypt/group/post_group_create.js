const echasync = require('echasync');
var count = 0;
exports.post_group_create = function post_group_create(req, res) {
  let
    data = req.body,
    session_token = data.session_token,
    soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
  soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
    if (err) {
      res.status(500).send({ msg_code: '006' });
    } else {
      if (result) {
        let group_data = data.data,
          group_name = group_data.group_name,
          member_list = group_data.member_list,
          admin_ID = result.ADMIN__ID.toString(),
          sogbn = require('../../mongodb/group/show_one_group_by_name.js');
        sogbn.show_one_group_by_name(group_name, function (err, result) {
          if (err) {
            res.status(500).send({ msg_code: '006' });
          } else {
            if (result) {
              res.status(200).send({ msg_code: '002' });
            } else {
              let cg = require('../../mongodb/group/create_group.js');
              cg.create_group(group_name, admin_ID, function (err, result) {
                if (err) {
                  res.status(500).send({ msg_code: '006' });
                } else {
                  if (result) {
                    echasync.do(member_list, function (next, element) {
                      let au = require('../../mongodb/group/add_user');
                      au.add_user(result, element.user_ID, element.PSID, function (err, result) {
                        if (err) {
                          res.status(500).send({ msg_code: '006' });
                        } else {
                          //console.log(result);
                          count = count + 1;
                        }
                        next();
                      })
                    }, function () {
                      //console.log('create group successfully');
                      console.log(count)
                      res.status(200).send({ msg_code: '001' });
                    });
                  } else {
                    res.status(500).send({ msg_code: '007' });
                  }
                }
              });
            }
          }
        });
      } else {
        res.status(400).send({ msg_code: '005' });
      }
    }
  });
}
