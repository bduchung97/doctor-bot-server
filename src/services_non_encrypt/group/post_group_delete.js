const echasync = require('echasync');
exports.post_group_delete = function post_group_delete(req, res) {
  let
    data = req.body,
    session_token = data.session_token,
    soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
  soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
    if (err) {
      res.status(500).send({ msg_code: '006' });
    } else {
      if (result) {
        let req_data = data.data,
          group_ID = req_data.group_ID,
          member_data = req_data.member_data;
        echasync.do(member_data, function (next, element) {
          let dm = require('../../mongodb/group/delete_member.js');
          dm.delete_member(group_ID, element.user_ID, function (err, result) {
            if (err) {
              res.status(500).send({ msg_code: '006' });
            } else {
              //console.log(result);
            }
            next();
          })
        }, function () {
          console.log('delete members from group successfully');
          let dg = require('../../mongodb/group/delete_group.js');
          dg.delete_group(group_ID, function (err, result) {
            if (err) {
              res.status(500).send({ msg_code: '006' });
            } else {
              res.status(200).send({ msg_code: '001' });
            }
          });
        });
      } else {
        res.status(400).send({ msg_code: '005' });
      }
    }
  });
}
