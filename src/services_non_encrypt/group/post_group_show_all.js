const echasync = require('echasync');
exports.post_group_show_all = function post_group_show_all(req, res) {
  let
    data = req.body,
    session_token = data.session_token,
    soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
  soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
    if (err) {
      res.status(500).send({ msg_code: '006' });
    } else {
      if (result) {
        let sag = require('../../mongodb/group/show_all_groups.js');
        sag.show_all_groups(
          function (err, result) {
            if (err) {
              res.status(500).send({ msg_code: '006' });
            } else {
              if (result) {
                var groups_data = [];
                echasync.do(result, function (next_group, e_group) {
                  var users_data = [];
                  echasync.do(e_group.MEMBERS, function (next_user, e_user) {
                    let sou = require('../../mongodb/user/show_one_user.js');
                    sou.show_one_user(e_user.user_ID, function (err, result) {
                      if (err) {
                        res.status(500).send({ msg_code: '006' });
                      } else {
                        if (result) {
                          users_data.push({
                            DISTINGUISHED_NAME: result.DISTINGUISHED_NAME,
                            USER_ID: result._id
                          });
                        }
                      }
                      next_user();
                    });
                  }, function () {
                    groups_data.push({
                      NAME: e_group.NAME,
                      GROUP_ID: e_group._id,
                      USERS: users_data
                    });
                    next_group();
                  });
                }, function () {
                  res.status(200).send({ msg_code: '001', groups_data: groups_data });
                });
              } else {
                res.status(500).send({ msg_code: '007' });
              }
            }
          });
      } else {
        res.status(400).send({ msg_code: '005' });
      }
    }
  });
}
