const echasync = require('echasync');
exports.post_user_show_all = function post_user_show_all(req, res) {
  let
    data = body.data,
    session_token = data.session_token,
    soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
  soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
    if (err) {
      res.status(500).send({ msg_code: '006' });
    } else {
      if (result) {
        let sau = require('../../mongodb/user/show_all_users');
        sau.show_all_users(
          function (err, result) {
            if (err) {
              res.status(500).send({ msg_code: '006' });
            } else {
              if (result) {
                var users_data = [];
                echasync.do(result, function (next_user, e_user) {
                  let gfud = require('../../utils/facebook/get_facebook_user_data.js');
                  gfud.get_facebook_user_data(e_user.PSID, function (err, result) {
                    var groups_data = [];
                    echasync.do(e_user.GROUPS, function (next_group, e_group) {
                      let sag = require('../../mongodb/group/show_one_group_by_id');
                      sag.show_one_group_by_id(e_group, function (err, result) {
                        if (err) {
                          res.status(500).send({ msg_code: '006' });
                        } else {
                          if (result) {
                            groups_data.push({ NAME: result.NAME });
                          }
                        }
                        next_group();
                      });
                    }, function () {
                      users_data.push({
                        _id: e_user._id,
                        FB_NAME: result.name,
                        FB_FIRST_NAME: result.first_name,
                        FB_LAST_NAME: result.last_name,
                        FB_PROFILE_PIC: result.profile_pic,
                        FB_LOCALE: result.locale,
                        FB_GENDER: result.gender,
                        FB_TIMEZONE: result.timezone,
                        PSID: e_user.PSID,
                        DISTINGUISHED_NAME: e_user.DISTINGUISHED_NAME,
                        PHONE_NUMBER: e_user.PHONE_NUMBER,
                        EMAIL: e_user.EMAIL,
                        GROUPS: groups_data,
                        STATUS: e_user.STATUS,
                        IS_ACTIVE: e_user.IS_ACTIVE
                      });
                      next_user();
                    });
                  });
                }, function () {
                  res.status(200).send({ msg_code: '001', users_data: users_data });
                });
              } else {
                res.status(500).send({ msg_code: '007' });
              }
            }
          });
      } else {
        res.status(400).send({ msg_code: '005' });
      }
    }
  });
}
