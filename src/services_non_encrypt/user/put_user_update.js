exports.put_user_update = function put_user_update(req, res) {
  let data = req.body,
    session_token = data.session_token,
    soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
  soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
    if (err) {
      res.status(500).send({ msg_code: '006' });
    } else {
      if (result) {
        let req_data = data.data,
          user_id = req_data.user_id,
          updated_data = req_data.updated_data,
          uu = require('../../mongodb/user/update_user');
        uu.update_user(user_id, updated_data,
          function (err, result) {
            if (err) {
              res.status(500).send({ msg_code: '006' });
            } else {
              if (result) {
                res.status(200).send({ msg_code: '001' });
              } else {
                res.status(500).send({ msg_code: '007' });
              }
            }
          });
      } else {
        res.status(400).send({ msg_code: '005' });
      }
    }
  });
}
