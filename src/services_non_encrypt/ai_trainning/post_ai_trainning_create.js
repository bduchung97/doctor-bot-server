exports.post_ai_trainning_create = function post_ai_trainning_create(req, res) {
  let data = req.body,
    session_token = data.session_token,
    soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
  soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
    if (err) {
      res.status(500).send({ msg_code: '006' });
    } else {
      if (result) {
        let ai_trainning_data = data.data,
          function_id = ai_trainning_data.function_id,
          value = ai_trainning_data.value,
          admin_id = ai_trainning_data.admin_id;
        var cat = require('../../mongodb/ai_trainning/create_ai_trainning');
        cat.create_ai_trainning(
          function_id,
          value,
          admin_id,
          function (err, result) {
            if (err) {
              res.status(500).send({ msg_code: '006' });
            } else {
              if (result) {
                res.status(200).send({ msg_code: '001' });
              } else {
                res.status(500).send({ msg_code: '007' });
              }
            }
          });
        let hv = require('../../utils/handle_vie/convert_vi');
        cat.create_ai_trainning(
          function_id,
          hv.change_alias(value),
          admin_id,
          function (err, result) { });
      } else {
        res.status(400).send({ msg_code: '005' });
      }
    }
  });
}
