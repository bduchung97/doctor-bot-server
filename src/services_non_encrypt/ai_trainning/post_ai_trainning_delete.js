let logger = require('../../utils/logger/logger.js'),
  error_logger = logger.getLogger();
exports.post_ai_trainning_delete = function post_ai_trainning_delete(req, res) {
  let data = req.body,
    session_token = data.session_token,
    soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
  soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
    if (err) {
      error_logger.error(err);
      res.status(500).send({ msg_code: '006' });
    } else {
      if (result) {
        let req_data = data.data,
          ai_trainning_id = req_data.ai_trainning_id,
          dat = require('../../mongodb/ai_trainning/delete_ai_trainning');
        dat.delete_ai_trainning(ai_trainning_id, function (err, result) {
          if (err) {
            error_logger.error(err);
            res.status(500).send({ msg_code: '006' });
          } else {
            if (result) {
              res.status(200).send({ msg_code: '001' });
            } else {
              res.status(500).send({ msg_code: '007' });
            }
          }
        });
      } else {
        res.status(400).send({ msg_code: '005' });
      }
    }
  });
}
