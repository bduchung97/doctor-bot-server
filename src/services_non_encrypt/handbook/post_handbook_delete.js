let logger = require('../../utils/logger/logger.js'),
  error_logger = logger.getLogger();
exports.post_handbook_delete = function post_handbook_delete(req, res) {
  let data = req.body,
    session_token = data.session_token,
    soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
  soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
    if (err) {
      error_logger.error(err);
      res.status(500).send({ msg_code: '006' });
    } else {
      if (result) {
        another_logger.info('check token successfully');
        let req_data = data.data,
          handbook_id = req_data.handbook_id,
          dh = require('../../mongodb/handbook/delete_handbook');
        dh.delete_handbook(handbook_id, function (err, result) {
          if (err) {
            error_logger.error(err);
            res.status(500).send({ msg_code: '006' });
          } else {
            if (result) {
              res.status(200).send({ msg_code: '001' });
            } else {
              res.status(500).send({ msg_code: '007' });
            }
          }
        });
      } else {
        res.status(400).send({ msg_code: '005' });
      }
    }
  });
}
