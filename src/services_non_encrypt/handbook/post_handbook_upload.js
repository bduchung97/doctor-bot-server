exports.post_handbook_upload = function post_handbook_upload(req, res) {
  let
    file = req.files.file,
    file_name = req.body.file_name,
    session_token = req.body.session_token,
    admin_id = req.body.admin_id,
    ihfef = require('../../mongodb/handbook/import_handbook_from_excel_file');
  soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
    if (err) {
      console.log(err);
      res.status(500).send({ msg_code: '006' });
    } else {
      if (result) {
        file.mv(`${__dirname}/uploads/${file_name}`, function (err) {
          if (err) {
            console.log(err);
            res.status(500).send({ msg_code: '006' });
          }
          else {
            ihfef.import_handbook_from_excel_file(`${__dirname}/uploads/${file_name}`, admin_id, function (err, result) {
              if (err) {
                console.log(err);
                res.status(500).send({ msg_code: '006' });
              } else {
                if (result) {
                  res.status(200).send({ msg_code: '001' });
                } else {
                  res.status(500).send({ msg_code: '007' });
                }
              }
            });
          }
        });
      } else {
        res.status(400).send({ msg_code: '005' });
      }
    }
  });

}

