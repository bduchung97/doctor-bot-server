exports.post_handbook_create = function post_handbook_create(req, res) {
  let data = req.body,
    session_token = data.session_token,
    soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
  soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
    if (err) {
      res.status(500).send({ msg_code: '006' });
    } else {
      if (result) {
        let handbook_data = data.data,
          // category = handbook_data.category,
          question = handbook_data.question,
          answer = handbook_data.answer,
          admin_id = handbook_data.admin_id,
          ch = require('../../mongodb/handbook/create_handbook');
        ch.create_handbook(
          // category,
          question,
          answer,
          admin_id,
          function (err, result) {
            if (err) {
              res.status(500).send({ msg_code: '006' });
            } else {
              if (result) {
                res.status(200).send({ msg_code: '001' });
              } else {
                res.status(500).send({ msg_code: '007' });
              }
            }
          });
      } else {
        res.status(400).send({ msg_code: '005' });
      }
    }
  });
}
