
exports.call_send_API_ = function call_send_API_(sender_psid, response) {
    let request = require('request'),
        request_body = {
            "recipient": {
                "id": sender_psid
            },
            "message": response
        },
        uri = "https://graph.facebook.com/v2.6/me/messages",
        options = {
            "qs": {
                "access_token": "EAAIMSYvpR4cBAB3SbJICX5ddNoRmQjedVnIdr4RMKEABnIY0TxHP9V2twrvIFsnscZCSh5T7iFEXup0RUBMPZCI9j4DFh5VdSq9R6NEvOM8RgusEbzshk1ZC4gUTjkorSBVrjT2ZAqYBTiNqN6JFTzXJ1zDAZAnW75VsVyZAOZAXgZDZD"
            },
            "method": "POST",
            "json": request_body
        };
    return new Promise(function (resolve, reject) {
        request(uri, options, function (error, res) {
            if (!error && res.statusCode == 200) {
                resolve(res.body);
            } else {
                reject(error);
            }
        });
    });
}
