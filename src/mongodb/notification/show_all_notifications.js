const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.show_all_notifications = function show_all_notifications(callback) {
  // Use connect method to connect to the server
  db.get().collection(config.collection.NOTIFICATION).find().toArray(function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
