const db = require('../connect_to_DB.js'),
  config = require('../config'),
  ObjectId = require('mongodb').ObjectID;

exports.update_notification = function update_notification(notification_id, data, callback) {
  db.get().collection(config.collection.NOTIFICATION).updateOne({ _id: ObjectId(notification_id) }, { $set: data }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      if (result.modifiedCount == 1) {
        callback(null, result.modifiedCount);
      }
      else {
        callback(null, null);
      }
    }
  });
}

