let logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger(),
  echasync = require('echasync'),
  call_send_API = require('../../call_send_API.js'),
  sol = require('../leaf/show_one_leaf.js');

exports.send_template = function send_template(target, template) {
  if (Array.isArray(target) && target.length > 0) {
    if (target.length == 1 && target[0] == '*') {
      let saufcg = require('../user/show_all_users_for_create_group.js');
      saufcg.show_all_users_for_create_group(function (err, result) {
        if (err) {
          console_logger.error(err);
        } else {
          if (result.length == 0) {
            console_logger.info('there is no user exists');
          } else {
            result.map(user => {
              template.map(tem => {
                call_send_API.callSendAPI(user.PSID, tem.VALUE);
                setTimeout(
                  function () {
                    sol.show_one_leaf('DEFAULT', function (err, result) {
                      if (result) {
                        result.map(e => {
                          call_send_API.callSendAPI(user.PSID, e.VALUE);
                        });
                      }
                    });
                  }, 3000);
              });
            });
          }
        }
      });
    }
    else {
      let sogbi = require('../group/show_one_group_by_id.js'),
        receiver = [];
      echasync.do(target, function (next, element) {
        sogbi.show_one_group_by_id(element, function (err, result) {
          if (err) {
            console_logger.error(err);
          } else {
            if (result) {
              result.MEMBERS.map(user => {
                receiver.push(user.PSID);
              });
            }
          }
          next();
        })
      }, function () {
        console_logger.info('send successfully');
        let PSIDs = [...new Set(receiver)];
        let ciub = require('../user/check_if_user_blocked.js');
        ciub.check_if_user_blocked(PSIDs, function (err, result) {
          if (result) {
            result.map(psid => {
              template.map(tem => {
                call_send_API.callSendAPI(psid, tem.VALUE);
                setTimeout(function () {
                  sol.show_one_leaf('DEFAULT', function (err, result) {
                    if (result) {
                      result.map(e => {
                        call_send_API.callSendAPI(psid, e.VALUE);
                      });
                    }
                  })
                }, 3000);
              })
            })
          }
        });
      });
    }
  }
  else {
    console_logger.info('invalid target');
  }
}
