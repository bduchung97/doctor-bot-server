const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config');

exports.create_notification = function create_notification(
  title,
  templates,
  created_by,
  status,
  schedule_second,
  schedule_minute,
  schedule_hour,
  schedule_day_of_month,
  schedule_month,
  schedule_day_of_week,
  target,
  type,
  callback
) {
  let data = {
    TITLE: title,
    TEMPLATES: templates,
    CREATED_DATE: new Date(),
    CREATED_BY: ObjectId(created_by),
    STATUS: status,
    SCHEDULE_SECOND: schedule_second,
    SCHEDULE_MINUTE: schedule_minute,
    SCHEDULE_HOUR: schedule_hour,
    SCHEDULE_DAY_OF_MONTH: schedule_day_of_month,
    SCHEDULE_MONTH: schedule_month,
    SCHEDULE_DAY_OF_WEEK: schedule_day_of_week,
    TARGET: target,
    TYPE: type,
    UPDATED_AT: null,
    UPDATED_BY: null
  }
  db.get().collection(config.collection.NOTIFICATION).insertOne(data, function (err, result) {
    if (err) {
      callback(null, err);
    }
    else {
      callback(null, result.insertedId);
    }
  });
}
