const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config.js');

exports.delete_notification = function delete_notification(notification_id, callback) {
  db.get().collection(config.collection.NOTIFICATION).deleteOne({ _id: ObjectId(notification_id) }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      if (result.deletedCount == 1) {
        callback(null, result);
      }
      else {
        callback(null, null);
      }
    }
  });
}
