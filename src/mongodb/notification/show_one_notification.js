const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.show_one_notification = function show_one_notification(filter, project, callback) {
  db.get().collection(config.collection.NOTIFICATION).findOne(filter, project, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
