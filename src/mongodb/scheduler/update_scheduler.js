const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.update_scheduler = function update_scheduler(filter, data, callback) {
  db.get().collection(config.collection.SCHEDULER).updateOne(filter, { $set: data }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      if (result.modifiedCount == 1) {
        callback(null, result.modifiedCount);
      }
      else {
        callback(null, null);
      }
    }
  });
}
