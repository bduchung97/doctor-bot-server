const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.show_all_schedulers = function show_all_schedulers(callback) {
  db.get().collection(config.collection.SCHEDULER).find().toArray(function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
