const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config.js');

exports.delete_scheduler = function delete_scheduler(scheduler_id, callback) {
  db.get().collection(config.collection.SCHEDULER).deleteOne({ _id: ObjectId(scheduler_id) }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      if (result.deletedCount == 1) {
        callback(null, result);
      }
      else {
        callback(null, null);
      }
    }
  });
}
