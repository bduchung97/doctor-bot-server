const db = require('../connect_to_DB.js'),
  config = require('../config.js');

exports.create_temp_scheduler = function create_temp_scheduler(
  callback
) {
  let data = {
    NAME: 'train_AI_temp',
    MODIFIED_AT: new Date(),
  }
  db.get().collection(config.collection.SCHEDULER).insertOne(data, function (err, result) {
    if (err) {
      console.log(err)
    } else {
      callback(null, result.insertedId);
    }
  });
}
