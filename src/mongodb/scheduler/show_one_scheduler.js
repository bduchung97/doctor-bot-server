const db = require('../connect_to_DB.js'),
  config = require('../config.js');

exports.show_one_scheduler = function show_one_scheduler(filter, project, callback) {
  db.get().collection(config.collection.SCHEDULER).findOne(filter, project, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}

