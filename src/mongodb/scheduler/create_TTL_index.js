const db = require('../connect_to_DB.js'),
  config = require('../config.js');

exports.create_TTL_index = function create_TTL_index(
  callback
) {
  db.get().collection(config.collection.SCHEDULER).createIndex({ "MODIFIED_AT": 1 }, { expireAfterSeconds: 600 }, function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      callback(null, result);
    }
  });
}
