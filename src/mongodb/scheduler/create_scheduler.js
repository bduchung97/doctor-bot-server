const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config.js');

exports.create_scheduler = function create_scheduler(
  name,
  status,
  second,
  minute,
  hour,
  day_of_month,
  month,
  day_of_week,
  admin_id,
  callback
) {
  let data = {
    NAME: name,
    STATUS: status,
    SECOND: second,
    MINUTE: minute,
    HOUR: hour,
    DAY_OF_MONTH: day_of_month,
    MONTH: month,
    DAY_OF_WEEK: day_of_week,
    CREATED_AT: new Date(),
    CREATED_BY: ObjectId(admin_id),
    IN_PROCESS: false,
    UPDATED_AT: null,
    UPDATED_BY: null
  }
  db.get().collection(config.collection.SCHEDULER).insertOne(data, function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      callback(null, result.insertedId);
    }
  });
}
