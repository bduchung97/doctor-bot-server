const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config');

exports.create_message = async function create_message(
  sender_psid, message_content, livechat_admin,
) {
  // Use connect method to connect to the server

  let data = {
    SENDER: sender_psid,
    CONTENT: message_content,
    RECEIVER: ObjectId(livechat_admin),
    CREATED_AT: new Date(),
    UPDATED_AT: null,
    UPDATED_BY: null,
    IS_ACTIVE: 1
  },
    result = await db.get().collection(config.collection.LIVECHAT_MESSAGE).insertOne(data);
  return result.insertedId;
}
