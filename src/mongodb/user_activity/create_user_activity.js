const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.create_user_activity = function create_user_activity(
  psid,
  payload,
  callback
) {

  let data = {
    PSID: psid,
    FUNCTION_ID: payload,
    CREATED_DATE: new Date()
  }
  db.get().collection(config.collection.USER_ACTIVITY).insertOne(data, function (err, result) {
    if (err) {
      callback(null, err);
    }
    else {
      callback(null, result.insertedId);
    }
  });
}
