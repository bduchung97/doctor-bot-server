const db = require('../connect_to_DB.js'),
  MongoClient = require('mongodb').MongoClient,
  config = require('../config');

exports.show_all_user_activities = function show_all_user_activities(filter, callback) {
  MongoClient.connect(config.url, {
    useNewUrlParser: true
  }, function (err, client) {
    const mgdb = client.db(config.dbName);
    mgdb.collection(config.collection.USER_ACTIVITY).find(filter).toArray(function (err, result) {
      // db.get().collection(config.collection.USER_ACTIVITY).find(filter).toArray(function (err, result) {
      if (err) {
        callback(err, null);
      }
      else {
        callback(null, result);
      }
    });
  });
}
