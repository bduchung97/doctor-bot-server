exports.count_traffic = function count_traffic(callback) {
  let saua = require('./show_all_user_activities.js');
  saua.show_all_user_activities({}, function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      let total_function_id = [],
        total_PSID = [],
        PSIDs = [],
        function_ids = [];
      result.map(e => {
        function_ids.push(e.FUNCTION_ID);
      });

      let new_func = [...new Set(function_ids)];
      new_func.map(e1 => {
        let co = require('../../utils/handle_array/count_occurence.js');
        total_function_id.push(co.count_occurence(result, e1));
      })

      result.map(e => {
        PSIDs.push(e.PSID);
      });
      let new_psids = [...new Set(PSIDs)];
      // new_psids.map(e1 => {
      //   let cpo = require('../../utils/handle_array/count_PSID_occurrence.js');
      //   total_PSID.push(cpo.count_PSID_occurrence(result, e1));
      // })
      let sa = require('../../utils/handle_array/sort_array.js');
      //total_PSID.sort(sa.sort_array);
      total_function_id.sort(sa.sort_array);
      let traffic = {
        // PSID: total_PSID,
        FUNCTION_ID: total_function_id,
        REQUEST_QUANTITY: result.length,
        USER_QUANTITY: new_psids.length
      }
      //console.log(traffic)
      callback(null, traffic);
    }
  });
}
//count_traffic()
