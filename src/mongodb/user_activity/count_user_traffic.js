exports.count_user_traffic = function count_user_traffic(psid, callback) {
  let saua = require('./show_all_user_activities.js');
  saua.show_all_user_activities({ PSID: psid }, function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      let total_function_id = [],
        function_ids = [];
      result.map(e => {
        function_ids.push(e.FUNCTION_ID);
      });
      let new_func = [...new Set(function_ids)];
      new_func.map(e1 => {
        let co = require('../../utils/handle_array/count_occurence.js');
        total_function_id.push(co.count_occurence(result, e1));
      })
      let sa = require('../../utils/handle_array/sort_array.js');
      total_function_id.sort(sa.sort_array);
      let user_traffic = {
        TOTAL: result.length,
        FUNCTION_ID: total_function_id
      }
      callback(null, user_traffic);
    }
  });
}
//count_user_traffic('1984892361559396')
