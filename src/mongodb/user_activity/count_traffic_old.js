//exports.count_traffic = 
function count_traffic(callback) {
  let saua = require('./show_all_user_activities.js');
  saua.show_all_user_activities({}, function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      let cd = require('../../utils/date-time/calculate_datediff.js'),
        weekly_traffic = result.filter(value => {
          return cd.calculate_datediff(value.CREATED_DATE, new Date()) <= 7;
        }),
        monthly_traffic = result.filter(value => {
          return cd.calculate_datediff(value.CREATED_DATE, new Date()) <= 30;
        }),
        yearly_traffic = result.filter(value => {
          return cd.calculate_datediff(value.CREATED_DATE, new Date()) <= 365;
        }),
        payload_traffic = result.reduce((acc, o) => (acc[o.FUNCTION_ID] = (acc[o.FUNCTION_ID] || 0) + 1, acc), {}),
        PSID_traffic = result.reduce((acc, o) => (acc[o.PSID] = (acc[o.PSID] || 0) + 1, acc), {}),
        weekly_payload_traffic = weekly_traffic.reduce((acc, o) => (acc[o.FUNCTION_ID] = (acc[o.FUNCTION_ID] || 0) + 1, acc), {}),
        weekly_PSID_traffic = weekly_traffic.reduce((acc, o) => (acc[o.PSID] = (acc[o.PSID] || 0) + 1, acc), {}),
        monthly_payload_traffic = monthly_traffic.reduce((acc, o) => (acc[o.FUNCTION_ID] = (acc[o.FUNCTION_ID] || 0) + 1, acc), {}),
        monthly_PSID_traffic = monthly_traffic.reduce((acc, o) => (acc[o.PSID] = (acc[o.PSID] || 0) + 1, acc), {}),
        yearly_payload_traffic = yearly_traffic.reduce((acc, o) => (acc[o.FUNCTION_ID] = (acc[o.FUNCTION_ID] || 0) + 1, acc), {}),
        yearly_PSID_traffic = yearly_traffic.reduce((acc, o) => (acc[o.PSID] = (acc[o.PSID] || 0) + 1, acc), {}),
        sorted_PSID_traffic = {},
        sorted_payload_traffic = {},
        sorted_weekly_PSID_traffic = {},
        sorted_weekly_payload_traffic = {},
        sorted_monthly_PSID_traffic = {},
        sorted_monthly_payload_traffic = {},
        sorted_yearly_PSID_traffic = {},
        sorted_yearly_payload_traffic = {};
      Object.keys(payload_traffic).sort(function (a, b) { return payload_traffic[b] - payload_traffic[a] })
        .map(key => sorted_payload_traffic[key] = payload_traffic[key]);
      Object.keys(PSID_traffic).sort(function (a, b) { return PSID_traffic[b] - PSID_traffic[a] })
        .map(key => sorted_PSID_traffic[key] = PSID_traffic[key]);

      Object.keys(weekly_payload_traffic).sort(function (a, b) { return weekly_payload_traffic[b] - weekly_payload_traffic[a] })
        .map(key => sorted_weekly_payload_traffic[key] = weekly_payload_traffic[key]);
      Object.keys(weekly_PSID_traffic).sort(function (a, b) { return weekly_PSID_traffic[b] - weekly_PSID_traffic[a] })
        .map(key => sorted_weekly_PSID_traffic[key] = weekly_PSID_traffic[key]);

      Object.keys(monthly_payload_traffic).sort(function (a, b) { return monthly_payload_traffic[b] - monthly_payload_traffic[a] })
        .map(key => sorted_monthly_payload_traffic[key] = monthly_payload_traffic[key]);
      Object.keys(monthly_PSID_traffic).sort(function (a, b) { return monthly_PSID_traffic[b] - monthly_PSID_traffic[a] })
        .map(key => sorted_monthly_PSID_traffic[key] = monthly_PSID_traffic[key]);

      Object.keys(yearly_payload_traffic).sort(function (a, b) { return yearly_payload_traffic[b] - yearly_payload_traffic[a] })
        .map(key => sorted_yearly_payload_traffic[key] = yearly_payload_traffic[key]);
      Object.keys(yearly_PSID_traffic).sort(function (a, b) { return yearly_PSID_traffic[b] - yearly_PSID_traffic[a] })
        .map(key => sorted_yearly_PSID_traffic[key] = yearly_PSID_traffic[key]);
      let a = Object.entries(sorted_payload_traffic),
        b = Object.entries(sorted_PSID_traffic),
        c = Object.entries(sorted_monthly_PSID_traffic),
        d = Object.entries(sorted_monthly_payload_traffic),
        e = Object.entries(sorted_weekly_PSID_traffic),
        f = Object.entries(sorted_weekly_payload_traffic),
        g = Object.entries(sorted_yearly_PSID_traffic),
        h = Object.entries(sorted_yearly_payload_traffic),
        sorted_payload_traffic_ = [],
        sorted_PSID_traffic_ = [],
        sorted_monthly_PSID_traffic_ = [],
        sorted_monthly_payload_traffic_ = [],
        sorted_weekly_PSID_traffic_ = [],
        sorted_weekly_payload_traffic_ = [],
        sorted_yearly_PSID_traffic_ = [],
        sorted_yearly_payload_traffic_ = [];
      a.map(e => {
        sorted_payload_traffic_.push({
          FUNCTION_ID: e[0],
          COUNT: e[1]
        })
      });
      b.map(e => {
        sorted_PSID_traffic_.push({
          PSID: e[0],
          COUNT: e[1]
        })
      });
     
      let traffic = {
        all: {
          total: result.length,
          payload: sorted_payload_traffic_,
          PSID: sorted_PSID_traffic
        },
        weekly: {
          total: weekly_traffic.length,
          payload: sorted_weekly_payload_traffic,
          PSID: sorted_weekly_PSID_traffic
        },
        monthly: {
          total: monthly_traffic.length,
          payload: sorted_monthly_payload_traffic,
          PSID: sorted_monthly_PSID_traffic
        },
        yearly: {
          total: yearly_traffic.length,
          payload: sorted_yearly_payload_traffic,
          PSID: sorted_yearly_PSID_traffic
        }
      }

      //callback(null, traffic);
    }
  });
}

count_traffic()