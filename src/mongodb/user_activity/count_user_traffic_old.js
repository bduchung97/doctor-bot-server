exports.count_user_traffic = function count_user_traffic(psid, callback) {
  let saua = require('./show_all_user_activities.js');
  saua.show_all_user_activities({ PSID: psid }, function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      let cd = require('../../utils/date-time/calculate_datediff.js'),
        weekly_traffic = result.filter(value => {
          return cd.calculate_datediff(value.CREATED_DATE, new Date()) <= 7;
        }),
        monthly_traffic = result.filter(value => {
          return cd.calculate_datediff(value.CREATED_DATE, new Date()) <= 30;
        }),
        yearly_traffic = result.filter(value => {
          return cd.calculate_datediff(value.CREATED_DATE, new Date()) <= 365;
        }),
        payload_traffic = result.reduce((acc, o) => (acc[o.PAYLOAD] = (acc[o.PAYLOAD] || 0) + 1, acc), {}),
        weekly_payload_traffic = weekly_traffic.reduce((acc, o) => (acc[o.PAYLOAD] = (acc[o.PAYLOAD] || 0) + 1, acc), {}),
        monthly_payload_traffic = monthly_traffic.reduce((acc, o) => (acc[o.PAYLOAD] = (acc[o.PAYLOAD] || 0) + 1, acc), {}),
        yearly_payload_traffic = yearly_traffic.reduce((acc, o) => (acc[o.PAYLOAD] = (acc[o.PAYLOAD] || 0) + 1, acc), {}),
        sorted_payload_traffic = {},
        sorted_weekly_payload_traffic = {},
        sorted_monthly_payload_traffic = {},
        sorted_yearly_payload_traffic = {};
      Object.keys(payload_traffic).sort(function (a, b) { return payload_traffic[b] - payload_traffic[a] })
        .map(key => sorted_payload_traffic[key] = payload_traffic[key]);
      Object.keys(weekly_payload_traffic).sort(function (a, b) { return weekly_payload_traffic[b] - weekly_payload_traffic[a] })
        .map(key => sorted_weekly_payload_traffic[key] = weekly_payload_traffic[key]);
      Object.keys(monthly_payload_traffic).sort(function (a, b) { return monthly_payload_traffic[b] - monthly_payload_traffic[a] })
        .map(key => sorted_monthly_payload_traffic[key] = monthly_payload_traffic[key]);
      Object.keys(yearly_payload_traffic).sort(function (a, b) { return yearly_payload_traffic[b] - yearly_payload_traffic[a] })
        .map(key => sorted_yearly_payload_traffic[key] = yearly_payload_traffic[key]);
      let user_traffic = {
        all: Object.assign({ total: result.length }, sorted_payload_traffic),
        weekly: Object.assign({ total: weekly_traffic.length }, sorted_weekly_payload_traffic),
        monthly: Object.assign({ total: monthly_traffic.length }, sorted_monthly_payload_traffic),
        yearly: Object.assign({ total: yearly_traffic.length }, sorted_yearly_payload_traffic)
      }
      callback(null, user_traffic);
    }
  });
}

//count_user_traffic('1984892361559396')
