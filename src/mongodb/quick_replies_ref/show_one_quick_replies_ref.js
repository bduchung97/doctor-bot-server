const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.show_one_quick_replies_ref = function show_one_quick_replies_ref(title, callback) {
  db.get().collection(config.collection.QUICK_REPLIES_REF).findOne({ TITLE: title }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
