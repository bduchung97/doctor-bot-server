const ObjectId = require('mongodb').ObjectID;
exports.create_quick_replies_ref = function create_quick_replies_ref(create_by, callback) {
  let input = [],
    sol = require('../leaf/show_one_leaf.js');
  sol.show_one_leaf('DEFAULT', function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      if (result.length == 0) {
        callback(null, null);
      } else {
        let templates = result[0].VALUE.quick_replies;
        templates.map(element => {
          input.push({
            TITLE: element.title,
            PAYLOAD: element.payload
          });
        });
        input.map(e => {
          e.CREATED_AT = new Date(),
            e.CREATED_BY = ObjectId(create_by)
        });

        let im = require('./insert_many.js');
        im.insert_many(input, function (err, result) {
          if (err) {
            callback(err, null);
          } else {
            if (result.length == 0) {
              callback(null, null);
            } else {
              callback(null, result);
            }
          }
        });
      }
    }
  });
}
