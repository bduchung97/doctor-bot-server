const db = require('../connect_to_DB.js'),
  config = require('../config.js');

exports.delete_all_documents = function delete_all_documents(
  callback
) {
  db.get().collection(config.collection.QUICK_REPLIES_REF).deleteMany({}, function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      callback(null, result.deletedCount);
    }
  });
}
