const db = require('../connect_to_DB.js'),
  config = require('../config.js');

exports.insert_many = function insert_many(
  data,
  callback
) {
  db.get().collection(config.collection.QUICK_REPLIES_REF).insertMany(data, function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      callback(null, result.insertedIds);
    }
  });
}
