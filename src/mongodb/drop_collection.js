let db = require('./connect_to_DB.js'),
  config = require('./config'),
  logger = require('../utils/logger/logger'),
  console_logger = logger.getLogger();

exports.drop_collection = function drop_collection(coll, callback) {
  db.get().collection(coll).drop(function (err, result) {
    if (err) {
      console_logger.info(err);
    } else {
      if (result) {
        console_logger.info('done');
        callback(null, 'done');
      }
    }
  });
}
