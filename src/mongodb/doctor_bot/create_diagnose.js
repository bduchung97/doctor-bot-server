const
	chan_doan = require('../../../test/chan_doan.js'),
	config = require('../config.js'),
	db = require('../connect_to_DB.js');

exports.create_diagnose = async function create_diagnose(
	fullname,
	age,
	gender,
	action,
	heart_rate,
	PSID
)	{
	let data = {
		FULLNAME: fullname,
		STATUS: chan_doan.chan_doan(age, gender, action, heart_rate),
		CREATED_AT: new Date(),
		CREATED_BY: PSID
	}
	let result = await db.get().collection(config.collection.USER_STATUS).insertOne(data);
	return result.insertedId;
}