const logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger();
exports.delete_group = function delete_group(user_ID, group_name, callback) {
  let sog = require('../group/show_one_group.js');
  sog.show_one_group(group_name, function (err, result) {
    if (err) {
      console_logger.error(err);
      callback(err, null);
    } else {
      if (result) {
        let group_ID = _id.toString(),
          member_data = result.MEMBERS,
          defa = require('../group/delete_element_from_array.js'),
          new_member_data = defa.delete_element_from_array(member_data, user_ID.toString());
        if (new_member_data !== null) {
          let ug = require('../group/update_group.js');
          ug.update_group(group_ID, { MEMBERS: new_member_data }, function (err, result) {
            if (err) {
              console_logger.error(err);
              callback(err, null);
            } else {
              if (result) {
                let sou = require('./show_one_user.js');
                sou.show_one_user(user_ID, function (err, result) {
                  if (err) {
                    console_logger.error(err);
                    callback(err, null);
                  } else {
                    if (result) {
                      let group_data = result.GROUPS,
                        new_group_data = defa.delete_element_from_array(group_data, group_ID);
                      if (new_group_data !== null) {
                        let uu = require('./update_user.js');
                        uu.update_user(user_ID, { GROUPS: new_group_data }, function (err, result) {
                          if (err) {
                            console_logger.error(err);
                            callback(err, null);
                          } else {
                            if (result) {
                              console_logger.info('delete group succesfully');
                              callback(null, 'done');
                            } else {
                              console_logger.info('update user unsuccesfully');
                              callback(null, null);
                            }
                          }
                        });
                      } else {
                        console_logger.info('delete group unsuccessfully');
                        callback(null, null);
                      }
                    }
                  }
                });
              } else {
                console_logger.info('update group unsuccesfully');
                callback(null, null);
              }
            }
          });
        } else {
          console_logger.info('delete group unsuccessfully');
          callback(null, null);
        }
      } else {
        console_logger.info('group does not exist');
        callback(null, null);
      }
    }
  });
}