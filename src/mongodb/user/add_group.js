const logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger();
exports.add_group = function add_group(user_ID, group_name, callback) {
  let sog = require('../group/show_one_group.js');
  sog.show_one_group(group_name, function (err, result) {
    if (err) {
      console_logger.error(err);
      callback(err, null);
    } else {
      if (result) {
        let group_ID = _id.toString(),
          member_data = result.MEMBERS,
          ceia = require('./check_element_in_array.js');
        if (ceia.check_element_in_array(member_data, user_ID.toString()) == false) {
          let new_member_data = member_data.push(user_ID.toString()),
            ug = require('../group/update_group.js');
          ug.update_group(group_ID, { MEMBERS: new_member_data }, function (err, result) {
            if (err) {
              console_logger.error(err);
              callback(err, null);
            } else {
              if (result) {
                let sou = require('./show_one_user.js');
                sou.show_one_user(user_ID, function (err, result) {
                  if (err) {
                    console_logger.error(err);
                    callback(err, null);
                  } else {
                    if (result) {
                      let group_data = result.GROUPS,
                        new_group_data = group_data.push(group_ID);
                      if (new_group_data !== null) {
                        let uu = require('./update_user.js');
                        uu.update_user(user_ID, { GROUPS: new_group_data }, function (err, result) {
                          if (err) {
                            console_logger.error(err);
                            callback(err, null);
                          } else {
                            if (result) {
                              console_logger.info('delete group succesfully');
                              callback(null, 'done');
                            } else {
                              console_logger.info('update user unsuccesfully');
                              callback(null, null);
                            }
                          }
                        });
                      } else {
                        console_logger.info('delete group unsuccessfully');
                        callback(null, null);
                      }
                    }
                  }
                });
              } else {
                console_logger.info('update group unsuccesfully');
                callback(null, null);
              }
            }
          });
        } else {
          console_logger.info('delete group unsuccessfully');
          callback(null, null);
        }
      } else {
        console_logger.info('group does not exist');
        callback(null, null);
      }
    }
  });
}