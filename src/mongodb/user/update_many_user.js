const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.update_many_user = function update_many_user(filter, data, callback) {
  db.get().collection(config.collection.USER).updateMany(filter, { $set: data }, { upsert: false }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      if (result.modifiedCount) {
        callback(null, result.modifiedCount);
      }
      else {
        callback(null, null);
      }
    }
  });
}
