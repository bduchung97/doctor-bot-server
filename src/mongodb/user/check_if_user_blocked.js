exports.check_if_user_blocked = async function check_if_user_blocked(psids, callback) {
  let subp = require('./show_user_by_psid_.js');
  let result = await Promise.all(psids.map(async (psid) => {
    let user = await subp.show_user_by_psid_(psid);
    if (user.IS_ACTIVE == 1) {
      return psid;
    }
  }));
  result = result.filter(x => !!x)
  callback(null, result);
}

//check_if_user_blocked(['2394866633878537','1984892361559396','2558001157566882','2904348559575591'])