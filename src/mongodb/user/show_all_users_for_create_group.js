const db = require('../connect_to_DB.js'),
  config = require('../config.js');

exports.show_all_users_for_create_group = function show_all_users_for_create_group(callback) {
  db.get().collection(config.collection.USER).find({ STATUS: 1, IS_ACTIVE : 1 }).toArray(function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
