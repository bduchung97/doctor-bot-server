const db = require('../connect_to_DB.js'),
  config = require('../config.js');

exports.show_one_user_by_psid = function show_one_user_by_psid(psid, callback) {
  db.get().collection(config.collection.USER).findOne({ PSID: psid }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
