const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config.js');

exports.show_one_user = function show_one_user(user_id, callback) {
  db.get().collection(config.collection.USER).findOne({ _id: ObjectId(user_id), IS_ACTIVE: 1 }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}

