const db = require('../connect_to_DB.js'),
  config = require('../config.js');

exports.show_all_users = function show_all_users(callback) {
  db.get().collection(config.collection.USER).find().toArray(function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
