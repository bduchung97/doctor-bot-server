const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.update_user_by_psid = function update_user_by_psid(PSID, data, callback) {
  db.get().collection(config.collection.USER).updateOne({ PSID: PSID }, { $set: data }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      if (result.modifiedCount == 1) {
        callback(null, result.modifiedCount);
      }
      else {
        callback(null, null);
      }
    }
  });
}
