const db = require('../connect_to_DB.js'),
  config = require('../config.js');

exports.create_user = function create_user(
  psid,
  callback
) {
  let data = {
    PSID: psid,
    CREATED_AT: new Date(),
    IS_ACTIVE: 1,
    FULLNAME: null,
    FIRST_NAME: null,
    LAST_NAME: null,
    PHONE_NUMBER: null,
    EMAIL: null,
    GROUPS: [],
    FB_URL: null,
    STATUS: 1,
    LIVE_CHAT: 0,
    UPDATED_AT: null,
    UPDATED_BY: null
  }
  db.get().collection(config.collection.USER).insertOne(data, function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      callback(null, result.insertedId);
    }
  });
}
