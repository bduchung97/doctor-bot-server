const db = require('../connect_to_DB.js'),
  config = require('../config'),
  ObjectId = require('mongodb').ObjectID;

exports.update_user = function update_user(user_id, data, callback) {
  db.get().collection(config.collection.USER).updateOne({ _id: ObjectId(user_id) }, { $set: data }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      if (result.modifiedCount == 1) {
        callback(null, result.modifiedCount);
      }
      else {
        callback(null, null);
      }
    }
  });
}
