const MongoClient = require('mongodb').MongoClient,
  config = require('../config.js');

exports.show_user_by_psid_ = async function show_user_by_psid_(psid) {
  try {
    const client = await MongoClient.connect(config.url, { useNewUrlParser: true }),
      mgdb = client.db(config.dbName);
    let user = await mgdb.collection(config.collection.USER).findOne({ PSID: psid });
    return user;
  } catch (err) {
    console.log(err);
  }
}
