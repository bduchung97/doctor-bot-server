const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config');

exports.create_template = function create_template(
  node_ID,
  value,
  created_by,
  type,
  callback
) {
  let stbo = require('./show_templates_by_order');
  stbo.show_templates_by_order(node_ID, function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      let order = 0;
      if (result.length) {
        order = result[result.length - 1].ORDER + 1
      }
      let data = {
        NODE__ID: ObjectId(node_ID),
        VALUE: value,
        TYPE: type,
        ORDER: order,
        CREATED_DATE: new Date(),
        CREATED_BY: ObjectId(created_by),
        UPDATED_AT: null,
        UPDATED_BY: null
      }
      db.get().collection(config.collection.TEMPLATE).insertOne(data, function (err, result) {
        if (err) {
          callback(null, err);
        }
        else {
          callback(null, result.insertedId);
        }
      });

    }
  });
}
