const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config.js');

exports.delete_template = function delete_template(template_id, callback) {
  db.get().collection(config.collection.TEMPLATE).deleteOne({ _id: ObjectId(template_id) }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      if (result.deletedCount == 1) {
        callback(null, result);
      }
      else {
        callback(null, null);
      }
    }
  });
}
