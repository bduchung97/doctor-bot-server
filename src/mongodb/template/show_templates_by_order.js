const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config');

exports.show_templates_by_order = function show_templates_by_order(node_ID, callback) {
  db.get().collection(config.collection.TEMPLATE).find({ NODE__ID: ObjectId(node_ID) }).sort({ ORDER: 1 }).toArray(function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}

// const MongoClient = require('mongodb').MongoClient,
//   config = require('../config'),
//   ObjectId = require('mongodb').ObjectID;

// function get_leaf_data(node_ID, callback) {
//   // Use connect method to connect to the server
//   MongoClient.connect(config.url, {
//     useNewUrlParser: true
//   }, function (err, client) {
//     const mgdb = client.db(config.dbName);
//     mgdb.collection(config.collection.TEMPLATE).find({ NODE__ID: ObjectId(node_ID) }).sort({ ORDER: 1 }).toArray(function (err, result) {
//       if (err) {
//         callback(err, null);
//       }
//       else {
//         console.log(result);
//       }
//     });
//   })
// }

// get_leaf_data(ObjectId(ObjectId("5cf5518606242829f841e2a")));

