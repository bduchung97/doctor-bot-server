const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config.js');

exports.delete_many_templates = function delete_many_templates(node_id, callback) {
  db.get().collection(config.collection.TEMPLATE).deleteMany({ NODE__ID: ObjectId(node_id) }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      if (result.deletedCount >= 1) {
        callback(null, result);
      }
      else {
        callback(null, null);
      }
    }
  });
}
