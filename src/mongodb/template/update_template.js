const db = require('../connect_to_DB.js'),
  config = require('../config'),
  mongodb = require('mongodb'),
  ObjectId = require('mongodb').ObjectID;

exports.update_template = function update_template(template_id, data, callback) {
  if (mongodb.ObjectID.isValid(template_id) == true) {
    db.get().collection(config.collection.TEMPLATE).updateOne({ _id: ObjectId(template_id) }, { $set: data }, function (err, result) {
      if (err) {
        callback(err, null);
      }
      else {
        if (result.modifiedCount == 1) {
          callback(null, result.modifiedCount);
        }
        else {
          callback(null, null);
        }
      }
    });
  }
  else {
    callback(null, null);
  }
}
