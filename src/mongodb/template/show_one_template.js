const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config');

exports.show_one_template = function show_one_template(template_id, callback) {
  db.get().collection(config.collection.TEMPLATE).findOne({ _id: ObjectId(template_id) }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
