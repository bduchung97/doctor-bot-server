const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.update_user_fb_info = function update_user_fb_info(psid, callback) {
  let soabu = require('../admin/show_one_admin_by_username.js');
  soabu.show_one_admin_by_username('root', function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      if (result) {
        let admin_ID = result._id,
          gfud = require('../../utils/facebook/get_facebook_user_data.js');
        gfud.get_facebook_user_data(psid, function (err, result) {
          if (err) {
            callback(err, null);
          } else {
            if (result) {
              result = JSON.parse(result);
              let data = {
                FB_NAME: result.name,
                FB_FIRST_NAME: result.first_name,
                FB_LAST_NAME: result.last_name,
                FB_PROFILE_PIC: result.profile_pic,
                FB_LOCALE: result.locale,
                FB_GENDER: result.gender,
                FB_TIMEZONE: result.timezone,
                UPDATED_AT: new Date(),
                UPDATED_BY: admin_ID
              }
              db.get().collection(config.collection.USER_FB_INFO).updateOne({ PSID: psid }, { $set: data }, function (err, result) {
                if (err) {
                  callback(err, null);
                }
                else {
                  if (result.modifiedCount == 1) {
                    callback(null, result.modifiedCount);
                  }
                  else {
                    callback(null, null);
                  }
                }
              });
            } else {
              callback(null, null);
            }
          }
        });
      }
    }
  });
}
