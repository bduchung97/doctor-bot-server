const db = require('../connect_to_DB.js'),
  config = require('../config.js');

exports.show_one_user_fb_info = function show_one_user_fb_info(psid, callback) {
  db.get().collection(config.collection.USER_FB_INFO).findOne({ PSID: psid }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}

