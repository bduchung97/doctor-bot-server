exports.update_all_user_fb_infos = function update_all_user_fb_infos() {
  let safui = require('./show_all_fb_user_infos.js');
  safui.show_all_fb_user_infos(function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      if (result.length == 0) {
        callback(null, null);
      } else {
        let uufi = require('./update_user_fb_info.js'),
          echasync = require('echasync'),
          updated_list = [];
        echasync.do(result, function (next, element) {
          uufi.update_user_fb_info('root_ID', element.PSID, function (err, result) {
            if (err) {
              callback(err, null);
            } else {
              if (result) {
                updated_list.push(element.PSID)
              }
            }
            next();
          })
        }, function () {
          callback(null, updated_list);
        });
      }
    }
  });
}
