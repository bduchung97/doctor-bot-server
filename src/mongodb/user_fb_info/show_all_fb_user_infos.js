const db = require('../connect_to_DB.js'),
  config = require('../config.js');

exports.show_all_fb_user_infos = function show_all_fb_user_infos(callback) {
  db.get().collection(config.collection.USER_FB_INFO).find().toArray(function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}

