const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.create_user_fb_info = function create_user_fb_info(
  psid,
  callback
) {
  let gufd = require('../../utils/facebook/get_facebook_user_data.js');
  gufd.get_facebook_user_data(psid, function (err, result) {
    if (err) { 
      callback(err, null);
    } else {
      if (result) {
        var info = JSON.parse(result);
        let data = {
          FB_NAME: info.name,
          FB_FIRST_NAME: info.first_name,
          FB_LAST_NAME: info.last_name,
          FB_PROFILE_PIC: info.profile_pic,
          FB_LOCALE: info.locale,
          FB_GENDER: info.gender,
          FB_TIMEZONE: info.timezone,
          PSID: psid,
          CREATED_AT: new Date(),
          UPDATED_AT: null,
          UPDATED_BY: null
        }
        db.get().collection(config.collection.USER_FB_INFO).insertOne(data, function (err, result) {
          if (err) {
            callback(null, err);
          }
          else {
            if (result) {
              callback(null, result.insertedId);
            } else {
              callback(null, null);
            }
          }
        });
      } else {
        callback(null, null);
      }
    }
  });
}
