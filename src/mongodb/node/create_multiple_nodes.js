const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.create_multiple_nodes = function create_multiple_nodes(data, callback) {
  db.get().collection(config.collection.NODE).insertMany(data, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      if (result.insertedCount == data.length) {
        callback(null, result.insertedIds);
      } else {
        callback(null, null);
      }
    }
  });
}
