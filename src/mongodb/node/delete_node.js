const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config.js');

exports.delete_node = function delete_node(node_id, callback) {
  db.get().collection(config.collection.NODE).deleteOne({ _id: ObjectId(node_id) }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      if (result.deletedCount == 1) {
        callback(null, result);
      }
      else {
        callback(null, null);
      }
    }
  });
}
