const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.show_one_node = function show_one_node(function_id, callback) {
  db.get().collection(config.collection.NODE).findOne({ FUNCTION_ID: function_id }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
