const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config');

exports.show_one_node_by_id = function show_one_node_by_id(node_id, callback) {
  db.get().collection(config.collection.NODE).findOne({ _id: ObjectId(node_id) }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
