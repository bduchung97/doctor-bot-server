const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config');

exports.create_node = function create_node(
  function_id,
  parent_function_id,
  created_by,
  description,
  callback
) {
  // Use connect method to connect to the server

  let data = {
    FUNCTION_ID: function_id,
    PARENT_FUNCTION__ID: parent_function_id,
    DESCRIPTION: description,
    CREATED_AT: new Date(),
    CREATED_BY: ObjectId(created_by),
    UPDATED_AT: null,
    UPDATED_BY: null
  }
  db.get().collection(config.collection.NODE).insertOne(data, function (err, result) {
    if (err) {
      callback(null, err);
    }
    else {
      callback(null, result.insertedId);
    }
  });

}
