exports.import_nodes_from_excel_file = function import_nodes_from_excel_file(file, admin_id, callback) {
  let rfe = require('../../utils/file/read_file_excel'),
    ObjectId = require('mongodb').ObjectID,
    cv = require('../../utils/handle_vie/convert_vi.js');
  rfe.read_file_excel(file, function (err, data) {
    if (err) {
      callback(err, null);
    } else {
      for (i in data) {
        // data[i].FUNCTION_ID = cv.change_alias(data[i].FUNCTION_ID);
        // data[i].PARENT_FUNCTION__ID = cv.change_alias(data[i].PARENT_FUNCTION__ID);
        data[i] = Object.assign(data[i], { CREATED_BY: ObjectId(admin_id), CREATED_AT: new Date(), UPDATED_AT: null, UPDATED_BY: null });
      }
      let cmn = require('./create_multiple_nodes.js');
      cmn.create_multiple_nodes(data, function (err, result) {
        if (err) {
          callback(err, null);
        } else {
          if (result) {
            callback(null, result);
          } else {
            callback(null, null);
          }
        }
      });
    }
  });
}
