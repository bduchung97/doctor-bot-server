const db = require('../connect_to_DB.js'),
  config = require('../config'),
  ObjectId = require('mongodb').ObjectID;

exports.update_node = function update_node(node_id, data, callback) {
  db.get().collection(config.collection.NODE).updateOne({ _id: ObjectId(node_id) }, { $set: data }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      if (result.modifiedCount == 1) {
        callback(null, result.modifiedCount);
      }
      else {
        callback(null, null);
      }
    }
  });
}

