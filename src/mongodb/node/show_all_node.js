const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.show_all_node = function show_all_node(callback) {
  db.get().collection(config.collection.NODE).find({
    FUNCTION_ID: { $ne: 'PERSISTENT_MENU' }
  }).toArray(function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
