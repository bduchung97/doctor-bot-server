const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.show_all_node_ = function show_all_node_(callback) {
  db.get().collection(config.collection.NODE).find({
    PARENT_FUNCTION__ID: { $ne: 'ROOT' }
  }).toArray(function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
