const db = require('../connect_to_DB.js'),
  config = require('../config.js');

exports.create_multiple_admin_permissions = function create_multiple_admin_permissions(
  data,
  callback
) {
  db.get().collection(config.collection.ADMIN_PERMISSION).insertMany(data, function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      callback(null, result.insertedIds);
    }
  });
}
