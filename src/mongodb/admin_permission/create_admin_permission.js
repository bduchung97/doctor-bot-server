const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config.js');

exports.create_admin_permission = function create_admin_permission(
  code,
  description,
  admin_id,
  callback
) {
  let data = {
    CODE: code,
    DESCRIPTION: description,
    CREATED_AT: new Date(),
    UPDATED_AT: null,
    UPDATED_BY: null,
    CREATED_BY: ObjectId(admin_id)
  }
  db.get().collection(config.collection.ADMIN_PERMISSION).insertOne(data, function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      callback(null, result.insertedId);
    }
  });
}
