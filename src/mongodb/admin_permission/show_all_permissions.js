const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.show_all_permissions = function show_all_permissions(callback) {
  db.get().collection(config.collection.ADMIN_PERMISSION).find().toArray(function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
