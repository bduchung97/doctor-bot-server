const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config.js');

exports.delete_admin_permission = function delete_admin_permission(permission_id, callback) {
  db.get().collection(config.collection.ADMIN_PERMISSION).deleteOne({ _id: ObjectId(permission_id) }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      if (result.deletedCount == 1) {
        callback(null, result);
      }
      else {
        callback(null, null);
      }
    }
  });
}
