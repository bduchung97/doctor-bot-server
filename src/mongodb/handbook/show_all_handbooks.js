const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.show_all_handbooks = function show_all_handbooks(callback) {
  db.get().collection(config.collection.HANDBOOK).find().toArray(function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
// const ObjectId = require('mongodb').ObjectID,
//   MongoClient = require('mongodb').MongoClient, 
//   echasync = require('echasync');;
// function show_all_handbooks() {
//   MongoClient.connect(config.url, {
//     useNewUrlParser: true
//   }, function (err, client) {
//     const mgdb = client.db(config.dbName);
//     mgdb.collection(config.collection.HANDBOOK).find().toArray(function (err, result) {
//       console.log(result);
//     })
//   })
// }
// show_all_handbooks()