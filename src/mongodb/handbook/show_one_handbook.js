const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config.js');

exports.show_one_handbook = function show_one_handbook(handbook_id, callback) {
  db.get().collection(config.collection.HANDBOOK).findOne({ _id: ObjectId(handbook_id) }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
