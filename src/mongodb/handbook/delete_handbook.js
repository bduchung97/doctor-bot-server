const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config.js');

exports.delete_handbook = function delete_handbook(handbook_id, callback) {
  db.get().collection(config.collection.HANDBOOK).deleteOne({ _id: ObjectId(handbook_id) }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      if (result.deletedCount == 1) {
        callback(null, result);
      }
      else {
        callback(null, null);
      }
    }
  });
}
