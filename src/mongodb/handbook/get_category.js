const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.get_category = function get_category(callback) {
  db.get().collection(config.collection.HANDBOOK).distinct('CATEGORY', function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
