const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.create_multiple_handbooks = function create_multiple_handbooks(data, callback) {
  db.get().collection(config.collection.HANDBOOK).insertMany(data, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      if (result.insertedCount == data.length) {
        callback(null, result.insertedIds);
      } else {
        callback(null, null);
      }
    }
  });
}

