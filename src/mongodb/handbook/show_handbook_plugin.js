const db = require('../connect_to_DB.js'),
  echasync = require('echasync'),
  config = require('../config');

exports.show_handbook_plugin = function show_handbook_plugin(callback) {
  let gc = require('./get_category.js');
  gc.get_category(function (err, categories) {
    if (err) {
      callback(err, null);
    } else {
      if (categories) {
        let data = [];
        echasync.do(categories, function (next, element) {
          db.get().collection(config.collection.HANDBOOK).find({ CATEGORY: element }, {
            projection: {
              _id: 0,
              CATEGORY: 0
            }
          }).toArray(function (err, result) {
            if (err) throw err;
            else {
              if (result) {
                data.push({
                  CATEGORY: element,
                  CONTENT: result,
                })
              }
            }
            next();
          });
        }, function () {
          callback(null, data);
        })
      } else {
        callback(null, null);
      }
    }
  })
}
//show_QnA_by_category()

