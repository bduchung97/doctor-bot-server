exports.import_handbook_from_excel_file = function import_handbook_from_excel_file(file, admin_id, callback) {
  let rfe = require('../../utils/file/read_file_excel'),
    ObjectId = require('mongodb').ObjectID;
  rfe.read_file_excel(file, function (err, data) {
    if (err) {
      callback(err, null);
    } else {
      for (i in data) {
        data[i] = Object.assign(data[i], { CREATED_BY: ObjectId(admin_id), CREATED_AT: new Date(), UPDATED_AT: null, UPDATED_BY: null });
      }
      let cmh = require('./create_multiple_handbooks');
      cmh.create_multiple_handbooks(data, function (err, result) {
        if (err) {
          callback(err, null);
        } else {
          if (result) {
            callback(null, result);
          } else {
            callback(null, null);
          }
        }
      });
    }
  });
}
