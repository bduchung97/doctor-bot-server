const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config.js');

exports.create_handbook = function create_handbook(
  // category,
  question,
  answer,
  admin_id,
  callback
) {
  let data = {
    // CATEGORY: category,
    QUESTION: question,
    ANSWER: answer,
    CREATED_AT: new Date(),
    CREATED_BY: ObjectId(admin_id),
    UPDATED_AT: null,
    UPDATED_BY: null
  }
  db.get().collection(config.collection.HANDBOOK).insertOne(data, function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      callback(null, result.insertedId);
    }
  });
}
