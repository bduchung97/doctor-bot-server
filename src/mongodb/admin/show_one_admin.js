const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config.js');

exports.show_one_admin = function show_one_admin(admin_id, callback) {
  db.get().collection(config.collection.ADMIN).findOne({ _id: ObjectId(admin_id) }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      //console.log(result)
      callback(null, result);
    }
  });
}
//show_one_admin(ObjectId("5ced0f67db5a0d1d9c6f9aa9"))
