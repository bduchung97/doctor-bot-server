const db = require('../connect_to_DB.js'),
  config = require('../config.js');

exports.show_one_admin_by_username = function show_one_admin_by_username(username, callback) {
  db.get().collection(config.collection.ADMIN).findOne({ USERNAME: username, IS_ACTIVE: 1 }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      // console.log(result)
      callback(null, result);
    }
  });
}

// show_one_admin_by_username('quanh_nguyen')
