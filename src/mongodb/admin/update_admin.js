const db = require('../connect_to_DB.js'),
  config = require('../config'),
  ObjectId = require('mongodb').ObjectID;

exports.update_admin = function update_admin(admin_id, data, callback) {
  db.get().collection(config.collection.ADMIN).updateOne({ _id: ObjectId(admin_id) }, { $set: data }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      if (result.modifiedCount == 1) {
        callback(null, result.modifiedCount);
      }
      else {
        callback(null, null);
      }
    }
  });
}
