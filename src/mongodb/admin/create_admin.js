const
  config = require('../config.js'),
  hp = require('../../utils/crypt/hash_password.js'),
  ObjectId = require('mongodb').ObjectID,
  db = require('../connect_to_DB');

exports.create_admin = function create_admin(
  full_name,
  username,
  password,
  email,
  phone_number,
  permission,
  admin_id,
  callback
) {

  let data = {
    FULL_NAME: full_name,
    CREATED_AT: new Date(),
    IS_ACTIVE: 1,
    USERNAME: username,
    PASSWORD: hp.hash_password(password),
    PHONE_NUMBER: phone_number,
    EMAIL: email,
    PERMISSION: permission,
    UPDATED_AT: null,
    UPDATED_BY: null,
    CREATED_BY: ObjectId(admin_id),
  }
  db.get().collection(config.collection.ADMIN).insertOne(data, function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      callback(null, result.insertedId);
    }
  });

}
