const config = require('../config.js'),
  db = require('../connect_to_DB');

exports.show_all_admins = function show_all_admins(callback) {
  db.get().collection(config.collection.ADMIN).find({ IS_ACTIVE: 1 }).toArray(function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
