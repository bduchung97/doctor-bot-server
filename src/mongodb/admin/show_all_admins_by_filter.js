const config = require('../config.js'),
  db = require('../connect_to_DB');

exports.show_all_admins_by_filter = async function show_all_admins_by_filter(filter, projection) {
  return db.get().collection(config.collection.ADMIN).find(filter, projection).toArray();
}
