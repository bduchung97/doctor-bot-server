let echasync = require('echasync'),
  ht = require('../../api/api_helper/handle_templates/generate_text_template.js');

function import_data_from_excel_file(filename) {
  let rfe = require('../../../test/read_file_excel.js'),
    cl = require('./create_leaf.js');
  rfe.read_file_excel(filename, function (err, data) {
    if (err) {
      console.log(err);
    } else {
      console.log(data);
      echasync.do(data, function (next, element) {
        cl.create_leaf(element.PAYLOAD, element.PARENT_PAYLOAD, ht.generate_text_template(element.TEXT), function (err, result) {
          if (err) {
            console.log(err);
          } else {
            console.log(result);
          }
          next();
        })
      }, function () {
        console.log('import successfully');
      })

    }
  })
}

import_data_from_excel_file('../../../test/leaf_data_new.xlsx')