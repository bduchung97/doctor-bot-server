let MongoClient = require('mongodb').MongoClient,
  config = require('../config');

exports.show_all_leafs = function show_all_leafs(callback) {
  MongoClient.connect(config.url, {
    useNewUrlParser: true
  }, function (err, client) {
    if (err) {
      callback(err, null);
    } else {
      const mgdb = client.db(config.dbName);
      mgdb.collection(config.collection.NODE).aggregate([
        {
          $lookup:
          {
            from: config.collection.TEMPLATE,
            localField: '_id',
            foreignField: 'NODE__ID',
            as: 'templates'
          }
        }
        ,
        {
          $match:
          {
            PARENT_FUNCTION__ID: { $ne: 'ROOT' }
          }
        }
      ]).toArray(function (err, result) {
        if (err) {
          callback(err, null);
        }
        else {
          callback(null, result);
        }
      });
    }
  });
}
