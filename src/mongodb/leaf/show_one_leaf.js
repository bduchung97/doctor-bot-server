exports.show_one_leaf = function show_one_leaf(function_id, callback) {
  let son = require('../node/show_one_node.js'),
    stbo = require('../template/show_templates_by_order.js');
  son.show_one_node(function_id, function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      if (result) {
        let node_ID = result._id;
        stbo.show_templates_by_order(node_ID, function (err, result) {
          if (err) {
            callback(err, null);
          } else {
            callback(null, result);
          }
        });
      } else {
        callback(null, null);
      }
    }
  });
}
