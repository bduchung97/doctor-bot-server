exports.create_leaf = function create_leaf(function_id, parent_function_id, value, created_by, description, type, callback) {
  let show_one_node = require('../node/show_one_node.js'),
    create_template = require('../template/create_template.js');
  show_one_node.show_one_node(function_id, function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      if (result) {
        callback(null, null);
      } else {
        let create_node = require('../node/create_node.js')
        create_node.create_node(function_id, parent_function_id, created_by, description, function (err, result) {
          if (err) {
            callback(err, null);
          }
          else {
            if (result) {
              create_template.create_template(result, value, created_by, type, function (err, res) {
                if (err) {
                  callback(err, null);
                } else {
                  if (res) {
                    callback(null, result);
                  } else {
                    callback(null, null);
                  }
                }
              });
            } else {
              callback(null, null);
            }
          }
        });
      }
    }
  });
}
// let data = {
//   'text': 'Chúc quý khách có một ngày làm việc hiệu quả. Đây là thông báo thử nghiệm. Cảm ơn quý khách đã sử dụng dịch vụ của chúng tôi!'
// }
// create_leaf('TEST', 'HOME', data)
