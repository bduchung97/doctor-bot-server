const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config');

exports.show_one_group_by_id = function show_one_group_by_id(group_ID, callback) {
  db.get().collection(config.collection.GROUP).findOne({ _id: ObjectId(group_ID) }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
