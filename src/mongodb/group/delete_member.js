const logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger();

exports.delete_member = function delete_member(group_ID, user_ID, callback) {
  let sou = require('../user/show_one_user.js');
  sou.show_one_user(user_ID, function (err, result) {
    if (err) {
      console_logger.error(err);
      callback(err, null);
    } else {
      if (result) {
        let
          group_data = result.GROUPS,
          defa = require('../../utils/handle_array/delete_element_from_array.js'),
          new_group_data = defa.delete_element_from_array(group_data, group_ID.toString());
        if (new_group_data !== null) {
          let uu = require('../user/update_user.js');
          uu.update_user(user_ID, { GROUPS: new_group_data }, function (err, result) {
            if (err) {
              console_logger.error(err);
              callback(err, null);
            } else {
              if (result) {
                let sogbi = require('./show_one_group_by_id.js');
                sogbi.show_one_group_by_id(group_ID, function (err, result) {
                  if (err) {
                    console_logger.error(err);
                    callback(err, null);
                  } else {
                    if (result) {
                      let member_data = result.MEMBERS,
                        dofoa = require('../../utils/handle_array/delete_object_from_object_array.js'),
                        new_member_data = dofoa.delete_object_from_object_array(member_data, user_ID);
                      if (new_member_data !== null) {
                        let ug = require('./update_group.js');
                        ug.update_group(group_ID, { MEMBERS: new_member_data }, function (err, result) {
                          if (err) {
                            console_logger.error(err);
                            callback(err, null);
                          } else {
                            if (result) {
                              console_logger.info('delete member from group successfully');
                              callback(null, 'done');
                            } else {
                              console_logger.info('delete member from group unsuccessfully');
                              callback(null, null);
                            }
                          }
                        });
                      } else {
                        console_logger.info('delete member from group unsuccessfully');
                        callback(null, null);
                      }
                    }
                  }
                });
              }
            }
          });
        }
        else {
          console_logger.info('delete member from group unsuccessfully');
          callback(null, null);
        }
      } else {
        console_logger.info('user does not exist');
        console_logger.info('delete member from group unsuccessfully');
        callback(null, null);
      }
    }
  });
}
