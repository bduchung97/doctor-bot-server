const logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger();
exports.add_user = function add_user(group_ID, user_ID, PSID, callback) {
  var sou = require('../user/show_one_user.js');
  sou.show_one_user(user_ID, function (err, result) {
    if (err) {
      console_logger.error(err);
      callback(err, null);
    }
    else {
      if (result) {
        var group_data = result.GROUPS,
          ceia = require('../../utils/handle_array/check_element_in_array.js');
        if (ceia.check_element_in_array(group_data, group_ID.toString()) == false) {
          group_data.push(group_ID.toString()),
            uu = require('../user/update_user.js');
          uu.update_user(user_ID, { GROUPS: group_data }, function (err, result) {
            if (err) {
              console_logger.error(err);
              callback(err, null);
            }
            else {
              if (result) {
                var sogbi = require('../group/show_one_group_by_id.js');
                sogbi.show_one_group_by_id(group_ID, function (err, result) {
                  if (err) {
                    console_logger.error(err);
                    callback(err, null);
                  }
                  else {
                    if (result) {
                      var member_data = result.MEMBERS,
                        ug = require('../group/update_group.js');
                      member_data.push({
                        user_ID: user_ID.toString(),
                        PSID: PSID
                      });
                      ug.update_group(group_ID, { MEMBERS: member_data }, function (err, result) {
                        if (err) {
                          console_logger.error(err);
                          callback(err, null);
                        }
                        else {
                          if (result) {
                            console_logger.info('update group successfully');
                            callback(null, 'done');
                          } else {
                            console_logger.info('update group failed');
                            callback(null, null);
                          }
                        }
                      });
                    }
                  }
                });
              } else {
                console_logger.info('update user failed');
                callback(null, null);
              }
            }
          });
        } else {
          console_logger.info('user existed in group');
          callback(null, null);
        }
      } else {
        callback(null, null);
      }
    }
  });
}
