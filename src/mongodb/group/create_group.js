const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config');

exports.create_group = function create_group(name, created_by, callback) {
  let data = {
    NAME: name,
    MEMBERS: [],
    CREATED_BY: ObjectId(created_by),
    CREATED_DATE: new Date(),
    UPDATED_AT: null,
    UPDATED_BY: null
  }
  db.get().collection(config.collection.GROUP).insertOne(data, function (err, result) {
    if (err) {
      callback(null, err);
    }
    else {
      callback(null, result.insertedId);
    }
  });
}
