const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.show_all_groups = function show_all_groups(callback) {
  db.get().collection(config.collection.GROUP).find().toArray(function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
