const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.show_one_group_by_name = function show_one_group_by_name(group_name, callback) {
  db.get().collection(config.collection.GROUP).findOne({ NAME: group_name }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
