const db = require('../connect_to_DB.js'),
  config = require('../config');

exports.show_all_admin_session_history = function show_all_admin_session_history(admin_id, callback) {
  db.get().collection(config.collection.ADMIN_SESSION_HISTORY).find({ ADMIN_ID: admin_id }).toArray(function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
