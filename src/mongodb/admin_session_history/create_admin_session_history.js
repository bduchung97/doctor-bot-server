const db = require('../connect_to_DB.js'),
  config = require('../config.js'),
  uuidv4 = require('uuid/v4');

exports.create_admin_session_history = function create_admin_session_history(
  admin_id,
  callback
) {

  let data = {
    ADMIN__ID: admin_id,
    LOGGED_AT: new Date(),
    TOKEN: uuidv4()
  }
  db.get().collection(config.collection.ADMIN_SESSION_HISTORY).insertOne(data, function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      callback(null, result.insertedId);
    }
  });
}
