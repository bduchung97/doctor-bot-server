const MongoClient = require('mongodb').MongoClient,
  config = require('./config.js');
let mgdb;

function connect(callback) {
  MongoClient.connect(config.url, {
    useNewUrlParser: true,
    poolSize: 80,
  }, (err, client) => {
    if (err) {
      callback(err, null);
    } else {
      mgdb = client.db(config.dbName);
      callback(null, mgdb);
    }
  });
}
function get() {
  return mgdb;
}

function close() {
  mgdb.close();
}

module.exports = {
  connect,
  get,
  close
};
