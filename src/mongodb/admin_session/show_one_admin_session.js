const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config.js');

exports.show_one_admin_session = function show_one_admin_session(admin_session_id, callback) {
  db.get().collection(config.collection.ADMIN_SESSION).findOne({ _id: ObjectId(admin_session_id) }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
