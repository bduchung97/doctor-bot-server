const db = require('../connect_to_DB'),
  config = require('../config.js');

exports.show_one_admin_session_by_token = function show_one_admin_session_by_token(token, callback) {
  db.get().collection(config.collection.ADMIN_SESSION).findOne({ TOKEN: token }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
