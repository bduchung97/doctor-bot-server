const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config.js'),
  uuidv4 = require('uuid/v4');

exports.create_admin_session = function create_admin_session(
  admin_id,
  callback
) {
  let data = {
    ADMIN__ID: ObjectId(admin_id),
    CREATED_AT: new Date(),
    TOKEN: uuidv4()
  }
  db.get().collection(config.collection.ADMIN_SESSION).insertOne(data, function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      callback(null, result.insertedId);
    }
  });

}
