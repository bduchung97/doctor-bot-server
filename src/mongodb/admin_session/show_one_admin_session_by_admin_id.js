const db = require('../connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../config.js');

exports.show_one_admin_session_by_admin_id = function show_one_admin_session_by_admin_id(admin_id, callback) {
  db.get().collection(config.collection.ADMIN_SESSION).findOne({ ADMIN__ID: ObjectId(admin_id) }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}
