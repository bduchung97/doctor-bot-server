let db = require('../mongodb/connect_to_DB.js'),
  config = require('../mongodb/config'),
  logger = require('../utils/logger/logger'),
  console_logger = logger.getLogger();

exports.drop_collection = function drop_collection(callback) {
  db.get().collection(config.collection.SCHEDULER).drop(function (err, result) {
    if (err) {
      console_logger.info(err);
    } else {
      if (result) {
        console_logger.info('done');
        callback(null, 'done');
      }
    }
  });
}
