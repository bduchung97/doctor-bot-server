let db = require('../mongodb/connect_to_DB.js'),
  logger = require('../utils/logger/logger'),
  console_logger = logger.getLogger();

exports.drop_all_collections = function drop_all_collections(callback) {
  db.get().listCollections().toArray(function (err, result) {
    if (err) {
      console_logger.error(err);
      callback(err, null);
    } else {
      if (result.length == 0) {
        console_logger.info('no collection exist');
        callback(null, 'done');
      } else {
        let drop_collections = []
        result.map(e => {
          if (e.name !== 'system.indexes') {
            let drops = db.get().collection(e.name).drop();
            drop_collections.push(drops);
          }
        });
        Promise.all(drop_collections).then(values => {
          console_logger.info(values);
          console_logger.info('drop all');
          callback(null, 'done');
        }).catch(reason => {
          console_logger.info(reason);
        });
      }
    }
  });
}
