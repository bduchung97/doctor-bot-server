exports.start = function start() {
  const logger = require('../utils/logger/logger'),
    console_logger = logger.getLogger(),
    PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN || 'EAAIMSYvpR4cBAB3SbJICX5ddNoRmQjedVnIdr4RMKEABnIY0TxHP9V2twrvIFsnscZCSh5T7iFEXup0RUBMPZCI9j4DFh5VdSq9R6NEvOM8RgusEbzshk1ZC4gUTjkorSBVrjT2ZAqYBTiNqN6JFTzXJ1zDAZAnW75VsVyZAOZAXgZDZD',
    request = require('request'),
    ai_server_url = process.env.AI_SERVER_URL || "http://149.28.213.165",
    ai_server_port = process.env.AI_SERVER_PORT || ":8081";

  let dac = require('./drop_all_collections.js');
  dac.drop_all_collections(function (err, result) {
    if (err) {
      console_logger.error(err);
    } else {
      if (result == 'done') {
        // create admin
        let ca = require('../mongodb/admin/create_admin.js');
        ca.create_admin('root', 'root', 'root', null, null, null, null, function (err, result) {
          if (err) {
            console_logger.error(err);
          } else {
            let root_id = result,
              created_date = new Date();
            if (root_id) {
              console_logger.info('create root admin successfully');
              let cap = require('../mongodb/admin_permission/create_multiple_admin_permissions.js'),
                admin_permission = [{
                  CODE: 'ADMN',
                  DESCRIPTION: 'Quản trị admin.',
                  CREATED_AT: created_date,
                  UPDATED_AT: null,
                  UPDATED_BY: null,
                  CREATED_BY: result,
                }, {
                  CODE: 'USER',
                  DESCRIPTION: 'Quản trị người dùng.',
                  CREATED_AT: created_date,
                  UPDATED_AT: null,
                  UPDATED_BY: null,
                  CREATED_BY: result,
                }, {
                  CODE: 'GROU',
                  DESCRIPTION: 'Quản trị nhóm.',
                  CREATED_AT: created_date,
                  UPDATED_AT: null,
                  UPDATED_BY: null,
                  CREATED_BY: result,
                }, {
                  CODE: 'DATA',
                  DESCRIPTION: 'Quản trị dữ liệu cây chatbot.',
                  CREATED_AT: created_date,
                  UPDATED_AT: null,
                  UPDATED_BY: null,
                  CREATED_BY: result,
                }, {
                  CODE: 'AITR',
                  DESCRIPTION: 'Quản trị dữ liệu trainning AI.',
                  CREATED_AT: created_date,
                  UPDATED_AT: null,
                  UPDATED_BY: null,
                  CREATED_BY: result,
                }, {
                  CODE: 'HAND',
                  DESCRIPTION: 'Quản trị sổ tay tư vấn.',
                  CREATED_AT: created_date,
                  UPDATED_AT: null,
                  UPDATED_BY: null,
                  CREATED_BY: result,
                }, {
                  CODE: 'NOTI',
                  DESCRIPTION: 'Quản trị thông báo.',
                  CREATED_AT: created_date,
                  UPDATED_AT: null,
                  UPDATED_BY: null,
                  CREATED_BY: result,
                }, {
                  CODE: 'SCHE',
                  DESCRIPTION: 'Quản trị lập lịch trainning chatbot.',
                  CREATED_AT: created_date,
                  UPDATED_AT: null,
                  UPDATED_BY: null,
                  CREATED_BY: result,
                }, {
                  CODE: 'STAT',
                  DESCRIPTION: 'Quản trị thống kê.',
                  CREATED_AT: created_date,
                  UPDATED_AT: null,
                  UPDATED_BY: null,
                  CREATED_BY: result,
                },
                {
                  CODE: 'LIVECHAT',
                  DESCRIPTION: 'Quản trị tin nhắn liveChat.',
                  CREATED_AT: created_date,
                  UPDATED_AT: null,
                  UPDATED_BY: null,
                  CREATED_BY: result,
                }];
              cap.create_multiple_admin_permissions(admin_permission, function (err, result) {
                if (err) {
                  console_logger.error(err);
                } else {
                  if (result) {
                    ca.create_admin(
                      'Nguyễn Quang Anh',
                      'quanh_nguyen',
                      'root',
                      'quanh_nguyen@outlook.com',
                      '0915781197',
                      ['ADMN', 'USER', 'GROU', 'DATA', 'AITR', 'HAND', 'NOTI', 'SCHE', 'STAT', 'LIVECHAT'],
                      root_id,
                      function (err, result) {
                        if (err) {
                          console_logger.error(err);
                        } else {
                          if (result) {
                            console_logger.info('create admin Quang Anh successfully');
                          } else {
                            console_logger.info('create admin Quang Anh unsuccessfully');
                          }
                        }
                      });
                    ca.create_admin(
                      'Nguyễn Hoàng Linh',
                      'hlinh188',
                      'root',
                      'hlinh188@gmail.com',
                      '0365295389',
                      ['ADMN', 'USER', 'GROU', 'DATA', 'AITR', 'HAND', 'NOTI', 'SCHE', 'STAT', 'LIVECHAT'],
                      root_id, function (err, result) {
                        if (err) {
                          console_logger.error(err);
                        } else {
                          if (result) {
                            console_logger.info('create admin Hoang Linh successfully');
                          } else {
                            console_logger.info('create admin Hoang Linh unsuccessfully');
                          }
                        }
                      });
                    ca.create_admin(
                      'Nguyễn Châu Linh',
                      'admin_cl',
                      'root',
                      'clinh@gmail.com',
                      '0123456789',
                      ['ADMN', 'USER', 'GROU', 'DATA', 'AITR', 'HAND', 'NOTI', 'SCHE', 'STAT', 'LIVECHAT'],
                      root_id, function (err, result) {
                        if (err) {
                          console_logger.error(err);
                        } else {
                          if (result) {
                            console_logger.info('create admin CHau Linh successfully');
                          } else {
                            console_logger.info('create admin cHau Linh unsuccessfully');
                          }
                        }
                      });
                    ca.create_admin(
                      'Hùng Bùi',
                      'admin_hung',
                      'root',
                      'admin_hung@gmail.com',
                      '0123456789',
                      ['ADMN', 'USER', 'GROU', 'DATA', 'AITR', 'HAND', 'NOTI', 'SCHE', 'STAT', 'LIVECHAT'],
                      root_id, function (err, result) {
                        if (err) {
                          console_logger.error(err);
                        } else {
                          if (result) {
                            console_logger.info('create admin Hung successfully');
                          } else {
                            console_logger.info('create admin Hung unsuccessfully');
                          }
                        }
                      });
                  }
                }
              });

              //create persistent menu
              let cl = require('../mongodb/leaf/create_leaf.js'),
                persistent_menu_value = {
                  "persistent_menu": [
                    {
                      "locale": "default",
                      "composer_input_disabled": false,
                      "call_to_actions": [
                        {
                          "title": "Các bệnh hỗ trợ",
                          "type": "nested",
                          "call_to_actions": [
                            {
                              "title": "Tuân thủ chung",
                              "type": "postback",
                              "payload": "GENERAL_COMPLIANCE"
                            },
                            {
                              "title": "Tuân thủ nghiệp vụ",
                              "type": "postback",
                              "payload": "BUSINESS_COMPLIANCE"
                            }
                          ]
                        },
                        {
                          "title": "Chat với cộng tác viên",
                          "type": "postback",
                          "payload": "ACTIVATE/INACTIVATE_BOT"
                        },
                        {
                          "title": "Thời tiết",
                          "type": "postback",
                          "payload": "WEATHER"
                        }
                      ]
                    }
                  ]
                }

              cl.create_leaf('PERSISTENT_MENU', 'ROOT', persistent_menu_value, root_id, 'Menu nhanh', 'PERSISTENT_MENU', function (err, result) {
                if (err) {
                  console_logger.error(err);
                } else {
                  if (result) {
                    request({
                      url: 'https://graph.facebook.com/v2.6/me/messenger_profile?access_token=' + PAGE_ACCESS_TOKEN,
                      method: 'POST',
                      headers: { 'Content-Type': 'application/json' },
                      form: persistent_menu_value
                    }, (err, _res, body) => {
                      if (!err) {
                        console_logger.info('Create permission_menu successful!! ' + body);
                      } else {
                        console_logger.error(err);
                      }
                    });
                  } else {
                    console_logger.info('Create permission_menu unsuccessful!!');
                  }
                }
              });

              //create_get_started_leaf
              let get_started_value = {
                "text": "Xin chào mừng quý khách đến với Chatbot hỗ trợ chẩn đoán triệu chứng"
              }
              cl.create_leaf('GET_STARTED', 'ROOT', get_started_value, root_id, 'Câu chào', 'TEXT', function (err, result) {
                if (err) {
                  console_logger.error(err);
                } else {
                  if (result) {
                    console_logger.info('create get_started_leaf successfully');
                  } else {
                    console_logger.info('create get_started_leaf unsuccessfully');
                  }
                }
              });
            } else {
              console_logger.info('Create root admin unsuccessful!!');
            }
          }
        });
      }
    }
  });

  let gt = {
    "get_started": { "payload": "GET_STARTED" }
  }
  request({
    url: 'https://graph.facebook.com/v2.6/me/messenger_profile?access_token=' + PAGE_ACCESS_TOKEN,
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    form: gt
  }, (err, _res, body) => {
    if (!err) {
      console_logger.info('Create get_started successful!! ' + body);
    } else {
      console_logger.error(err);
    }
  });
}



