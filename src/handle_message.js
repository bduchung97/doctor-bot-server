const call_send_API = require("./call_send_API"),
  saon = require('./sender_action_on'),
  sol = require('./mongodb/leaf/show_one_leaf.js'),
  logger = require('./utils/logger/logger.js'),
  console_logger = logger.getLogger(),
  cufi = require('./mongodb/user_fb_info/create_user_fb_info.js'),
  us = require('./mongodb/user/update_user_by_psid.js'),
  soqrr = require('./mongodb/quick_replies_ref/show_one_quick_replies_ref');
plugin_url = process.env.PLUGIN_URL || "https://legal-chatbot-ai-plugin.herokuapp.com",
  plugin_port = process.env.PLUGIN_PORT || '';

// Handles messages events
exports.handleMessage = function handleMessage(sender_psid, received_message) {
  saon.sender_action_on(sender_psid, function (err, result) { });
  let soubp = require('./mongodb/user/show_one_user_by_psid.js');
  soubp.show_one_user_by_psid(sender_psid.toString(), function (err, result) {
    if (result) {
      let user_ID = result._id;
      if (result.IS_ACTIVE == 1) {
        if (result.LIVE_CHAT == 0) {
          saon.sender_action_on(sender_psid, function (err, result) { });
          if (received_message.attachments) {
            sol.show_one_leaf('DEFAULT', function (err, result) {
              if (err) {
                console_logger.error(err);
              } else {
                if (result) {
                  call_send_API.callSendAPI(sender_psid, result[0].VALUE);
                }
              }
            });
            let cua = require('../src/mongodb/user_activity/create_user_activity.js');
            cua.create_user_activity(sender_psid.toString(), 'DEFAULT', function (err, result) {
              if (err) {
                console_logger.error(err);
              } else {
                if (result) {
                  console_logger.info('create user activity successfully');
                } else {
                  console_logger.info('create user activity unsuccessfully');
                }
              }
            });
          }
          else {
            saon.sender_action_on(sender_psid, function (err, result) { });
            if (received_message.text) {
              console_logger.info(received_message.text)

              soqrr.show_one_quick_replies_ref(received_message.text, function (err, result) {
                if (err) {
                  console_logger.error(err);
                }
                else {
                  if (result) {
                    let payload = result.PAYLOAD,
                      cua = require('./mongodb/user_activity/create_user_activity.js');
                    cua.create_user_activity(sender_psid.toString(), payload, function (err, result) {
                      if (err) {
                        console_logger.error(err);
                      } else {
                        if (result) {
                          console_logger.info('create user activity successfully');
                        } else {
                          console_logger.info('create user activity unsuccessfully');
                        }
                      }
                    });
                    if (payload == 'WEATHER') {
                      let hwp = require('./utils/weather/handle_weather_payload.js');
                      hwp.handle_weather_payload(function (err, result) {
                        if (err) {
                          console_logger.error(err);
                        } else {
                          if (result) {
                            call_send_API.callSendAPI(sender_psid, result);
                            setTimeout(() => {
                              saon.sender_action_on(sender_psid, function (err, result) { });
                              sol.show_one_leaf('DEFAULT', function (err, result) {
                                if (result) {
                                  call_send_API.callSendAPI(sender_psid, result[0].VALUE);
                                }
                              });
                            }, 4000)
                          }
                        }
                      });
                    }
                    else if (payload == 'ACTIVATE/INACTIVATE_BOT') {
                      saon.sender_action_on(sender_psid, function (err, result) { });
                      us.update_user_by_psid(sender_psid, { LIVE_CHAT: 1 }, function (err, result) {
                        if (result) {
                          let inactivate_bot = {
                            "text": "Bạn đã tắt chức năng chat với bot để sử dụng tính năng livechat."
                          }
                          call_send_API.callSendAPI(sender_psid, inactivate_bot);
                        }
                      })

                    }
                    else {
                      saon.sender_action_on(sender_psid, function (err, result) { });
                      sol.show_one_leaf(payload, function (err, result) {
                        if (err) {
                          console_logger.error(err);
                        } else {
                          if (result) {
                            console_logger.info(result)
                            result.map((e, i) => {
                              (function a(e, i) {
                                setTimeout(() => {
                                  saon.sender_action_on(sender_psid, function (err, result) { });
                                  call_send_API.callSendAPI(sender_psid, e.VALUE);
                                }, (2000 * i))
                              })(e, i);
                            });
                            setTimeout(() => {
                              saon.sender_action_on(sender_psid, function (err, result) { });
                              sol.show_one_leaf('DEFAULT', function (err, result) {
                                if (result) {
                                  call_send_API.callSendAPI(sender_psid, result[0].VALUE);
                                }
                              });
                            }, 4000);
                          } else {
                            let cua = require('../src/mongodb/user_activity/create_user_activity.js');
                            cua.create_user_activity(sender_psid.toString(), 'DEFAULT', function (err, result) {
                              if (err) {
                                console_logger.error(err);
                              } else {
                                if (result) {
                                  console_logger.info('create user activity successfully');
                                } else {
                                  console_logger.info('create user activity unsuccessfully');
                                }
                              }
                            });
                            saon.sender_action_on(sender_psid, function (err, result) { });
                            sol.show_one_leaf('DEFAULT', function (err, result) {
                              if (err) {
                                console_logger.error(err);
                              } else {
                                if (result) {
                                  call_send_API.callSendAPI(sender_psid, result[0].VALUE);
                                }
                              }
                            });
                          }
                        }
                      });
                    }
                  }
                  else {
                    saon.sender_action_on(sender_psid, function (err, result) { });
                    sol.show_one_leaf('DEFAULT', function (err, result) {
                      if (err) {
                        console_logger.error(err);
                      } else {
                        console.log(result)
                        if (result) {
                          call_send_API.callSendAPI(sender_psid, result[0].VALUE);
                        }
                      }
                    });
                  }
                }
              });
            }
          }
        } else {
          if (received_message.text) {
            console_logger.info(received_message.text);
            soqrr.show_one_quick_replies_ref(received_message.text, function (err, result) {
              if (err) {
                console_logger.error(err);
              }
              else {
                if (result) {
                  let payload = result.PAYLOAD,
                    cua = require('./mongodb/user_activity/create_user_activity.js');
                  cua.create_user_activity(sender_psid.toString(), payload, function (err, result) {
                    if (err) {
                      console_logger.error(err);
                    } else {
                      if (result) {
                        console_logger.info('create user activity successfully');
                      } else {
                        console_logger.info('create user activity unsuccessfully');
                      }
                    }
                  });
                  if (payload == 'ACTIVATE/INACTIVATE_BOT') {
                    saon.sender_action_on(sender_psid, function (err, result) { });
                    us.update_user_by_psid(sender_psid, { LIVE_CHAT: 0 }, function (err, result) {
                      if (result) {
                        console.log('ok');
                        let inactivate_bot = {
                          "text": "Bạn đã dừng sử dụng tính năng live chat và bật lại chức năng chat với bot."
                        }
                        call_send_API.callSendAPI(sender_psid, inactivate_bot);
                      }
                    })
                  }
                } else {
                  (async () => {
                    let saabf = require('./mongodb/admin/show_all_admins_by_filter.js'),
                      livechat_admins = await saabf.show_all_admins_by_filter({ IS_ACTIVE: 1, PERMISSION: 'LIVECHAT' }, {}),
                      IDs = [];
                    livechat_admins.map(e => {
                      IDs.push(e._id);
                    })
                    console_logger.info(IDs);
                    if (IDs.length) {
                      let random = require('./utils/handle_array/random.js'),
                        admin_ID = random.random(IDs),
                        cm = require('./mongodb/livechat_message/create_message.js'),
                        result = await cm.create_message(sender_psid, received_message.text, admin_ID);
                      console.log(result);
                    }
                  })()
                }
              }
            });
          }
          if (received_message.attachments) {
            (async () => {
              let saabf = require('./mongodb/admin/show_all_admins_by_filter.js'),
                livechat_admins = await saabf.show_all_admins_by_filter({ IS_ACTIVE: 1, PERMISSION: 'LIVECHAT' }, {}),
                IDs = [];
              livechat_admins.map(e => {
                IDs.push(e._id);
              })
              console_logger.info(IDs);
              if (IDs.length) {
                let random = require('./utils/handle_array/random.js'),
                  admin_ID = random.random(IDs),
                  cm = require('./mongodb/livechat_message/create_message.js'),
                  result = await cm.create_message(sender_psid, received_message.text, admin_ID);
                console.log(result);
              }
            })()
          }
        }
      }
    } else {
      saon.sender_action_on(sender_psid, function (err, result) { });
      let cu = require('./mongodb/user/create_user.js');
      cu.create_user(sender_psid, function (err, result) {
        if (err) {
          console_logger.error(err);
        } else {
          if (result) {
            sol.show_one_leaf('GET_STARTED', function (err, result) {
              if (err) {
                console_logger.error(err);
              } else {
                result.map((e, i) => {
                  (function a(e, i) {
                    setTimeout(() => {
                      saon.sender_action_on(sender_psid, function (err, result) { });
                      call_send_API.callSendAPI(sender_psid, e.VALUE);
                    }, (2000 * i))
                  })(e, i);
                });
              }
            });
          }
        }
      });
      cufi.create_user_fb_info(sender_psid, function (err, result) { })
    }
  });
}
