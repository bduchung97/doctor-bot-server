exports.post_webhook = function post_webhook(req, res) {
  let handle_message = require('../../handle_message.js'),
    handle_postback = require('../../handle_postback'),
    logger = require('../../utils/logger/logger.js'),
    console_logger = logger.getLogger();
  // Parse the request body from the POST
  let body = req.body;
  //console_logger.info(body);

  // Check the webhook event is from a Page subscription
  if (body.object === 'page') {

    // Iterate over each entry - there may be multiple if batched
    body.entry.map(function (entry) {
      // console.log(entry);

      // Gets the body of the webhook event
      let webhook_event = entry.messaging[0];
      // console.log(webhook_event);


      // Get the sender PSID
      var sender_psid = webhook_event.sender.id;
      console_logger.info('Sender PSID: ' + sender_psid);

      // Check if the event is a message or postback and
      // pass the event to the appropriate handler function
      if (webhook_event.message) {
        handle_message.handleMessage(sender_psid, webhook_event.message);
      } else if (webhook_event.postback) {
        handle_postback.handlePostback(sender_psid, webhook_event.postback);
      }

    });

    // Return a '200 OK' response to all events
    res.status(200).send('EVENT_RECEIVED');

  } else {
    // Return a '404 Not Found' if event is not from a page subscription
    res.sendStatus(404);
  }
}