const fs = require('fs');
exports.check_file_exists = function check_file_exists(file, callback) {
  fs.exists(file, function (exists) {
    if (exists) {
      let stats = fs.statSync(file),
        fileSizeInBytes = stats.size,
        file_size = fileSizeInBytes / 1048576;
      if (file_size < 1000) {
        callback(null, 'true');
      }
      else callback(null, 'false');
    } 
    else callback(null, 'false');
  })
}