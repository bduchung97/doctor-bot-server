exports.validate_file = function validate_file(file, callback) {
  let cfex = require('./check_file_exists.js')
  cfex.check_file_exists(file, function (err, result) {
    if (result == 'false') {
      callback(null, 'false');
    } else {
      let cfe = require('./check_file_extname.js')
      if (cfe.check_file_extname(file) == true) {
        callback(null, 'true');
      } else callback(null, 'false');
    }
  })
}