const path = require('path');

exports.check_file_extname = function check_file_extname(file) {
  let ext = path.extname(file);
  if (ext.toString() == '.xlsx') return true;
  return false;
}
