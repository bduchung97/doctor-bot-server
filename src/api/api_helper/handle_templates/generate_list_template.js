// var input = {
//   'type': 'list',
//   'top': {
//     "title_top": "Dâu tây",
//     "subtitle_top": "Dâu tây được trồng lấy trái ở vùng ôn đới.",
//     "image_url_top": "http://www.dacsanhanydalat.com/image/data/sanpham/18-04-17%2048_34%20_dau%20tuoi.jpg",
//     "default_action_top": {
//       "type_top": "web_url",
//       "url_top": "https://vi.wikipedia.org/wiki/D%C3%A2u_t%C3%A2y",
//     }
//   },
//   'data': [
//     {
//       "title": "Chuối",
//       "image_url": "http://sohanews.sohacdn.com/thumb_w/660/2017/thanh-phan-dinh-duong-cua-chuoi-suy-chuoi-tay-1508127700332-0-71-560-973-crop-1508127705053.jpg",
//       "subtitle": "Chuối được trồng ở ít nhất 107 quốc gia.",
      // "default_action": {
      //   "type": "web_url",
      //   "url": "https://vi.wikipedia.org/wiki/Chu%E1%BB%91i",
      // },
//       "buttons": [
//         {
//           "title": "Chi tiết",
//           "type": "web_url",
//           "url": "https://vi.wikipedia.org/wiki/Chu%E1%BB%91i",
//         }
//       ]
//     },
//     {
//       "title": "Bơ",
//       "image_url": "https://znews-photo.zadn.vn/w660/Uploaded/sgorvz/2016_06_03/qua_bo.jpg",
//       "subtitle": "Trái của cây bơ hình như cái bầu nước, dài 7–20 cm, nặng 100g-1 kg.",
//       "default_action": {
//         "type": "web_url",
//         "url": "https://vi.wikipedia.org/wiki/B%C6%A1_(th%E1%BB%B1c_v%E1%BA%ADt)",
//       },
//       "buttons": [
//         {
//           "title": "Chi tiết",
//           "type": "web_url",
//           "url": "https://vi.wikipedia.org/wiki/B%C6%A1_(th%E1%BB%B1c_v%E1%BA%ADt)",
//         }
//       ]
//     },
//     {
//       "title": "Óc chó",
//       "image_url": "https://trungtamduoclieu.vn/files/sanpham/914/1/jpg/nhan-qua-oc-cho-my.jpg",
//       "subtitle": "Óc chó hay Hồ đào, Hạch đào là một Chi thực vật thuộc Họ Óc chó.",
//       "default_action": {
//         "type": "web_url",
//         "url": "https://vi.wikipedia.org/wiki/Chi_%C3%93c_ch%C3%B3",
//       },
//       "buttons": [
//         {
//           "title": "Chi tiết",
//           "type": "web_url",
//           "url": "https://vi.wikipedia.org/wiki/Chi_%C3%93c_ch%C3%B3",
//         }
//       ]
//     }
//   ],
//   "buttons": [
//     {
//       "title": "Xem thêm",
//       "type": "postback",
//       "payload": "MORE_SPECIAL"
//     }
//   ]
// }

exports.generate_list_template = function generate_list_template(input) {
  var elements = [{
    "title": input.top.title_top,
    "subtitle": input.top.subtitle_top,
    "image_url": input.top.image_url_top,
    "default_action": input.top.default_action_top
  }];
  for (i = 0; i < input.data.length; i++) {
    elements.push({
      "title": input.data[i].title,
      "image_url": input.data[i].image_url,
      "subtitle": input.data[i].subtitle,
      "default_action": input.data[i].default_action,
      "buttons": input.data[i].buttons
    })
  }
  let template = {
    "attachment": {
      "type": "template",
      "payload": {
        "template_type": 'list',
        "top_element_style": "LARGE",
        "elements": elements,
        "buttons": input.buttons
      }
    }
  }
  // console.log(template);
  // console.log(template.attachment.payload.elements)
  // console.log(elements)
  return template;
}

