// var text = {
//   'type': 'text',
//   'data': {
//     'text': 'alo alo'
//   }
// }

exports.handle_templates = function handle_templates(input) {
  if (input.type == 'text') {
    let gtt = require('./generate_text_template.js');
    console.log(gtt.generate_text_template(input.data.text));
    return gtt.generate_text_template(input.data.text)
  }
  else if (input.type == 'quick_replies') {
    let gqrt = require('./generate_quick_reply_template.js');
    console.log(gqrt.generate_quick_reply_template(input.data));
    return gqrt.generate_quick_reply_template(input.data);
  }
  else if (input.type == 'media') {
    let gmt = require('./generate_media_template.js');
    console.log(gmt.generate_media_template(input.data));
    return gmt.generate_media_template(input.data);
  }
  else if (input.type == 'list') {
    let glt = require('./generate_list_template.js');
    console.log(glt.generate_list_template(input.data));
    return glt.generate_list_template(input.data);
  }
  else if (input.type == 'generic') {
    let ggt = require('./generate_generic_template.js');
    console.log(input.data);
    console.log(ggt.generate_generic_template(input.data));
    return ggt.generate_generic_template(input.data);
  }
  else return 0;
}

// handle_templates(text);