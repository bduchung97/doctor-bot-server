// var input = {
//   'type': 'media',
//   'data': {
//     'template_type': 'image',
//     'url': 'abc.com',
//     'buttons': [{
//       "type": "web_url",
//       "url": "https://www.msb.com.vn/tin-tuc/4390/Chuong-trinh-khuyen-mai--Quet-M-QR-don-bao-qua-tang-",
//       "title": "M-QR",
//       'payload': null
//     }, {
//       'type': 'payload',
//       'title': 'abc',
//       'payload': 'CLGD',
//       'url': null
//     }]
//   }
// }

exports.generate_media_template = function generate_media_template(input) {
  var buttons = [];
  input.buttons.forEach(element => {
    buttons.push({
      type: element.type,
      title: element.title,
      url: element.url,
      payload: element.payload
    });
  });

  let template = {
    "attachment": {
      "type": "template",
      "payload": {
        "template_type": "media",
        "elements": [
          {
            "media_type": input.template_type,
            "url": input.url,
            "buttons": input.buttons
          }]
      }
    }
  }
  return template;
}

// generate_media_template(input)