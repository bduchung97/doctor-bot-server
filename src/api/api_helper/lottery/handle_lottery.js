const call_send_API = require('../../../call_send_API.js'),
  gni = require('../../../mongodb/leaf/get_node_ID.js');

exports.handle_lottery = function handle_lottery(psid) {
  let glr = require('./get_lottery_result');
  glr.get_lottery_result(function (err, lottery) {
    if (lottery) {
      let response = {
        'text' : lottery
      }
      call_send_API.callSendAPI(psid, response);
    }
  })
  setTimeout(function () {
    gni.get_node_ID('DEFAULT', function (err, res_home) {
      if (res_home) {
        let stbo = require('../../../mongodb/leaf/show_templates_by_order.js');
        stbo.show_templates_by_order(res_home, function (err, array_res_home) {
          array_res_home.map(e => {
            call_send_API.callSendAPI(psid, e.VALUE);
          });
        });
      }
    });
  }, 500);
}

//handle_lottery('1984892361559396') 