const call_send_API = require('../../../call_send_API.js'),
  wf = require('./weather_forecast.js'),
  fwf = require('./fetch_weather_forecast.js'),
  gni = require('../../../mongodb/leaf/get_node_ID.js');

exports.handle_weather_payload = function handle_weather_payload(
  psid
) {
  fwf.fetch_weather_forecast(function (err, fwf_result) {
    if (fwf_result) {
      wf.weather_forecast(fwf_result, function (err, result) {
        console.log(result.attachment.payload.elements[0]);
        call_send_API.callSendAPI(psid, result);
        gni.get_node_ID('DEFAULT', function (err, res_home) {
          if (res_home) {
            let stbo = require('../../../mongodb/leaf/show_templates_by_order.js');
            stbo.show_templates_by_order(res_home, function (err, array_res_home) {
              array_res_home.map(e => {
                call_send_API.callSendAPI(psid, e.VALUE);
              });
            });
          }
        });
      })
    }
  })
}
