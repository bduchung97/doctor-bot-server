const gni = require('../../../mongodb/leaf/get_node_ID.js'),
  stbo = require('../../../mongodb/leaf/show_templates_by_order.js');

exports.weather_forecast = function weather_forecast(result, callback) {
  gni.get_node_ID('WEATHER', function (err, gni_result) {
    if (gni_result !== null) {
      stbo.show_templates_by_order(gni_result, function (err, stbo_result) {
        stbo_result[0].VALUE.attachment.payload.elements[0].subtitle = result[1].location.name + "\nNhiệt độ: " + result[1].current.temperature + " độ C\nTrạng thái: " + result[1].current.skytext + "\nĐộ ẩm: " + result[1].current.humidity + "%";
        console.log(stbo_result[0].VALUE.attachment.payload.elements[0].subtitle)
        callback(null, stbo_result[0].VALUE);
      });
    }
  });
}

