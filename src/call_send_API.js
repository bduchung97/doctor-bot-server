const request = require('request'), logger = require('./utils/logger/logger.js'),
  console_logger = logger.getLogger();

// Sends response messages via the Send API
exports.callSendAPI = function callSendAPI(sender_psid, response) {
  // Construct the message body
  let request_body = {
    "recipient": {
      "id": sender_psid
    },
    "message": response
  }

  // Send the HTTP request to the Messenger Platform
  request({
    "uri": "https://graph.facebook.com/v2.6/me/messages",
    "qs": {
      "access_token": "EAAIMSYvpR4cBAB3SbJICX5ddNoRmQjedVnIdr4RMKEABnIY0TxHP9V2twrvIFsnscZCSh5T7iFEXup0RUBMPZCI9j4DFh5VdSq9R6NEvOM8RgusEbzshk1ZC4gUTjkorSBVrjT2ZAqYBTiNqN6JFTzXJ1zDAZAnW75VsVyZAOZAXgZDZD"
    },
    "method": "POST",
    "json": request_body
  }, (err, res, body) => {
    if (!err) {
      console_logger.info('Message sent!');
    } else {
      console_logger.error("Unable to send message:" + err);
    }
  });
}

// let sender_psid = 2584269004917663;

// let ed = require('./utils/crypt/encrypt_data.js');

// let url = "https://legal-chatbot-ai-plugin.herokuapp.com/login?data=" + ed.encrypt_data(sender_psid.toString())

// let login_form_value = {
//   "attachment": {
//     "type": "template",
//     "payload": {
//       "template_type": "generic",
//       "elements": [
//         {
//           "title": "Đăng nhập",
//           "image_url": "https://i.imgur.com/TGIwLTX.png",
//           "subtitle": "Mời bạn đăng nhập để sử dụng hệ thống.",
//           "buttons": [
//             {
//               "title": "ĐĂNG NHẬP",
//               "type": "web_url",
//               "url": url,
//               "webview_height_ratio": "tall"
//             }
//           ]
//         }
//       ]
//     }
//   }
// }

// callSendAPI('2584269004917663', login_form_value)