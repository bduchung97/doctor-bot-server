const log4js = require('log4js');

log4js.configure({
  appenders: {
    //logfile: { type: 'file', filename: './src/utils/logger/filelog.log' },
    console: { type: 'console' }
  },
  categories: {
    //err: { appenders: ['errorfile'], level: 'error' },
    //'': { appenders: ['console'], level: 'all' },
    default: { appenders: ['console'], level: 'all' }
  }
});

module.exports = log4js;
