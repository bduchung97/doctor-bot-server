const cron = require('./cron_job.js');

module.exports = {
  addCron: (cron_ID, time, func) => {
    cron.add(
      cron_ID,
      time,
      () => {
        func()
      },
      { start: false }
    )
  },
  add: (cron_ID, time, func, target, templates) => {
    cron.add(
      cron_ID,
      time,
      () => {
        func.send_template(target, templates);
      },
      { start: false }
    )
  },
  start: (cron_ID) => {
    cron.start(cron_ID);
  },
  stop: (cron_ID) => {
    cron.stop(cron_ID);
  },
  stopAll: () => {
    cron.stopAll();
  },
  findAll: () => {
    return cron.listCrons();
  },
  delete: (cron_ID) => {
    cron.deleteJob(cron_ID);
  }
}
