// var input = {
//   'type': 'quick_replies',
//   'data': {
//     'text': 'alo alo',
//     'quick_replies': [{
//       'title': 'nút 1',
//       'image_url': 'url ảnh',
//       'payload': 'QR_1'
//     }, {
//       'title': 'nút 2',
//       'image_url': 'url ảnh',
//       'payload': 'QR_2'
//     }]
//   }
// }

exports.quick_reply_template = function quick_reply_template(input) {
  var quick_replies = [];

  input.quick_replies.forEach(element => {
    quick_replies.push({
      content_type: "text",
      title: element.title,
      image_url: element.image_url,
      payload: element.payload
    });
  });

  let template = {
    "text": input.text,
    "quick_replies": quick_replies
  }
  return template;
}

// generate_quick_reply_template(input);