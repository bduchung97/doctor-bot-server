// var text = {
//   'type': 'text',
//   'data': {
//     'text': 'alo alo'
//   }
// }

exports.handle_templates = function handle_templates(input) {
  if (input.type == 'text') {
    let tt = require('./text_template');
    return tt.text_template(input.data.text)
  }
  else if (input.type == 'quick_replies') {
    let qrt = require('./quick_reply_template');
    return qrt.quick_reply_template(input.data);
  }
  else if (input.type == 'media') {
    let mt = require('./media_template');
    return mt.list_template(input.data);
  }
  else if (input.type == 'list') {
    let lt = require('./list_template')
    return lt.generate_list_template(input.data);
  }
  else if (input.type == 'generic') {
    let gt = require('./generic_template');
    return gt.generic_template(input.data);
  }
  else return null;
}

// handle_templates(text);