// var input = {
//   'type': 'template',
//   'data': [
//     {
//       "title": "Cộng đồng ưu đãi JOY",
//       "image_url": "https://i.imgur.com/IjEHC7W.jpg",
//       "buttons": [{
//         "title": "Khám phá cuộc sống",
//         "type": "postback",
//         "payload": "KHAMPHA_JOY_PROM_OTHER_SERVICES"
//       },
//       {
//         "title": "Tận hưởng cuộc sống",
//         "type": "postback",
//         "payload": "TANHUONG_JOY_PROM_OTHER_SERVICES"
//       },
//       {
//         "title": "An tâm cuộc sống",
//         "type": "postback",
//         "payload": "ANTAM_JOY_PROM_OTHER_SERVICES"
//       }]
//     },
//     {
//       "title": "Cộng đồng ưu đãi JOY",
//       "image_url": "https://i.imgur.com/NBTyycZ.jpg",
//       "buttons": [{
//         "title": "Nâng tầm cuộc sống",
//         "type": "postback",
//         "payload": "NANGTAM_JOY_PROM_OTHER_SERVICES"
//       },
//       {
//         "title": "Đầu tư tương lai",
//         "type": "postback",
//         "payload": "DAUTU_JOY_PROM_OTHER_SERVICES"
//       }]
//     }
//   ]
// }
exports.generic_template = function generic_template(input) {
  var elements = [];
  input.forEach(i => {
    elements.push({
      title: i.title,
      image_url: i.image_url,
      buttons: i.buttons
    })
  })
  let template = {
    "attachment": {
      "type": 'template',
      "payload": {
        "template_type": "generic",
        "elements": elements
      }
    }
  }
  return template;
}

