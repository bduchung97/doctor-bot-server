exports.sort_array = function sort_array(a, b) {
  if (a.COUNT > b.COUNT) {
    return -1;
  }
  if (a.COUNT < b.COUNT) {
    return 1;
  }
  return 0;
}
