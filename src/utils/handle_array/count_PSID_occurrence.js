exports.count_PSID_occurrence = function count_PSID_occurrence(arr, value) {
  let count = 0;
  for (i = 0; i < arr.length; i++) {
    if (arr[i].PSID == value) {
      count = count + 1;
    }
  }
  let result = {
    PSID: value,
    COUNT: count
  }
  return result;
}

