exports.delete_object_from_object_array = function delete_object_from_object_array(object_array, user_ID) {
  let removeIndex = object_array.map(function (item) { return item.user_ID; }).indexOf(user_ID);
  if (removeIndex != -1) {
    object_array.splice(removeIndex, 1);
    return object_array;
  }
  else return null;
}
