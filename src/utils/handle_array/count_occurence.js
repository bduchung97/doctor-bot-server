exports.count_occurence = function count_occurence(arr, value) {
  let count = 0;
  for (i = 0; i < arr.length; i++) {
    if (arr[i].FUNCTION_ID == value) {
      count = count + 1;
    }
  }
  let result = {
    FUNCTION_ID: value,
    COUNT: count
  }
  return result;
}

