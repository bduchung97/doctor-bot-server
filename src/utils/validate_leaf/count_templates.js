const db = require('../../mongodb/connect_to_DB.js'),
  ObjectId = require('mongodb').ObjectID,
  config = require('../../mongodb/config.js');

exports.count_templates = function count_templates(node_id, callback) {
  db.get().collection(config.collection.TEMPLATE).countDocuments({ NODE__ID: ObjectId(node_id) }, function (err, result) {
    if (err) {
      callback(err, null);
    }
    else {
      callback(null, result);
    }
  });
}

// const MongoClient = require('mongodb').MongoClient,
//   ObjectId = require('mongodb').ObjectID,
//   config = require('../../mongodb/config');

// //exports.create_leaf_template = 
// function create_leaf_template(node_id, callback) {
//   // Use connect method to connect to the server
//   MongoClient.connect(config.url, {
//     useNewUrlParser: true
//   }, function (err, client) {
//     const mgdb = client.db(config.dbName);
//     mgdb.collection(config.collection.TEMPLATE).countDocuments({ NODE__ID: ObjectId(node_id) }, function (err, res) {
//       if (err) throw err;
//       else {
//         console.log(res);
//       }
//     })
//   });
// }
// create_leaf_template('5cf889e8358d7415bc177bc0')