exports.check_if_special_leaf = function check_if_special_leaf(node_id, callback) {
  let special_function_ID = ['GET_STARTED', 'HOME', 'DEFAULT', 'PERSISTENT_MENU'],
    echasync = require('echasync'),
    son = require('../../mongodb/node/show_one_node.js'),
    special_leaf_ID = [];
  echasync.do(special_function_ID, function (next, element) {
    son.show_one_node(element, function (err, result) {
      if (err) {
        callback(err, null);
      } else {
        if (result) {
          special_leaf_ID.push(result._id.toString());
        } else {
          callback(null, null);
        }
      }
      next();
    })
  }, function () {
    let ceia = require('../handle_array/check_element_in_array.js');
    if (ceia.check_element_in_array(special_leaf_ID, node_id.toString()) == true) {
      callback(null, 'true');
    } else {
      callback(null, 'false');
    }
  });
}