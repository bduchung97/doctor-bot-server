let CryptoJS = require("crypto-js");

exports.encrypt_data = function encrypt_data(data) {
  if (typeof data == 'string') {
    let key = process.env.MASTER_KEY || 'mcl-2019';
    return CryptoJS.AES.encrypt(data, key).toString();
  } else {
    return null;
  }
}

