const bcrypt = require('bcryptjs'),
  saltRounds = 10;

exports.hash_password = function hash_password(password) {
  return bcrypt.hashSync(password, saltRounds);
}

