const bcrypt = require('bcryptjs');

exports.compare_password = function compare_password(password_1, password_2) {
  if (bcrypt.compareSync(password_1, password_2) == true) {
    return true;
  }
  else {
    return false;
  }
}

// function compare_password(password_1, password_2) {
//   if (bcrypt.compareSync(password_1, password_2) == true) {
//     console.log('OK')
//     return true;
//   }
//   else {
//     return false;
//   }
// }

// compare_password('root', '$2a$10$d1N83aVqit/fmSHe7LrR5euu5aqCSbN1HQLpv14626d9/7/KdlW2e');