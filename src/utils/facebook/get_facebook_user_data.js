exports.get_facebook_user_data = function get_facebook_user_data(PSID, callback) {
  const request = require('request'),
    logger = require('../logger/logger'),
    console_logger = logger.getLogger(),
    PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN || 'EAAIMSYvpR4cBAB3SbJICX5ddNoRmQjedVnIdr4RMKEABnIY0TxHP9V2twrvIFsnscZCSh5T7iFEXup0RUBMPZCI9j4DFh5VdSq9R6NEvOM8RgusEbzshk1ZC4gUTjkorSBVrjT2ZAqYBTiNqN6JFTzXJ1zDAZAnW75VsVyZAOZAXgZDZD';

  request.get({ url: 'https://graph.facebook.com/' + PSID.toString() + '?fields=first_name,last_name,profile_pic,name,locale,timezone,gender&access_token=' + PAGE_ACCESS_TOKEN }, function (err, res) {
    if (!err) {
      callback(null, res.body);
      // console.log(res.body)
    } else {
      console_logger.error(err);
      callback(err, null);
    }
  });
}

// get_facebook_user_data(3048424208504702);
