var weather = require('weather-js');

exports.fetch_weather_forecast = function fetch_weather_forecast(callback) {
  // Options:
  // search:     location name or zipcode
  // degreeType: F or C
  weather.find({ search: 'Hanoi', degreeType: 'C' }, function (err, result) {
    if (err) {
      console.log(err);
      callback(err, null);
    } else {
      // console.log(result[1].location.name);
      // console.log(result[1].current.temperature);
      // console.log(result[1].current.skytext);
      // console.log(result[1].current.humidity);
      callback(null, result);
    }
  });
}
