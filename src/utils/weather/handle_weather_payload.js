let fwf = require('./fetch_weather_forecast.js');

exports.handle_weather_payload = function handle_weather_payload(callback) {
  fwf.fetch_weather_forecast(function (err, result) {
    if (err) {
      callback(err, null);
    } else {
      if (result) {
        let location = result[1].location,
          current = result[1].current,
          sol = require('../../mongodb/leaf/show_one_leaf.js');
        sol.show_one_leaf('WEATHER', function (err, result) {
          if (result) {
            result[0].VALUE.attachment.payload.elements[0].subtitle = location.name + "\nNhiệt độ: " + current.temperature + " độ C\nTrạng thái: " + current.skytext + "\nĐộ ẩm: " + current.humidity + "%";
            callback(null, result[0].VALUE);
          }
        });
      } else {
        callback(null, null);
      }
    }
  });
}
