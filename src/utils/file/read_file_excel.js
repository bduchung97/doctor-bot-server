const XLSX = require('xlsx');

exports.read_file_excel = function read_file_excel(file, callback) {
  var workbook = XLSX.readFile(file);
  var sheet_name_list = workbook.SheetNames;
  var data = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]])
  //console.log(data)
  callback(null, data);
}

//read_file_excel('./data training.xlsx')