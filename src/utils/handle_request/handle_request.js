exports.handle_request = function handle_request(req) {
  let body = req.body,
    hb = require('./handle_body.js'),
    data = hb.handle_body(body);
  if (data) {
    return data;
  } else {
    return null;
  }
}
