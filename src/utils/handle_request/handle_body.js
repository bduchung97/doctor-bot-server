exports.handle_body = function handle_body(body) {
  var encrypted_data = body.data.encrypted_data,
    checksum = body.checksum;
  let cs = require('../crypt/checksum.js');
  // console.log(JSON.stringify(body.data));
  // console.log(cs.checksum(JSON.stringify(body.data)));
  if (cs.checksum(JSON.stringify(body.data)) == checksum) {
    let dc = require('../crypt/decrypt_data.js'),
      decrypted_data = JSON.parse(dc.decrypt_data(encrypted_data));
    if (decrypted_data == null) {
      return null;
    } else {
      if (body.data.session_token) {
        let data = {
          decrypted_data: decrypted_data,
          session_token: body.data.session_token
        }
        return data;
      } else {
        let data = {
          decrypted_data: decrypted_data,
        }
        return data;
      }
    }
  } else {
    return null;
  }
}
