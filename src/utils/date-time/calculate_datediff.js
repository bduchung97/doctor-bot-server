exports.calculate_datediff = function calculate_datediff(date1, date2) {
  return (date2 - date1) / ((1000 * 60 * 60 * 24))
}
