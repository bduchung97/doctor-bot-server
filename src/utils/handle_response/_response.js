exports._response = function _response(res_data, status, res) {
  let ed = require('../crypt/encrypt_data.js'),
    cs = require('../crypt/checksum.js');
  encrypted_data = ed.encrypt_data(JSON.stringify(res_data));
  checksum = cs.checksum(JSON.stringify(encrypted_data));
  _res = {
    encrypted_data: encrypted_data,
    checksum: checksum
  }
  res.status(status).send(_res);
}
