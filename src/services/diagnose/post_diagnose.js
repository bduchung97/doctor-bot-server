exports.post_diagnose = async function post_diagnose(req, res) {
	try {
		let PSID = req.body.PSID,
			fullname = req.body.FULLNAME,
			age = req.body.AGE,
			gender = req.body.GENDER,
			action = req.body.ACTION,
			heart_rate = req.body.HEART_RATE,
			PSID = req.body.PSID,
			chan_doan = require('../../../test/chan_doan.js'),
			cd = require('../../mongodb/doctor_bot/create_diagnose.js');
		if (fullname && age && gender && action && heart_rate && PSID) {
			await cd.create_diagnose(
				fullname,
				age,
				gender,
				heart_rate,
				PSID
			)
			console.log('post_diagnose successfully');
			let csa = require('../../call_send_API_.js'),
				sol = require('../../mongodb/leaf/show_one_leaf.js'),
				response = {
					'text': 'Kết quả chẩn đoán của bạn là: ' + chan_doan.chan_doan(age, gender, action, heart_rate)
				},
				default_leaf = await sol.show_one_leaf_('DEFAULT');
			await csa.call_send_API_(PSID, response);
			console.log(default_leaf[0].VALUE)
			setTimeout(async () => {
				await csa.call_send_API_(PSID, default_leaf[0].VALUE);
			}, 1000)
			res.status(200).send({ msg_code: '001' });
		} else {
			console.log('Input is invalid!')
			res.status(400).send({ msg_code: '004' });
		}
	} catch (error) {
		console.log(error);
		res.status(500).send({ msg_Code: '500' });
	}
}