let echasync = require('echasync'),
  logger = require('../../utils/logger/logger'),
  console_logger = logger.getLogger(),
  count = 0;
exports.post_group_delete = function post_group_delete(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js'),
    data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let group_data = data.decrypted_data,
            group_ID = group_data.group_ID,
            sogbi = require('../../mongodb/group/show_one_group_by_id');
          console_logger.info(group_data);
          console_logger.info(group_ID);
          sogbi.show_one_group_by_id(group_ID, function (err, result) {
            if (err) {
              console_logger.error(err);
              hres._response({ msg_code: '006' }, 500, res);
            } else {
              if (result) {
                if (result.MEMBERS.length == 0) {
                  // console_logger.info('delete members from group successfully');
                  let dg = require('../../mongodb/group/delete_group.js');
                  dg.delete_group(group_ID, function (err, result) {
                    if (err) {
                      console_logger.error(err);
                      hres._response({ msg_code: '006' }, 500, res);
                    } else {
                      if (result) {
                        console_logger.info('delete group successfully');
                        hres._response({ msg_code: '001' }, 200, res);
                      }
                      else {
                        console_logger.info('delete group unsuccessfully');
                        hres._response({ msg_code: '002' }, 200, res);
                      }
                    }
                  });
                } else {
                  echasync.do(result.MEMBERS, function (next, element) {
                    let dm = require('../../mongodb/group/delete_member.js');
                    dm.delete_member(group_ID, element.user_ID, function (err, result) {
                      if (err) {
                        console_logger.error(err);
                        hres._response({ msg_code: '006' }, 500, res);
                      } else {
                        count = count + 1;
                      }
                      next();
                    })
                  }, function () {
                    console_logger.info('delete members from group successfully');
                    let dg = require('../../mongodb/group/delete_group.js');
                    dg.delete_group(group_ID, function (err, result) {
                      if (err) {
                        console_logger.error(err);
                        hres._response({ msg_code: '006' }, 500, res);
                      } else {
                        if (result) {
                          console_logger.info('delete group successfully');
                          hres._response({ msg_code: '001' }, 200, res);
                        } else {
                          console_logger.info('delete group unsuccessfully');
                          hres._response({ msg_code: '007' }, 500, res);
                        }
                      }
                    });
                  });
                }
              } else {
                console_logger.info('show group unsuccessfully');
                hres._response({ msg_code: '007' }, 500, res);
              }
            }
          });
        } else {
          console_logger.info('check token failed');
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}
