let logger = require('../../utils/logger/logger'),
  console_logger = logger.getLogger();
exports.put_group_update = function put_group_update(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js'),
    data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let group_data = data.decrypted_data,
            group_ID = group_data.group_ID,
            updated_data = group_data.updated_data,
            sogbn = require('../../mongodb/group/show_one_group_by_name.js');
          sogbn.show_one_group_by_name(updated_data.NAME, function (err, result) {
            if (err) {
              console_logger.error(err);
              hres._response({ msg_code: '006' }, 500, res);
            } else {
              if (result) {
                console_logger.info('duplicate group name');
                hres._response({ msg_code: '002' }, 200, res);
              } else {
                let ug = require('../../mongodb/group/update_group.js');
                ug.update_group(group_ID, updated_data, function (err, result) {
                  if (err) {
                    console_logger.error(err);
                    hres._response({ msg_code: '006' }, 500, res);
                  } else {
                    if (result) {
                      console_logger.info('update group successfully');
                      hres._response({ msg_code: '001' }, 200, res);
                    } else {
                      console_logger.info('update group unsuccessfully');
                      hres._response({ msg_code: '007' }, 500, res);
                    }
                  }
                });
              }
            }
          });
        } else {
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    hres._response({ msg_code: '004' }, 400, res);
  }
}
