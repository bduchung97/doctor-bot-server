let logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger();
const echasync = require('echasync');
var count = 0;
exports.post_group_create = function post_group_create(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js'),
    data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let group_data = data.decrypted_data,
            group_name = group_data.group_name,
            member_list = group_data.member_list,
            admin_ID = result.ADMIN__ID.toString(),
            sogbn = require('../../mongodb/group/show_one_group_by_name.js');
          sogbn.show_one_group_by_name(group_name, function (err, result) {
            if (err) {
              console_logger.error(err);
              hres._response({ msg_code: '006' }, 500, res);
            } else {
              if (result) {
                console_logger.info('group exists');
                hres._response({ msg_code: '002' }, 200, res);
              } else {
                let cg = require('../../mongodb/group/create_group.js');
                cg.create_group(group_name, admin_ID, function (err, result) {
                  if (err) {
                    console_logger.error(err);
                    hres._response({ msg_code: '006' }, 500, res);
                  } else {
                    if (result) {
                      console_logger.info('create group successfully');
                      echasync.do(member_list, function (next, element) {
                        let au = require('../../mongodb/group/add_user');
                        au.add_user(result, element.user_ID, element.PSID, function (err, result) {
                          if (err) {
                            hres._response({ msg_code: '006' }, 500, res);
                          } else {
                            count = count + 1;
                          }
                          next();
                        })
                      }, function () {
                        console_logger.info('add members successfully');
                        hres._response({ msg_code: '001' }, 200, res);
                      });
                    } else {
                      console_logger.info('create group unsuccessfully');
                      hres._response({ msg_code: '007' }, 500, res);
                    }
                  }
                });
              }
            }
          });
        } else {
          console_logger.info('check token unsuccessfully');
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}
