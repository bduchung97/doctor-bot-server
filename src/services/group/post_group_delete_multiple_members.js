let logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger();
exports.post_group_delete_multiple_members = function post_group_delete_multiple_members(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js'),
    data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let group_data = data.decrypted_data,
            group_ID = group_data.group_ID,
            user_list = group_data.user_list,
            dm = require('../../mongodb/group/delete_member.js'),
            echasync = require('echasync'),
            count = 0;
          echasync.do(user_list, function (next, element) {
            dm.delete_member(group_ID, element, function (err, result) {
              if (err) {
                hres._response({ msg_code: '006' }, 500, res);
              } else {
                if (result) {
                  count = count + 1;
                }
              }
              next();
            })
          }, function () {
            console_logger.info(count);
            console_logger.info(user_list.length);
            if (count == user_list.length) {
              console_logger.info('delete enough members');
              hres._response({ msg_code: '001' }, 200, res);
            }
            else {
              console_logger.info('delete not enough members');
              hres._response({ msg_code: '002' }, 200, res);
            }
          });
        } else {
          console_logger.info('check token failed');
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}
