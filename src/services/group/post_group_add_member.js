let logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger();
exports.post_group_add_member = function post_group_add_member(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js'),
    data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let group_data = data.decrypted_data,
            group_ID = group_data.group_ID,
            user_ID = group_data.user_ID,
            PSID = group_data.PSID,
            au = require('../../mongodb/group/add_user.js');
          au.add_user(group_ID, user_ID, PSID, function (err, result) {
            if (err) {
              console_logger.error(err);
              hres._response({ msg_code: '006' }, 500, res);
            } else {
              if (result) {
                console_logger.info('add member successfully');
                hres._response({ msg_code: '001' }, 200, res);
              } else {
                console_logger.info('add member unsuccessfully');
                hres._response({ msg_code: '002' }, 200, res);
              }
            }
          });
        } else {
          console_logger.info('check token failed');
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}
