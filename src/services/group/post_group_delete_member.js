exports.post_group_delete_member = function post_group_delete_member(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js'),
    data = hreq.handle_request(req);
  if (data) {
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          let group_data = data.decrypted_data,
            group_ID = group_data.group_ID,
            user_ID = group_data.user_ID,
            dm = require('../../mongodb/group/delete_member.js');
          dm.delete_member(group_ID, user_ID, function (err, result) {
            if (err) {
              hres._response({ msg_code: '006' }, 500, res);
            } else {
              if (result) {
                hres._response({ msg_code: '001' }, 200, res);
              } else {
                hres._response({ msg_code: '002' }, 200, res);
              }
            }
          });
        } else {
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    hres._response({ msg_code: '004' }, 400, res);
  }
}
