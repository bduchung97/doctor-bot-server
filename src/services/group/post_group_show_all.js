let echasync = require('echasync'),
  logger = require('../../utils/logger/logger'),
  console_logger = logger.getLogger();

exports.post_group_show_all = function post_group_show_all(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js'),
    data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let sag = require('../../mongodb/group/show_all_groups.js');
          sag.show_all_groups(
            function (err, result) {
              if (err) {
                console_logger.error(err);
                hres._response({ msg_code: '006' }, 500, res);
              } else {
                if (result) {
                  let groups_data = [];
                  echasync.do(result, function (next_group, e_group) {
                    let users_data = [];
                    echasync.do(e_group.MEMBERS, function (next_user, e_user) {
                      let sou = require('../../mongodb/user/show_one_user.js');
                      sou.show_one_user(e_user.user_ID, function (err, result) {
                        if (err) {
                          console_logger.error(err);
                          hres._response({ msg_code: '006' }, 500, res);
                        } else {
                          if (result) {
                            console_logger.info('show user successfully');
                            users_data.push({
                              FULLNAME: result.FULLNAME,
                              DISTINGUISHED_NAME: result.DISTINGUISHED_NAME,
                              USER_ID: result._id,
                              PSID: result.PSID,
                              EMAIL: result.EMAIL,
                              IS_ACTIVE: result.IS_ACTIVE,
                            });
                          }
                        }
                        next_user();
                      });
                    }, function () {
                      groups_data.push({
                        NAME: e_group.NAME,
                        GROUP_ID: e_group._id,
                        USERS: users_data
                      });
                      next_group();
                    });
                  }, function () {
                    console_logger.info('show all groups successfully');
                    hres._response({ msg_code: '001', groups_data: groups_data }, 200, res);
                  });
                } else {
                  console_logger.info('show all groups unsuccessfully');
                  hres._response({ msg_code: '007' }, 500, res);
                }
              }
            });
        } else {
          console_logger.info('check token failed');
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}
