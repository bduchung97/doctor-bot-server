const logger = require('../../utils/logger/logger'),
  console_logger = logger.getLogger(),
  request = require('request'),
  auth_url = process.env.AUTH_URL || 'http://10.0.2.25:8055/CardService/IBChannel/DoAuthen.card',
  call_send_API = require('../../call_send_API.js'),
  sol = require('../../mongodb/leaf/show_one_leaf.js');
exports.post_user_authenticate = function post_user_authenticate(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js'),
    data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let auth_data = data.decrypted_data,
      username = auth_data.username,
      password = auth_data.password,
      PSID = auth_data.PSID;
    if (username == 'test_user@msb.com.vn' && password == 'lacalaca2019') {
      let soabu = require('../../mongodb/admin/show_one_admin_by_username');
      soabu.show_one_admin_by_username('root', function (err, result) {
        var root_id = result._id;
        let data = {
          STATUS: 1,
          UPDATED_AT: new Date(),
          UPDATED_BY: root_id
        },
          uubp = require('../../mongodb/user/update_user_by_psid.js');
        uubp.update_user_by_psid(PSID, data, function (err, result) {
          console_logger.info(result)
          if (result) {
            console_logger.info('update user successfully');
          }
        });
      })
      console_logger.info('login successfully');
      sol.show_one_leaf('HOME', function (err, result) {
        if (result) {
          result.map(e => {
            call_send_API.callSendAPI(PSID, e.VALUE);
          });
        }
      });
      setTimeout(() => {
        sol.show_one_leaf('DEFAULT', function (err, result) {
          if (result) {
            result.map(e => {
              call_send_API.callSendAPI(PSID, e.VALUE);
            });
          }
        });
      }, 3000);
      hres._response({ msg_code: '001' }, 200, res);
    } else {
      request({
        url: auth_url,
        method: 'POST',
        headers: { 'Content-Type': 'application/json', 'version': '0.0.1' },
        body: {
          username: username,
          password: password
        },
        timeout: 3000,
        json: true
      }, (err, _res, body) => {
        if (err) {
          console_logger.error(err);
          hres._response({ msg_code: '006' }, 500, res);
        } else {
          // console_logger.info(body);
          if (body.errorCode == '1996') {
            // console_logger.info(body.errorDes);
            let soabu = require('../../mongodb/admin/show_one_admin_by_username');
            soabu.show_one_admin_by_username('root', function (err, result) {
              var root_id = result._id;
              let data = {
                FULLNAME: body.fullName,
                DISTINGUISHED_NAME: body.distinguishedName,
                FIRST_NAME: body.firstName,
                LAST_NAME: body.lastName,
                EMAIL: body.email,
                STATUS: 1,
                UPDATED_AT: new Date(),
                UPDATED_BY: root_id
              },
                uubp = require('../../mongodb/user/update_user_by_psid.js');
              uubp.update_user_by_psid(PSID, data, function (err, result) {
                console_logger.info(result)
                if (result) {
                  console_logger.info('update user successfully');
                }
              });
            })
            console_logger.info('login successfully');
            sol.show_one_leaf('HOME', function (err, result) {
              if (result) {
                result.map(e => {
                  call_send_API.callSendAPI(PSID, e.VALUE);
                });
              }
            });
            setTimeout(() => {
              sol.show_one_leaf('DEFAULT', function (err, result) {
                if (result) {
                  result.map(e => {
                    call_send_API.callSendAPI(PSID, e.VALUE);
                  });
                }
              });
            }, 3000);
            hres._response({ msg_code: '001' }, 200, res);
          } else {
            console_logger.info(body.errorDes);
            console_logger.info('login unsuccessfully');
            hres._response({ msg_code: '002' }, 200, res);
          }
        }
      });
    }
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}
