let logger = require('../../utils/logger/logger'),
  console_logger = logger.getLogger();

exports.post_user_show_all_ = function post_user_show_all_(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js'),
    data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let sau = require('../../mongodb/user/show_all_users_');
          sau.show_all_users_(
            function (err, result) {
              if (err) {
                console_logger.error(err);
                hres._response({ msg_code: '006' }, 500, res);
              } else {
                if (result) {
                  console_logger.info('show all user successfully');
                  hres._response({ msg_code: '001', users_data: result }, 200, res);
                } else {
                  console_logger.info('show all user unsuccessfully');
                  hres._response({ msg_code: '007' }, 500, res);
                }
              }
            });
        } else {
          console_logger.info('check token failed');
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}
