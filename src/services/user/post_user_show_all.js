let logger = require('../../utils/logger/logger'),
  console_logger = logger.getLogger(),
  echasync = require('echasync');
exports.post_user_show_all = function post_user_show_all(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js'),
    data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let sau = require('../../mongodb/user/show_all_users');
          sau.show_all_users(
            function (err, result) {
              if (err) {
                console_logger.error(err);
                hres._response({ msg_code: '006' }, 500, res);
              } else {
                if (result.length) {
                  var users_data = [];
                  echasync.do(result, function (next_user, e_user) {
                    let soufi = require('../../mongodb/user_fb_info/show_one_user_fb_info.js');
                    soufi.show_one_user_fb_info(e_user.PSID, function (err, fb_info) {
                      if (fb_info) {
                        var groups_data = [];
                        echasync.do(e_user.GROUPS, function (next_group, e_group) {
                          let sag = require('../../mongodb/group/show_one_group_by_id');
                          sag.show_one_group_by_id(e_group, function (err, result) {
                            if (err) {
                              console_logger.error(err);
                              hres._response({ msg_code: '006' }, 500, res);
                            } else {
                              if (result) {
                                groups_data.push({ NAME: result.NAME });
                              }
                            }
                            next_group();
                          });
                        }, function () {
                          users_data.push({
                            _id: e_user._id,
                            FB_NAME: fb_info.FB_NAME,
                            FB_FIRST_NAME: fb_info.FB_FIRST_NAME,
                            FB_LAST_NAME: fb_info.FB_LAST_NAME,
                            FB_PROFILE_PIC: fb_info.FB_PROFILE_PIC,
                            FB_LOCALE: fb_info.FB_LOCALE,
                            FB_GENDER: fb_info.FB_GENDER,
                            FB_TIMEZONE: fb_info.FB_TIMEZONE,
                            PSID: e_user.PSID,
                            FULLNAME: e_user.FULLNAME,
                            PHONE_NUMBER: e_user.PHONE_NUMBER,
                            EMAIL: e_user.EMAIL,
                            GROUPS: groups_data,
                            IS_ACTIVE: e_user.IS_ACTIVE,
                            FB_URL: e_user.FB_URL
                          });
                          next_user();
                        });
                      }
                      else {
                        var groups_data = [];
                        echasync.do(e_user.GROUPS, function (next_group, e_group) {
                          let sag = require('../../mongodb/group/show_one_group_by_id');
                          sag.show_one_group_by_id(e_group, function (err, result) {
                            if (err) {
                              console_logger.error(err);
                              hres._response({ msg_code: '006' }, 500, res);
                            } else {
                              if (result) {
                                groups_data.push({ NAME: result.NAME });
                              }
                            }
                            next_group();
                          });
                        }, function () {
                          users_data.push({
                            _id: e_user._id,
                            FB_NAME: null,
                            FB_FIRST_NAME: null,
                            FB_LAST_NAME: null,
                            FB_PROFILE_PIC: null,
                            FB_LOCALE: null,
                            FB_GENDER: null,
                            FB_TIMEZONE: null,
                            PSID: e_user.PSID,
                            FULLNAME: e_user.FULLNAME,
                            PHONE_NUMBER: e_user.PHONE_NUMBER,
                            EMAIL: e_user.EMAIL,
                            GROUPS: groups_data,
                            IS_ACTIVE: e_user.IS_ACTIVE,
                            FB_URL: e_user.FB_URL
                          });
                          next_user();
                        });
                      }
                    });
                  }, function () {
                    console_logger.info('show all users successfully');
                    console_logger.info(users_data)
                    hres._response({ msg_code: '001', users_data: users_data }, 200, res);
                  });
                } else {
                  console_logger.info('users not existed');
                  hres._response({ msg_code: '001', users_data: [] }, 200, res);
                }
              }
            });
        } else {
          console_logger.info('check token failed');
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}
