let logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger();

exports.post_template_delete = function post_template_delete(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js'),
    data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let leaf_data = data.decrypted_data,
            template_id = leaf_data.template_id,
            node_id = leaf_data.node_id,
            ct = require('../../utils/validate_leaf/count_templates.js');
          let dt = require('../../mongodb/template/delete_template.js');
          dt.delete_template(template_id, function (err, result) {
            if (err) {
              console_logger.error(err);
              hres._response({ msg_code: '006' }, 500, res);
            } else {
              if (result) {
                console_logger.info('delete template successfully');
                hres._response({ msg_code: '001' }, 200, res);
              } else {
                console_logger.info('delete template unsuccessfully');
                hres._response({ msg_code: '007' }, 500, res);
              }
            }
          });
          // ct.count_templates(node_id, function (err, result) {
          //   if (err) {
          //     console_logger.error(err);
          //     hres._response({ msg_code: '006' }, 500, res);
          //   } else {
          //     if (typeof result === 'number') {
          //       if (result == 1) {
          //         let cisl = require('../../utils/validate_leaf/check_if_special_leaf.js');
          //         cisl.check_if_special_leaf(node_id, function (err, result) {
          //           if (err) {
          //             console_logger.error(err);
          //             hres._response({ msg_code: '006' }, 500, res);
          //           } else {
          //             if (result == 'true') {
          //               console_logger.info('cannot delete the last special leaf template');
          //               hres._response({ msg_code: '002' }, 200, res);
          //             } else {
          //               let dt = require('../../mongodb/template/delete_template.js');
          //               dt.delete_template(template_id, function (err, result) {
          //                 if (err) {
          //                   console_logger.error(err);
          //                   hres._response({ msg_code: '006' }, 500, res);
          //                 } else {
          //                   if (result) {
          //                     console_logger.info('delete template successfully');
          //                     let dn = require('../../mongodb/node/delete_node.js');
          //                     dn.delete_node(node_id, function (err, result) {
          //                       if (err) {
          //                         console_logger.error(err);
          //                         hres._response({ msg_code: '006' }, 500, res);
          //                       } else {
          //                         if (result) {
          //                           console_logger.info('delete leaf successfully');
          //                           hres._response({ msg_code: '001' }, 200, res);
          //                         } else {
          //                           console_logger.info('delete leaf unsuccessfully');
          //                           hres._response({ msg_code: '007' }, 500, res);
          //                         }
          //                       }
          //                     });
          //                   } else {
          //                     console_logger.info('delete template unsuccessfully');
          //                     hres._response({ msg_code: '007' }, 500, res);
          //                   }
          //                 }
          //               });
          //             }
          //           }
          //         });
          //       }
          //       if (result >= 2) {
          //         let dt = require('../../mongodb/template/delete_template.js');
          //         dt.delete_template(template_id, function (err, result) {
          //           if (err) {
          //             console_logger.error(err);
          //             hres._response({ msg_code: '006' }, 500, res);
          //           } else {
          //             if (result) {
          //               console_logger.info('delete template successfully');
          //               hres._response({ msg_code: '001' }, 200, res);
          //             } else {
          //               console_logger.info('delete template unsuccessfully');
          //               hres._response({ msg_code: '007' }, 500, res);
          //             }
          //           }
          //         });
          //       }
          //     } else {
          //       console_logger.info('quantity of templates is not numberic');
          //       hres._response({ msg_code: '007' }, 500, res);
          //     }
          //   }
          // });
        } else {
          console_logger.info('check token failed');
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}
