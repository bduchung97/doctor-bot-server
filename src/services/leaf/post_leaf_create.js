let logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger();

exports.post_leaf_create = function post_leaf_create(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js');
  let data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let leaf_data = data.decrypted_data,
            function_id = leaf_data.function_id,
            parent_function_id = leaf_data.parent_function_id,
            value = leaf_data.value,
            created_by = result.ADMIN__ID,
            description = leaf_data.description,
            type = leaf_data.type,
            sol = require('../../mongodb/leaf/show_one_leaf');
          sol.show_one_leaf(
            function_id,
            function (err, result) {
              if (err) {
                console_logger.error(err);
                hres._response({ msg_code: '006' }, 500, res);
              } else {
                if (result) {
                  console_logger.info('leaf exists');
                  hres._response({ msg_code: '002', }, 200, res);
                } else {
                  let cl = require('../../mongodb/leaf/create_leaf.js');
                  cl.create_leaf(function_id, parent_function_id, value, created_by, description, type, function (err, result) {
                    if (err) {
                      console_logger.error(err);
                      hres._response({ msg_code: '006' }, 500, res);
                    } else {
                      if (result) {
                        console_logger.info('create leaf successfully');
                        hres._response({ msg_code: '001', }, 200, res);
                      } else {
                        console_logger.info('create leaf unsuccessfully');
                        hres._response({ msg_code: '007', }, 500, res);
                      }
                    }
                  });
                }
              }
            });
        } else {
          console_logger.info('check token failed');
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}
