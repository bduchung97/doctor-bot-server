let logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger();

exports.post_node_upload = function post_node_upload(req, res) {
  let
    file = req.files.file,
    file_name = req.body.file_name,
    session_token = req.body.session_token,
    admin_id = req.body.admin_id,
    infef = require('../../mongodb/node/import_nodes_from_excel_file.js'),
    hres = require('../../utils/handle_response/_response.js'), 
    soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    console_logger.info(file);
    console_logger.info(admin_id);
  soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
    if (err) {
      console_logger.error(err);
      hres._response({ msg_code: '006' }, 500, res);
    } else {
      if (result) {
        file.mv(`${__dirname}/uploads/${file_name}`, function (err) {
          if (err) {
            console_logger.error(err);
            hres._response({ msg_code: '006' }, 500, res);
          }
          else {

            infef.import_nodes_from_excel_file(`${__dirname}/uploads/${file_name}`, admin_id, function (err, result) {
              if (err) {
                console_logger.error(err);
                hres._response({ msg_code: '006' }, 500, res);
              } else {
                if (result) {
                  console_logger.info('import nodes from excel successfully');
                  hres._response({ msg_code: '001' }, 200, res);
                } else {
                  console_logger.info('import nodes from excel unsuccessfully');
                  hres._response({ msg_code: '007' }, 500, res);
                }
              }
            });
          }
        });
      } else {
        console_logger.info('check token failed');
        hres._response({ msg_code: '000' }, 200, res);
      }
    }
  });
}
