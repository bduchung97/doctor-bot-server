let logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger();

exports.put_node_update = function put_node_update(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js'),
    data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let req_data = data.decrypted_data,
            node_id = req_data.node_id,
            updated_data = req_data.updated_data,
            un = require('../../mongodb/node/update_node.js'),
            cisl = require('../../utils/validate_leaf/check_if_special_leaf.js');
          cisl.check_if_special_leaf(node_id, function (err, result) {
            if (err) {
              console_logger.error(err);
              hres._response({ msg_code: '006' }, 500, res);
            } else {
              if (result == 'true') {
                console_logger.info('cannot update special node');
                hres._response({ msg_code: '002' }, 200, res);
              }
              else {
                if (updated_data.FUNCTION_ID) {
                  let son = require('../../mongodb/node/show_one_node.js');
                  son.show_one_node(updated_data.FUNCTION_ID, function (err, result) {
                    if (err) {
                      console_logger.error(err);
                      hres._response({ msg_code: '006' }, 500, res);
                    } else {
                      if (result) {
                        console_logger.info('duplicate node');
                        hres._response({ msg_code: '002' }, 200, res);
                      } else {
                        un.update_node(node_id, updated_data,
                          function (err, result) {
                            if (err) {
                              console_logger.error(err);
                              hres._response({ msg_code: '006' }, 500, res);
                            } else {
                              if (result) {
                                console_logger.info('update node successfully');
                                hres._response({ msg_code: '001' }, 200, res);
                              } else {
                                console_logger.info('update node unsuccessfully');
                                hres._response({ msg_code: '007' }, 500, res);
                              }
                            }
                          });
                      }
                    }
                  });
                } else {
                  un.update_node(node_id, updated_data,
                    function (err, result) {
                      if (err) {
                        console_logger.error(err);
                        hres._response({ msg_code: '006' }, 500, res);
                      } else {
                        if (result) {
                          console_logger.info('update node successfully');
                          hres._response({ msg_code: '001' }, 200, res);
                        } else {
                          console_logger.info('update node unsuccessfully');
                          hres._response({ msg_code: '007' }, 500, res);
                        }
                      }
                    });
                }
              }
            }
          });
        } else {
          console_logger.info('check token failed');
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}
