let logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger();

exports.post_leaf_create = function post_leaf_create(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js');
  let data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let template_data = data.decrypted_data,
            node_ID = template_data.node_ID,
            value = template_data.value,
            created_by = template_data.admin_ID,
            type = template_data.type,
            ct = require('../../mongodb/template/create_template.js');
          ct.create_template(node_ID, value, created_by, type, function (err, result) {
            if (err) {
              console_logger.error(err);
              hres._response({ msg_code: '006' }, 500, res);
            } else {
              if (result) {
                console_logger.info('create template successfully');
                hres._response({ msg_code: '001' }, 200, res);
              } else {
                console_logger.info('create template unsuccessfully')
                hres._response({ msg_code: '007' }, 500, res);
              }
            }
          });
        } else {
          console_logger.info('check token failed');
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}
