exports.put_persistent_menu_update = function put_persistent_menu_update(req, res) {
  const PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN || 'EAAIMSYvpR4cBAB3SbJICX5ddNoRmQjedVnIdr4RMKEABnIY0TxHP9V2twrvIFsnscZCSh5T7iFEXup0RUBMPZCI9j4DFh5VdSq9R6NEvOM8RgusEbzshk1ZC4gUTjkorSBVrjT2ZAqYBTiNqN6JFTzXJ1zDAZAnW75VsVyZAOZAXgZDZD',
    request = require('request'),
    logger = require('../../utils/logger/logger'),
    console_logger = logger.getLogger(),
    hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js'),
    data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let req_data = data.decrypted_data,
            template_id = req_data.template_id,
            updated_data = req_data.updated_data,
            ut = require('../../mongodb/template/update_template');
          ut.update_template(template_id.toString(), updated_data,
            function (err, result) {
              if (err) {
                console_logger.error(err);
                hres._response({ msg_code: '006' }, 500, res);
              } else {
                if (result) {
                  console_logger.info('update template successfully');
                  let sot = require('../../mongodb/template/show_one_template');
                  sot.show_one_template(template_id, function (err, result) {
                    if (err) {
                      console_logger.error(err);
                      hres._response({ msg_code: '006' }, 500, res);
                    } else {
                      if (result) {
                        request({
                          url: 'https://graph.facebook.com/v2.6/me/messenger_profile?access_token=' + PAGE_ACCESS_TOKEN,
                          method: 'POST',
                          headers: { 'Content-Type': 'application/json' },
                          form: result.VALUE
                        }, (err, _res, body) => {
                          if (!err) {
                            let res1 = JSON.parse(body);
                            if (res1["result"] == 'success') {
                              console_logger.info('Update permission_menu successful!');
                              hres._response({ msg_code: '001' }, 200, res);
                            }
                            else {  
                              console_logger.info('Update permission_menu unsuccessful!');
                              hres._response({ msg_code: '002' }, 200, res);
                            }
                          } else {
                            console_logger.error(err);
                            hres._response({ msg_code: '006' }, 500, res);
                          }
                        });
                      } else {
                        console_logger.info('show one template unsuccessfully');
                        hres._response({ msg_code: '007' }, 500, res);
                      }
                    }
                  });
                } else {
                  console_logger.info('update template unsuccessfully');
                  hres._response({ msg_code: '007' }, 500, res);
                }
              }
            });
        } else {
          console_logger.info('check token failed');
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}
