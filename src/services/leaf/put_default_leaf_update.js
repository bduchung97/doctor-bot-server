let logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger();

exports.put_default_leaf_update = function put_default_leaf_update(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js');
  let data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let req_data = data.decrypted_data,
            template_id = req_data.template_id,
            updated_data = req_data.updated_data,
            admin_ID = result.ADMIN__ID;
          if (updated_data.VALUE.quick_replies.length > 0 && updated_data.VALUE.quick_replies.length < 12) {
            let ut = require('../../mongodb/template/update_template');
            ut.update_template(template_id, updated_data,
              function (err, result) {
                if (err) {
                  console_logger.error(err);
                  hres._response({ msg_code: '006' }, 500, res);
                } else {
                  if (result) {
                    console_logger.info('update default leaf successfully');
                    let dad = require('../../mongodb/quick_replies_ref/delete_all_documents.js');
                    dad.delete_all_documents(function (err, result) {
                      if (err) {
                        console_logger.error(err);
                        hres._response({ msg_code: '006' }, 500, res);
                      } else {
                        console_logger.info('delete all quick replies refs successfully');
                        if (result) {
                          let cqrr = require('../../mongodb/quick_replies_ref/create_quick_replies_ref.js');
                          cqrr.create_quick_replies_ref(admin_ID, function (err, result) {
                            if (err) {
                              console_logger.error(err);
                              hres._response({ msg_code: '006' }, 500, res);
                            } else {
                              if (result) {
                                console_logger.info('create quick replies refs successfully');
                                hres._response({ msg_code: '001' }, 200, res);
                              } else {
                                console_logger.info('create quick replies refs unsuccessfully');
                                hres._response({ msg_code: '007' }, 500, res);
                              }
                            }
                          });
                        } else {
                          console_logger.info('delete all quick replies refs unsuccessfully')
                          hres._response({ msg_code: '007' }, 500, res);
                        }
                      }
                    });
                  } else {
                    console_logger.info('update default template unsuccesfully');
                    hres._response({ msg_code: '007' }, 500, res);
                  }
                }
              });
          } else {
            console_logger.info('quick replies exceeds over 11');
            hres._response({ msg_code: '002' }, 200, res);
          }
        } else {
          console_logger.info('check token failed');
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}
