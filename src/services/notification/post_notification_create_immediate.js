let logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger(),
  st = require('../../mongodb/notification/send_template.js');

exports.post_notification_create_immediate = function post_notification_create_immediate(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js'),
    data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let req_data = data.decrypted_data,
            title = req_data.title,
            template = req_data.template,
            created_by = req_data.created_by,
            target = req_data.target,
            son = require('../../mongodb/notification/show_one_notification.js')
          cn = require('../../mongodb/notification/create_notification.js');
          son.show_one_notification({ TITLE: title }, {}, function (err, result) {
            if (err) {
              console_logger.error(err);
              hres._response({ msg_code: '006' }, 500, res);
            } else {
              if (result) {
                console_logger.info('cannot create duplicate notification');
                hres._response({ msg_code: '002' }, 200, res);
              } else {
                cn.create_notification(title, template, created_by, false, '00', '00', '00', '*', '*', '*', target, 'DEFAULT',
                  function (err, result) {
                    if (err) {
                      console_logger.error(err);
                      hres._response({ msg_code: '006' }, 500, res);
                    } else {
                      if (result) {
                        console_logger.info('create notification successfully');
                        hres._response({ msg_code: '001' }, 200, res);
                        st.send_template(target, template);
                      } else {
                        console_logger.info('create notification unsuccessfully');
                        hres._response({ msg_code: '007' }, 500, res);
                      }
                    }
                  });
              }
            }
          });

        } else {
          console_logger.info('check token failed');
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}
