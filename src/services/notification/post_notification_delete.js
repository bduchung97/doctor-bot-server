let logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger();

exports.post_delete_notification = function post_delete_notification(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js'),
    data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let req_data = data.decrypted_data,
            notification_ID = req_data.notification_ID,
            notification_status = req_data.notification_status,
            notification_type = req_data.notification_type,
            dn = require('../../mongodb/notification/delete_notification.js');
          if (notification_type == 'SPECIAL') {
            console_logger.info('cannot delete special notification');
            hres._response({ msg_code: '002' }, 200, res);
          } else {
            if (notification_status == false) {
              dn.delete_notification(notification_ID,
                function (err, result) {
                  if (err) {
                    console_logger.error(err);
                    hres._response({ msg_code: '006' }, 500, res);
                  } else {
                    if (result) {
                      console_logger.info('delete notification successfully');
                      hres._response({ msg_code: '001' }, 200, res);
                    } else {
                      console_logger.info('create notification unsuccessfully');
                      hres._response({ msg_code: '007' }, 500, res);
                    }
                  }
                });
            } else {
              console_logger.info('stop and delete cron job');
              let cron_ID = notification_ID.toString(),
                cron = require('../../utils/cron_manager/cron_manager.js');
              cron.stop(cron_ID);
              cron.delete(cron_ID);
              console_logger.info(cron.findAll());
              dn.delete_notification(notification_ID,
                function (err, result) {
                  if (err) {
                    console_logger.error(err);
                    hres._response({ msg_code: '006' }, 500, res);
                  } else {
                    if (result) {
                      console_logger.info('delete notification successfully');
                      hres._response({ msg_code: '001' }, 200, res);
                    } else {
                      console_logger.info('create notification unsuccessfully');
                      hres._response({ msg_code: '007' }, 500, res);
                    }
                  }
                });
            }
          }

        } else {
          console_logger.info('check token failed');
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}
