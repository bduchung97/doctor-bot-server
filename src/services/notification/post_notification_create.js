let logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger(),
  st = require('../../mongodb/notification/send_template.js');

exports.post_create_notification = function post_create_notification(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js'),
    data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let req_data = data.decrypted_data,
            title = req_data.title,
            templates = req_data.templates,
            created_by = req_data.created_by,
            status = req_data.status,
            schedule_second = req_data.schedule_second,
            schedule_minute = req_data.schedule_minute,
            schedule_hour = req_data.schedule_hour,
            schedule_day_of_month = req_data.schedule_day_of_month,
            schedule_month = req_data.schedule_month,
            schedule_day_of_week = req_data.schedule_day_of_week,
            target = req_data.target,
            sn = require('../../mongodb/notification/show_one_notification.js');
          sn.show_one_notification({ TITLE: title }, {}, function (err, result) {
            if (err) {
              console_logger.error(err);
              hres._response({ msg_code: '006' }, 500, res);
            } else {
              if (result) {
                console_logger.info('cannot create duplicate notification');
                hres._response({ msg_code: '002' }, 200, res);
              } else {
                let cn = require('../../mongodb/notification/create_notification.js');
                cn.create_notification(title, templates, created_by, status, schedule_second, schedule_minute, schedule_hour,
                  schedule_day_of_month, schedule_month, schedule_day_of_week, target, 'DEFAULT',
                  function (err, result) {
                    if (err) {
                      console_logger.error(err);
                      hres._response({ msg_code: '006' }, 500, res);
                    } else {
                      if (result) {
                        console_logger.info('create notification successfully');
                        hres._response({ msg_code: '001' }, 200, res);
                        console_logger.info('start creating cron job');
                        if (status == true) {
                          let cron_ID = result.toString(),
                            cron_time = schedule_second + ' ' + schedule_minute + ' ' + schedule_hour + ' ' + schedule_day_of_month + ' ' + schedule_month + ' ' + schedule_day_of_week,
                            cron = require('../../utils/cron_manager/cron_manager.js');
                          cron.add(cron_ID, cron_time, st, target, templates);
                          cron.start(cron_ID);
                          console_logger.info(cron.findAll());
                        }
                      } else {
                        console_logger.info('create notification unsuccessfully');
                        hres._response({ msg_code: '007' }, 500, res);
                      }
                    }
                  });
              }
            }
          });
        } else {
          console_logger.info('check token failed');
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}
