let logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger(),
  st = require('../../mongodb/notification/send_template.js'),
  ObjectId = require('mongodb').ObjectID;

exports.post_notification_update = function post_notification_update
  (req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js'),
    data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let req_data = data.decrypted_data,
            notification_ID = req_data.notification_ID,
            updated_data = req_data.updated_data,
            un = require('../../mongodb/notification/update_notification.js');
          un.update_notification(notification_ID, updated_data,
            function (err, result) {
              if (err) {
                console_logger.error(err);
                hres._response({ msg_code: '006' }, 500, res);
              } else {
                if (result) {
                  console_logger.info('update notification successfully');
                  hres._response({ msg_code: '001' }, 200, res);
                  let son = require('../../mongodb/notification/show_one_notification.js');
                  son.show_one_notification({ _id: ObjectId(notification_ID) }, {}, function (err, result) {
                    if (err) {
                      console_logger.error(err);
                      hres._response({ msg_code: '006' }, 500, res);
                    } else {
                      if (result) {
                        let status = result.STATUS,
                          templates = result.TEMPLATES,
                          title = result.TITLE,
                          target = result.TARGET,
                          schedule_second = result.SCHEDULE_SECOND,
                          schedule_minute = result.SCHEDULE_MINUTE,
                          schedule_hour = result.SCHEDULE_HOUR,
                          schedule_month = result.SCHEDULE_MONTH,
                          schedule_day_of_month = result.SCHEDULE_DAY_OF_MONTH,
                          schedule_day_of_week = result.SCHEDULE_DAY_OF_WEEK,
                          cron_ID = notification_ID.toString(),
                          cron_time = schedule_second + ' ' + schedule_minute + ' ' + schedule_hour + ' ' + schedule_day_of_month + ' ' + schedule_month + ' ' + schedule_day_of_week,
                          cron = require('../../utils/cron_manager/cron_manager.js');
                        if (status == true) {
                          if (title == 'Thời tiết') {
                            let hwp = require('../../utils/weather/handle_weather_payload.js');
                            hwp.handle_weather_payload(function (err, result) {
                              if (err) {
                                console_logger.error(err);
                                hres._response({ msg_code: '006' }, 500, res);
                              } else {
                                if (result) {
                                  let weather = result,
                                    template = [];
                                  template.push({ VALUE: weather });
                                  cron.delete(cron_ID);
                                  cron.add(cron_ID, cron_time, st, ['*'], template);
                                  cron.start(cron_ID);
                                  console_logger.info(cron.findAll());
                                }
                              }
                            });
                          }
                          else {
                            cron.delete(cron_ID);
                            cron.add(cron_ID.toString(), cron_time, st, target, templates);
                            cron.start(cron_ID.toString());
                            console_logger.info(cron.findAll());
                          }
                        } else {
                          cron.stop(cron_ID);
                          cron.delete(cron_ID);
                          console_logger.info(cron.findAll());
                        }
                      } else {
                        console_logger.info('notification not found ??');
                        hres._response({ msg_code: '007' }, 500, res);
                      }
                    }
                  });
                } else {
                  console_logger.info('update notification unsuccessfully');
                  hres._response({ msg_code: '007' }, 500, res);
                }
              }
            });
        } else {
          console_logger.info('check token failed');
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}

