let logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger();
exports.post_admin_create = function post_admin_create(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js');
  let data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let admin_info = data.decrypted_data,
            fullname = admin_info.fullname,
            username = admin_info.username,
            password = admin_info.password,
            email = admin_info.email,
            phone_number = admin_info.phone_number,
            permission = admin_info.permission,
            admin_id = admin_info.admin_id,
            soabu = require('../../mongodb/admin/show_one_admin_by_username.js');
          soabu.show_one_admin_by_username(username, function (err, result) {
            if (err) {
              console_logger.error(err);
              hres._response({ msg_code: '006' }, 500, res);
            } else {
              if (result) {
                console_logger.info('create admin unsuccessfully because of duplicate');
                hres._response({ msg_code: '002' }, 200, res);
              } else {
                let ca = require('../../mongodb/admin/create_admin.js');
                ca.create_admin(
                  fullname,
                  username,
                  password,
                  email,
                  phone_number,
                  permission,
                  admin_id,
                  function (err, result) {
                    if (err) {
                      console_logger.error(err);
                      hres._response({ msg_code: '006' }, 500, res);
                    } else {
                      if (result) {
                        console_logger.info('create admin successfully');
                        hres._response({ msg_code: '001' }, 200, res);
                      } else {
                        console_logger.info('create admin unsuccessfully  ');
                        hres._response({ msg_code: '007' }, 500, res);
                      }
                    }
                  });
              }
            }
          });
        } else {
          console_logger.info('check token failed');
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}
