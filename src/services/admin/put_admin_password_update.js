const logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger();
exports.put_admin_password_update = function put_admin_password_update(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js');
  let data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let req_data = data.decrypted_data,
            admin_ID = req_data.admin_ID,
            old_password = req_data.old_password,
            new_password = req_data.new_password,
            soa = require('../../mongodb/admin/show_one_admin.js');
          soa.show_one_admin(admin_ID, function (err, result) {
            if (err) {
              console_logger.error(err);
              hres._response({ msg_code: '006' }, 500, res);
            }
            else {
              if (result) {
                let password = result.PASSWORD,
                  cp = require('../../utils/crypt/compare_password.js'),
                  hp = require('../../utils/crypt/hash_password.js');
                if (cp.compare_password(old_password, password) == true) {
                  let ua = require('../../mongodb/admin/update_admin.js');
                  ua.update_admin(admin_ID, { PASSWORD: hp.hash_password(new_password) },
                    function (err, result) {
                      if (err) {
                        console_logger.error(err);
                        hres._response({ msg_code: '006' }, 500, res);
                      } else {
                        if (result) {
                          console_logger.info('Update password successfully');
                          hres._response({ msg_code: '001' }, 200, res);
                        } else {
                          console_logger.info('Update password unsuccessfully  ');
                          hres._response({ msg_code: '007' }, 500, res);
                        }
                      }
                    });
                }
                else {
                  console_logger.info('password invalid');
                  hres._response({ msg_code: '002' }, 200, res);
                }
              } else {
                console_logger.info("no admin exist");
                hres._response({ msg_code: '002' }, 200, res);
              }
            }
          })
        } else {
          console_logger.info("check token failed");
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info("invalid data");
    hres._response({ msg_code: '004' }, 400, res);
  }
}
