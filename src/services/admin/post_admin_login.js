let logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger();
exports.post_admin_login = function post_admin_login(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js');
  let data = hreq.handle_request(req);
  if (data) {
    let username = data.decrypted_data.username,
      password = data.decrypted_data.password,
      soabu = require('../../mongodb/admin/show_one_admin_by_username.js');
    soabu.show_one_admin_by_username(username, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('valid username');
          var cp = require('../../utils/crypt/compare_password.js'),
            admin_data = result,
            admin_id = admin_data._id;
          if (cp.compare_password(password, admin_data.PASSWORD) == true) {
            let soasbai = require('../../mongodb/admin_session/show_one_admin_session_by_admin_id.js');
            soasbai.show_one_admin_session_by_admin_id(admin_id, function (err, result) {
              if (err) {
                console_logger.error(err);
                hres._response({ msg_code: '006' }, 500, res);
              } else {
                if (result) {
                  console_logger.info('admin is on session');
                  let das = require('../../mongodb/admin_session/delete_admin_session.js');
                  das.delete_admin_session(result._id, function (err, result) {
                    if (err) {
                      console_logger.error(err);
                      hres._response({ msg_code: '006' }, 500, res);
                    } else {
                      if (result) {
                        console_logger.info('delete session successfully');
                        let cas = require('../../mongodb/admin_session/create_admin_session.js');
                        cas.create_admin_session(admin_id, function (err, result) {
                          if (err) {
                            console_logger.error(err);
                            hres._response({ msg_code: '006' }, 500, res);
                          } else {
                            console_logger.info('create session successfully');
                            if (result) {
                              var admin_session_id = result;
                              let soas = require('../../mongodb/admin_session/show_one_admin_session');
                              soas.show_one_admin_session(admin_session_id, function (err, result) {
                                if (err) {
                                  hres._response({ msg_code: '006' }, 500, res);
                                }
                                else {
                                  if (result) {
                                    console_logger.info('login successfully');
                                    hres._response({ msg_code: '001', data: { admin_data: admin_data, session_token: result.TOKEN } }, 200, res);
                                  } else {
                                    console_logger.info('login failed  ');
                                    hres._response({ msg_code: '007' }, 500, res);
                                  }
                                }
                              });
                            } else {
                              console_logger.info('create session unsuccessfully  ');
                              hres._response({ msg_code: '007' }, 500, res);
                            }
                          }
                        });
                        let cash = require('../../mongodb/admin_session_history/create_admin_session_history.js');
                        cash.create_admin_session_history(admin_id, function (err, result) {
                          if (err) {
                            console_logger.error(err);
                          } else {
                            if(result){
                              console_logger.info('create login history successfully');
                            }
                          }
                        });
                      } else {
                        console_logger.info('delete session unsuccessfully  ');
                        hres._response({ msg_code: '007' }, 500, res);
                      }
                    }
                  });
                } else {
                  let cas = require('../../mongodb/admin_session/create_admin_session.js');
                  cas.create_admin_session(admin_id, function (err, result) {
                    if (err) {
                      console_logger.error(err);
                      hres._response({ msg_code: '006' }, 500, res);
                    } else {
                      if (result) {
                        var admin_session_id = result;
                        let soas = require('../../mongodb/admin_session/show_one_admin_session');
                        soas.show_one_admin_session(admin_session_id, function (err, result) {
                          if (err) {
                            console_logger.error(err);
                            hres._response({ msg_code: '006' }, 500, res);
                          }
                          else {
                            if (result) {
                              console_logger.info('login successfully');
                              hres._response({ msg_code: '001', data: { admin_data: admin_data, session_token: result.TOKEN } }, 200, res);
                            } else {
                              console_logger.info('login unsuccessfully  ');
                              hres._response({ msg_code: '007' }, 500, res);
                            }
                          }
                        });
                      } else {
                        console_logger.info('create session unsuccessfully  ');
                        hres._response({ msg_code: '007' }, 500, res);
                      }
                    }
                  });
                  let cash = require('../../mongodb/admin_session_history/create_admin_session_history.js');
                  cash.create_admin_session_history(admin_id, function (err, result) {
                    if (err) {
                      console_logger.error(err);
                    } else {
                      if(result){
                        console_logger.info('create login history successfully');
                      }
                    }
                  });
                }
              }
            });
          } else {
            console_logger.info('wrong password');
            hres._response({ msg_code: '002' }, 200, res);
          }
        } else {
          console_logger.info('invalid username');
          hres._response({ msg_code: '002' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}
