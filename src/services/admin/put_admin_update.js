const logger = require('../../utils/logger/logger.js'),
  console_logger = logger.getLogger();
exports.put_admin_update = function put_admin_update(req, res) {
  let hreq = require('../../utils/handle_request/handle_request.js'),
    hres = require('../../utils/handle_response/_response.js');
  let data = hreq.handle_request(req);
  if (data) {
    console_logger.info('valid data');
    let session_token = data.session_token,
      soasbt = require('../../mongodb/admin_session/show_one_admin_session_by_token.js');
    soasbt.show_one_admin_session_by_token(session_token, function (err, result) {
      if (err) {
        console_logger.error(err);
        hres._response({ msg_code: '006' }, 500, res);
      } else {
        if (result) {
          console_logger.info('check token successfully');
          let req_data = data.decrypted_data,
            admin_ID = req_data.admin_ID,
            updated_data = req_data.updated_data,
            ua = require('../../mongodb/admin/update_admin.js');
          if (updated_data.PASSWORD == null) {
            delete updated_data.PASSWORD
          } else {
            let hp = require('../../utils/crypt/hash_password');
            updated_data.PASSWORD = hp.hash_password(updated_data.PASSWORD)
          }
          ua.update_admin(admin_ID, updated_data,
            function (err, result) {
              if (err) {
                console_logger.error(err);
                hres._response({ msg_code: '006' }, 500, res);
              } else {
                if (result) {
                  console_logger.info('update admin successfully');
                  hres._response({ msg_code: '001' }, 200, res);
                } else {
                  console_logger.info('update admin unsuccessfully  ');
                  hres._response({ msg_code: '007' }, 500, res);
                }
              }
            });
        } else {
          console_logger.info('check token failed');
          hres._response({ msg_code: '000' }, 200, res);
        }
      }
    });
  } else {
    console_logger.info('invalid data');
    hres._response({ msg_code: '004' }, 400, res);
  }
}
