const
  express = require('express'),
  bodyParser = require('body-parser'),
  fileUpload = require('express-fileupload'),
  app = express().use(bodyParser.json()),
  start_db = require('./src/deploy/start.js'),
  logger = require('./src/utils/logger/logger.js'),
  console_logger = logger.getLogger(),
  db = require('./src/mongodb/connect_to_DB');


app.use(function (_req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods',
    'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers',
    'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

app.use(fileUpload());
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({
  extended: true
})); // support encoded bodies

// Sets server port and logs message on successful
db.connect(() => {
  // start_db.start();
  app.listen(process.env.PORT || 1337, () => console_logger.info('webhook is listening'));
});

// Facebook
// Creates the endpoint for our webhook
app.post('/webhook', (req, res) => {
  console_logger.info('/webhook');
  let pw = require('./src/api/api_facebook/post_webhook.js');
  pw.post_webhook(req, res);
});

// Adds support for GET requests to our webhook
app.get('/webhook', (req, res) => {
  console_logger.info('/webhook');
  let gw = require('./src/api/api_facebook/get_webhook.js');
  gw.get_webhook(req, res);
});

// Admin

app.post('/post_admin_login', (req, res) => {
  console_logger.info('POST /post_admin_login');
  let pal = require('./src/services/admin/post_admin_login.js');
  pal.post_admin_login(req, res);
});

app.post('/post_admin_logout', (req, res) => {
  console_logger.info('POST /post_admin_logout');
  let pal = require('./src/services/admin/post_admin_logout.js');
  pal.post_admin_logout(req, res);
});

app.post('/post_admin_create', (req, res) => {
  console_logger.info('POST /post_admin_create');
  let pac = require('./src/services/admin/post_admin_create.js');
  pac.post_admin_create(req, res);
});

app.post('/post_admin_show_all', (req, res) => {
  console_logger.info('POST /post_admin_show_all');
  let pasa = require('./src/services/admin/post_admin_show_all.js');
  pasa.post_admin_show_all(req, res);
});

app.post('/post_admin_show_one', (req, res) => {
  console_logger.info('POST /post_admin_show_one');
  let paso = require('./src/services/admin/post_admin_show_one.js');
  paso.post_admin_show_one(req, res);
});

app.post('/post_admin_delete', (req, res) => {
  console_logger.info('POST /post_admin_delete');
  let pad = require('./src/services/admin/post_admin_delete.js');
  pad.post_admin_delete(req, res);
});

app.put('/put_admin_update', (req, res) => {
  console_logger.info('PUT /put_admin_update');
  let pau = require('./src/services/admin/put_admin_update.js');
  pau.put_admin_update(req, res);
});

app.put('/put_admin_password_update', (req, res) => {
  console_logger.info('PUT /put_admin_password_update');
  let papu = require('./src/services/admin/put_admin_password_update.js');
  papu.put_admin_password_update(req, res);
});

// Handbook

app.get('/get_handbook_show', (req, res) => {
  console_logger.info('GETT /get_handbook_show');
  let ghs = require('./src/services/handbook/get_handbook_show.js');
  ghs.get_handbook_show(req, res);
});

app.post('/post_handbook_create', (req, res) => {
  console_logger.info('POST /post_handbook_create');
  let phc = require('./src/services/handbook/post_handbook_create');
  phc.post_handbook_create(req, res);
});

app.post('/post_handbook_show_all', (req, res) => {
  console_logger.info('POST /post_handbook_show_all');
  let phsa = require('./src/services/handbook/post_handbook_show_all');
  phsa.post_handbook_show_all(req, res);
});

app.put('/put_handbook_update', (req, res) => {
  console_logger.info('PUT /put_handbook_update');
  let phu = require('./src/services/handbook/put_handbook_update');
  phu.put_handbook_update(req, res);
});

app.post('/post_handbook_delete', (req, res) => {
  console_logger.info('POST /post_handbook_delete');
  let phd = require('./src/services/handbook/post_handbook_delete');
  phd.post_handbook_delete(req, res);
});

app.post('/post_handbook_upload', (req, res) => {
  console_logger.info('POST /post_handbook_upload');
  let phu = require('./src/services/handbook/post_handbook_upload');
  phu.post_handbook_upload(req, res);
});

// AI Trainning

app.post('/post_ai_trainning_create', (req, res) => {
  console_logger.info('POST /post_ai_trainning_create');
  let patc = require('./src/services/ai_trainning/post_ai_trainning_create');
  patc.post_ai_trainning_create(req, res);
});

app.post('/post_ai_trainning_show_all', (req, res) => {
  console_logger.info('POST /post_ai_trainning_show_all');
  let phata = require('./src/services/ai_trainning/post_ai_trainning_show_all');
  phata.post_ai_trainning_show_all(req, res);
});

app.put('/put_ai_trainning_update', (req, res) => {
  console_logger.info('PUT /put_ai_trainning_update');
  let patu = require('./src/services/ai_trainning/put_ai_trainning_update');
  patu.put_ai_trainning_update(req, res);
});

app.post('/post_ai_trainning_delete', (req, res) => {
  console_logger.info('POST /post_ai_trainning_delete');
  let patd = require('./src/services/ai_trainning/post_ai_trainning_delete');
  patd.post_ai_trainning_delete(req, res);
});

app.post('/post_ai_trainning_upload', (req, res) => {
  console_logger.info('POST /post_ai_trainning_upload');
  let patu = require('./src/services/ai_trainning/post_ai_trainning_upload');
  patu.post_handbook_upload(req, res);
});

app.put('/put_ai_training_approve', (req, res) => {
  console_logger.info('PUT /put_ai_training_approve');
  let pata = require('./src/services/ai_trainning/put_ai_training_approve.js');
  pata.put_ai_training_approve(req, res);
});

app.put('/put_ai_training_approve_all', (req, res) => {
  console_logger.info('PUT /put_ai_training_approve_all');
  let pataa = require('./src/services/ai_trainning/put_ai_training_approve_all.js');
  pataa.put_ai_training_approve_all(req, res);
});

app.post('/post_ai_training_not_approve_show_all', (req, res) => {
  console_logger.info('POST /post_ai_training_not_approve_show_all');
  let patnasa = require('./src/services/ai_trainning/post_ai_training_not_approve_show_all.js');
  patnasa.post_ai_training_not_approve_show_all(req, res);
});

// User

app.post('/post_user_show_all', (req, res) => {
  console_logger.info('POST /post_user_show_all');
  let pusa = require('./src/services/user/post_user_show_all');
  pusa.post_user_show_all(req, res);
});

app.post('/post_user_show_all_for_create_group', (req, res) => {
  console_logger.info('POST /post_user_show_all_for_create_group');
  let pusafcg = require('./src/services/user/post_user_show_all_for_create_group');
  pusafcg.post_user_show_all_for_create_group(req, res);
});

app.put('/put_user_update', (req, res) => {
  console_logger.info('PUT /put_user_update');
  let puu = require('./src/services/user/put_user_update');
  puu.put_user_update(req, res);
});

app.post('/post_user_show_all_', (req, res) => {
  console_logger.info('POST /post_user_show_all_');
  let pusa_ = require('./src/services/user/post_user_show_all_');
  pusa_.post_user_show_all_(req, res);
});

app.post('/post_user_authenticate', (req, res) => {
  console_logger.info('POST /post_user_authenticate');
  let pua = require('./src/services/user/post_user_authenticate');
  pua.post_user_authenticate(req, res);
});

// Group

app.post('/post_group_show_all', (req, res) => {
  console_logger.info('POST /post_group_show_all');
  let pgsa = require('./src/services/group/post_group_show_all');
  pgsa.post_group_show_all(req, res);
});

app.put('/put_group_update', (req, res) => {
  console_logger.info('PUT /put_group_update');
  let pgu = require('./src/services/group/put_group_update');
  pgu.put_group_update(req, res);
});

app.post('/post_group_delete', (req, res) => {
  console_logger.info('POST /post_group_delete');
  let pgd = require('./src/services/group/post_group_delete.js');
  pgd.post_group_delete(req, res);
});

app.post('/post_group_delete_member', (req, res) => {
  console_logger.info('POST /post_group_delete_member');
  let pgdm = require('./src/services/group/post_group_delete_member.js');
  pgdm.post_group_delete_member(req, res);
});

app.post('/post_group_delete_multiple_members', (req, res) => {
  console_logger.info('POST /post_group_delete_multiple_members');
  let pgdmm = require('./src/services/group/post_group_delete_multiple_members.js');
  pgdmm.post_group_delete_multiple_members(req, res);
});

app.post('/post_group_add_multiple_members', (req, res) => {
  console_logger.info('POST /post_group_add_multiple_members');
  let pgamm = require('./src/services/group/post_group_add_multiple_members.js');
  pgamm.post_group_add_multiple_members(req, res);
});

app.post('/post_group_add_member', (req, res) => {
  console_logger.info('POST /post_group_add_member');
  let pgam = require('./src/services/group/post_group_add_member.js');
  pgam.post_group_add_member(req, res);
});

app.post('/post_group_create', (req, res) => {
  console_logger.info('POST /post_group_create');
  let pgc = require('./src/services/group/post_group_create.js');
  pgc.post_group_create(req, res);
});

app.post('/post_group_show_all_', (req, res) => {
  console_logger.info('POST /post_group_show_all_');
  let pgsa = require('./src/services/group/post_group_show_all_.js');
  pgsa.post_group_show_all_(req, res);
});

// Leaf

app.post('/post_leaf_show_all', (req, res) => {
  console_logger.info('POST /post_leaf_show_all');
  let plsa = require('./src/services/leaf/post_leaf_show_all.js');
  plsa.post_leaf_show_all(req, res);
});

app.post('/post_node_upload', (req, res) => {
  console_logger.info('POST /post_node_upload');
  let pnu = require('./src/services/leaf/post_node_upload.js');
  pnu.post_node_upload(req, res);
});

app.post('/post_leaf_show', (req, res) => {
  console_logger.info('POST /post_leaf_show');
  let pls = require('./src/services/leaf/post_leaf_show');
  pls.post_leaf_show(req, res);
});

app.post('/post_node_show_all', (req, res) => {
  console_logger.info('POST /post_node_show_all');
  let pnsa = require('./src/services/leaf/post_node_show_all');
  pnsa.post_node_show_all(req, res);
});

app.post('/post_node_show_all_', (req, res) => {
  console_logger.info('POST /post_node_show_all_');
  let pnsa = require('./src/services/leaf/post_node_show_all_');
  pnsa.post_node_show_all_(req, res);
});

app.put('/put_template_update', (req, res) => {
  console_logger.info('PUT /put_template_update');
  let ptu = require('./src/services/leaf/put_template_update');
  ptu.put_template_update(req, res);
});

app.put('/put_node_update', (req, res) => {
  console_logger.info('PUT /put_node_update');
  let pnu = require('./src/services/leaf/put_node_update.js');
  pnu.put_node_update(req, res);
});

app.post('/post_leaf_create', (req, res) => {
  console_logger.info('POST /post_leaf_create');
  let plc = require('./src/services/leaf/post_leaf_create.js');
  plc.post_leaf_create(req, res);
});

app.post('/post_template_create', (req, res) => {
  console_logger.info('POST /post_template_create');
  let ptc = require('./src/services/leaf/post_template_create.js');
  ptc.post_leaf_create(req, res);
});

app.put('/put_persistent_menu_update', (req, res) => {
  console_logger.info('PUT /put_persistent_menu_update');
  let ppmu = require('./src/services/leaf/put_persistent_menu_update');
  ppmu.put_persistent_menu_update(req, res);
});

app.post('/post_leaf_delete', (req, res) => {
  console_logger.info('POST /post_leaf_delete');
  let pld = require('./src/services/leaf/post_leaf_delete.js');
  pld.post_leaf_delete(req, res);
});

app.post('/post_template_delete', (req, res) => {
  console_logger.info('POST /post_template_delete');
  let ptd = require('./src/services/leaf/post_template_delete.js');
  ptd.post_template_delete(req, res);
});

app.put('/put_default_leaf_update', (req, res) => {
  console_logger.info('PUT /put_default_leaf_update');
  let pdlu = require('./src/services/leaf/put_default_leaf_update.js');
  pdlu.put_default_leaf_update(req, res);
});


//notification

app.post('/post_notification_show_all', (req, res) => {
  console_logger.info('POST /post_notification_show_all');
  let pnsa = require('./src/services/notification/post_notification_show_all.js');
  pnsa.post_notification_show_all(req, res);
});

app.post('/post_notification_create', (req, res) => {
  console_logger.info('POST /post_notification_create');
  let pnc = require('./src/services/notification/post_notification_create');
  pnc.post_create_notification(req, res);
});

app.post('/post_notification_push_immediate', (req, res) => {
  console_logger.info('POST /post_notification_push_immediate');
  let pnpi = require('./src/services/notification/post_notification_push_immediate');
  pnpi.post_notification_push_immediate(req, res);
});

app.post('/post_notification_create_immediate', (req, res) => {
  console_logger.info('POST /post_notification_create_immediate');
  let pnci = require('./src/services/notification/post_notification_create_immediate.js');
  pnci.post_notification_create_immediate(req, res);
});

app.post('/post_notification_delete', (req, res) => {
  console_logger.info('POST /post_notification_delete');
  let pnd = require('./src/services/notification/post_notification_delete.js');
  pnd.post_delete_notification(req, res);
});


app.post('/post_notification_show', (req, res) => {
  console_logger.info('POST /post_notification_show');
  let pns = require('./src/services/notification/post_notification_show.js');
  pns.post_notification_show(req, res);
});

app.post('/post_notification_update', (req, res) => {
  console_logger.info('POST /post_notification_update');
  let pnu = require('./src/services/notification/post_notification_update.js');
  pnu.post_notification_update(req, res);
});

//statistics
app.post('/post_statistics_show_all', (req, res) => {
  console_logger.info('POST /post_statistics_show_all');
  let pssa = require('./src/services/statistics/post_statistics_show_all.js');
  pssa.post_statistics_show_all(req, res);
});

app.post('/post_statistics_show_one_user', (req, res) => {
  console_logger.info('POST /post_statistics_show_one_user');
  let pssou = require('./src/services/statistics/post_statistics_show_one_user.js');
  pssou.post_statistics_show_one_user(req, res);
});

// scheduler
app.post('/post_train_AI_scheduler_show', (req, res) => {
  console_logger.info('POST /post_train_AI_scheduler_show');
  let ptass = require('./src/services/scheduler/post_train_AI_scheduler_show');
  ptass.post_train_AI_scheduler_show(req, res);
});

app.put('/put_train_AI_scheduler_update', (req, res) => {
  console_logger.info('PUT /put_train_AI_scheduler_update');
  let ptasu = require('./src/services/scheduler/put_train_AI_scheduler_update.js');
  ptasu.put_train_AI_scheduler_update(req, res);
});

//diagnose
app.post('/post_diagnose', (req, res) => {
  console_logger.info('POST /post_diagnose');
  let pd = require('./src/services/diagnose/post_diagnose.js');
  pd.post_diagnose(req, res);
});