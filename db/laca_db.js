/*
 Navicat Premium Data Transfer

 Source Server         : msb_legal_chatbot
 Source Server Type    : MongoDB
 Source Server Version : 30609
 Source Host           : ds235197.mlab.com:35197
 Source Schema         : msb_legal_chatbot

 Target Server Type    : MongoDB
 Target Server Version : 30609
 File Encoding         : 65001

 Date: 24/07/2019 15:02:49
*/


// ----------------------------
// Collection structure for MCL_ADMIN
// ----------------------------
db.getCollection("MCL_ADMIN").drop();
db.createCollection("MCL_ADMIN");

// ----------------------------
// Documents of MCL_ADMIN
// ----------------------------
db.getCollection("MCL_ADMIN").insert([ {
    _id: ObjectId("5d2c5d9c72e61100084c0a3a"),
    "FULL_NAME": "root",
    "CREATED_AT": ISODate("2019-07-15T11:03:55.58Z"),
    "IS_ACTIVE": NumberInt("1"),
    USERNAME: "root",
    PASSWORD: "$2a$10$bU7hM5NXvxgCFIPEW1SOQeZpd1zL0lkdtr06fKKefHtzhaL5K9YSy",
    "PHONE_NUMBER": null,
    EMAIL: null,
    PERMISSION: null,
    "UPDATED_AT": null,
    "UPDATED_BY": null,
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a39")
} ]);
db.getCollection("MCL_ADMIN").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a7f"),
    "FULL_NAME": "Nguyễn Quang Anh",
    "CREATED_AT": ISODate("2019-07-15T11:03:58.296Z"),
    "IS_ACTIVE": NumberInt("1"),
    USERNAME: "quanh_nguyen",
    PASSWORD: "$2a$10$sUTBpuTWTGDfU/a0QR6PEOytaWfvT2vWNIa3QrrGUwxEl/hfK/8c6",
    "PHONE_NUMBER": "0915781197",
    EMAIL: "quanh_nguyen@outlook.com",
    PERMISSION: [
        "ADMN",
        "USER",
        "GROU",
        "DATA",
        "AITR",
        "HAND",
        "NOTI",
        "SCHE",
        "STAT"
    ],
    "UPDATED_AT": null,
    "UPDATED_BY": null,
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a")
} ]);
db.getCollection("MCL_ADMIN").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a84"),
    "FULL_NAME": "Trần Mai Thuỳ Linh",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.009Z"),
    "IS_ACTIVE": NumberInt("1"),
    USERNAME: "admin_linh",
    PASSWORD: "$2a$10$ekhbWwSHtmOkwV/gGdphi.iObD2k.uWziOiV5g7ptkr8MErkonr/y",
    "PHONE_NUMBER": "",
    EMAIL: "",
    PERMISSION: [
        "ADMN",
        "USER",
        "GROU",
        "DATA",
        "AITR",
        "HAND",
        "NOTI",
        "SCHE",
        "STAT"
    ],
    "UPDATED_AT": null,
    "UPDATED_BY": null,
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a")
} ]);
db.getCollection("MCL_ADMIN").insert([ {
    _id: ObjectId("5d3809b51ca9ba00172b6250"),
    "FULL_NAME": "Nguyễn Thị Lương",
    "CREATED_AT": ISODate("2019-07-24T07:33:09.008Z"),
    "IS_ACTIVE": NumberInt("1"),
    USERNAME: "admin_luong",
    PASSWORD: "$2a$10$vrbA6SDJP10VFdZqmURwHuRRSvYDfHg022dd2VWYW8ZghGoDMiiLe",
    "PHONE_NUMBER": "0973018866",
    EMAIL: "luongnt2@msb.com.vn",
    PERMISSION: [
        "ADMN",
        "AITR",
        "DATA",
        "HAND",
        "NOTI"
    ],
    "UPDATED_AT": null,
    "UPDATED_BY": null,
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84")
} ]);

// ----------------------------
// Collection structure for MCL_ADMIN_PERMISSION
// ----------------------------
db.getCollection("MCL_ADMIN_PERMISSION").drop();
db.createCollection("MCL_ADMIN_PERMISSION");

// ----------------------------
// Documents of MCL_ADMIN_PERMISSION
// ----------------------------
db.getCollection("MCL_ADMIN_PERMISSION").insert([ {
    _id: ObjectId("5d2c5d9c72e61100084c0a3c"),
    CODE: "ADMN",
    DESCRIPTION: "Quản trị admin.",
    "CREATED_AT": ISODate("2019-07-15T11:03:56.185Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null,
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a")
} ]);
db.getCollection("MCL_ADMIN_PERMISSION").insert([ {
    _id: ObjectId("5d2c5d9c72e61100084c0a3d"),
    CODE: "USER",
    DESCRIPTION: "Quản trị người dùng.",
    "CREATED_AT": ISODate("2019-07-15T11:03:56.185Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null,
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a")
} ]);
db.getCollection("MCL_ADMIN_PERMISSION").insert([ {
    _id: ObjectId("5d2c5d9c72e61100084c0a3e"),
    CODE: "GROU",
    DESCRIPTION: "Quản trị nhóm.",
    "CREATED_AT": ISODate("2019-07-15T11:03:56.185Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null,
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a")
} ]);
db.getCollection("MCL_ADMIN_PERMISSION").insert([ {
    _id: ObjectId("5d2c5d9c72e61100084c0a3f"),
    CODE: "DATA",
    DESCRIPTION: "Quản trị dữ liệu cây chatbot.",
    "CREATED_AT": ISODate("2019-07-15T11:03:56.185Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null,
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a")
} ]);
db.getCollection("MCL_ADMIN_PERMISSION").insert([ {
    _id: ObjectId("5d2c5d9c72e61100084c0a40"),
    CODE: "AITR",
    DESCRIPTION: "Quản trị dữ liệu trainning AI.",
    "CREATED_AT": ISODate("2019-07-15T11:03:56.185Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null,
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a")
} ]);
db.getCollection("MCL_ADMIN_PERMISSION").insert([ {
    _id: ObjectId("5d2c5d9c72e61100084c0a41"),
    CODE: "HAND",
    DESCRIPTION: "Quản trị sổ tay tư vấn.",
    "CREATED_AT": ISODate("2019-07-15T11:03:56.185Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null,
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a")
} ]);
db.getCollection("MCL_ADMIN_PERMISSION").insert([ {
    _id: ObjectId("5d2c5d9c72e61100084c0a42"),
    CODE: "NOTI",
    DESCRIPTION: "Quản trị thông báo.",
    "CREATED_AT": ISODate("2019-07-15T11:03:56.185Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null,
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a")
} ]);
db.getCollection("MCL_ADMIN_PERMISSION").insert([ {
    _id: ObjectId("5d2c5d9c72e61100084c0a43"),
    CODE: "SCHE",
    DESCRIPTION: "Quản trị lập lịch trainning chatbot.",
    "CREATED_AT": ISODate("2019-07-15T11:03:56.185Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null,
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a")
} ]);
db.getCollection("MCL_ADMIN_PERMISSION").insert([ {
    _id: ObjectId("5d2c5d9c72e61100084c0a44"),
    CODE: "STAT",
    DESCRIPTION: "Quản trị thống kê.",
    "CREATED_AT": ISODate("2019-07-15T11:03:56.185Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null,
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a")
} ]);

// ----------------------------
// Collection structure for MCL_ADMIN_SESSION
// ----------------------------
db.getCollection("MCL_ADMIN_SESSION").drop();
db.createCollection("MCL_ADMIN_SESSION");

// ----------------------------
// Documents of MCL_ADMIN_SESSION
// ----------------------------
db.getCollection("MCL_ADMIN_SESSION").insert([ {
    _id: ObjectId("5d2e93a10349d20017501f95"),
    "ADMIN__ID": ObjectId("5d2c5d9f72e61100084c0a81"),
    "CREATED_AT": ISODate("2019-07-17T03:18:57.021Z"),
    TOKEN: "a964a200-5528-4f37-a844-c9c12b0c7987"
} ]);
db.getCollection("MCL_ADMIN_SESSION").insert([ {
    _id: ObjectId("5d3673d9a945f1001769ab37"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-23T02:41:29.128Z"),
    TOKEN: "f98748e2-40e6-4eac-98e3-12759b52ffcb"
} ]);
db.getCollection("MCL_ADMIN_SESSION").insert([ {
    _id: ObjectId("5d36b296229bc60017eaa0cd"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a80"),
    "CREATED_AT": ISODate("2019-07-23T07:09:10.911Z"),
    TOKEN: "5f661b3c-8f4c-43c1-8095-33dc0232d9cb"
} ]);
db.getCollection("MCL_ADMIN_SESSION").insert([ {
    _id: ObjectId("5d36bf0e229bc60017eaa11d"),
    "ADMIN__ID": ObjectId("5d367e92011a790017ac1e95"),
    "CREATED_AT": ISODate("2019-07-23T08:02:22.762Z"),
    TOKEN: "d8026607-61f4-409d-a820-afe0b3218867"
} ]);
db.getCollection("MCL_ADMIN_SESSION").insert([ {
    _id: ObjectId("5d37c95c65d0fc00173a6e4c"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-24T02:58:36.159Z"),
    TOKEN: "e23d9452-c8e1-461e-882f-6de4bd43ae0c"
} ]);

// ----------------------------
// Collection structure for MCL_ADMIN_SESSION_HISTORY
// ----------------------------
db.getCollection("MCL_ADMIN_SESSION_HISTORY").drop();
db.createCollection("MCL_ADMIN_SESSION_HISTORY");

// ----------------------------
// Documents of MCL_ADMIN_SESSION_HISTORY
// ----------------------------
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2c5dc872e61100084c0abe"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-15T11:04:40.25Z"),
    TOKEN: "b222f220-7163-4392-80f7-c3fc533d93db"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2c5e7e9d7b6f000870a559"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-15T11:07:42.226Z"),
    TOKEN: "4ea0d7f5-f693-4a78-aa2c-cc726f4870c7"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2ca4e09d7b6f000870a561"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a80"),
    "LOGGED_AT": ISODate("2019-07-15T16:08:00.965Z"),
    TOKEN: "acdfc13b-a18c-48e2-92f7-8a049c8c8064"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2d2e759b18b20009a3c31d"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "LOGGED_AT": ISODate("2019-07-16T01:55:01.822Z"),
    TOKEN: "8223f21b-c7e6-4de0-9f2a-21c51a17aa22"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2d32d09b18b20009a3c322"),
    "ADMIN__ID": ObjectId("5d2c5d9f72e61100084c0a82"),
    "LOGGED_AT": ISODate("2019-07-16T02:13:36.461Z"),
    TOKEN: "11ebb7b4-b09d-49e1-8bdd-9f5915d0cd7e"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2d37049b18b20009a3c328"),
    "ADMIN__ID": ObjectId("5d2c5d9f72e61100084c0a81"),
    "LOGGED_AT": ISODate("2019-07-16T02:31:32.003Z"),
    TOKEN: "02bc3341-4afa-4b2f-b9f3-5a93a16fd935"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2d38c3fd4db6000917d2fb"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-16T02:38:59.205Z"),
    TOKEN: "960f7af1-b1a5-4059-8b84-37d67a8dc504"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2d3970fd4db6000917d2fd"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a80"),
    "LOGGED_AT": ISODate("2019-07-16T02:41:52.652Z"),
    TOKEN: "baf189b7-75dc-41cc-9b56-f2ad6fb52026"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2d3a95fd4db6000917d2ff"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a80"),
    "LOGGED_AT": ISODate("2019-07-16T02:46:45.339Z"),
    TOKEN: "ceccc91f-0729-407c-a639-74e81083f3ef"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2d40ccfd4db6000917d319"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a80"),
    "LOGGED_AT": ISODate("2019-07-16T03:13:16.72Z"),
    TOKEN: "52c1f6db-de68-4b22-bb75-82dc3a10421d"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2d4af8c82920000860a4b6"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-16T03:56:40.807Z"),
    TOKEN: "f57ba4d0-3814-43fd-832a-83b1c65cc543"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2d7450ef364d0008b48949"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a83"),
    "LOGGED_AT": ISODate("2019-07-16T06:53:04.568Z"),
    TOKEN: "194d0641-6722-4833-9201-831e0da630f2"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2d7e47ef364d0008b48951"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "LOGGED_AT": ISODate("2019-07-16T07:35:35.983Z"),
    TOKEN: "c0ba7725-286b-4dd4-beaf-5c5641ee0a29"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2d80ebef364d0008b48953"),
    "ADMIN__ID": ObjectId("5d2c5d9f72e61100084c0a82"),
    "LOGGED_AT": ISODate("2019-07-16T07:46:51.677Z"),
    TOKEN: "3f662ccd-7381-40c9-b8ac-35f0aa953b17"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2d82b5ef364d0008b48955"),
    "ADMIN__ID": ObjectId("5d2c5d9f72e61100084c0a82"),
    "LOGGED_AT": ISODate("2019-07-16T07:54:29.748Z"),
    TOKEN: "b590e9d4-f633-4050-a5c1-bce67589414d"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2d84bfef364d0008b48958"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "LOGGED_AT": ISODate("2019-07-16T08:03:11.23Z"),
    TOKEN: "61aee5d6-b198-427f-866a-de0edfd9b5aa"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2d8b90ef364d0008b4895a"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-16T08:32:16.291Z"),
    TOKEN: "e0fd367f-08ad-4851-a926-67dbb0f85aa9"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2d8e4f6fb49b001771b433"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-16T08:43:59.152Z"),
    TOKEN: "39f5bb60-2654-430a-bc81-82c0154b4dc2"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2d94716fb49b001771b439"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-16T09:10:09.311Z"),
    TOKEN: "b99d93b8-9b7e-4239-8519-3c1a961b5ac9"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2d9be06fb49b001771b457"),
    "ADMIN__ID": ObjectId("5d2c5d9f72e61100084c0a81"),
    "LOGGED_AT": ISODate("2019-07-16T09:41:52.754Z"),
    TOKEN: "f4454497-aa70-4f26-8454-ac5633f6d089"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2da220ef364d0008b48961"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a83"),
    "LOGGED_AT": ISODate("2019-07-16T10:08:32.91Z"),
    TOKEN: "85f4c6c9-234a-4a8c-9d10-d27e81094914"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2e804b3bf1ca00174e8206"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "LOGGED_AT": ISODate("2019-07-17T01:56:27.679Z"),
    TOKEN: "9ca58374-59e3-4804-a0d5-32893f2be976"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2e93a10349d20017501f96"),
    "ADMIN__ID": ObjectId("5d2c5d9f72e61100084c0a81"),
    "LOGGED_AT": ISODate("2019-07-17T03:18:57.023Z"),
    TOKEN: "3d119eff-2401-438d-88ad-53bcb0966a13"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2e954a0349d20017501f98"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-17T03:26:02.381Z"),
    TOKEN: "5a414bbf-068c-40cd-9f20-c167568ff058"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2ef6d57cd18c0017afa7cf"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-17T10:22:13.978Z"),
    TOKEN: "199b8804-6227-4193-9231-c78ab48c1f04"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2efcb47cd18c0017afa7d2"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-17T10:47:16.288Z"),
    TOKEN: "e7fc8d60-af07-4a2c-a4d2-4f42a69c2f2b"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2fd0571330ca0017dacc42"),
    "ADMIN__ID": ObjectId("5d2e80ac3bf1ca00174e8207"),
    "LOGGED_AT": ISODate("2019-07-18T01:50:15.138Z"),
    TOKEN: "ffdf78b4-d7bf-47f8-8841-d9b9c2759fe8"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2fd3301330ca0017dacc44"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "LOGGED_AT": ISODate("2019-07-18T02:02:24.14Z"),
    TOKEN: "23781bbf-4e4a-4dd0-99ed-0824e390102a"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2fd9c11330ca0017dacc48"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-18T02:30:25.58Z"),
    TOKEN: "f51dc8ff-5bee-479a-9be6-ec948685ca05"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2fdc981330ca0017dacc4a"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "LOGGED_AT": ISODate("2019-07-18T02:42:32.231Z"),
    TOKEN: "c2d31453-f0c9-4645-9f51-2a6305a0ae83"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2fdd141330ca0017dacc4c"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "LOGGED_AT": ISODate("2019-07-18T02:44:36.436Z"),
    TOKEN: "caee6b3e-dcdd-4a6e-b990-5b6287722232"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2ff6bd1330ca0017dacc7a"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "LOGGED_AT": ISODate("2019-07-18T04:34:05.161Z"),
    TOKEN: "45a7c888-1cb8-457e-8e43-f9ccbf588fa6"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d2ff71e1330ca0017dacc7c"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "LOGGED_AT": ISODate("2019-07-18T04:35:42.413Z"),
    TOKEN: "4f9f3d14-d29e-4d88-b71e-3671143b35c6"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d301c58733e7000173e9a0b"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "LOGGED_AT": ISODate("2019-07-18T07:14:32.505Z"),
    TOKEN: "8fbc063c-5df4-4c96-ad63-62af791664ae"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d302312733e7000173e9a13"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "LOGGED_AT": ISODate("2019-07-18T07:43:14.337Z"),
    TOKEN: "60e22938-7737-4c50-8f69-1cf86cf0af4f"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d302323733e7000173e9a15"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "LOGGED_AT": ISODate("2019-07-18T07:43:31.333Z"),
    TOKEN: "0d2e46c7-a151-4bab-a84d-7e0e0a495900"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d302348733e7000173e9a17"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "LOGGED_AT": ISODate("2019-07-18T07:44:08.67Z"),
    TOKEN: "e61afbd7-9c50-4415-a680-77d8d437d368"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d302bcc733e7000173e9a1d"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "LOGGED_AT": ISODate("2019-07-18T08:20:28.806Z"),
    TOKEN: "b7f26ea6-7479-4b47-9d77-36bf5ee4ddc9"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d312775ad4d73001754a354"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "LOGGED_AT": ISODate("2019-07-19T02:14:13.15Z"),
    TOKEN: "f7ee1a60-b886-4864-bf91-9a3781f4a1ca"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d313932ad4d73001754a3a1"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "LOGGED_AT": ISODate("2019-07-19T03:29:54.708Z"),
    TOKEN: "44d8e73b-714c-48a2-bfee-ee6369d51163"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d313a58ad4d73001754a3a6"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "LOGGED_AT": ISODate("2019-07-19T03:34:48.313Z"),
    TOKEN: "0b52ed50-3699-4f24-ad1c-f2dd7b5bec7a"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d352d23124be900177bb06a"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "LOGGED_AT": ISODate("2019-07-22T03:27:31.089Z"),
    TOKEN: "4cebce12-e0a2-445c-8398-f06f37bf80af"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d3559bcb8a2db0017952f44"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-22T06:37:48.2Z"),
    TOKEN: "e6bcf9ae-e95a-447a-9090-8ea65e2f3aea"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d3562dfb8a2db0017952f4b"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-22T07:16:47.326Z"),
    TOKEN: "6fbdb30f-6c15-4c29-9a94-78073a6c0cfd"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d35632ab8a2db0017952f4e"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-22T07:18:02.425Z"),
    TOKEN: "7f5ed18b-156b-460e-a542-1364bed9e5fd"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d3564e9b8a2db0017952f53"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-22T07:25:29.629Z"),
    TOKEN: "4d20aeb0-69b6-419a-991b-0ad1933003f3"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d356551b8a2db0017952f55"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-22T07:27:13.362Z"),
    TOKEN: "4eaa31de-e28b-4ae1-9189-28c6b6f38904"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d356563b8a2db0017952f57"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "LOGGED_AT": ISODate("2019-07-22T07:27:31.242Z"),
    TOKEN: "5c2812e7-d7c1-4123-bc19-debf96f66ca1"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d356a43a697e600172bfc51"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-22T07:48:19.2Z"),
    TOKEN: "dc64e62d-4539-4af6-be71-8750f657d90b"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d35864b620721001773efb4"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-22T09:47:55.885Z"),
    TOKEN: "53fb9139-4c9e-46fd-a9f0-d0e3fccba0fe"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d35890a620721001773efb6"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a83"),
    "LOGGED_AT": ISODate("2019-07-22T09:59:38.898Z"),
    TOKEN: "e7b95f17-bba2-444c-9541-612101e1d3c3"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d358a99620721001773efbf"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-22T10:06:17.76Z"),
    TOKEN: "c9e73a3d-975f-4da5-9165-dd9d6d56011f"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d3666efa945f1001769ab2c"),
    "ADMIN__ID": ObjectId("5d2c5d9f72e61100084c0a82"),
    "LOGGED_AT": ISODate("2019-07-23T01:46:23.647Z"),
    TOKEN: "17066ece-c848-481f-9ea8-956e05b51043"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d366ec9a945f1001769ab30"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-23T02:19:53.319Z"),
    TOKEN: "eca4d446-235d-4d86-92a2-6ddf20a4fd49"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d36717fa945f1001769ab34"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-23T02:31:27.722Z"),
    TOKEN: "4af98c67-183b-4ef3-a5fe-2695d204cc66"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d3671f4a945f1001769ab36"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a83"),
    "LOGGED_AT": ISODate("2019-07-23T02:33:24.668Z"),
    TOKEN: "326a5800-54c4-49c4-a3c8-ba11d292a429"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d3673d9a945f1001769ab38"),
    "ADMIN__ID": ObjectId("5d2c5da072e61100084c0a84"),
    "LOGGED_AT": ISODate("2019-07-23T02:41:29.129Z"),
    TOKEN: "be45b302-0fb2-4630-8eeb-ca1e92c1652d"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d36773ca945f1001769ab3a"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a80"),
    "LOGGED_AT": ISODate("2019-07-23T02:55:56.189Z"),
    TOKEN: "25915df5-c2bc-493a-b51f-e553b166db06"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d367ea1011a790017ac1e97"),
    "ADMIN__ID": ObjectId("5d367e92011a790017ac1e95"),
    "LOGGED_AT": ISODate("2019-07-23T03:27:29.997Z"),
    TOKEN: "16065558-8d63-449a-a36a-25abb2ec4296"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d36840d011a790017ac1e9a"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-23T03:50:37.597Z"),
    TOKEN: "410888ec-9bba-440e-8014-e6e8b4c257e2"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d368438011a790017ac1e9c"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-23T03:51:20.984Z"),
    TOKEN: "56b52675-1632-4c04-9c54-481d0658c158"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d36b296229bc60017eaa0ce"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a80"),
    "LOGGED_AT": ISODate("2019-07-23T07:09:10.919Z"),
    TOKEN: "d6dc93df-9fa5-4c69-a8c4-02a6cc375892"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d36bafe229bc60017eaa119"),
    "ADMIN__ID": ObjectId("5d367e92011a790017ac1e95"),
    "LOGGED_AT": ISODate("2019-07-23T07:45:02.504Z"),
    TOKEN: "a74e55a1-dfce-4062-8121-f910b43367ae"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d36bf0e229bc60017eaa11e"),
    "ADMIN__ID": ObjectId("5d367e92011a790017ac1e95"),
    "LOGGED_AT": ISODate("2019-07-23T08:02:22.762Z"),
    TOKEN: "e6f6eb53-0115-41af-946a-5fa3d1e22d0a"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d36d6fc229bc60017eaa132"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-23T09:44:28.215Z"),
    TOKEN: "186c737f-3af4-4c50-be20-f7558071d455"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d36d820229bc60017eaa134"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-23T09:49:20.968Z"),
    TOKEN: "0e432d0f-21f2-4333-a379-0d88c7d1d6f9"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d36dc34229bc60017eaa138"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-23T10:06:44.464Z"),
    TOKEN: "f0470a21-d13d-4e6a-8462-99d5f4eadccd"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d36e494229bc60017eaa13b"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-23T10:42:28.92Z"),
    TOKEN: "08577b51-c252-4519-99a1-97a29007f37a"
} ]);
db.getCollection("MCL_ADMIN_SESSION_HISTORY").insert([ {
    _id: ObjectId("5d37c95c65d0fc00173a6e4d"),
    "ADMIN__ID": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "LOGGED_AT": ISODate("2019-07-24T02:58:36.16Z"),
    TOKEN: "d909fa22-bdc1-4473-9a6a-7f18774d52bf"
} ]);

// ----------------------------
// Collection structure for MCL_AI_TRAINNING
// ----------------------------
db.getCollection("MCL_AI_TRAINNING").drop();
db.createCollection("MCL_AI_TRAINNING");

// ----------------------------
// Documents of MCL_AI_TRAINNING
// ----------------------------
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a60"),
    "FUNCTION_ID": "GREETINGS",
    VALUE: "chao ban",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a61"),
    "FUNCTION_ID": "GREETINGS",
    VALUE: "xin chao",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a62"),
    "FUNCTION_ID": "GREETINGS",
    VALUE: "hello",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a63"),
    "FUNCTION_ID": "WEATHER",
    VALUE: "thoi tiet nhu nao",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a64"),
    "FUNCTION_ID": "WEATHER",
    VALUE: "thoi tiet",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a65"),
    "FUNCTION_ID": "WEATHER",
    VALUE: "thoi tiet hom nay",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a66"),
    "FUNCTION_ID": "GENERAL_COMPLIANCE",
    VALUE: "tuan thu chung",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a67"),
    "FUNCTION_ID": "GENERAL_COMPLIANCE",
    VALUE: "dieu le chung",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a68"),
    "FUNCTION_ID": "BUSINESS_COMPLIANCE",
    VALUE: "tuan thu nghiep vu",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a69"),
    "FUNCTION_ID": "BUSINESS_COMPLIANCE",
    VALUE: "hoat dong nghiep vu",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a6a"),
    "FUNCTION_ID": "BUSINESS_COMPLIANCE",
    VALUE: "nghiep vu",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a6b"),
    "FUNCTION_ID": "LISENSE_REGISTRATION",
    VALUE: "dang ky giay phep",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a6c"),
    "FUNCTION_ID": "LICENSE_MANAGEMENT",
    VALUE: "giay phep",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a6d"),
    "FUNCTION_ID": "LICENSE_MANAGEMENT",
    VALUE: "quan ly giay phep",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a6e"),
    "FUNCTION_ID": "HANDBOOK",
    VALUE: "cam nang",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a6f"),
    "FUNCTION_ID": "HANDBOOK",
    VALUE: "cam nang phap luat",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a70"),
    "FUNCTION_ID": "FEE",
    VALUE: "phi",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a71"),
    "FUNCTION_ID": "FEE",
    VALUE: "le phi",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a72"),
    "FUNCTION_ID": "USE",
    VALUE: "su dung",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a73"),
    "FUNCTION_ID": "CONTENT",
    VALUE: "noi dung",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a74"),
    "FUNCTION_ID": "USAGE",
    VALUE: "thoi han",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a75"),
    "FUNCTION_ID": "REGULATION",
    VALUE: "luat le",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a76"),
    "FUNCTION_ID": "REGULATION",
    VALUE: "quy dinh",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a77"),
    "FUNCTION_ID": "DETECTED_LISENCE",
    VALUE: "giay phep hong",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a78"),
    "FUNCTION_ID": "DETECTED_LISENCE",
    VALUE: "giay phep loi",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a79"),
    "FUNCTION_ID": "DETECTED_LISENCE",
    VALUE: "giay phep chay",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2c5d9e72e61100084c0a7a"),
    "FUNCTION_ID": "DETECTED_LISENCE",
    VALUE: "giay phep rach",
    "IS_ACTIVE": NumberInt("1"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "CREATED_AT": ISODate("2019-07-15T11:03:58.145Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2d4143fd4db6000917d31f"),
    "FUNCTION_ID": "ACCEPTING_CHANGE",
    VALUE: "xin chap nhan thay doi",
    "CREATED_AT": ISODate("2019-07-16T03:15:15.101Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2d415ffd4db6000917d321"),
    "FUNCTION_ID": "ACCEPTING_CHANGE",
    VALUE: "xin chap nhan thay doi",
    "CREATED_AT": ISODate("2019-07-16T03:15:43.632Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2d4165fd4db6000917d322"),
    "FUNCTION_ID": "ACCEPTING_CHANGE",
    VALUE: "xin chap nhan thay doi",
    "CREATED_AT": ISODate("2019-07-16T03:15:49.99Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2d41c7fd4db6000917d325"),
    "FUNCTION_ID": "NHUNGTHAYDOIPHAIDUOCNHNNCHAPNHAN",
    VALUE: "nhung thay doi can nhnn chap nhan",
    "CREATED_AT": ISODate("2019-07-16T03:17:27.82Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2d41dffd4db6000917d326"),
    "FUNCTION_ID": "NHUNGTHAYDOIPHAIDUOCNHNNCHAPNHAN",
    VALUE: "nhung thay doi can duoc nhnn chap nhan",
    "CREATED_AT": ISODate("2019-07-16T03:17:51.98Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2d41eefd4db6000917d327"),
    "FUNCTION_ID": "NHUNGTHAYDOIPHAIDUOCNHNNCHAPNHAN",
    VALUE: "nhung thay doi can nhnn chap nhan",
    "CREATED_AT": ISODate("2019-07-16T03:18:06.667Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2d423a319a1c000832b83c"),
    "FUNCTION_ID": "THUCHIENTHUTUCSAUKHIDUOCNHNNCHAPTHUANTHAYDOI",
    VALUE: "thuc hien thu tuc sau khi duoc nhnn chap thuan thay doi",
    "CREATED_AT": ISODate("2019-07-16T03:19:22.04Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2d4254319a1c000832b83e"),
    "FUNCTION_ID": "THUCHIENTHUTUCSAUKHIDUOCNHNNCHAPTHUANTHAYDOI",
    VALUE: "thu tuc sau khi duoc nhnn chap thuan thay doi",
    "CREATED_AT": ISODate("2019-07-16T03:19:48.046Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2d426c319a1c000832b83f"),
    "FUNCTION_ID": "THUCHIENTHUTUCSAUKHIDUOCNHNNCHAPTHUANTHAYDOI",
    VALUE: "thuc hien thu tuc sau khi duoc nhnn chap thuan thay doi",
    "CREATED_AT": ISODate("2019-07-16T03:20:12.821Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2d428f319a1c000832b840"),
    "FUNCTION_ID": "THUCHIENTHUTUCSAUKHIDUOCNHNNCHAPTHUANTHAYDOI",
    VALUE: "thu tuc sau khi nhnn chap nhan thay doi",
    "CREATED_AT": ISODate("2019-07-16T03:20:47.287Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2d9ede6fb49b001771b45f"),
    "FUNCTION_ID": "GREETINGS",
    VALUE: "hello",
    "CREATED_AT": ISODate("2019-07-16T09:54:38.349Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("0"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2ed3dcc782c400177694b4"),
    "FUNCTION_ID": "GENERAL_COMPLIANCE",
    VALUE: "tuan thu chung",
    "CREATED_AT": ISODate("2019-07-17T07:53:00.289Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("0"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d2fd3681330ca0017dacc45"),
    "FUNCTION_ID": "COSOPHATHANHCONGCUCHUYENNHUONG",
    VALUE: "co so phat hanh cong cu chuyen nhuong",
    "CREATED_AT": ISODate("2019-07-18T02:03:20.738Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": "2019-07-19T02:17:34.895Z",
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312987ad4d73001754a355"),
    "FUNCTION_ID": "CONGCUCHUYENNHUONG",
    VALUE: "cong cu chuyen nhuong",
    "CREATED_AT": ISODate("2019-07-19T02:23:03.998Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d3129a8ad4d73001754a356"),
    "FUNCTION_ID": "CONGCUCHUYENNHUONG",
    VALUE: "cac cong cu chuyen nhuong",
    "CREATED_AT": ISODate("2019-07-19T02:23:36.23Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d3129c9ad4d73001754a357"),
    "FUNCTION_ID": "CONGCUCHUYENNHUONG",
    VALUE: "nhung cong cu chuyen nhuong",
    "CREATED_AT": ISODate("2019-07-19T02:24:09.693Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d3129e3ad4d73001754a358"),
    "FUNCTION_ID": "COSOPHATHANHCONGCUCHUYENNHUONG",
    VALUE: "co so phat hanh cong cu chuyen nhuong la gi",
    "CREATED_AT": ISODate("2019-07-19T02:24:35.133Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d3129f7ad4d73001754a359"),
    "FUNCTION_ID": "COSOPHATHANHCONGCUCHUYENNHUONG",
    VALUE: "nhung co so de phat hanh cong cu chuyen nhuong",
    "CREATED_AT": ISODate("2019-07-19T02:24:55.297Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312a0dad4d73001754a35a"),
    "FUNCTION_ID": "QUYDINHCHUNGVECONGCUCHUYENNHUONG",
    VALUE: "quy dinh chung ve cong cu chuyen nhuong",
    "CREATED_AT": ISODate("2019-07-19T02:25:17.749Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312a45ad4d73001754a35b"),
    "FUNCTION_ID": "QUYDINHCHUNGVECONGCUCHUYENNHUONG",
    VALUE: "quy dinh chung ve cong cu chuyen nhuong la gi",
    "CREATED_AT": ISODate("2019-07-19T02:26:13.932Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312a62ad4d73001754a35c"),
    "FUNCTION_ID": "QUYDINHCHUNGVECONGCUCHUYENNHUONG",
    VALUE: "nhung quy dinh chung ve cong cu chuyen nhuong",
    "CREATED_AT": ISODate("2019-07-19T02:26:42.114Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312aafad4d73001754a35d"),
    "FUNCTION_ID": "THANHTOANCONGCUCHUYENNHUONGGHITRABANGNGOAITE",
    VALUE: "thanh toan cong cu chuyen nhuong ghi tra bang ngoai te",
    "CREATED_AT": ISODate("2019-07-19T02:27:59.79Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312acdad4d73001754a35e"),
    "FUNCTION_ID": "THANHTOANCONGCUCHUYENNHUONGGHITRABANGNGOAITE",
    VALUE: "co duoc thanh toan cong cu chuyen nhuong ghi tra bang ngoai te khong",
    "CREATED_AT": ISODate("2019-07-19T02:28:29.221Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312ae7ad4d73001754a35f"),
    "FUNCTION_ID": "THANHTOANCONGCUCHUYENNHUONGGHITRABANGNGOAITE",
    VALUE: "thanh toan cong cu chuyen nhuong ghi tra bang ngoai te nhu the nao",
    "CREATED_AT": ISODate("2019-07-19T02:28:55.744Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312b04ad4d73001754a360"),
    "FUNCTION_ID": "KYCONGCUCHUYENNHUONG",
    VALUE: "quy dinh ve ky cong cu chuyen nhuong",
    "CREATED_AT": ISODate("2019-07-19T02:29:24.134Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312b15ad4d73001754a361"),
    "FUNCTION_ID": "KYCONGCUCHUYENNHUONG",
    VALUE: "quy dinh phap luat ve ky cong cu chuyen nhuong",
    "CREATED_AT": ISODate("2019-07-19T02:29:41.483Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312b28ad4d73001754a362"),
    "FUNCTION_ID": "KYCONGCUCHUYENNHUONG",
    VALUE: "ky cong cu chuyen nhuong",
    "CREATED_AT": ISODate("2019-07-19T02:30:00.194Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312b46ad4d73001754a363"),
    "FUNCTION_ID": "NGONNGUTRENCONGCUCHUYENNHUONG",
    VALUE: "ngon ngu tren cong cu chuyen nhuong",
    "CREATED_AT": ISODate("2019-07-19T02:30:30.871Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312b5cad4d73001754a364"),
    "FUNCTION_ID": "NGONNGUTRENCONGCUCHUYENNHUONG",
    VALUE: "quy dinh ve ngon ngu tren cong cu chuyen nhuong",
    "CREATED_AT": ISODate("2019-07-19T02:30:52.549Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312b70ad4d73001754a365"),
    "FUNCTION_ID": "NGONNGUTRENCONGCUCHUYENNHUONG",
    VALUE: "quy dinh phap luat ve ngon ngu tren cong cu chuyen nhuong",
    "CREATED_AT": ISODate("2019-07-19T02:31:12.356Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312ba9ad4d73001754a366"),
    "FUNCTION_ID": "HUHONGCONGCUCHUYENNHUONG",
    VALUE: "hu hong cong cu chuyen nhuong",
    "CREATED_AT": ISODate("2019-07-19T02:32:09.187Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312e15ad4d73001754a367"),
    "FUNCTION_ID": "HUHONGCONGCUCHUYENNHUONG",
    VALUE: "cong cu chuyen nhuong bi hu hong",
    "CREATED_AT": ISODate("2019-07-19T02:42:29.468Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312e3cad4d73001754a368"),
    "FUNCTION_ID": "HUHONGCONGCUCHUYENNHUONG",
    VALUE: "cong cu chuyen nhuong bi hong",
    "CREATED_AT": ISODate("2019-07-19T02:43:08.858Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312e85ad4d73001754a369"),
    "FUNCTION_ID": "CACHANHVIBICAMKHIGIAODICHCONGCUCHUYENNHUONG",
    VALUE: "cac hanh vi bi cam khi giao dich cong cu chuyen nhuong",
    "CREATED_AT": ISODate("2019-07-19T02:44:21.393Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312e95ad4d73001754a36a"),
    "FUNCTION_ID": "CACHANHVIBICAMKHIGIAODICHCONGCUCHUYENNHUONG",
    VALUE: "cac hanh vi bi cam khi giao dich cong cu chuyen nhuong",
    "CREATED_AT": ISODate("2019-07-19T02:44:37.401Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312e9ead4d73001754a36b"),
    "FUNCTION_ID": "CACHANHVIBICAMKHIGIAODICHCONGCUCHUYENNHUONG",
    VALUE: "cac hanh vi bi cam khi giao dich cong cu chuyen nhuong",
    "CREATED_AT": ISODate("2019-07-19T02:44:46.544Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312eb4ad4d73001754a36c"),
    "FUNCTION_ID": "SEC",
    VALUE: "sec",
    "CREATED_AT": ISODate("2019-07-19T02:45:08.091Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312ec5ad4d73001754a36d"),
    "FUNCTION_ID": "SEC",
    VALUE: "to sec",
    "CREATED_AT": ISODate("2019-07-19T02:45:25.966Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312ed7ad4d73001754a36e"),
    "FUNCTION_ID": "SEC",
    VALUE: "sec",
    "CREATED_AT": ISODate("2019-07-19T02:45:43.084Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312f11ad4d73001754a36f"),
    "FUNCTION_ID": "CACNOIDUNGCUASEC",
    VALUE: "cac noi dung cua sec",
    "CREATED_AT": ISODate("2019-07-19T02:46:41.798Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312f2aad4d73001754a370"),
    "FUNCTION_ID": "CACNOIDUNGCUASEC",
    VALUE: "cac noi dung cua sec",
    "CREATED_AT": ISODate("2019-07-19T02:47:06.627Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312f35ad4d73001754a371"),
    "FUNCTION_ID": "CACNOIDUNGCUASEC",
    VALUE: "cac noi dung cua sec",
    "CREATED_AT": ISODate("2019-07-19T02:47:17.176Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d312f81ad4d73001754a372"),
    "FUNCTION_ID": "IN,GIAONHANVABAOQUANSECTRANG",
    VALUE: "in giao nhan va bao quan sec trang",
    "CREATED_AT": ISODate("2019-07-19T02:48:33.619Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d31317bad4d73001754a373"),
    "FUNCTION_ID": "IN,GIAONHANVABAOQUANSECTRANG",
    VALUE: "in giao nhan va bao quan sec trang",
    "CREATED_AT": ISODate("2019-07-19T02:56:59.037Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313185ad4d73001754a374"),
    "FUNCTION_ID": "IN,GIAONHANVABAOQUANSECTRANG",
    VALUE: "in giao nhan va bao quan sec trang",
    "CREATED_AT": ISODate("2019-07-19T02:57:09.792Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313199ad4d73001754a375"),
    "FUNCTION_ID": "THANHTOANSECTAINGUOIBIKYPHAT ",
    VALUE: "thanh toan sec tai nguoi bi ky phat",
    "CREATED_AT": ISODate("2019-07-19T02:57:29.823Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d3131a1ad4d73001754a376"),
    "FUNCTION_ID": "THANHTOANSECTAINGUOIBIKYPHAT ",
    VALUE: "thanh toan sec tai nguoi bi ky phat",
    "CREATED_AT": ISODate("2019-07-19T02:57:37.804Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d3131abad4d73001754a377"),
    "FUNCTION_ID": "THANHTOANSECTAINGUOIBIKYPHAT ",
    VALUE: "thanh toan sec tai nguoi bi ky phat",
    "CREATED_AT": ISODate("2019-07-19T02:57:47.892Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d3131e7ad4d73001754a378"),
    "FUNCTION_ID": "XULYSECKHONGDUKHANANGTHANHTOAN",
    VALUE: "xu ly sec khong du kha nang thanh toan",
    "CREATED_AT": ISODate("2019-07-19T02:58:47.418Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313205ad4d73001754a379"),
    "FUNCTION_ID": "XULYSECKHONGDUKHANANGTHANHTOAN",
    VALUE: "xu ly sec khong du kha nang thanh toan",
    "CREATED_AT": ISODate("2019-07-19T02:59:17.536Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313219ad4d73001754a37a"),
    "FUNCTION_ID": "XULYSECKHONGDUKHANANGTHANHTOAN",
    VALUE: "xu ly sec khong du kha nang thanh toan",
    "CREATED_AT": ISODate("2019-07-19T02:59:37.291Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313253ad4d73001754a37b"),
    "FUNCTION_ID": "QUYDINHCUNGUNG,THANHTOAN,ĐINHCHINHTHANHTOANSEC",
    VALUE: "quy dinh cung ung thanh toan dinh chinh thanh toan sec",
    "CREATED_AT": ISODate("2019-07-19T03:00:35.104Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313260ad4d73001754a37c"),
    "FUNCTION_ID": "QUYDINHCUNGUNG,THANHTOAN,ĐINHCHINHTHANHTOANSEC",
    VALUE: "quy dinh ve cung ung thanh toan dinh chinh thanh toan sec",
    "CREATED_AT": ISODate("2019-07-19T03:00:48.261Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313270ad4d73001754a37d"),
    "FUNCTION_ID": "QUYDINHCUNGUNG,THANHTOAN,ĐINHCHINHTHANHTOANSEC",
    VALUE: "quy dinh phap luat ve cung ung thanh toan dinh chinh thanh toan sec",
    "CREATED_AT": ISODate("2019-07-19T03:01:04.742Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d31327ead4d73001754a37e"),
    "FUNCTION_ID": "XULYMATSEC",
    VALUE: "xu ly mat sec",
    "CREATED_AT": ISODate("2019-07-19T03:01:18.423Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d3132daad4d73001754a37f"),
    "FUNCTION_ID": "XULYMATSEC",
    VALUE: "xu ly khi mat sec",
    "CREATED_AT": ISODate("2019-07-19T03:02:50.993Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d3132e7ad4d73001754a380"),
    "FUNCTION_ID": "XULYMATSEC",
    VALUE: "xu ly mat sec",
    "CREATED_AT": ISODate("2019-07-19T03:03:03.037Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313562ad4d73001754a381"),
    "FUNCTION_ID": "HOIPHIEU",
    VALUE: "hoi phieu",
    "CREATED_AT": ISODate("2019-07-19T03:13:38.532Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313572ad4d73001754a382"),
    "FUNCTION_ID": "HOIPHIEU",
    VALUE: "hoi phieu",
    "CREATED_AT": ISODate("2019-07-19T03:13:54.348Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313580ad4d73001754a383"),
    "FUNCTION_ID": "HOIPHIEU",
    VALUE: "hoi phieu",
    "CREATED_AT": ISODate("2019-07-19T03:14:08.109Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d31359dad4d73001754a384"),
    "FUNCTION_ID": "NOIDUNGCUAHOIPHIEUDOINO",
    VALUE: "noi dung cua hoi phieu doi no",
    "CREATED_AT": ISODate("2019-07-19T03:14:37.773Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d3135a4ad4d73001754a385"),
    "FUNCTION_ID": "NOIDUNGCUAHOIPHIEUDOINO",
    VALUE: "noi dung cua hoi phieu doi no",
    "CREATED_AT": ISODate("2019-07-19T03:14:44.78Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d3135abad4d73001754a386"),
    "FUNCTION_ID": "NOIDUNGCUAHOIPHIEUDOINO",
    VALUE: "noi dung cua hoi phieu doi no",
    "CREATED_AT": ISODate("2019-07-19T03:14:51.356Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d3135c4ad4d73001754a387"),
    "FUNCTION_ID": "NOIDUNGCUAHOIPHIEUNHANNO",
    VALUE: "noi dung cua hoi phieu nhan no",
    "CREATED_AT": ISODate("2019-07-19T03:15:16.541Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d3135ccad4d73001754a388"),
    "FUNCTION_ID": "NOIDUNGCUAHOIPHIEUNHANNO",
    VALUE: "noi dung cua hoi phieu nhan no",
    "CREATED_AT": ISODate("2019-07-19T03:15:24.123Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d3135d7ad4d73001754a389"),
    "FUNCTION_ID": "NOIDUNGCUAHOIPHIEUNHANNO",
    VALUE: "noi dung cua hoi phieu nhan no",
    "CREATED_AT": ISODate("2019-07-19T03:15:35.572Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d3135ecad4d73001754a38a"),
    "FUNCTION_ID": "NGHIAVUCUANGUOIKYPHATHOIPHIEUDOINO",
    VALUE: "nghia vu cua nguoi ky phat hoi phieu doi no",
    "CREATED_AT": ISODate("2019-07-19T03:15:56.847Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313608ad4d73001754a38b"),
    "FUNCTION_ID": "NGHIAVUCUANGUOIKYPHATHOIPHIEUDOINO",
    VALUE: "nghia vu cua nguoi ky phat hoi phieu doi no",
    "CREATED_AT": ISODate("2019-07-19T03:16:24.13Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d31360ead4d73001754a38c"),
    "FUNCTION_ID": "NGHIAVUCUANGUOIKYPHATHOIPHIEUDOINO",
    VALUE: "nghia vu cua nguoi ky phat hoi phieu doi no",
    "CREATED_AT": ISODate("2019-07-19T03:16:30.717Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313620ad4d73001754a38d"),
    "FUNCTION_ID": "NGHIAVUCUANGUOIPHATHANHHOIPHIEUNHANNO",
    VALUE: "nghia vu cua nguoi phat hanh hoi phieu nhan no",
    "CREATED_AT": ISODate("2019-07-19T03:16:48.353Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313628ad4d73001754a38e"),
    "FUNCTION_ID": "NGHIAVUCUANGUOIPHATHANHHOIPHIEUNHANNO",
    VALUE: "nghia vu cua nguoi phat hanh hoi phieu nhan no",
    "CREATED_AT": ISODate("2019-07-19T03:16:56.821Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d31362fad4d73001754a38f"),
    "FUNCTION_ID": "NGHIAVUCUANGUOIPHATHANHHOIPHIEUNHANNO",
    VALUE: "nghia vu cua nguoi phat hanh hoi phieu nhan no",
    "CREATED_AT": ISODate("2019-07-19T03:17:03.901Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d31364bad4d73001754a390"),
    "FUNCTION_ID": "NGHIAVUCUANGUOICHAPNHANHOIPHIEUDOINO",
    VALUE: "nghia vu cua nguoi chap nhan hoi phieu doi no",
    "CREATED_AT": ISODate("2019-07-19T03:17:31.908Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313652ad4d73001754a391"),
    "FUNCTION_ID": "NGHIAVUCUANGUOICHAPNHANHOIPHIEUDOINO",
    VALUE: "nghia vu cua nguoi chap nhan hoi phieu doi no",
    "CREATED_AT": ISODate("2019-07-19T03:17:38.663Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d31365aad4d73001754a392"),
    "FUNCTION_ID": "NGHIAVUCUANGUOICHAPNHANHOIPHIEUDOINO",
    VALUE: "nghia vu cua nguoi chap nhan hoi phieu doi no",
    "CREATED_AT": ISODate("2019-07-19T03:17:46.458Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d31366bad4d73001754a393"),
    "FUNCTION_ID": "NGHIAVUCUANGUOITHUHOHOIPHIEUDOINO",
    VALUE: "nghia vu cua nguoi thu ho hoi phieu doi no",
    "CREATED_AT": ISODate("2019-07-19T03:18:03.81Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313673ad4d73001754a394"),
    "FUNCTION_ID": "NGHIAVUCUANGUOITHUHOHOIPHIEUDOINO",
    VALUE: "nghia vu cua nguoi thu ho hoi phieu doi no",
    "CREATED_AT": ISODate("2019-07-19T03:18:11.036Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313679ad4d73001754a395"),
    "FUNCTION_ID": "NGHIAVUCUANGUOITHUHOHOIPHIEUDOINO",
    VALUE: "nghia vu cua nguoi thu ho hoi phieu doi no",
    "CREATED_AT": ISODate("2019-07-19T03:18:17.929Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313692ad4d73001754a396"),
    "FUNCTION_ID": "HUONGDANTHUCHIENVENHOTHUHOIPHIEUQUANGUOITHUHO",
    VALUE: "huong dan thuc hien ve nho thu hoi phieu qua nguoi thu ho",
    "CREATED_AT": ISODate("2019-07-19T03:18:42.161Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d3136bcad4d73001754a398"),
    "FUNCTION_ID": "HUONGDANTHUCHIENVENHOTHUHOIPHIEUQUANGUOITHUHO",
    VALUE: "huong dan thuc hien ve nho thu hoi phieu qua nguoi thu ho",
    "CREATED_AT": ISODate("2019-07-19T03:19:24.269Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d3136cead4d73001754a399"),
    "FUNCTION_ID": "HUONGDANTHUCHIENVENHOTHUHOIPHIEUQUANGUOITHUHO",
    VALUE: "nho thu hoi phieu qua nguoi thu ho",
    "CREATED_AT": ISODate("2019-07-19T03:19:42.07Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313734ad4d73001754a39a"),
    "FUNCTION_ID": "KIEMSOAT,XULYHOIPHIEUBIMAT",
    VALUE: "kiem soat xu ly hoi phieu bi mat",
    "CREATED_AT": ISODate("2019-07-19T03:21:24.147Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d31373bad4d73001754a39b"),
    "FUNCTION_ID": "KIEMSOAT,XULYHOIPHIEUBIMAT",
    VALUE: "kiem soat xu ly hoi phieu bi mat",
    "CREATED_AT": ISODate("2019-07-19T03:21:31.645Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313745ad4d73001754a39c"),
    "FUNCTION_ID": "KIEMSOAT,XULYHOIPHIEUBIMAT",
    VALUE: "kiem soat xu ly hoi phieu bi mat",
    "CREATED_AT": ISODate("2019-07-19T03:21:41.016Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313755ad4d73001754a39d"),
    "FUNCTION_ID": "PHIDICHVUTHUHOHOIPHIEU",
    VALUE: "phi dich vu thu ho hoi phieu",
    "CREATED_AT": ISODate("2019-07-19T03:21:57.019Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d31375fad4d73001754a39e"),
    "FUNCTION_ID": "PHIDICHVUTHUHOHOIPHIEU",
    VALUE: "phi dich vu thu ho hoi phieu",
    "CREATED_AT": ISODate("2019-07-19T03:22:07.202Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d313767ad4d73001754a39f"),
    "FUNCTION_ID": "PHIDICHVUTHUHOHOIPHIEU",
    VALUE: "phi dich vu thu ho hoi phieu",
    "CREATED_AT": ISODate("2019-07-19T03:22:15.642Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d3175dd559ea80017284867"),
    "FUNCTION_ID": "HANDBOOK",
    VALUE: "cam nang tu van",
    "CREATED_AT": ISODate("2019-07-19T07:48:45.052Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d3175f9559ea80017284868"),
    "FUNCTION_ID": "GENERAL_COMPLIANCE",
    VALUE: "tuan thu chung",
    "CREATED_AT": ISODate("2019-07-19T07:49:13.175Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("1"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d368112011a790017ac1e98"),
    "FUNCTION_ID": "GREETINGS",
    VALUE: "test gi ma ci ngau the",
    "CREATED_AT": ISODate("2019-07-23T03:37:54.974Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("0"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d36c9c6229bc60017eaa127"),
    "FUNCTION_ID": "LICENSE_MANAGEMENT",
    VALUE: "cap giay phep",
    "CREATED_AT": ISODate("2019-07-23T08:48:06.394Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("0"),
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_AI_TRAINNING").insert([ {
    _id: ObjectId("5d37c2ad65d0fc00173a6e46"),
    "FUNCTION_ID": "LISENSE_REGISTRATION",
    VALUE: "dang ky giay phep kinh doanh",
    "CREATED_AT": ISODate("2019-07-24T02:30:05.712Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "IS_ACTIVE": NumberInt("0"),
    "UPDATED_BY": null
} ]);

// ----------------------------
// Collection structure for MCL_GROUP
// ----------------------------
db.getCollection("MCL_GROUP").drop();
db.createCollection("MCL_GROUP");

// ----------------------------
// Documents of MCL_GROUP
// ----------------------------
db.getCollection("MCL_GROUP").insert([ {
    _id: ObjectId("5d36cf61229bc60017eaa12b"),
    NAME: "                         Nhóm MSB Legal                                   ",
    MEMBERS: [
        {
            "user_ID": "5d2c5da072e61100084c0a8b",
            PSID: "3048424208504702"
        },
        {
            "user_ID": "5d2c5da072e61100084c0a86",
            PSID: "2394866633878537"
        },
        {
            "user_ID": "5d2c5da072e61100084c0a85",
            PSID: "1984892361559396"
        }
    ],
    "CREATED_BY": ObjectId("5d367e92011a790017ac1e95"),
    "CREATED_DATE": ISODate("2019-07-23T09:12:01.776Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);

// ----------------------------
// Collection structure for MCL_HANDBOOK
// ----------------------------
db.getCollection("MCL_HANDBOOK").drop();
db.createCollection("MCL_HANDBOOK");

// ----------------------------
// Documents of MCL_HANDBOOK
// ----------------------------
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed28"),
    QUESTION: " Nếu doanh nghiệp tư nhân và hộ gia đình có nhu cầu cấp tín dụng thì tổ chức tín dụng sẽ cho vay theo hình thức nào?",
    ANSWER: "Theo Khoản 3, Điều 2 của Thông tư 39/2016/TT-NHNN ngày 30/12/2016 của Ngân hàng Nhà nước Việt Nam quy định về hoạt động cho vay của tổ chức tín dụng, chi nhánh ngân hàng nước ngoài đối với khách hàng, Khách hàng vay vốn tại TCTD bao gồm:\r\n“a) Pháp nhân được thành lập và hoạt động tại Việt Nam, pháp nhân được thành lập ở nước ngoài và hoạt động hợp pháp tại Việt Nam;\r\nb) Cá nhân có quốc tịch Việt Nam, cá nhân có quốc tịch nước ngoài.”\r\nTheo quy định trên thì chủ thể được phép vay vốn tại TCTD chỉ bao gồm pháp nhân và cá nhân, quy định này phù hợp với quy định về chủ thể tham gia giao dịch tại Bộ luật Dân sự năm 2015. Vì vậy, trường hợp hộ gia đình và doanh nghiệp tư nhân có nhu cầu cấp tín dụng để phục vụ hoạt động sản xuất kinh doanh chung của hộ gia đình và của doanh nghiệp tư nhân thì TCTD xem xét cho vay đối với cá nhân chủ doanh nghiệp tư nhân, từng cá nhân của hộ gia đình. Trong phương án và mục đích sử dụng vốn vay của Khách hàng có thể ghi là phục vụ hoạt động kinh doanh của doanh nghiệp tư nhân, hoạt động sản xuất chung của hộ gia đình.\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed29"),
    QUESTION: "Công ty A đang có khoản nợ tại Ngân hàng B, Ngân hàng C có được phép cho Công ty A vay để trả nợ Ngân hàng B không?",
    ANSWER: "Điều 8 Thông tư 39/2016/TT-NHNN ngày 30/12/2016 của Ngân hàng Nhà nước Việt Nam quy định về hoạt động cho vay của tổ chức tín dụng, chi nhánh ngân hàng nước ngoài đối với khách hàng quy định những nhu cầu vốn không được cho vay thì TCTD chỉ được phép cho Khách hàng vay để trả nợ khoản cấp tín dụng tại TCTD khác trước hạn với điều kiện khoản vay này phải đáp ứng đầy đủ các yêu cầu sau đây:\r\n(i) Là khoản vay phục vụ hoạt động kinh doanh; \r\n(ii) Thời hạn cho vay không vượt quá thời hạn cho vay còn lại của khoản vay cũ;\r\n(iii) Là khoản vay chưa thực hiện cơ cấu lại thời hạn trả nợ. \r\nNhư vậy, Ngân hàng C chỉ được phép cho Công ty A vay để trả nợ khoản cấp tín dụng tại Ngân hàng B khi khoản vay này đáp ứng được đầy đủ các điều kiện như nêu trên. Ngân hàng C phải xem xét hồ sơ/khoản nợ của Công ty A tại Ngân hàng B để đánh giá trước khi cấp tín dụng.\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed34"),
    QUESTION: "Khi KHDN thay đổi thông tin về tên doanh nghiệp thì có phải thông báo cho Ngân hàng nơi mở tài khoản thanh toán không?",
    ANSWER: "Theo quy định tại điểm đ, Khoản 2 Điều 5 Quyền và nghĩa vụ của chủ tài khoản thanh toán, Thông tư 23/2014/TT-NHNN ngày 19/08/2014 của Ngân hàng Nhà nước Việt Nam hướng dẫn mở, sử dụng tài khoản thanh toán tại tổ chức cung ứng dịch vụ thanh toán đã được sửa đổi, bổ sung bởi Thông tư 32/2016/TT-NHNN ngày 26/12/2016 của Ngân hàng Nhà nước Việt Nam sửa đổi Thông tư 23/2014/TT-NHNN hướng dẫn việc mở và sử dụng tài khoản thanh toán tại tổ chức cung ứng dịch vụ thanh toán thì Chủ tài khoản thanh toán có nghĩa vụ: \r\n“đ) Cung cấp đầy đủ, rõ ràng, chính xác các thông tin liên quan về mở và sử dụng tài khoản thanh toán. Thông báo kịp thời và gửi các giấy tờ liên quan cho tổ chức cung ứng dịch vụ thanh toán nơi mở tài khoản khi có sự thay đổi về thông tin trong hồ sơ mở tài khoản thanh toán. Việc thay đổi thông tin về tài khoản thanh toán mở tại Ngân hàng Nhà nước thực hiện theo Phụ lục số 03 đính kèm Thông tư này”.\r\nNhư vậy, theo quy định nêu trên, khi KHDN thay đổi thông tin về tên Doanh nghiệp thì cần phải thông báo kịp thời và gửi giấy tờ liên quan cho Ngân hàng nơi mở tài khoản thanh toán để cập nhật thông tin thay đổi.\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed2a"),
    QUESTION: "Những đối tượng nào được phép vay tại TCTD để thực hiện hoạt động đầu tư ra nước ngoài?",
    ANSWER: "Theo quy định tại Điều 5 Thông tư 36/2018/TT-NHNN ngày 25/12/2018 quy định về hoạt động cho vay để đầu tư ra nước ngoài của TCTD, chi nhánh ngân hàng nước ngoài đối với khách hàng (có hiệu lực từ 15/02/2019) thì TCTD được xem xét, quyết định cho vay để đầu tư ra nước ngoài khi khách hàng đáp ứng các điều kiện sau đây: \\n\r\n(1) Khách hàng là pháp nhân có năng lực pháp luật dân sự theo quy định của pháp luật. Khách hàng là cá nhân (bao gồm cả cá nhân là thành viên hoặc người đại diện được ủy quyền của hộ gia đình, tổ hợp tác và tổ chức khác không có tư cách pháp nhân) từ đủ 18 tuổi trở lên có năng lực hành vi dân sự đầy đủ theo quy định của pháp luật. \\n\r\n(2) Đã được cấp Giấy chứng nhận đăng ký đầu tư ra nước ngoài và hoạt động đầu tư đã được cơ quan có thẩm quyền của nước tiếp nhận đầu tư chấp thuận hoặc cấp phép. Trường hợp pháp luật của nước tiếp nhận đầu tư không quy định về việc cấp phép đầu tư hoặc chấp thuận đầu tư, nhà đầu tư phải có tài liệu chứng minh quyền hoạt động đầu tư tại nước tiếp nhận đầu tư. \\n\r\n(3) Có dự án, phương án đầu tư ra nước ngoài được TCTD đánh giá là khả thi và khách hàng có khả năng trả nợ TCTD. \\n\r\n(4) Có 2 năm liên tiếp không phát sinh nợ xấu tính đến thời điểm đề nghị vay vốn. \\n\r\nNhư vậy, những đối tượng đáp ứng đầy đủ các điều kiện nêu trên sẽ được phép vay tại TCTD để thực hiện hoạt động đầu tư ra nước ngoài. \\n\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed2b"),
    QUESTION: "Ngân hàng cho Khách hàng vay bằng ngoại tệ trong những trường hợp nào?",
    ANSWER: "Theo quy định tại Điều 3 Thông tư 24/2015/TT-NHNN ngày 08/12/2015 của Ngân hàng Nhà nước Việt Nam quy định cho vay bằng ngoại tệ của TCTD, chi nhánh ngân hàng nước ngoài đối với khách hàng vay là người cư trú (đã được sửa đổi bởi Thông tư 42/2018/TT-NHNN, hiệu lực từ 01/01/2019) thì TCTD được xem xét quyết định cho vay bằng ngoại tệ đối với các nhu cầu vốn như sau:\r\na) Cho vay ngắn hạn để thanh toán ra nước ngoài tiền nhập khẩu hàng hóa, dịch vụ nhằm thực hiện phương án sản xuất, kinh doanh hàng hóa để phục vụ nhu cầu trong nước khi khách hàng vay có đủ ngoại tệ từ nguồn thu sản xuất, kinh doanh để trả nợ vay. Quy định này thực hiện đến hết ngày 31 tháng 03 năm 2019; \\n\r\nb) Cho vay ngắn hạn để thanh toán ra nước ngoài tiền nhập khẩu hàng hóa, dịch vụ nhằm thực hiện phương án sản xuất, kinh doanh hàng hóa xuất khẩu qua cửa khẩu biên giới Việt Nam khi khách hàng vay có đủ ngoại tệ từ nguồn thu sản xuất, kinh doanh để trả nợ vay; \r\nc) Cho vay trung hạn và dài hạn để thanh toán ra nước ngoài tiền nhập khẩu hàng hóa, dịch vụ khi khách hàng vay có đủ ngoại tệ từ nguồn thu sản xuất, kinh doanh để trả nợ vay. Quy định này thực hiện đến hết ngày 30 tháng 09 năm 2019; \r\nd) Cho vay ngắn hạn đối với doanh nghiệp đầu mối nhập khẩu xăng dầu được Bộ Công thương giao hạn mức nhập khẩu xăng dầu hàng năm để thanh toán ra nước ngoài tiền nhập khẩu xăng dầu khi doanh nghiệp đầu mối nhập khẩu xăng dầu không có hoặc không có đủ nguồn thu ngoại tệ từ hoạt động sản xuất, kinh doanh để trả nợ vay; \r\ne) Cho vay ngắn hạn để đáp ứng các nhu cầu vốn ở trong nước nhằm thực hiện phương án sản xuất, kinh doanh hàng hóa xuất khẩu qua cửa khẩu biên giới Việt Nam mà khách hàng vay có đủ ngoại tệ từ nguồn thu xuất khẩu để trả nợ vay. Khi được tổ chức tín dụng, chi nhánh ngân hàng nước ngoài giải ngân vốn cho vay, khách hàng vay phải bán số ngoại tệ vay đó cho tổ chức tín dụng, chi nhánh ngân hàng nước ngoài cho vay theo hình thức giao dịch hối đoái giao ngay, trừ trường hợp nhu cầu vay vốn của khách hàng để thực hiện giao dịch thanh toán mà pháp luật quy định đồng tiền thanh toán phải bằng ngoại tệ; \r\nf) Cho vay để đầu tư ra nước ngoài đối với các dự án đầu tư được Quốc hội hoặc Thủ tướng Chính phủ quyết định chủ trương đầu tư ra nước ngoài và đã được Bộ Kế hoạch và Đầu tư cấp giấy chứng nhận đăng ký đầu tư ra nước ngoài.\r\nCần lưu ý là đối với các nhu cầu vốn ngoài các trường hợp nêu trên mà thuộc lĩnh vực ưu tiên, khuyến khích phát triển sản xuất, kinh doanh theo quy định tại các Nghị quyết, Nghị định, Quyết định, Chỉ thị và các văn bản khác của Chính phủ, Thủ tướng Chính phủ thì TCTD, chi nhánh ngân hàng nước ngoài chỉ được xem xét quyết định cho vay bằng ngoại tệ sau khi được NHNN chấp thuận bằng văn bản đối với từng trường hợp cụ thể.\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed2c"),
    QUESTION: "Những khoản vay nước ngoài nào phải đăng ký với Ngân hàng Nhà nước? Cách xác định thời hạn của khoản vay?",
    ANSWER: "Điều 9 Thông tư 03/2016/TT-NHNN ngày 26/02/2016 của Ngân hàng Nhà nước Việt Nam hướng dẫn về quản lý ngoại hối đối với việc vay, trả nợ nước ngoài của doanh nghiệp (đã được sửa đổi, bổ sung bởi Thông tư 05/2016/TT-NHNN ngày 15/04/2016 của Ngân hàng Nhà nước Việt Nam hướng dẫn về nội dung quản lý ngoại hối đối với việc vay, trả nợ nước ngoài của doanh) quy định khoản vay thuộc đối tượng phải thực hiện đăng ký với Ngân hàng Nhà nước bao gồm:\r\n(i) Khoản vay trung, dài hạn nước ngoài. Theo quy định tại Thông tư 39/2016/TT-NHNN ngày 30/12/2016 của Ngân hàng Nhà nước Việt Nam Quy định về hoạt động cho vay của tổ chức tín dụng, chi nhánh ngân hàng nước ngoài đối với khách hàng thì khoản vay trung hạn là khoản vay có thời hạn cho vay trên 01 (một) năm và tối đa 05 (năm) năm, khoản vay dài hạn là khoản vay có thời hạn cho vay trên 05 (năm) năm.\r\n(ii) Khoản vay ngắn hạn được gia hạn mà tổng thời hạn của Khoản vay là trên 01 (một) năm.\r\n(iii) Khoản vay ngắn hạn không có hợp đồng gia hạn nhưng còn dư nợ gốc tại thời điểm tròn 01 (một) năm kể từ ngày rút vốn đầu tiên, trừ trường hợp Bên đi vay hoàn thành việc trả nợ Khoản vay trong thời gian 10 (mười) ngày kể từ thời Điểm tròn 01 (một) năm kể từ ngày rút vốn đầu tiên.\r\nVề cách tính thời hạn khoản vay để xác định nghĩa vụ đăng ký với Ngân hàng Nhà nước, Điều 10 Thông tư 03/2016/TT-NHNN quy định cách tính như sau:\r\n(i) Đối với các Khoản vay trung hạn, dài hạn thì thời hạn khoản vay được xác định từ ngày dự kiến rút vốn đầu tiên đến ngày dự kiến trả nợ cuối cùng trên cơ sở các quy định tại thỏa thuận vay nước ngoài.\r\n(ii) Đối với các khoản vay ngắn hạn thì thời hạn khoản vay được xác định từ ngày rút vốn đầu tiên đến ngày dự kiến trả nợ cuối cùng trên cơ sở các quy định tại thỏa thuận vay nước ngoài, thỏa thuận gia hạn vay nước ngoài.\r\n(iii) Đối với các khoản vay ngắn hạn không được gia hạn nhưng còn dư nợ gốc tại thời điểm tròn 01 năm kể từ ngày rút vốn đầu tiên thì thời hạn Khoản vay được xác định từ ngày rút vốn đầu tiên đến ngày dự kiến trả nợ cuối cùng.\r\nLưu ý: Ngày rút vốn là ngày giải ngân tiền vay đối với các khoản vay giải ngân bằng tiền, ngày thông quan hàng hóa đối với các khoản vay dưới hình thức thuê tài chính nước ngoài phù hợp với các quy định của Pháp luật có liên quan.\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed2d"),
    QUESTION: " Anh A nhận chuyển nhượng quyền sử dụng đất vào năm 2008, trước thời kỳ hôn nhân (anh A kết hôn với chị B vào năm 2013). Hiện anh A muốn thế chấp tài sản này để bảo đảm cho khoản vay của anh A tại MSB. Trong trường hợp này, chị B có phải ký tên trên hợp đồng thế chấp hay không?",
    ANSWER: "Điều 43 của Luật Hôn nhân và gia đình năm 2014 quy định về tài sản riêng của vợ, chồng như sau:\r\n“1. Tài sản riêng của vợ, chồng gồm tài sản mà mỗi người có trước khi kết hôn; tài sản được thừa kế riêng, được tặng cho riêng trong thời kỳ hôn nhân; tài sản được chia riêng cho vợ, chồng theo quy định tại các điều 38, 39 và 40 của Luật này; tài sản phục vụ nhu cầu thiết yếu của vợ, chồng và tài sản khác mà theo quy định của Pháp luật thuộc sở hữu riêng của vợ, chồng.\r\n2. Tài sản được hình thành từ tài sản riêng của vợ, chồng cũng là tài sản riêng của vợ, chồng. Hoa lợi, lợi tức phát sinh từ tài sản riêng trong thời kỳ hôn nhân được thực hiện theo quy định tại khoản 1 Điều 33 và khoản 1 Điều 40 của Luật này.”\r\nNhư vậy, trong trường hợp này có thể xác định rằng quyền sử dụng đất mà anh A nhận chuyển nhượng vào năm 2008 là tài sản riêng của anh A, trừ khi anh A và chị B có thỏa thuận nhập tài sản này vào tài sản chung (và cần đăng ký thay đổi chủ sở hữu quyền sử dụng đất).\r\nTuy nhiên, khi nhận thế chấp quyền sử dụng đất, phù hợp với quy định của Pháp luật, thì MSB vẫn yêu cầu, quy định rằng hoa lợi, lợi tức từ quyền sử dụng đất cũng là tài sản thế chấp. Theo quy định tại Điều 33 của Luật Hôn nhân và gia đình năm 2014, thì hoa lợi, lợi tức phát sinh từ tài sản riêng và thu nhập hợp pháp khác trong thời kỳ hôn nhân cũng thuộc tài sản chung của vợ, chồng. Ngoài ra, nếu quyền sử dụng đất là nơi đang sinh sống chung của vợ chồng thì thực tế thường có cải tạo, sửa chữa, cơi nới nhà ở, công trình, tài sản gắn liền với đất là tài sản chung phục vụ cuộc sống, sinh hoạt gia đình mà không có đăng ký quyền sở hữu tài sản rõ ràng trên Giấy chứng nhận QSDĐ, quyền sở hữu nhà ở và tài sản khác gắn liền với đất.\r\nDo vậy, để hạn chế rủi ro pháp lý trong trường hợp các bên có tranh chấp tài sản chung của vợ chồng (liên quan đến lợi tức phát sinh từ tài sản; tài sản chung của vợ chồng được dùng để xây, cải tạo, sửa chữa thêm trên đất mà không đăng ký quyền sở hữu; có thỏa thuận về gộp tài sản chung mà chưa đăng ký…), Đơn vị nên thực hiện theo phương án sau:\r\nChỉ anh A ký tên vào hợp đồng thế chấp; và có thêm cam kết (có công chứng/chứng thực) của chị B về việc: (i) quyền sử dụng đất và tài sản gắn liền là tài sản riêng của anh A có trước thời kỳ hôn nhân, không được nhập vào thành tài sản chung của hai vợ chồng; (ii) anh A được toàn quyền định đoạt, thế chấp đối tài sản này, các tài sản gắn liền với đất đã, đang, sẽ hình thành trong tương lai và hoa lợi, lợi tức phát sinh từ tài sản.\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed2e"),
    QUESTION: " Anh A được cha mẹ tặng cho quyền sử dụng đất vào năm 2015, trong thời kì hôn nhân với chị B. Hiện anh A muốn thế chấp quyền sử dụng đất này cho MSB thì hợp đồng thế chấp chỉ cần chữ ký của anh A hay cả chị B?",
    ANSWER: "Theo quy định tại Điều 43 của Luật Hôn nhân và gia đình năm 2014 thì tài sản được tặng cho riêng trong thời kỳ hôn nhân sẽ là tài sản riêng của vợ, chồng. Do vậy, việc thế chấp quyền sử dụng đất được tặng cho riêng, về nguyên tắc sẽ chỉ cần chữ ký của cá nhân chủ sở hữu tài sản (anh A). Tuy nhiên, khi nhận thế chấp quyền sử dụng đất, phù hợp với quy định của Pháp luật, thì MSB vẫn yêu cầu, quy định rằng hoa lợi, lợi tức từ quyền sử dụng đất cũng là tài sản thế chấp. Theo quy định tại Điều 33 của Luật Hôn nhân và gia đình năm 2014 thì hoa lợi, lợi tức phát sinh từ tài sản riêng và thu nhập hợp pháp khác trong thời kỳ hôn nhân cũng thuộc tài sản chung của vợ, chồng. Ngoài ra, nếu quyền sử dụng đất là nơi đang sinh sống chung của vợ chồng thì thực tế thường có cải tạo, sửa chữa, cơi nới nhà ở, công trình, tài sản gắn liền với đất là tài sản chung phục vụ cuộc sống, sinh hoạt gia đình mà không có đăng ký quyền sở hữu tài sản rõ ràng trên Giấy chứng nhận QSDĐ, quyền sở hữu nhà ở và tài sản khác gắn liền với đất.\r\nDo vậy, để hạn chế rủi ro pháp lý trong trường hợp các bên có tranh chấp tài sản chung của vợ chồng (liên quan đến lợi tức phát sinh từ tài sản; tài sản chung của vợ chồng được dùng để xây, cải tạo, sửa chữa thêm trên đất mà không đăng ký quyền sở hữu; có thỏa thuận về gộp tài sản chung mà chưa đăng ký…), Đơn vị nên thực hiện theo phương án sau:\r\nChỉ anh A ký tên vào hợp đồng thế chấp; và có thêm cam kết (có công chứng/chứng thực) của chị B về việc: (i) quyền sử dụng đất và tài sản gắn liền là tài sản riêng của anh A được cha mẹ tặng cho trong thời kỳ hôn nhân, không được nhập vào thành tài sản chung của hai vợ chồng; (ii) anh A được toàn quyền định đoạt, thế chấp đối tài sản này, các tài sản gắn liền với đất đã, đang, sẽ hình thành trong tương lai và hoa lợi, lợi tức phát sinh từ tài sản.\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed2f"),
    QUESTION: "Quyền sử dụng đất của Khách hàng đã được thế chấp tại MSB. Hiện Khách hàng có nhu cầu đầu tư, xây dựng nhà ở trên đất để cho thuê và Sở xây dựng đang yêu cầu MSB phải có văn bản chấp thuận về việc này thì Sở xây dựng mới cấp Giấy phép xây dựng. MSB có thể làm văn bản chấp thuận để Khách hàng xin giấy phép xây dựng hay không?",
    ANSWER: "Theo quy định tại Điều 321.2 của Bộ Luật Dân sự 2015 thì bên thế chấp được đầu tư để làm tăng giá trị của tài sản thế chấp. Thông thường các hợp đồng thế chấp quyền sử dụng đất theo mẫu của MSB cũng có điều khoản quy định về nội dung này, cụ thể: \r\n- Một số mẫu hợp đồng thế chấp của MSB đang có quy định là: Bên bảo đảm có quyền: Đầu tư hoặc cho bên thứ ba đầu tư vào tài sản bảo đảm nếu được MSB đồng ý bằng văn bản;\r\n- Một số mẫu hợp đồng thế chấp khác lại quy định: Bên bảo đảm có quyền: Được đầu tư hoặc cho người thứ ba đầu tư vào Tài sản thế chấp để làm tăng giá trị của Tài sản thế chấp nhưng phải thông báo cho MSB biết trước bằng văn bản và toàn bộ giá trị đầu tư cũng thuộc về Tài sản thế chấp, trừ trường hợp các bên có thoả thuận khác bằng văn bản.\r\nNhư vậy, điều kiện để Khách hàng được phép đầu tư xây dựng nhà ở trên đất đã thế chấp có cần phải có một văn bản chấp thuận từ MSB hay không là phụ thuộc vào thỏa thuận chi tiết tại Hợp đồng thế chấp.MSB chấp thuận cho Khách hàng đầu tư xây dựng tài sản trên đất (làm văn bản chấp thuận để Khách hàng xin Giấy phép xây dựng) với điều kiện tài sản xây dựng phải được thế chấp cho MSB (yêu cầu Khách hàng cam kết về thế chấp; hoàn tất thủ tục sửa đổi, bổ sung Hợp đồng thế chấp khi hoàn công xây dựng).\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed30"),
    QUESTION: "Khách hàng dự kiến thế chấp nhà ở hình thành trong tương lai. Tuy nhiên, Khách hàng không mua trực tiếp nhà ở từ Chủ đầu tư mà lại nhận chuyển nhượng từ tổ chức khác, Văn bản chuyển nhượng hợp đồng mua bán nhà ở không được công chứng. Trong trường hợp này, văn bản chuyển nhượng có hiệu lực pháp lý hay không và những trường hợp nào thì văn bản chuyển nhượng hợp đồng mua bán nhà ở phải được công chứng.\r\n",
    ANSWER: "Theo quy định tại Điều 122 Luật Nhà ở năm 2014 thì các giao dịch nhà ở, chuyển nhượng hợp đồng mua bán nhà ở thương mại bắt buộc phải được công chứng, chứng thực trừ một vài trường hợp đặc biệt liên quan đến giao dịch nhà tình nghĩa, tình thương, nhà ở xã hội, cho thuê cho mượn nhà ở.\r\nTuy nhiên, Luật Nhà ở năm 2014 cũng quy định rằng giao dịch mua bán nhà ở thương mại của doanh nghiệp kinh doanh bất động sản thì thực hiện theo quy định của Pháp luật kinh doanh bất động sản. Phù hợp với quy định đó, Điều 17 Luật kinh doanh bất động sản 2014 và Điều 33.2 Thông tư số 19/2016/TT-BXD ngày 30/06/2016 của Bộ Xây dựng hướng dẫn thực hiện Luật Nhà ở và Nghị định 99/2015/NĐ-CP hướng dẫn Luật Nhà ở có quy định: Văn bản chuyển nhượng hợp đồng mua bán nhà ở hình thành trong tương lai mà một bên là doanh nghiệp có chức năng kinh doanh bất động sản thì không bắt buộc phải công chứng, chứng thực.\r\nNhư vậy:\r\n- Văn bản chuyển nhượng hợp đồng mua bán nhà ở mà một bên là doanh nghiệp có chức năng kinh doanh bất động sản thì không bắt buộc phải công chứng, chứng thực. Các bên thực hiện công chứng, chứng thực trên cơ sở nhu cầu tự nguyện.\r\n- Văn bản chuyển nhượng hợp đồng giữa các cá nhân, hộ gia đình, tổ chức khác thì bắt buộc phải được công chứng, chứng thực.\r\nTại trường hợp thực tế thì MSB cần kiểm tra lại về thông tin chuyển nhượng của các bên (là tổ chức, hay cá nhân, có chức năng kinh doanh bất động sản hay không…) và đối chiếu quy định nêu trên để xác định xem văn bản chuyển nhượng hợp đồng có bắt buộc phải công chứng, chứng thực hay không.\r\nNgoài ra, MSB cũng cần lưu ý nội dung sau khi kiểm tra hồ sơ tài sản bảo đảm đó là: Việc chuyển nhượng hợp đồng mua bán, thuê mua nhà ở hình thành trong tương lai phải được xác nhận của chủ đầu tư vào văn bản chuyển nhượng (Điều 59.1 Luật kinh doanh bất động sản 2014.\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed31"),
    QUESTION: " Nhà ở dự án thuộc sở hữu người nước ngoài có được thế chấp vay vốn không? Có gặp giới hạn gì về thời gian sở hữu tài sản không?",
    ANSWER: "Theo quy định tại Luật nhà ở 2014 thì người nước ngoài được phép nhập cảnh vào Việt Nam, không thuộc diện được hưởng quyền ưu đãi, miễn trừ ngoại giao, lãnh sự là đối tượng được sở hữu nhà ở tại Việt Nam:\r\n- Nhà ở mà người nước ngoài được sở hữu chỉ bao gồm căn hộ chung cư và nhà ở riêng lẻ trong dự án đầu tư xây dựng nhà ở thương mại (Điều 159, Điều 160).\r\n- Thời gian sở hữu nhà ở của người nước ngoài tối đa không quá 50 năm kể từ ngày được cấp giấy chứng nhận và có thể được gia hạn thêm theo quy định của pháp luật (Khoản 2 Điều 161). \r\n- Người nước ngoài sở hữu nhà ở tại Việt Nam có các quyền của chủ sở hữu nhà ở như công dân Việt Nam trong đó có quyền được thế chấp nhà ở theo quy định tại Điều 10. Tuy nhiên, người nước ngoài sở hữu nhà ở tại Việt Nam cũng có 1 số hạn chế nhất định theo quy định tại khoản 2 Điều 161. \r\n- Việc thế chấp nhà ở dự án tùy hình thức sẽ có những rủi ro pháp lý riêng theo các sản phẩm đặc thù của Ngân hàng. VD: Pháp luật Nhà ở không có quy định cụ thể cho phép thế chấp quyền và lợi ích phát sinh từ HĐ mua bán nhà ở dự án hình thành trong tương lai…\r\n- Bên cạnh việc đáp ứng các quy định của pháp luật thì Khách hàng là người nước ngoài phải đảm bảo đáp ứng các quy định nội bộ của MSB. Theo QĐ.TD.152 về cho vay mua căn hộ chung cư, biệt thự, nhà liền kề thuộc dự án bất động sản thì một trong các điều kiện áp dụng với Khách hàng là người nước ngoài phải có thời gian cư trú/làm việc tại Việt Nam liên tục tối thiểu 36 tháng.\r\n- Như vậy, Người nước ngoài sở hữu nhà ở tại Việt Nam được thế chấp để vay vốn tại các TCTD. Thời gian sở hữu nhà ở tối đa là 50 năm, nhưng có thể được gia hạn theo quy định của Pháp luật.\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed32"),
    QUESTION: " Khách hàng là cá nhân người Việt Nam dùng Giấy chứng minh công an nhân dân, Giấy chứng minh sĩ quan quân đội để mở tài khoản thì có phù hợp với quy định của Pháp luật và của MSB không?",
    ANSWER: "Theo quy định tại điểm b khoản 1 Điều 12 “Hồ sơ mở tài khoản thanh toán”, Thông tư 23/2014/TT-NHNN ngày 19/08/2014 của Ngân hàng Nhà nước Việt Nam hướng dẫn mở, sử dụng tài khoản thanh toán tại tổ chức cung ứng dịch vụ thanh toán đã được sửa đổi, bổ sung bởi Thông tư 32/2016/TT-NHNN ngày 26/12/2016 của Ngân hàng Nhà nước Việt Nam sửa đổi Thông tư 23/2014/TT-NHNN hướng dẫn việc mở và sử dụng tài khoản thanh toán tại tổ chức cung ứng dịch vụ thanh toán thì Khách hàng là cá nhân phải cung cấp giấy tờ tùy thân như sau:\r\n“b) Thẻ căn cước công dân hoặc giấy chứng minh nhân dân hoặc hộ chiếu còn thời hạn, giấy khai sinh (đối với cá nhân là công dân Việt Nam chưa đủ 14 tuổi), thị thực nhập cảnh hoặc giấy tờ chứng minh được miễn thị thực nhập cảnh (đối với cá nhân là người nước ngoài) của chủ tài khoản”.\r\nTheo quy định tại Phụ lục 01A, QD.DV.002 Quy định mở. sử dụng và quản lý tài khoản thanh toán thì Khách hàng phải cung cấp giấy tờ tùy thân như sau:\r\n- Chứng minh thư nhân dân hoặc; \r\n- Hộ chiếu hoặc; \r\n- Thẻ căn cước công dân hoặc;\r\n- Giấy khai sinh (đối với Khách hàng cá nhân và công dân Việt Nam chưa đủ 15 tuồi).\r\nNhư vậy, theo quy dịnh của MSB thì Khách hàng phải cung cấp Chứng minh thư nhân dân/Thẻ căn cước công dân/Hộ chiếu/Giấy khai sinh mà không chấp nhận Giấy chứng minh công an nhân dân, Giấy chứng minh sĩ quan quân đội, vì 02 giấy tờ này chỉ dùng để chứng minh người được cấp Giấy chứng minh là sĩ quan quân đội hoặc công an đang phục vụ tại ngũ, và trên thực tế khó xác định được hiệu lực của các giấy tờ này.\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed33"),
    QUESTION: "Chủ tài khoản có thể ủy quyền cho Kế toán trưởng ký đồng thời chữ ký người đại diện hợp pháp và chữ ký Kế toán trưởng trên “Giấy đề nghị mở và sử dụng tài khoản” và chứng từ kế toán không?",
    ANSWER: "1. Theo quy định tại điểm c, khoản 2 Điều 8 về Ký chứng từ kế toán ngân hàng, Quyết định 1789/2005/QĐ-NHNN ngày 12/12/2005 quy định về chế độ chứng từ kế toán ngân hàng thì:\r\n“c. Đối với chứng từ giao dịch với ngân hàng được lập bởi Khách hàng là những đơn vị, tổ chức phải bố trí kế toán trưởng theo quy định của Pháp luật thì trên chứng từ bắt buộc phải có đủ chữ ký của chủ tài khoản, kế toán trưởng hoặc người được uỷ quyền ký thay và dấu đơn vị (nếu là chứng từ bằng giấy)”. \r\n2. Theo Khoản 6, Điều 118, Thông tư số 200/2014/TT-BTC hướng dẫn Chế độ kế toán Doanh nghiệp có quy định về lập và ký chứng từ kế toán như sau:\r\n“Kế toán trưởng (hoặc người được uỷ quyền) không được ký “thừa uỷ quyền” của người đứng đầu doanh nghiệp. Người được uỷ quyền không được uỷ quyền lại cho người khác.”\r\n3. Theo điểm d, khoản 1, Điều 13, QĐ.DV.002 Quy định mở, sử dụng và quản lý tài khoản thanh toán thì\r\n“Một cá nhân không được đồng thời đăng ký chữ ký thuộc hai nhóm chữ ký khác nhau trên một chứng từ giao dịch liên quan đến tài khoản thanh toán”. \r\n Như vậy, Theo các quy định trên, đối với các Doanh nghiệp thuộc trường hợp bắt buộc phải có Kế toán trưởng/Người phụ trách kế toán thì trên các chứng từ kế toán phải có đầy đủ hai chữ ký “Kế toán trưởng” và “Người đại diện hợp pháp” theo mẫu đã đăng ký với ngân hàng. Kế toán trưởng không được đăng ký chữ ký đồng thời 02 tư cách ở cả nhóm chữ ký vừa là “Người đại diện hợp pháp”, vừa là “Kế toán trưởng”. \r\n\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed35"),
    QUESTION: "Khách hàng có thể lập văn bản ủy quyền ngoài MSB không?",
    ANSWER: "Theo quy định tại khoản 2 Điều 4 Ủy quyền trong sử dụng tài khoản thanh toán, Thông tư 23/2014/TT-NHNN ngày 19/08/2014 của Ngân hàng Nhà nước Việt Nam hướng dẫn mở, sử dụng tài khoản thanh toán tại tổ chức cung ứng dịch vụ thanh toán đã được sửa đổi, bổ sung bởi Thông tư 32/2016/TT-NHNN ngày 26/12/2016 của Ngân hàng Nhà nước Việt Nam sửa đổi Thông tư 23/2014/TT-NHNN hướng dẫn việc mở và sử dụng tài khoản thanh toán tại tổ chức cung ứng dịch vụ thanh toán thì:\r\n“2. Việc ủy quyền trong sử dụng tài khoản thanh toán phải bằng văn bản và được thực hiện theo quy định của Pháp luật về ủy quyền.”\r\nTheo quy định tại điểm b khoản 1 Điều 18 Ủy quyền sử dụng tài khoản, QD.DV.002 Quy định mở, sử dụng và quản lý tài khoản thanh toán thì MSB chấp nhận việc ủy quyền được lập ngoài MSB, nhưng văn bản ủy quyền phải được lập tại:\r\n- Tại UBND xã, phương, thị trấn, quận, huyện (trường hợp chứng thực chữ ký) hoặc tại Phòng công chứng/Văn phòng công chứng (trường hợp công chứng văn bản ủy quyền;\r\n- Tại cơ quan đại diện ngoại giao, cơ quan lãnh sự của Việt Nam ở nước ngoài (đối với cá nhân Việt Nam ở nước ngoài); hoặc tại cơ quan có thẩm quyền chứng thực tại nước mà người ủy quyền mang quốc tịch; hoặc tại cơ quan đại diện ngoại giao, cơ quan lãnh sự của nước mà người ủy quyền mang quốc tịch tại quốc gia thứ ba (đối với cá nhân nước ngoài không ở Việt Nam).\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed36"),
    QUESTION: "Khách hàng Doanh nghiệp có được sử dụng song song cả 02 tên (tên cũ và tên mới) sau khi đã thực hiện thủ tục thay đổi tên trên Đăng ký kinh doanh không?",
    ANSWER: "Theo quy định tại khoản 2 Điều 13 Thông tư 23/2014/TT-NHNN ngày 19/08/2014 của Ngân hàng Nhà nước Việt Nam hướng dẫn mở, sử dụng tài khoản thanh toán tại tổ chức cung ứng dịch vụ thanh toán đã được sửa đổi, bổ sung bởi Thông tư 32/2016/TT-NHNN ngày 26/12/2016 của Ngân hàng Nhà nước Việt Nam sửa đổi Thông tư 23/2014/TT-NHNN hướng dẫn việc mở và sử dụng tài khoản thanh toán tại tổ chức cung ứng dịch vụ thanh toán thì:\r\n“2. Giấy đề nghị mở tài khoản thanh toán của tổ chức phải có đủ những nội dung chủ yếu sau:\r\na) Tên giao dịch đầy đủ và viết tắt; địa chỉ đặt trụ sở chính, địa chỉ giao dịch, số điện thoại, lĩnh vực hoạt động, kinh doanh”.\r\nTheo quy định tại Điểm đ khoản 2 Điều 5 “Quyền và nghĩa vụ của chủ tài khoản thanh toán” Thông tư 23/2014/TT-NHNN được sửa đổi, bổ sung thì Chủ tài khoản có nghĩa vụ: \r\n“đ) Cung cấp đầy đủ, rõ ràng, chính xác các thông tin liên quan về mở và sử dụng tài khoản thanh toán. Thông báo kịp thời và gửi các giấy tờ liên quan cho tổ chức cung ứng dịch vụ thanh toán nơi mở tài khoản khi có sự thay đổi về thông tin trong hồ sơ mở tài khoản thanh toán. Việc thay đổi thông tin về tài khoản thanh toán mở tại Ngân hàng Nhà nước thực hiện theo Phụ lục số 03 đính kèm Thông tư này;\"\r\nMặc dù theo quy định trên tại Thông tư 23/2014/TT-NHNN không quy định rõ về tên của chủ tài khoản nhưng sẽ hiểu rằng thông tin về chủ tài khoản phải trùng khớp với thông tin của về tên của tổ chức ghi trong giấy tờ chứng minh về việc tổ chức được thành lập.\r\nĐồng thời, theo quy định của MSB tại khoản 4 Điều 12 về Giấy đề nghị mở và sử dụng tài khoản thanh toán, QD.DV.002 Quy định mở. sử dụng và quản lý tài khoản thanh toán thì:\r\n“4. Tên đăng ký trên Giấy đề nghị mở và sử dụng tài khoản thanh toán phải khớp đúng với tên tổ chức ghi trong giấy tờ chứng minh về việc tổ chức được thành lập (đối với tài khoản của tổ chức) hoặc tên của người mở tài khoản trên Giấy tờ tùy thân (đối với tài khoản của cá nhân). Đối với tài khoản thanh toán chung, tên đăng ký trên Giấy đề nghị mở và sử dụng tài khoản thanh toán phải khớp đúng với tên của các chủ thể đứng tên mở tài khoản thanh toán chung”.\r\nNhư vậy, theo quy định của Pháp luật và MSB thì tên tài khoản phải trùng khớp với tên của Khách hàng trên Đăng ký kinh doanh, do đó sau khi Khách hàng thực hiện thủ tục thay đổi tên trên Đăng ký kinh doanh thì Khách hàng phải làm thủ tục thay đổi tên chủ tài khoản tại MSB, do đó Khách hàng không được dùng song song 02 tên.",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed37"),
    QUESTION: "Tiền gửi ngân hàng nói chung và tiền gửi tiết kiệm nói riêng bằng ngoại tệ của cá nhân có được bảo hiểm hay không?",
    ANSWER: "Theo quy định tại Điều 18 Luật Bảo hiểm tiền gửi 2012 thì:\r\n“Điều 18. Tiền gửi được bảo hiểm\r\nTiền gửi được bảo hiểm là tiền gửi bằng đồng Việt Nam của cá nhân gửi tại tổ chức tham gia bảo hiểm tiền gửi dưới hình thức tiền gửi có kỳ hạn, tiền gửi không kỳ hạn, tiền gửi tiết kiệm, chứng chỉ tiền gửi, kỳ phiếu, tín phiếu và các hình thức tiền gửi khác theo quy định của Luật Các tổ chức tín dụng, trừ các loại tiền gửi quy định tại Điều 19 của Luật này.”\r\nNhư vậy, theo quy định hiện hành thì tiền gửi được bảo hiểm phải là tiền gửi bằng đồng Việt Nam của cá nhân gửi tại tổ chức tham gia bảo hiểm tiền gửi dưới hình thức tiền gửi có kỳ hạn, tiền gửi không kỳ hạn, tiền gửi tiết kiệm, chứng chỉ tiền gửi, kỳ phiếu, tín phiếu và các hình thức tiền gửi khác. Do đó, tiền gửi ngân hàng nói chung và tiền gửi tiết kiệm nói riêng bằng ngoại tệ của cá nhân sẽ không được bảo hiểm.\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed38"),
    QUESTION: "Có phải tất cả các cá nhân gửi tiền bằng đồng Việt Nam tại các tổ chức tham gia bảo hiểm tiền gửi đều được bảo hiểm không?",
    ANSWER: "Theo quy định tại Điều 19 Luật Bảo hiểm tiền gửi 2012 thì:\r\n“Điều 19. Tiền gửi không được bảo hiểm\r\n1. Tiền gửi tại tổ chức tín dụng của cá nhân là người sở hữu trên 5% vốn điều lệ của chính tổ chức tín dụng đó.\r\n2. Tiền gửi tại tổ chức tín dụng của cá nhân là thành viên Hội đồng thành viên, thành viên Hội đồng quản trị, thành viên Ban kiểm soát, Tổng giám đốc (Giám đốc), Phó Tổng giám đốc (Phó Giám đốc) của chính tổ chức tín dụng đó; tiền gửi tại chi nhánh ngân hàng nước ngoài của cá nhân là Tổng giám đốc (Giám đốc), Phó Tổng giám đốc (Phó Giám đốc) của chính chi nhánh ngân hàng nước ngoài đó.\r\n3. Tiền mua các giấy tờ có giá vô danh do tổ chức tham gia bảo hiểm tiền gửi phát hành.”\r\nNhư vậy, với các cá nhân gửi tiền tại TCTD thuộc một trong các trường hợp quy định tại Điều 19 Luật Bảo hiểm tiền gửi 2012 thì sẽ không được bảo hiểm tiền gửi.\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed39"),
    QUESTION: "Khách hàng là người Việt Nam đang học tập ở nước ngoài có được gửi tiết kiệm ngoại tệ tại MSB không?",
    ANSWER: "Theo khoản 2 Điều 3 về Đối tượng gửi tiền gửi tiết kiệm, Quy chế về Tiền gửi tiết kiệm ban hành kèm theo Quyết định số 1160/2004/QĐ-NHNN ngày 13/09/2004 của Ngân hàng Nhà nước(đã được sửa đổi bổ sung theo Quyết định số 47/2006/QĐ-NHNN ngày 25/09/2006 của Ngân hàng Nhà nước sửa đổi Quy chế về tiền gửi tiết kiệm) thì đối tượng được gửi tiền gửi tiết kiệm bằng ngoại tệ là các cá nhân người cư trú. \r\nTheo khoản 2 Điều 4 của Pháp lệnh Ngoại hối 2005 (đã được sửa đổi bởi khoản 1 Điều 1 Pháp lệnh sửa đổi Pháp lệnh Ngoại hối 2013) thì Người cư trú là cá nhân được quy định như sau:\r\n“Điều 4. Giải thích từ ngữ\r\n2. Người cư trú là tổ chức, cá nhân thuộc các đối tượng sau đây:\r\ne) Công dân Việt Nam cư trú tại Việt Nam; công dân Việt Nam cư trú ở nước ngoài có thời hạn dưới 12 tháng; công dân Việt Nam làm việc tại các tổ chức quy định tại điểm d và điểm đ khoản này và cá nhân đi theo họ;\r\ng) Công dân Việt Nam đi du lịch, học tập, chữa bệnh và thăm viếng ở nước ngoài;”\r\nNhư vậy, căn cứ theo các quy định trên thì Khách hàng là người Việt Nam đang học tập ở nước ngoài có thể gửi tiết kiệm ngoại tệ tại MSB. Tuy nhiên, theo điểm a, khoản 1, Điều 8, Quy chế về Tiền gửi tiết kiệm, ban hành kèm theo Quyết định số 1160/2004/QĐ-NHNN thì người gửi tiền phải trực tiếp thực hiện thủ tục gửi tiền tiết kiệm lần đầu tại Ngân hàng.\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed3a"),
    QUESTION: " Người nước ngoài có được gửi tiền gửi tiết kiệm bằng ngoại tệ không?",
    ANSWER: "Điều 13 Nghị định 70/2014/NĐ-CP ngày 17/07/2014 của Ngân hàng Nhà nước quy định chi tiết thi hành một số điều của Pháp lệnh Ngoại hối và Pháp lệnh sửa đổi, bổ sung một số điều của Pháp lệnh Ngoại hối quy định việc sử dụng ngoại tệ tiền mặt của cá nhân như sau:\r\n“Điều 13. Sử dụng ngoại tệ tiền mặt của cá nhân\r\n1. Người cư trú, người không cư trú là cá nhân có ngoại tệ tiền mặt được quyền cất giữ, mang theo người, cho, tặng, thừa kế, bán cho tổ chức tín dụng được phép, chuyển, mang ra nước ngoài theo các quy định tại Nghị định này, thanh toán cho các đối tượng được phép thu ngoại tệ tiền mặt.\r\n2. Người cư trú là công dân Việt Nam được sử dụng ngoại tệ tiền mặt để gửi tiết kiệm ngoại tệ tại tổ chức tín dụng được phép, được rút tiền gốc, lãi bằng đồng tiền đã gửi.”\r\nKhoản 2 Điều 4 Thông tư 16/2014/TT-NHNN của Ngân hàng Nhà nước Việt Nam về việc hướng dẫn sử dụng tài khoản ngoại tệ, tài khoản đồng Việt Nam của người cư trú, người không cư trú tại ngân hàng được phép quy định việc sử dụng tài khoản ngoại tệ của người cư trú là cá nhân: “Chi chuyển sang gửi tiết kiệm ngoại tệ tại ngân hàng được phép đối với người cư trú là công dân Việt Nam.”\r\nĐiều 3 về Đối tượng gửi tiền tiết kiệm,Quy chế tiền gửi tiết kiệm ban hành theo Quyết định 1160/2001/QĐ-NHNN ngày 13/09/2004 của Ngân hàng Nhà nước quy định:\r\n“1. Đối tượng gửi tiền gửi tiết kiệm bằng đồng Việt Nam là các cá nhân Việt Nam và cá nhân nước ngoài đang sinh sống và hoạt động hợp pháp tại Việt Nam.\r\n2. Đối tượng gửi tiền gửi tiết kiệm bằng ngoại tệ là các cá nhân người cư trú.”\r\nNhư vậy, căn cứ theo các quy định trên thì chỉ có người cư trú là công dân Việt Nam mới được phép sử dụng ngoại tệ tiền mặt hoặc chuyển khoản để gửi tiết kiệm ngoại tệ tại Ngân hàng được phép. Người nước ngoài không được gửi tiền gửi tiết kiệm bằng ngoại tệ.\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed3b"),
    QUESTION: " Khách hàng đang bị tạm giam có được ủy quyền cho người khác rút tiền gửi tiết kiệm không?",
    ANSWER: "Nếu số tiền gửi tiết kiệm của Khách hàng không thuộc đối tượng bị tịch thu hoặc bị áp dụng biện pháp bảo đảm thi hành án thì việc ủy quyền của Khách hàng cho người khác được thực hiện giao dịch rút tiền tiết kiệm là hợp pháp.\r\nTuy nhiên, theo quy định tại mục 5 Chỉ thị số 44/2018/CT-TGD14 về việc tăng cường kiểm soát đối với các giao dịch tiền gửi tiết kiệm của khách hàng ngày 21 tháng 12 năm 2018 của Tổng giám đốc MSB thì: “Các đơn vị không thực hiện các giao dịch ủy quyền liên quan đến rút tiền gửi tiết kiệm. Nếu khách hàng có nhu cầu này, các đơn vị tư vấn để chuyển sang hình thức chuyển nhượng. Đối với các hồ sơ ủy quyền đã ký, trước khi thực hiện giao dịch rút tiền từ Khách hàng được ủy quyền, Giao dịch viên gọi điện cho Khách hàng ủy quyền (tương tự mục 2) để xác nhận về việc rút tiền theo ủy quyền này”.\r\nNhư vậy, mặc dù pháp luật có quy định cho phép ủy quyền cho người khác rút tiền gửi tiết kiệm nhưng theo Chỉ thị số 44/2018/CT-TGD14 thì các đơn vị không thực hiện các giao dịch ủy quyền liên quan đến rút tiền gửi tiết kiệm. Văn bản chỉ đạo này của Tổng giám đốc nhằm mục đích phòng ngừa, ngăn chặn hành vi gian Iận, tổn thất có thể xảy ra đối với Khách hàng và MSB.\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed3c"),
    QUESTION: "Theo quy định của Pháp luật thì doanh nghiệp nào thuộc nhóm đối tượng không bắt buộc phải có kế toán trưởng?",
    ANSWER: "Điều 20 Nghị định 174/2016/NĐ-CP ngày 30/12/2016 của Chính phủ hướng dẫn Luật kế toán quy định như sau: \r\n“Điều 20. Kế toán trưởng, phụ trách kế toán\r\n1. Đơn vị kế toán phải bố trí kế toán trưởng trừ các đơn vị quy định tại khoản 2 Điều này. Trường hợp đơn vị chưa bổ nhiệm được ngay kế toán trưởng thì bố trí người phụ trách kế toán hoặc thuê dịch vụ làm kế toán trưởng theo quy định. Thời gian bố trí người phụ trách kế toán tối đa là 12 tháng, sau thời gian này đơn vị kế toán phải bố trí người làm kế toán trưởng.\r\n2. Phụ trách kế toán:\r\na) Các đơn vị kế toán trong lĩnh vực nhà nước bao gồm: Đơn vị kế toán chỉ có một người làm kế toán hoặc một người làm kế toán kiêm nhiệm; đơn vị kế toán ngân sách và tài chính xã, phường, thị trấn thì không thực hiện bổ nhiệm kế toán trưởng mà chỉ bổ nhiệm phụ trách kế toán.\r\nb) Các doanh nghiệp siêu nhỏ theo quy định của Pháp luật về hỗ trợ doanh nghiệp nhỏ và vừa được bố trí phụ trách kế toán mà không bắt buộc phải bố trí kế toán trưởng.”\r\nNhư vậy, theo quy định trên thì có 02 nhóm đối tượng không bắt buộc phải bố trí kế toán trưởng, bao gồm:\r\n1. Các đơn vị kế toán trong lĩnh vực nhà nước bao gồm: Đơn vị kế toán chỉ có một người làm kế toán hoặc một người làm kế toán kiêm nhiệm; đơn vị kế toán ngân sách và tài chính xã, phường, thị trấn.\r\n2. Các doanh nghiệp siêu nhỏ theo quy định của Pháp luật về hỗ trợ doanh nghiệp nhỏ và vừa.\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed3d"),
    QUESTION: "Doanh nghiệp không đăng ký và sử dụng chữ ký kế toán trưởng trên chứng từ giao dịch với Ngân hàng có phù hợp với quy định của Pháp luật không?",
    ANSWER: "Theo các quy định về ký chứng từ kế toán tại Điều 19 Luật kế toán 2015 và Điều 85 Thông tư 133/2016/TT-BTC ngày 26/08/2016 của Bộ Tài chính hướng dẫn chế độ kế toán doanh nghiệp vừa và nhỏ thì “Chứng từ kế toán chi tiền phải do người có thẩm quyền duyệt chi và kế toán trưởng hoặc người được ủy quyền ký trước khi thực hiện. Chữ ký trên chứng từ kế toán dùng để chi tiền phải ký theo từng liên.”\r\nĐồng thời, theo điểm c khoản 2 Điều 8 của Chế độ chứng từ kế toán ban hành theo Quyết định 1789/2005/QĐ-NHNN ngày 12/12/2005 về chế độ chứng từ kế toán ngân hàng thì: “Đối với chứng từ giao dịch với ngân hàng được lập bởi Khách hàng là những đơn vị, tổ chức phải bố trí kế toán trưởng theo quy định của Pháp luật thì trên chứng từ bắt buộc phải có đủ chữ ký của chủ tài khoản, kế toán trưởng hoặc người được uỷ quyền ký thay và dấu đơn vị (nếu là chứng từ bằng giấy).”\r\nNhư vậy, theo quy định của Pháp luật, tất cả các chứng từ giao dịch với ngân hàng được lập bởi Khách hàng là những đơn vị, tổ chức phải bố trí kế toán trưởng thì trên chứng từ bắt buộc phải có chữ ký của kế toán trưởng hoặc người được uỷ quyền ký thay. Việc doanh nghiệp là đơn vị kế toán phải bố trí kế toán trưởng theo quy định của Pháp luật mà không đăng ký và sử dụng chữ ký kế toán trưởng trên chứng từ giao dịch với Ngân hàng là không phù hợp với quy định của Pháp luật.\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed3e"),
    QUESTION: " Vợ/chồng của Giám đốc doanh nghiệp có được làm Kế toán của chính doanh nghiệp đó không?",
    ANSWER: "Tại Điều 52 Luật kế toán 2015 có quy định những người không được làm kế toán như sau:\r\n“Điều 52. Những người không được làm kế toán\r\n1. Người chưa thành niên; người bị Tòa án tuyên bố hạn chế hoặc mất năng lực hành vi dân sự; người đang phải chấp hành biện pháp đưa vào cơ sở giáo dục bắt buộc, cơ sở cai nghiện bắt buộc.\r\n2. Người đang bị cấm hành nghề kế toán theo bản án hoặc quyết định của Tòa án đã có hiệu lực Pháp luật; người đang bị truy cứu trách nhiệm hình sự; người đang phải chấp hành hình phạt tù hoặc đã bị kết án về một trong các tội xâm phạm trật tự quản lý kinh tế, tội phạm về chức vụ liên quan đến tài chính, kế toán mà chưa được xóa án tích.\r\n3. Cha đẻ, mẹ đẻ, cha nuôi, mẹ nuôi, vợ,chồng, con đẻ, con nuôi, anh, chị, em ruột của người đại diện theo Pháp luật, của người đứng đầu, của giám đốc, tổng giám đốc và của cấp phó của người đứng đầu, phó giám đốc, phó tổng giám đốc phụ trách công tác tài chính - kế toán, kế toán trưởng trong cùng một đơn vị kế toán, trừ doanh nghiệp tư nhân, công ty trách nhiệm hữu hạn do một cá nhân làm chủ sở hữu và các trường hợp khác do Chính phủ quy định”.\r\nTheo quy định trên thì chỉ có doanh nghiệp tư nhân, công ty TNHH do một cá nhân làm chủ sở hữu và các trường hợp khác do Chính phủ quy định mới được phép bố trí vợ/chồng của Giám đốc doanh nghiệp làm kế toán của doanh nghiệp đó, còn đối với các doanh nghiệp khác thì vợ/chồng của Giám đốc doanh nghiệp không được làm kế toán của doanh nghiệp đó. \r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed3f"),
    QUESTION: " Một người đồng thời làm Kế toán trưởng của hai công ty khác nhau có bị coi là vi phạm quy định của Pháp luật không?",
    ANSWER: "Bộ luật lao động 2012 không cấm việc một người được làm việc cho nhiều hơn một doanh nghiệp, theo đó, một người có thể được làm Kế toán trưởng cho nhiều hơn một doanh nghiệp miễn là đáp ứng được các quy định/điều kiện/tiêu chuẩn của Pháp luật về kế toán.\r\nCác quy định của Pháp luật về kế toán hiện tại cũng không cấm hay hạn chế việc một người làm Kế toán trưởng của hai công ty độc lập khác nhau (chỉ hạn chế người làm kế toán phụ trách Kế toán trưởng trong cùng một đơn vị kế toán) miễn là người làm Kế toán trưởng phải đáp ứng đầy đủ các điều kiện/tiêu chuẩn theo quy định của Pháp luật.  \r\nNhư vậy, về mặt pháp lý thì việc một người đồng thời làm Kế toán trưởng của hai công ty độc lập (không có quan hệ về sở hữu, quản lý, góp vốn,...) không bị coi là vi phạm quy định của Pháp luật. \r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_HANDBOOK").insert([ {
    _id: ObjectId("5d2d7a545a8a2e001751ed40"),
    QUESTION: "Thời hạn bổ nhiệm Kế toán trưởng tại các Đơn vị kế toán là bao lâu?",
    ANSWER: "Tại khoản 1, 2 và 3 Điều 20 Nghị định 174/2016/NĐ-CP ngày 30/12/2016 của Chính phủ hướng dẫn Luật kế toán 2015 quy định như sau:\r\n“Điều 20. Kế toán trưởng, phụ trách kế toán\r\n1. Đơn vị kế toán phải bố trí kế toán trưởng trừ các đơn vị quy định tại khoản 2 Điều này. Trường hợp đơn vị chưa bổ nhiệm được ngay kế toán trưởng thì bố trí người phụ trách kế toán hoặc thuê dịch vụ làm kế toán trưởng theo quy định. Thời gian bố trí người phụ trách kế toán tối đa là 12 tháng, sau thời gian này đơn vị kế toán phải bố trí người làm kế toán trưởng.\r\n2. Phụ trách kế toán:\r\na) Các đơn vị kế toán trong lĩnh vực nhà nước bao gồm: Đơn vị kế toán chỉ có một người làm kế toán hoặc một người làm kế toán kiêm nhiệm; đơn vị kế toán ngân sách và tài chính xã, phường, thị trấn thì không thực hiện bổ nhiệm kế toán trưởng mà chỉ bổ nhiệm phụ trách kế toán.\r\nb) Các doanh nghiệp siêu nhỏ theo quy định của Pháp luật về hỗ trợ doanh nghiệp nhỏ và vừa được bố trí phụ trách kế toán mà không bắt buộc phải bố trí kế toán trưởng.\r\n3. Thời hạn bổ nhiệm kế toán trưởng của các đơn vị kế toán trong lĩnh vực kế toán nhà nước, thời hạn bổ nhiệm phụ trách kế toán của các đơn vị quy định tại điểm a khoản 2 Điều này là 5 năm sau đó phải thực hiện các quy trình về bổ nhiệm lại kế toán trưởng, phụ trách kế toán.”\r\nTheo quy định trên thì thời hạn bổ nhiệm kế toán trưởng của các đơn vị kế toán trong lĩnh vực kế toán nhà nước và thời hạn bổ nhiệm người phụ trách kế toán của các đơn vị kế toán trong lĩnh vực nhà nước gồm đơn vị kế toán chỉ có một người làm kế toán hoặc một người làm kế toán kiêm nhiệm; đơn vị kế toán ngân sách và tài chính xã, phường, thị trấn là: 5 năm, hết thời hạn này phải bổ nhiệm lại. \r\nĐối với các đơn vị kế toán khác thì không bị hạn chế thời hạn bổ nhiệm kế toán trưởng. Tuy nhiên, đối với người phụ trách kế toán của doanh nghiệp chưa bố trí được kế toán trưởng thì bị giới hạn thời hạn bổ nhiệm (tối đa là 12 tháng theo quy định tại khoản 1 Điều 20 Nghị định 174/2016/NĐ-CP).\r\n",
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "CREATED_AT": ISODate("2019-07-16T07:18:44.113Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);

// ----------------------------
// Collection structure for MCL_NODE
// ----------------------------
db.getCollection("MCL_NODE").drop();
db.createCollection("MCL_NODE");

// ----------------------------
// Documents of MCL_NODE
// ----------------------------
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a90"),
    "FUNCTION_ID": "GREETINGS",
    "PARENT_FUNCTION__ID": "",
    DESCRIPTION: "Câu chào",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.58Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a96"),
    "FUNCTION_ID": "DEFAULT",
    "PARENT_FUNCTION__ID": "ROOT",
    DESCRIPTION: "Trả lời nhanh",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.756Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a91"),
    "FUNCTION_ID": "DEFAULT",
    "PARENT_FUNCTION__ID": "ROOT",
    DESCRIPTION: "TRỞ VỀ TRANG CHỦ",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.751Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a8f"),
    "FUNCTION_ID": "LOGIN_FORM",
    "PARENT_FUNCTION__ID": "ROOT",
    DESCRIPTION: "Form đăng nhập",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.578Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a8e"),
    "FUNCTION_ID": "PERSISTENT_MENU",
    "PARENT_FUNCTION__ID": "ROOT",
    DESCRIPTION: "Menu nhanh",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.577Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a93"),
    "FUNCTION_ID": "HOME",
    "PARENT_FUNCTION__ID": "GET_STARTED",
    DESCRIPTION: "Trang chủ",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.754Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a8d"),
    "FUNCTION_ID": "GET_STARTED",
    "PARENT_FUNCTION__ID": "ROOT",
    DESCRIPTION: "Câu chào",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.571Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a98"),
    "FUNCTION_ID": "LICENSE_MANAGEMENT",
    "PARENT_FUNCTION__ID": "LISENSE_REGISTRATION",
    DESCRIPTION: "Quản lý giấy phép",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.921Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a9c"),
    "FUNCTION_ID": "REGULATION",
    "PARENT_FUNCTION__ID": "LICENSE_MANAGEMENT",
    DESCRIPTION: "Quy tắc trong quản lý giấy phép",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.929Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a9f"),
    "FUNCTION_ID": "DETECTED_LISENCE",
    "PARENT_FUNCTION__ID": "LICENSE_MANAGEMENT",
    DESCRIPTION: "Giấy phép bị hỏng trong quản lý giấy phép",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.936Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a9a"),
    "FUNCTION_ID": "CONTENT",
    "PARENT_FUNCTION__ID": "LICENSE_MANAGEMENT",
    DESCRIPTION: "Nội trong quản lý giấy phép",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.928Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a99"),
    "FUNCTION_ID": "FEE",
    "PARENT_FUNCTION__ID": "LICENSE_MANAGEMENT",
    DESCRIPTION: "Phí trong quản lý giấy phép",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.926Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a9e"),
    "FUNCTION_ID": "USAGE",
    "PARENT_FUNCTION__ID": "LICENSE_MANAGEMENT",
    DESCRIPTION: "Hạn mức trong quản lý giấy phép",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.934Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a9b"),
    "FUNCTION_ID": "USE",
    "PARENT_FUNCTION__ID": "LICENSE_MANAGEMENT",
    DESCRIPTION: "Sử dụng trong quản lý giấy phép",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.929Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a9d"),
    "FUNCTION_ID": "LISENSE_REGISTRATION",
    "PARENT_FUNCTION__ID": "GENERAL_COMPLIANCE",
    DESCRIPTION: "Đăng ký giấy phép",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.93Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d3dcafd4db6000917d305"),
    "FUNCTION_ID": "ACCEPTING_CHANGE",
    "PARENT_FUNCTION__ID": "LISENSE_REGISTRATION",
    DESCRIPTION: "Đây là xin chấp nhận thay đổi trong quản lý giấy phép",
    "CREATED_AT": ISODate("2019-07-16T03:00:26.156Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d3f01fd4db6000917d309"),
    "FUNCTION_ID": "NHUNGTHAYDOIPHAIDUOCNHNNCHAPNHAN",
    "PARENT_FUNCTION__ID": "ACCEPTING_CHANGE",
    DESCRIPTION: "Những thay đổi phải được Ngân hàng Nhà nước chấp thuận",
    "CREATED_AT": ISODate("2019-07-16T03:05:37.682Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d3f50fd4db6000917d30b"),
    "FUNCTION_ID": "THUCHIENTHUTUCSAUKHIDUOCNHNNCHAPTHUANTHAYDOI",
    "PARENT_FUNCTION__ID": "ACCEPTING_CHANGE",
    DESCRIPTION: "Thực hiện thủ tục sau khi được NHNN chấp thuận thay đổi",
    "CREATED_AT": ISODate("2019-07-16T03:06:56.35Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b43a"),
    "FUNCTION_ID": "COSOPHATHANHCONGCUCHUYENNHUONG",
    "PARENT_FUNCTION__ID": "QUYDINHCHUNGVECONGCUCHUYENNHUONG",
    DESCRIPTION: "Cơ sở phát hành công cụ chuyển nhượng",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.392Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b43b"),
    "FUNCTION_ID": "THANHTOANCONGCUCHUYENNHUONGGHITRABANGNGOAITE",
    "PARENT_FUNCTION__ID": "QUYDINHCHUNGVECONGCUCHUYENNHUONG",
    DESCRIPTION: "Thanh toán công cụ chuyển nhượng ghi trả bằng ngoại tệ",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.392Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b43c"),
    "FUNCTION_ID": "KYCONGCUCHUYENNHUONG",
    "PARENT_FUNCTION__ID": "QUYDINHCHUNGVECONGCUCHUYENNHUONG",
    DESCRIPTION: "Ký công cụ chuyển nhượng",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.392Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ea63f0349d20017501f9a"),
    "FUNCTION_ID": "TRACUU",
    "PARENT_FUNCTION__ID": "HOME",
    DESCRIPTION: "Tra cứu thông tin",
    "CREATED_AT": ISODate("2019-07-17T04:38:23.095Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ecbeec782c40017769499"),
    "FUNCTION_ID": "CONGCUCHUYENNHUONG",
    "PARENT_FUNCTION__ID": "BUSINESS_COMPLIANCE",
    DESCRIPTION: "Công cụ chuyển nhượng ",
    "CREATED_AT": ISODate("2019-07-17T07:19:10.319Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b43d"),
    "FUNCTION_ID": "NGONNGUTRENCONGCUCHUYENNHUONG",
    "PARENT_FUNCTION__ID": "QUYDINHCHUNGVECONGCUCHUYENNHUONG",
    DESCRIPTION: "Ngôn ngữ trên công cụ chuyển nhượng",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.392Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b43e"),
    "FUNCTION_ID": "HUHONGCONGCUCHUYENNHUONG",
    "PARENT_FUNCTION__ID": "QUYDINHCHUNGVECONGCUCHUYENNHUONG",
    DESCRIPTION: "Hư hỏng công cụ chuyển nhượng",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.392Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b43f"),
    "FUNCTION_ID": "CACHANHVIBICAMKHIGIAODICHCONGCUCHUYENNHUONG",
    "PARENT_FUNCTION__ID": "QUYDINHCHUNGVECONGCUCHUYENNHUONG",
    DESCRIPTION: "Các hành vi bị cấm khi giao dịch công cụ chuyển nhượng",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.392Z"),
    "UPDATED_AT": "2019-07-17T11:27:19.270Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b440"),
    "FUNCTION_ID": "NOIDUNGCUAHOIPHIEUDOINO",
    "PARENT_FUNCTION__ID": "HOIPHIEU",
    DESCRIPTION: "Nội dung của hối phiếu đòi nợ",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.392Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b441"),
    "FUNCTION_ID": "NOIDUNGCUAHOIPHIEUNHANNO",
    "PARENT_FUNCTION__ID": "HOIPHIEU",
    DESCRIPTION: "Nội dung của hối phiếu nhận nợ",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.392Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b442"),
    "FUNCTION_ID": "NGHIAVUCUANGUOIKYPHATHOIPHIEUDOINO",
    "PARENT_FUNCTION__ID": "HOIPHIEU",
    DESCRIPTION: "Nghĩa vụ của người ký phát hối phiếu đòi nợ",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.392Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b443"),
    "FUNCTION_ID": "NGHIAVUCUANGUOIPHATHANHHOIPHIEUNHANNO",
    "PARENT_FUNCTION__ID": "HOIPHIEU",
    DESCRIPTION: "Nghĩa vụ của người phát hành hối phiếu nhận nợ",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.392Z"),
    "UPDATED_AT": "2019-07-18T04:35:01.038Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b444"),
    "FUNCTION_ID": "NGHIAVUCUANGUOICHAPNHANHOIPHIEUDOINO",
    "PARENT_FUNCTION__ID": "HOIPHIEU",
    DESCRIPTION: "Nghĩa vụ của người chấp nhận hối phiếu đòi nợ",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.392Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b445"),
    "FUNCTION_ID": "NGHIAVUCUANGUOITHUHOHOIPHIEUDOINO",
    "PARENT_FUNCTION__ID": "HOIPHIEU",
    DESCRIPTION: "Nghĩa vụ của người thu hộ hối phiếu đòi nợ",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.392Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b446"),
    "FUNCTION_ID": "HUONGDANTHUCHIENVENHOTHUHOIPHIEUQUANGUOITHUHO",
    "PARENT_FUNCTION__ID": "HOIPHIEU",
    DESCRIPTION: "Hướng dẫn thực hiện về nhờ thu hối phiếu qua người thu hộ",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.392Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b447"),
    "FUNCTION_ID": "KIEMSOAT,XULYHOIPHIEUBIMAT",
    "PARENT_FUNCTION__ID": "HOIPHIEU",
    DESCRIPTION: "Kiểm soát, xử lý hối phiếu bị mất",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.392Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b448"),
    "FUNCTION_ID": "PHIDICHVUTHUHOHOIPHIEU",
    "PARENT_FUNCTION__ID": "HOIPHIEU",
    DESCRIPTION: "Phí dịch vụ thu hộ hối phiếu",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.392Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b449"),
    "FUNCTION_ID": "CACNOIDUNGCUASEC",
    "PARENT_FUNCTION__ID": "SEC",
    DESCRIPTION: "Các nội dung của séc",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.392Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b44a"),
    "FUNCTION_ID": "IN,GIAONHANVABAOQUANSECTRANG",
    "PARENT_FUNCTION__ID": "SEC",
    DESCRIPTION: "In, giao nhận và bảo quản séc trắng",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.392Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b44b"),
    "FUNCTION_ID": "THANHTOANSECTAINGUOIBIKYPHAT ",
    "PARENT_FUNCTION__ID": "SEC",
    DESCRIPTION: "Thanh toán séc tại người bị ký phát ",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.392Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b44c"),
    "FUNCTION_ID": "XULYSECKHONGDUKHANANGTHANHTOAN",
    "PARENT_FUNCTION__ID": "SEC",
    DESCRIPTION: "Xử lý séc không đủ khả năng thanh toán",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.392Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b44d"),
    "FUNCTION_ID": "QUYDINHCUNGUNG,THANHTOAN,ĐINHCHINHTHANHTOANSEC",
    "PARENT_FUNCTION__ID": "SEC",
    DESCRIPTION: "Quy định cung ứng, thanh toán, đình chỉnh thanh toán séc",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.393Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b44e"),
    "FUNCTION_ID": "XULYMATSEC",
    "PARENT_FUNCTION__ID": "SEC",
    DESCRIPTION: "Xử lý mất séc",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.393Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b44f"),
    "FUNCTION_ID": "QUYDINHCHUNGVECONGCUCHUYENNHUONG",
    "PARENT_FUNCTION__ID": "CONGCUCHUYENNHUONG",
    DESCRIPTION: "Quy định chung về công cụ chuyển nhượng",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.393Z"),
    "UPDATED_AT": "2019-07-17T07:20:00.671Z",
    "UPDATED_BY": "5d2c5d9e72e61100084c0a7f"
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b451"),
    "FUNCTION_ID": "SEC",
    "PARENT_FUNCTION__ID": "CONGCUCHUYENNHUONG",
    DESCRIPTION: "Séc",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.393Z"),
    "UPDATED_AT": "2019-07-17T07:20:49.845Z",
    "UPDATED_BY": "5d2c5d9e72e61100084c0a7f"
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ea68d0349d20017501f9e"),
    "FUNCTION_ID": "AI",
    "PARENT_FUNCTION__ID": "HOME",
    DESCRIPTION: "Trí tuệ nhân tạo",
    "CREATED_AT": ISODate("2019-07-17T04:39:41.024Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c4001776947a"),
    "FUNCTION_ID": "GIAYTOCOGIA",
    "PARENT_FUNCTION__ID": "BUSINESS_COMPLIANCE",
    DESCRIPTION: "Sổ tay tuân thủ Hoạt động vay, cho vay, mua bán có kỳ hạn giấy tờ có giá giữa các tổ chức tín dụng\r\n",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": "2019-07-17T07:38:08.993Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c4001776947c"),
    "FUNCTION_ID": "NGUYENTACCHUNGTHUCHIENGIAODICHCGTCG",
    "PARENT_FUNCTION__ID": "QUYDINHCHUNGVEGTCG",
    DESCRIPTION: "Nguyên tắc chung thực hiện giao dịch cho vay, đi vay; mua, bán có kỳ hạn GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": "2019-07-17T07:35:48.726Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c4001776947d"),
    "FUNCTION_ID": "HINHTHUCTHUCHIENGIAODICHVATHANHTOAN",
    "PARENT_FUNCTION__ID": "QUYDINHCHUNGVEGTCG",
    DESCRIPTION: "Hình thức thực hiện giao dịch và thanh toán",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": "2019-07-17T07:36:05.375Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c4001776947e"),
    "FUNCTION_ID": "CAPNHAT,LUUTRUTHONGTINGIAODICHGTCG",
    "PARENT_FUNCTION__ID": "QUYDINHCHUNGVEGTCG",
    DESCRIPTION: "Cập nhật, lưu trữ thông tin giao dịch GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": "2019-07-17T07:36:38.825Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c40017769480"),
    "FUNCTION_ID": "TRICHLAPDUPHONGRUIROKHIGIAODICHGTCG",
    "PARENT_FUNCTION__ID": "QUYDINHCHUNGVEGTCG",
    DESCRIPTION: "Trích lập dự phòng rủi ro khi giao dịch GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": "2019-07-17T07:37:08.641Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c40017769482"),
    "FUNCTION_ID": "NGUYENTACCHOVAY,DIVAYGTCG",
    "PARENT_FUNCTION__ID": "GIAODICHCHOVAY,DIVAYGTCG",
    DESCRIPTION: "Nguyên tắc cho vay, đi vay GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c40017769483"),
    "FUNCTION_ID": "MUCDICHCHOVAY,DIVAYGTCG",
    "PARENT_FUNCTION__ID": "GIAODICHCHOVAY,DIVAYGTCG",
    DESCRIPTION: "Mục đích cho vay, đi vay GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c40017769484"),
    "FUNCTION_ID": "THOIHANCHOVAYGTCG",
    "PARENT_FUNCTION__ID": "GIAODICHCHOVAY,DIVAYGTCG",
    DESCRIPTION: "Thời hạn cho vay GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c40017769485"),
    "FUNCTION_ID": "LAISUATCHOVAYGTCG",
    "PARENT_FUNCTION__ID": "GIAODICHCHOVAY,DIVAYGTCG",
    DESCRIPTION: "Lãi suất cho vay GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c40017769486"),
    "FUNCTION_ID": "BAODAMTIENVAYKHIGIAODICHGTCG",
    "PARENT_FUNCTION__ID": "GIAODICHCHOVAY,DIVAYGTCG",
    DESCRIPTION: "Bảo đảm tiền vay khi giao dịch GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c40017769487"),
    "FUNCTION_ID": "DONGTIENCHOVAY,DIVAYGTCG",
    "PARENT_FUNCTION__ID": "GIAODICHCHOVAY,DIVAYGTCG",
    DESCRIPTION: "Đồng tiền cho vay, đi vay GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c40017769488"),
    "FUNCTION_ID": "HOPDONGCHOVAYGTCG",
    "PARENT_FUNCTION__ID": "GIAODICHCHOVAY,DIVAYGTCG",
    DESCRIPTION: "Hợp đồng cho vay GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c40017769489"),
    "FUNCTION_ID": "NGHIAVUCUABENCHOVAYGTCG",
    "PARENT_FUNCTION__ID": "GIAODICHCHOVAY,DIVAYGTCG",
    DESCRIPTION: "Nghĩa vụ của Bên cho vay GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c4001776948a"),
    "FUNCTION_ID": "NGHIAVUCUABENVAYGTCG",
    "PARENT_FUNCTION__ID": "GIAODICHCHOVAY,DIVAYGTCG",
    DESCRIPTION: "Nghĩa vụ của Bên vay GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c4001776948b"),
    "FUNCTION_ID": "HOATDONGGUITIEN,NHANTIENGUIGIUACACTCTD",
    "PARENT_FUNCTION__ID": "GIAYTOCOGIA",
    DESCRIPTION: "Hoạt động gửi tiền, nhận tiền gửi giữa các TCTD",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": "2019-07-17T07:38:41.218Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c4001776948c"),
    "FUNCTION_ID": "QUYDINHVETHOIHANGUITIEN,NHANTIENGUILIENNGANHANG",
    "PARENT_FUNCTION__ID": "HOATDONGGUITIEN,NHANTIENGUIGIUACACTCTD",
    DESCRIPTION: "Quy định về thời hạn gửi tiền, nhận tiền gửi liên ngân hàng",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c4001776948e"),
    "FUNCTION_ID": "NGUYENTACGIAODICHMUA,BANCOKYHANGTCG",
    "PARENT_FUNCTION__ID": "HOATDONGMUABANCOKYHANGTCG",
    DESCRIPTION: "Nguyên tắc giao dịch mua, bán có kỳ hạn GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c4001776948f"),
    "FUNCTION_ID": "CACLOAIGTCGDUOCPHEPMUA,BAN",
    "PARENT_FUNCTION__ID": "HOATDONGMUABANCOKYHANGTCG",
    DESCRIPTION: "Các loại GTCG được phép mua, bán",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c40017769490"),
    "FUNCTION_ID": "DONGTIENMUA,BANCOKYHANGTCG",
    "PARENT_FUNCTION__ID": "HOATDONGMUABANCOKYHANGTCG",
    DESCRIPTION: "Đồng tiền mua, bán có kỳ hạn GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c40017769491"),
    "FUNCTION_ID": "THOIHANMUA,BANCOKYHANGTCG",
    "PARENT_FUNCTION__ID": "HOATDONGMUABANCOKYHANGTCG",
    DESCRIPTION: "Thời hạn mua, bán có kỳ hạn GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c40017769492"),
    "FUNCTION_ID": "LAISUATMUAVACACHXACDINHGIAMUA,GIAMUALAIGTCG",
    "PARENT_FUNCTION__ID": "HOATDONGMUABANCOKYHANGTCG",
    DESCRIPTION: "Lãi suất mua và cách xác định giá mua, giá mua lại GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c40017769493"),
    "FUNCTION_ID": "HOPDONGMUALAIGTCG",
    "PARENT_FUNCTION__ID": "HOATDONGMUABANCOKYHANGTCG",
    DESCRIPTION: "Hợp đồng mua lại GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c40017769494"),
    "FUNCTION_ID": "QUYTRINHMUABANGTCG",
    "PARENT_FUNCTION__ID": "HOATDONGMUABANCOKYHANGTCG",
    DESCRIPTION: "Quy trình mua bán GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a94"),
    "FUNCTION_ID": "GENERAL_COMPLIANCE",
    "PARENT_FUNCTION__ID": "TRACUU",
    DESCRIPTION: "Tuân thủ chung",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.755Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": "2019-07-17T07:10:47.263Z",
    "UPDATED_BY": "5d2c5d9e72e61100084c0a7f"
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a97"),
    "FUNCTION_ID": "BUSINESS_COMPLIANCE",
    "PARENT_FUNCTION__ID": "TRACUU",
    DESCRIPTION: "Tuân thủ nghiệp vụ",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.756Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": "2019-07-17T07:11:03.967Z",
    "UPDATED_BY": "5d2c5d9e72e61100084c0a7f"
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a95"),
    "FUNCTION_ID": "WEATHER",
    "PARENT_FUNCTION__ID": "TRUYENTHONG_THONGBAO",
    DESCRIPTION: "THỜI TIẾT",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.755Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": "2019-07-17T07:11:30.598Z",
    "UPDATED_BY": "5d2c5d9e72e61100084c0a7f"
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a92"),
    "FUNCTION_ID": "HANDBOOK",
    "PARENT_FUNCTION__ID": "TRACUU",
    DESCRIPTION: "Cẩm nang tư vấn pháp luật",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.753Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": "2019-07-17T07:12:06.632Z",
    "UPDATED_BY": "5d2c5d9e72e61100084c0a7f"
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2d94a76fb49b001771b450"),
    "FUNCTION_ID": "HOIPHIEU",
    "PARENT_FUNCTION__ID": "CONGCUCHUYENNHUONG",
    DESCRIPTION: "Hối phiếu",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-16T09:11:03.393Z"),
    "UPDATED_AT": "2019-07-17T07:32:35.126Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c4001776947b"),
    "FUNCTION_ID": "QUYDINHCHUNGVEGTCG",
    "PARENT_FUNCTION__ID": "GIAYTOCOGIA",
    DESCRIPTION: "Quy định chung về vay, cho vay, mua bán có kỳ hạn GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": "2019-07-17T07:39:19.430Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c4001776947f"),
    "FUNCTION_ID": "XACNHANGIAODICHGTCG",
    "PARENT_FUNCTION__ID": "QUYDINHCHUNGVEGTCG",
    DESCRIPTION: "Xác nhận giao dịch GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": "2019-07-17T07:36:51.871Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c40017769481"),
    "FUNCTION_ID": "GIAODICHCHOVAY,DIVAYGTCG",
    "PARENT_FUNCTION__ID": "GIAYTOCOGIA",
    DESCRIPTION: "Giao dịch cho vay, đi vay GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": "2019-07-17T07:38:26.321Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ec5fbc782c4001776948d"),
    "FUNCTION_ID": "HOATDONGMUABANCOKYHANGTCG",
    "PARENT_FUNCTION__ID": "GIAYTOCOGIA",
    DESCRIPTION: "Hoạt động mua bán có kỳ hạn GTCG",
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "CREATED_AT": ISODate("2019-07-17T06:53:47.459Z"),
    "UPDATED_AT": "2019-07-17T07:38:55.849Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d2ea66d0349d20017501f9c"),
    "FUNCTION_ID": "TRUYENTHONG",
    "PARENT_FUNCTION__ID": "HOME",
    DESCRIPTION: "Truyền thông, thông báo",
    "CREATED_AT": ISODate("2019-07-17T04:39:09.969Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": "2019-07-18T01:47:20.825Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_NODE").insert([ {
    _id: ObjectId("5d358cfe1f65a10017041cd1"),
    "FUNCTION_ID": "PHUONGTEST",
    "PARENT_FUNCTION__ID": "HOME",
    DESCRIPTION: "aa",
    "CREATED_AT": ISODate("2019-07-22T10:16:30.805Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a83"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);

// ----------------------------
// Collection structure for MCL_NOTIFICATION
// ----------------------------
db.getCollection("MCL_NOTIFICATION").drop();
db.createCollection("MCL_NOTIFICATION");

// ----------------------------
// Documents of MCL_NOTIFICATION
// ----------------------------
db.getCollection("MCL_NOTIFICATION").insert([ {
    _id: ObjectId("5d38086e1ca9ba00172b624f"),
    TITLE: "Thời tiết",
    TEMPLATES: [
        {
            TYPE: "GENERIC",
            VALUE: {
                attachment: {
                    type: "template",
                    payload: {
                        "template_type": "generic",
                        elements: [
                            {
                                title: "Thời tiết",
                                subtitle: null,
                                "image_url": "https://www.iosicongallery.com/icons/weather-2017-06-19/512.png",
                                "default_action": {
                                    type: "web_url",
                                    url: "https://www.msn.com/vi-vn/weather",
                                    "webview_height_ratio": "tall"
                                }
                            }
                        ]
                    }
                }
            }
        }
    ],
    "CREATED_DATE": ISODate("2019-07-24T07:27:42.966Z"),
    "CREATED_BY": ObjectId("5d1affdadc8db800080c8a68"),
    STATUS: false,
    "SCHEDULE_SECOND": "00",
    "SCHEDULE_MINUTE": "00",
    "SCHEDULE_HOUR": "08",
    "SCHEDULE_DAY_OF_MONTH": "*",
    "SCHEDULE_MONTH": "*",
    "SCHEDULE_DAY_OF_WEEK": "*",
    TARGET: [
        "*"
    ],
    TYPE: "SPECIAL",
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);

// ----------------------------
// Collection structure for MCL_QUICK_REPLIES_REF
// ----------------------------
db.getCollection("MCL_QUICK_REPLIES_REF").drop();
db.createCollection("MCL_QUICK_REPLIES_REF");

// ----------------------------
// Documents of MCL_QUICK_REPLIES_REF
// ----------------------------
db.getCollection("MCL_QUICK_REPLIES_REF").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0abb"),
    TITLE: "MSB",
    PAYLOAD: "HOME",
    "CREATED_AT": ISODate("2019-07-15T11:04:02.914Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a")
} ]);
db.getCollection("MCL_QUICK_REPLIES_REF").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0abc"),
    TITLE: "Sổ tay",
    PAYLOAD: "HANDBOOK",
    "CREATED_AT": ISODate("2019-07-15T11:04:02.914Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a")
} ]);

// ----------------------------
// Collection structure for MCL_SCHEDULER
// ----------------------------
db.getCollection("MCL_SCHEDULER").drop();
db.createCollection("MCL_SCHEDULER");
db.getCollection("MCL_SCHEDULER").createIndex({
    "MODIFIED_AT": NumberInt("1")
}, {
    name: "MODIFIED_AT_1",
    expireAfterSeconds: NumberInt("600")
});

// ----------------------------
// Documents of MCL_SCHEDULER
// ----------------------------
db.getCollection("MCL_SCHEDULER").insert([ {
    _id: ObjectId("5d38086e1ca9ba00172b624d"),
    NAME: "train_AI",
    STATUS: true,
    SECOND: "00",
    MINUTE: "30",
    HOUR: "08",
    "DAY_OF_MONTH": "*",
    MONTH: "*",
    "DAY_OF_WEEK": "5",
    "CREATED_AT": ISODate("2019-07-24T07:27:42.96Z"),
    "CREATED_BY": ObjectId("5d1affdadc8db800080c8a68"),
    "IN_PROCESS": false,
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_SCHEDULER").insert([ {
    _id: ObjectId("5d38086e1ca9ba00172b624e"),
    NAME: "delete_session",
    STATUS: true,
    SECOND: "00",
    MINUTE: "30",
    HOUR: "00",
    "DAY_OF_MONTH": "1",
    MONTH: "*",
    "DAY_OF_WEEK": "*",
    "CREATED_AT": ISODate("2019-07-24T07:27:42.961Z"),
    "CREATED_BY": ObjectId("5d1affdadc8db800080c8a68"),
    "IN_PROCESS": false,
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_SCHEDULER").insert([ {
    _id: ObjectId("5d38086e1ca9ba00172b624c"),
    NAME: "update_user",
    STATUS: true,
    SECOND: "00",
    MINUTE: "30",
    HOUR: "08",
    "DAY_OF_MONTH": "*",
    MONTH: "*",
    "DAY_OF_WEEK": "*",
    "CREATED_AT": ISODate("2019-07-24T07:27:42.956Z"),
    "CREATED_BY": ObjectId("5d1affdadc8db800080c8a68"),
    "IN_PROCESS": false,
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);

// ----------------------------
// Collection structure for MCL_TEMPLATE
// ----------------------------
db.getCollection("MCL_TEMPLATE").drop();
db.createCollection("MCL_TEMPLATE");

// ----------------------------
// Documents of MCL_TEMPLATE
// ----------------------------
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0aa9"),
    "NODE__ID": ObjectId("5d2c5da072e61100084c0a91"),
    VALUE: {
        text: "Để hiển thị tất cả dịch vụ mời quý khách chọn MSB",
        "quick_replies": [
            {
                "content_type": "text",
                "image_url": "https://i.imgur.com/D92sDT6.png",
                title: "MSB",
                payload: "HOME"
            }
        ]
    },
    TYPE: "QUICK_REPLIES",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-15T11:04:02.144Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0aaa"),
    "NODE__ID": ObjectId("5d2c5da072e61100084c0a90"),
    VALUE: {
        text: "Ngân hàng TMCP Hàng Hải Việt Nam - MSB xin kính chào quý khách. Chúng tôi đang luôn không ngừng nâng cao trải nghiệm/chất lượng dịch vụ và hân hạnh được phục vụ quý khách trên nền tảng Chatbot robotic 4.0!"
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-15T11:04:02.145Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0aa8"),
    "NODE__ID": ObjectId("5d2c5da072e61100084c0a8f"),
    VALUE: {
        attachment: {
            type: "template",
            payload: {
                "template_type": "generic",
                elements: [
                    {
                        title: "Đăng nhập",
                        "image_url": "https://i.imgur.com/TGIwLTX.png",
                        subtitle: "Mời bạn đăng nhập để sử dụng hệ thống.",
                        buttons: [
                            {
                                title: "ĐĂNG NHẬP",
                                type: "web_url",
                                url: null,
                                "webview_height_ratio": "tall"
                            }
                        ]
                    }
                ]
            }
        }
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-15T11:04:02.143Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0aad"),
    "NODE__ID": ObjectId("5d2c5da072e61100084c0a93"),
    VALUE: {
        attachment: {
            type: "template",
            payload: {
                "template_type": "generic",
                elements: [
                    {
                        title: "TRA CỨU",
                        subtitle: "Tra cứu ",
                        "image_url": "https://yplea.com/wp-content/uploads/2018/02/signing-contract-yukon-legal-1024x683.jpg",
                        buttons: [
                            {
                                title: "CHI TIẾT",
                                type: "postback",
                                payload: "TRACUU"
                            }
                        ]
                    },
                    {
                        title: "TRUYỀN THÔNG",
                        subtitle: "Bản tin và nội dung khác",
                        "image_url": "https://cdn.lynda.com/course/170778/170778-636939601525883273-16x9.jpg",
                        buttons: [
                            {
                                title: "CHI TIẾT",
                                type: "postback",
                                payload: "TRUYENTHONG_THONGBAO"
                            }
                        ]
                    },
                    {
                        title: "AI ",
                        subtitle: "Trí tuệ nhân tạo",
                        "image_url": "http://tainangviet.vn/source/image/KHCN/hieu-biet-so-luoc-ve-tri-tue-nhan-tao-ai%201%20.jpg",
                        buttons: [
                            {
                                title: "KÍCH HOẠT",
                                type: "postback",
                                payload: "AI"
                            }
                        ]
                    }
                ]
            }
        }
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-15T11:04:02.153Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": "2019-07-18T01:45:08.435Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0aac"),
    "NODE__ID": ObjectId("5d2c5da072e61100084c0a8d"),
    VALUE: {
        text: "Xin chào mừng quý khách đến với Chatbot tư vấn pháp lý của Ngân hàng TMCP Hàng Hải Việt Nam - MSB 👍👍"
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-15T11:04:02.149Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": "2019-07-19T07:42:52.167Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0aab"),
    "NODE__ID": ObjectId("5d2c5da072e61100084c0a95"),
    VALUE: {
        attachment: {
            type: "template",
            payload: {
                "template_type": "generic",
                elements: [
                    {
                        title: "Thời tiết",
                        subtitle: null,
                        "image_url": "https://d279m997dpfwgl.cloudfront.net/wp/2017/12/weather_album-art-1000x1000.jpg",
                        "default_action": {
                            type: "web_url",
                            url: "https://www.msn.com/vi-vn/weather",
                            "webview_height_ratio": "tall"
                        }
                    }
                ]
            }
        }
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-15T11:04:02.146Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0aaf"),
    "NODE__ID": ObjectId("5d2c5da072e61100084c0a98"),
    VALUE: {
        attachment: {
            type: "template",
            payload: {
                "template_type": "generic",
                elements: [
                    {
                        title: "Quản lý giấy phép",
                        "image_url": "https://i.imgur.com/TGIwLTX.png",
                        subtitle: "",
                        buttons: [
                            {
                                title: "LỆ PHÍ",
                                type: "postback",
                                payload: "FEE"
                            },
                            {
                                title: "SỬ DỤNG",
                                type: "postback",
                                payload: "USE"
                            },
                            {
                                title: "NỘI DUNG",
                                type: "postback",
                                payload: "CONTENT"
                            }
                        ]
                    },
                    {
                        title: "Quản lý giấy phép",
                        "image_url": "https://i.imgur.com/TGIwLTX.png",
                        subtitle: "",
                        buttons: [
                            {
                                title: "THỜI HẠN",
                                type: "postback",
                                payload: "USAGE"
                            },
                            {
                                title: "QUY ĐỊNH",
                                type: "postback",
                                payload: "REGULATION"
                            },
                            {
                                title: "GIẤY PHÉP LỖI, HỎNG",
                                type: "postback",
                                payload: "DETECTED_LISENCE"
                            }
                        ]
                    }
                ]
            }
        }
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-15T11:04:02.154Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0aae"),
    "NODE__ID": ObjectId("5d2c5da072e61100084c0a96"),
    VALUE: {
        text: "Một số gợi ý dành cho quý khách",
        "quick_replies": [
            {
                "content_type": "text",
                title: "MSB",
                "image_url": "https://img.icons8.com/color/48/000000/museum.png",
                payload: "HOME"
            },
            {
                "content_type": "text",
                title: "Sổ tay",
                "image_url": "https://img.icons8.com/color/48/000000/bank-cards.png",
                payload: "HANDBOOK"
            }
        ]
    },
    TYPE: "QUICK_REPLIES",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-15T11:04:02.153Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2d3f01fd4db6000917d30a"),
    "NODE__ID": ObjectId("5d2d3f01fd4db6000917d309"),
    VALUE: null,
    TYPE: null,
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-16T03:05:37.752Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0ab0"),
    "NODE__ID": ObjectId("5d2c5da072e61100084c0a94"),
    VALUE: {
        attachment: {
            type: "template",
            payload: {
                "template_type": "generic",
                elements: [
                    {
                        title: "Tuân thủ chung",
                        "image_url": "https://i.imgur.com/QfNS9mS.jpg",
                        subtitle: "Bộ chỉ tiêu tuân thủ chung",
                        buttons: [
                            {
                                title: "GIẤY PHÉP, ĐĂNG KÝ KINH DOANH, CON DẤU",
                                type: "postback",
                                payload: "LISENSE_REGISTRATION"
                            },
                            {
                                title: "TỔ CHỨC BỘ MÁY",
                                type: "postback",
                                payload: "COMMITEE_ORGANIZATION"
                            },
                            {
                                title: "CỔ PHẦN, CỔ PHIẾU",
                                type: "postback",
                                payload: "STOCK_SHARE"
                            }
                        ]
                    },
                    {
                        title: "Tuân thủ chung",
                        "image_url": "https://i.imgur.com/QfNS9mS.jpg",
                        subtitle: "Bộ chỉ tiêu tuân thủ chung",
                        buttons: [
                            {
                                title: "THẨM QUYỀN, PHÂN CẤP, ỦY QUYỀN",
                                type: "postback",
                                payload: "AUTHORIZATION"
                            },
                            {
                                title: "CÔNG NGHỆ THÔNG TIN",
                                type: "postback",
                                payload: "INFORMATION_TECHNOLOGY"
                            },
                            {
                                title: "PHÒNG CHỐNG RỬA TIỀN",
                                type: "postback",
                                payload: "ANTI_MONEY_LAUNDERNG"
                            }
                        ]
                    },
                    {
                        title: "Tuân thủ chung",
                        "image_url": "https://i.imgur.com/QfNS9mS.jpg",
                        subtitle: "Bộ chỉ tiêu tuân thủ chung",
                        buttons: [
                            {
                                title: "LAO ĐỘNG",
                                type: "postback",
                                payload: "LABOUR"
                            },
                            {
                                title: "CHẾ ĐỘ BÁO CÁO",
                                type: "postback",
                                payload: "REPORT_MODE"
                            }
                        ]
                    }
                ]
            }
        }
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-15T11:04:02.155Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0ab3"),
    "NODE__ID": ObjectId("5d2c5da072e61100084c0a9a"),
    VALUE: {
        text: "[ĐIỀU KHOẢN] \r\nKhoản 2, Khoản 3 Điều 90 Luật các TCTD 2010\r\n[NỘI NỘI DUNG PHẢI TUÂN THỦ]\r\n1. MSB không được tiến hành bất kỳ hoạt động kinh doanh nào ngoài các hoạt động ngân hàng, hoạt động kinh doanh khác ghi trong Giấy phép được NHNN cấp cho MSB. \r\n2.  Các hoạt động ngân hàng, hoạt động kinh doanh khác của MSB với tư cách ngân hàng thương mại quy định tại Luật các TCTD 2010 thực hiện theo hướng dẫn của NHNN. \r\n[DIỄN GIẢI, THAM CHIẾU, CHẾ TÀI] \r\n4) Phạt tiền từ 150.000.000 đồng đến 200.000.000 đồng đối với một trong các hành vi vi phạm sau đây: b) Hoạt động không đúng nội dung ghi trong giấy phép, trừ trường hợp quy định tại Khoản 6 Điều 18, Khoản 7 Điều 24, Điểm d Khoản 4, Điểm b Khoản 5 Điều 25 Nghị định này. \r\n6) Phạt tiền từ 400.000.000 đồng đến 500.000.000 đồng đối với hành vi hoạt động không có giấy phép, trừ trường hợp quy định tại Khoản 7 Điều 24, Điểm d Khoản 2, Điểm a Khoản 4, Khoản 7 Điều 25 Nghị định này. \r\n8) Áp dụng biện pháp khắc phục hậu quả: a) Buộc nộp vào ngân sách nhà nước số lợi bất hợp pháp có được do thực hiện hành vi vi phạm quy định tại các Điểm a, b Khoản 3, các Khoản 4, 5 và 6 Điều này;b) Đề nghị cấp có thẩm quyền thu hồi giấy phép đối với hành vi vi phạm tại Điểm a  Khoản 3, Khoản 4 và Khoản 5 Điều này; c) Đề nghị hoặc yêu cầu cấp có thẩm quyền xem xét, áp dụng biện pháp đình chỉ từ 01 tháng đến 03 tháng hoặc miễn nhiệm chức danh quản trị, điều hành, kiểm soát; không cho đảm nhiệm chức vụ quản trị, điều hành, kiểm soát tại các tổ chức tín dụng, chi nhánh ngân hàng nước ngoài đối với cá nhân vi phạm và/hoặc cá nhân chịu trách nhiệm đối với hành vi vi phạm quy định tại các Khoản 3, 4, 5, 6 Điều này. Yêu cầu tổ chức tín dụng, chi nhánh ngân hàng nước ngoài cách chức và thực hiện các biện pháp xử lý khác theo quy định của pháp luật đối với cá nhân vi phạm thuộc thẩm quyền của tổ chức tín dụng, chi nhánh NHNN."
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-15T11:04:02.331Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0ab5"),
    "NODE__ID": ObjectId("5d2c5da072e61100084c0a9b"),
    VALUE: {
        text: "[ĐIỀU KHOẢN]: \r\nĐiều 27 Luật các TCTD 2010.\r\n[NỘI DUNG PHẢI TUÂN THỦ]\r\n 1. MSB được cấp Giấy phép phải sử dụng đúng tên và hoạt động đúng nội dung quy định trong Giấy phép. \r\n2. MSB được cấp Giấy phép không được tẩy xóa, mua, bán, chuyển nhượng, cho thuê, cho mượn Giấy phép. \r\n[DIỄN GIẢI, THAM CHIẾU, CHẾ TÀI] \r\nChế tài theo Khoản 1, Khoản 4, Khoản 6 và Khoản 8 Điều 4 Nghị định 96/2014/NĐ-CP \r\n1) Phạt tiền từ 20.000.000 đồng đến 40.000.000 đồng đối với hành vi sử dụng tên ghi trên các tài liệu, giấy tờ trong hoạt động không đúng tên ghi trong Giấy phép.\r\n"
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-15T11:04:02.336Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0ab7"),
    "NODE__ID": ObjectId("5d2c5da072e61100084c0a9c"),
    VALUE: {
        text: "[ĐIỀU KHOẢN] \r\nĐiều 4 Nghị định 96/2014/NĐ-CP.\r\n[NỘI NỘI DUNG PHẢI TUÂN THỦ]\r\nMSB không được: \r\n1. Gian lận các giấy tờ chứng minh đủ điều kiện để được cấp Giấy phép trong hồ sơ đề nghị cấp Giấy phép, trừ trường hợp quy định tại Khoản 2 Điều 19 Nghị định  96/2014/NĐ-CP; \r\n2. Cho mượn, cho thuê, mua, bán, chuyển nhượng Giấy phép; \r\n3. Tẩy xóa, sửa chữa giấy phép làm thay đổi nội dung Giấy phép. \r\n4. Giả mạo các giấy tờ chứng minh đủ điều kiện để được cấp Giấy phép trong hồ sơ đề nghị cấp Giấy phép, trừ trường hợp quy định tại Khoản 2 Điều 19 Nghị định  96/2014/NĐ-CP; \r\n5. Hoạt động không đúng nội dung ghi trong Giấy phép, trừ trường hợp quy định tại Khoản 6 Điều 18, Khoản 7 Điều 24, Điểm d Khoản 4, Điểm b Khoản 5 Điều 25 của Nghị định 96/2014/NĐ-CP.\r\n[DIỄN GIẢI, THAM CHIẾU, CHẾ TÀI] \r\nChế tài theo Khoản 3, Khoản 4, Khoản 5, Khoản 7 và Khoản 8 Điều 4 Nghị định 96/2014/NĐ-CP: \r\n 3) Phạt tiền từ 100.000.000 đồng đến 150.000.000 đồng đối với một trong các hành vi vi phạm sau đây:  a) Gian lận các giấy tờ chứng minh đủ điều kiện để được cấp giấy phép trong hồ sơ đề nghị cấp giấy phép, trừ trường hợp quy định tại Khoản 2 Điều 19 Nghị định này;  b) Cho mượn, cho thuê, mua, bán, chuyển nhượng giấy phép;  c) Tẩy xóa, sửa chữa giấy phép làm thay đổi nội dung giấy phép. \r\n4) Phạt tiền từ 150.000.000 đồng đến 200.000.000 đồng đối với một trong các hành vi vi phạm sau đây:  a) Giả mạo các giấy tờ chứng minh đủ điều kiện để được cấp giấy phép trong hồ sơ đề nghị cấp giấy phép, trừ trường hợp quy định tại Khoản 2 Điều 19 Nghị định này; \r\n5) Phạt tiền từ 300.000.000 đồng đến 400.000.000 đồng đối với hành vi hoạt động sau khi đã bị cơ quan có thẩm quyền áp dụng biện pháp đình chỉ theo quy định tại Điểm b Khoản 2 Điều 59 Luật Ngân hàng Nhà nước Việt Nam, Điểm a Khoản 2 Điều 148 Luật Các tổ chức tín dụng. \r\n7)  Áp dụng hình thức xử phạt bổ sung: Tịch thu tang vật là giấy phép đã bị tẩy xóa, sửa chữa đối với hành vi quy định tại Điểm c Khoản 3 Điều này. \r\n8)  Áp dụng biện pháp khắc phục hậu quả:\r\na) Buộc nộp vào ngân sách nhà nước số lợi bất hợp pháp có được do thực hiện hành vi vi phạm quy định tại các Điểm a, b Khoản 3, các Khoản 4, 5 và 6 Điều \r\n b) Đề nghị cấp có thẩm quyền thu hồi giấy phép đối với hành vi vi phạm tại Điểm a Khoản 3, Khoản 4 và Khoản 5 Điều này;  \r\nc) Đề nghị hoặc yêu cầu cấp có thẩm quyền xem xét, áp dụng biện pháp đình chỉ từ 01 tháng đến 03 tháng hoặc miễn nhiệm chức danh quản trị, điều hành, kiểm soát; không cho đảm nhiệm chức vụ quản trị, điều hành, kiểm soát tại các tổ chức tín dụng, chi nhánh ngân hàng nước ngoài đối với cá nhân vi phạm và/hoặc cá nhân chịu trách nhiệm đối với hành vi vi phạm quy định tại các Khoản 3, 4, 5, 6 Điều này. \r\nYêu cầu tổ chức tín dụng, chi nhánh ngân hàng nước ngoài cách chức và thực hiện các biện pháp xử lý khác theo quy định của pháp luật đối với cá nhân vi phạm thuộc thẩm quyền của tổ chức tín dụng, chi nhánh NHNN."
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-15T11:04:02.35Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0ab8"),
    "NODE__ID": ObjectId("5d2c5da072e61100084c0a97"),
    VALUE: {
        attachment: {
            type: "template",
            payload: {
                "template_type": "generic",
                elements: [
                    {
                        title: "Tuân thủ nghiệp vụ",
                        "image_url": "https://i.imgur.com/uWond2Q.jpg",
                        subtitle: "Sổ tay tuân thủ nghiệp vụ",
                        buttons: [
                            {
                                title: "CÔNG CỤ CHUYỂN NHƯỢNG",
                                type: "postback",
                                payload: "CONGCUCHUYENNHUONG"
                            }
                        ]
                    }
                ]
            }
        }
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-15T11:04:02.351Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": "2019-07-17T07:29:45.499Z",
    "UPDATED_BY": "5d2c5d9e72e61100084c0a7f"
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0ab9"),
    "NODE__ID": ObjectId("5d2c5da072e61100084c0a99"),
    VALUE: {
        text: "[ĐIỀU KHOẢN]: \r\n- Điều 23 Luật các  TCTD  2010 ; \r\n- Điều 6 Thông tư 40/2011/TT-NHNN; \r\n- Khoản 2 Điều 1 Thông tư 28/2018/TT-NHNN; \r\n[NỘI DUNG PHẢI TUÂN THỦ]\r\n 1. MSB khi được cấp Giấy phép phải nộp lệ phí cấp Giấy phép tại NHNN (Sở Giao dịch) trong thời hạn 15 ngày kể từ ngày được cấp Giấy phép. \r\n2. Mức lệ phí cấp Giấy phép theo quy định của pháp luật về phí, lệ phí \r\n"
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-15T11:04:02.352Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0ab2"),
    "NODE__ID": ObjectId("5d2c5da072e61100084c0a92"),
    VALUE: {
        attachment: {
            type: "template",
            payload: {
                "template_type": "generic",
                elements: [
                    {
                        title: "Cẩm nang tư vấn pháp luật",
                        "image_url": "https://i.imgur.com/CcUjisR.jpg",
                        subtitle: "Bấm vào nút bên dưới để mở Cẩm nang tư vấn pháp luật.",
                        buttons: [
                            {
                                title: "CHI TIẾT",
                                type: "web_url",
                                url: null,
                                "webview_height_ratio": "tall"
                            }
                        ]
                    }
                ]
            }
        }
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-15T11:04:02.165Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0ab4"),
    "NODE__ID": ObjectId("5d2c5da072e61100084c0a9f"),
    VALUE: {
        text: "[ĐIỀU KHOẢN] \r\nKhoản 2 Điều 4 Thông tư 40/2011/TT-NHNN\r\n[NỘI DUNG PHẢI TUÂN THỦ]\r\nTrường hợp Giấy phép bị mất, bị rách, bị cháy hoặc bị hủy dưới hình thức khác, MSB phải có văn bản nêu rõ lý do và gửi qua đường bưu điện hoặc nộp trực tiếp tại NHNN đề nghị NHNN xem xét cấp bản sao Giấy phép từ sổ gốc theo quy định của pháp luật.\r\nTrong thời hạn 02 ngày kể từ ngày nhận được văn bản đề nghị, NHNN cấp lại bản sao Giấy phép từ bản gốc cho MSB.\r\n[DIỄN GIẢI, THAM CHIẾU, CHẾ TÀI] \r\nChưa xác định chế tài xử phạt\r\n"
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-15T11:04:02.335Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0aba"),
    "NODE__ID": ObjectId("5d2c5da072e61100084c0a9e"),
    VALUE: {
        text: "[ĐIỀU KHOẢN] \r\nĐiều 22 Thông tư 40/2011/TT-NHNN.\r\n[NỘI NỘI DUNG PHẢI TUÂN THỦ]\r\n1. Thời hạn hoạt động của MSB được ghi trong Giấy phép tối đa không quá 99 năm.\r\n2. Trình tự, thủ tục và hồ sơ đề nghị thay đổi thời hạn hoạt động thực hiện theo hướng dẫn của NHNN.\r\n[DIỄN GIẢI, THAM CHIẾU, CHẾ TÀI] \r\n4) Phạt tiền từ 150.000.000 đồng đến 200.000.000 đồng đối với một trong các hành vi vi phạm sau đây: b) Hoạt động không đúng nội dung ghi trong giấy phép, trừ trường hợp quy định tại Khoản 6 Điều 18, Khoản 7 Điều 24, Điểm d Khoản 4, Điểm b Khoản 5 Điều 25 Nghị định này. \r\n6) Phạt tiền từ 400.000.000 đồng đến 500.000.000 đồng đối với hành vi hoạt động không có giấy phép, trừ trường hợp quy định tại Khoản 7 Điều 24, Điểm d Khoản 2, Điểm a Khoản 4, Khoản 7 Điều 25 Nghị định này.\r\n8) Áp dụng biện pháp khắc phục hậu quả: a) Buộc nộp vào ngân sách nhà nước số lợi bất hợp pháp có được do thực hiện hành vi vi phạm quy định tại các Điểm a, b Khoản 3, các Khoản 4, 5 và 6 Điều này;b) Đề nghị cấp có thẩm quyền thu hồi giấy phép đối với hành vi vi phạm tại Điểm a  Khoản 3, Khoản 4 và Khoản 5 Điều này; c) Đề nghị hoặc yêu cầu cấp có thẩm quyền xem xét, áp dụng biện pháp đình chỉ từ 01 tháng đến 03 tháng hoặc miễn nhiệm chức danh quản trị, điều hành, kiểm soát; không cho đảm nhiệm chức vụ quản trị, điều hành, kiểm soát tại các tổ chức tín dụng, chi nhánh ngân hàng nước ngoài đối với cá nhân vi phạm và/hoặc cá nhân chịu trách nhiệm đối với hành vi vi phạm quy định tại các Khoản 3, 4, 5, 6 Điều này. Yêu cầu tổ chức tín dụng, chi nhánh ngân hàng nước ngoài cách chức và thực hiện các biện pháp xử lý khác theo quy định của pháp luật đối với cá nhân vi phạm thuộc thẩm quyền của tổ chức tín dụng, chi nhánh NHNN."
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-15T11:04:02.357Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0ab6"),
    "NODE__ID": ObjectId("5d2c5da072e61100084c0a9d"),
    VALUE: {
        attachment: {
            type: "template",
            payload: {
                "template_type": "generic",
                elements: [
                    {
                        title: "Quản lý giấy phép",
                        "image_url": "https://i.imgur.com/TGIwLTX.png",
                        subtitle: "",
                        buttons: [
                            {
                                title: "CHI TIẾT",
                                type: "postback",
                                payload: "LICENSE_MANAGEMENT"
                            }
                        ]
                    },
                    {
                        title: "Xin chấp nhận thay đổi",
                        "image_url": "https://i.imgur.com/TGIwLTX.png",
                        subtitle: "",
                        buttons: [
                            {
                                title: "CHI TIẾT",
                                type: "postback",
                                payload: "ACCEPTING_CHANGE"
                            }
                        ]
                    },
                    {
                        title: "Mạng lưới hoạt động",
                        "image_url": "https://i.imgur.com/TGIwLTX.png",
                        subtitle: "",
                        buttons: [
                            {
                                title: "CHI TIẾT",
                                type: "postback",
                                payload: "NETWORK_OPERATION"
                            }
                        ]
                    },
                    {
                        title: "Đăng ký doanh nghiệp",
                        "image_url": "https://i.imgur.com/TGIwLTX.png",
                        subtitle: "",
                        buttons: [
                            {
                                title: "CHI TIẾT",
                                type: "postback",
                                payload: "BUSINESS_REGISTRATION"
                            }
                        ]
                    },
                    {
                        title: "Chi nhánh, địa điểm kinh doanh",
                        "image_url": "https://i.imgur.com/TGIwLTX.png",
                        subtitle: "",
                        buttons: [
                            {
                                title: "CHI TIẾT",
                                type: "postback",
                                payload: "BRANCH/TERMINAL"
                            }
                        ]
                    },
                    {
                        title: "Con dấu",
                        "image_url": "https://i.imgur.com/TGIwLTX.png",
                        subtitle: "",
                        buttons: [
                            {
                                title: "CHI TIẾT",
                                type: "postback",
                                payload: "STAMP"
                            }
                        ]
                    }
                ]
            }
        }
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-15T11:04:02.337Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2d3dcafd4db6000917d306"),
    "NODE__ID": ObjectId("5d2d3dcafd4db6000917d305"),
    VALUE: null,
    TYPE: null,
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-16T03:00:26.223Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2d3f50fd4db6000917d30c"),
    "NODE__ID": ObjectId("5d2d3f50fd4db6000917d30b"),
    VALUE: null,
    TYPE: null,
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-16T03:06:56.427Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2d402dfd4db6000917d30d"),
    "NODE__ID": ObjectId("5d2d3dcafd4db6000917d305"),
    VALUE: {
        attachment: {
            type: "template",
            payload: {
                "template_type": "generic",
                elements: [
                    {
                        title: "Xin chấp nhận thay đổi",
                        subtitle: "Xin chấp nhận thay đổi",
                        "image_url": "http://cafefcdn.com/thumb_w/650/Images/Uploaded/Share/15944fdeb6357ea97b8ed6c36bac9100/2014/02/11/luatkvn.jpg",
                        buttons: [
                            {
                                title: "NHỮNG THAY ĐỔI CẦN ĐƯỢC CHẤP NHẬN",
                                type: "postback",
                                payload: "NHUNGTHAYDOIPHAIDUOCNHNNCHAPNHAN"
                            },
                            {
                                title: "THỰC HIỆN THỦ TỤC SAU",
                                type: "postback",
                                payload: "THUCHIENTHUTUCSAUKHIDUOCNHNNCHAPTHUANTHAYDOI"
                            },
                            {
                                title: "Mở web",
                                type: "web_url",
                                "webview_height_ratio": "tall",
                                url: "Mở web"
                            }
                        ]
                    }
                ]
            }
        }
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("1"),
    "CREATED_DATE": ISODate("2019-07-16T03:10:37.917Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": "2019-07-16T03:29:00.733Z",
    "UPDATED_BY": "5d2c5d9e72e61100084c0a7f"
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2d40ccfd4db6000917d31a"),
    "NODE__ID": ObjectId("5d2d3f01fd4db6000917d309"),
    VALUE: {
        text: "[Điều khoản]\n- Khoản 1 Điều 29, Khoản 2 Điều 102, Khoản 5 Điều 103 và Khoản 1 Điều 153 Luật các TCTD 2010; \n- Khoản 4 Điều 1 Luật các TCTD 2017; \n- Điều 30 Thông tư 40/2011/TT-NHNN , \n[Nội dung]\nMSB phải được NHNN chấp thuận bằng văn bản trước khi thực hiện các thủ tục thay đổi một trong những nội dung sau đây:\n- Tên, địa điểm đặt trụ sở chính của MSB.\n- Địa điểm đặt chi nhánh của MSB.\n- Mức vốn điều lệ, mức vốn được cấp.\n- Nội dung, phạm vi và thời hạn hoạt động.\n- Mua bán, chuyển nhượng phần vốn góp của chủ sở hữu; mua bán, chuyển nhượng phần vốn góp của thành viên góp vốn; mua bán, chuyển nhượng cổ phần của cổ đông lớn; mua bán, chuyển nhượng cổ phần dẫn đến cổ đông lớn thành cổ đông thường và ngược lại.\n..."
    },
    TYPE: "TEXT",
    ORDER: NumberInt("1"),
    "CREATED_DATE": ISODate("2019-07-16T03:13:16.751Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2d40fdfd4db6000917d31b"),
    "NODE__ID": ObjectId("5d2d3f50fd4db6000917d30b"),
    VALUE: {
        text: "[Điều khoản]\n- Khoản 4 Điều 29 Luật các TCTD 2010;\n- Khoản 5 Điều 1 Luật các TCTD 2017;\n- Điều 2 Thông tư 28/2018/TT-NHNN.\n[Nội dung]\n1.\tKhi được chấp thuận thay đổi một hoặc một số nội dung quy định tại khoản 1 Điều 29, MSB phải: Sửa đổi, bổ sung Điều lệ của MSB phù hợp với thay đổi đã được chấp thuận \n2.\tMSB phải đăng ký với cơ quan nhà nước có thẩm quyền về những thay đổi quy định tại Khoản 1 Điều 29 Luật các TCTD 2010;\n3.\tMSB phải công bố nội dung thay đổi khi thay đổi về một trong những nội dung sau đây trên các phương tiện thông tin của NHNN và một tờ báo viết hằng ngày trong 03 số liên tiếp hoặc báo điện tử của Việt Nam trong thời hạn 07 ngày làm việc, kể từ ngày được NHNN chấp thuận:\na)\tTên, địa điểm đặt trụ sở chính của MSB; \nb)\tMức vốn điều lệ;\nc)\tĐịa điểm đặt trụ sở chi nhánh của MSB; \nd)\tNội dung, phạm vi và thời hạn hoạt động."
    },
    TYPE: "TEXT",
    ORDER: NumberInt("1"),
    "CREATED_DATE": ISODate("2019-07-16T03:14:05.155Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2ea63f0349d20017501f9b"),
    "NODE__ID": ObjectId("5d2ea63f0349d20017501f9a"),
    VALUE: null,
    TYPE: null,
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-17T04:38:23.103Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2ea66d0349d20017501f9d"),
    "NODE__ID": ObjectId("5d2ea66d0349d20017501f9c"),
    VALUE: null,
    TYPE: null,
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-17T04:39:09.974Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2ea68d0349d20017501f9f"),
    "NODE__ID": ObjectId("5d2ea68d0349d20017501f9e"),
    VALUE: null,
    TYPE: null,
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-17T04:39:41.027Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2ec438c782c40017769473"),
    "NODE__ID": ObjectId("5d2ea66d0349d20017501f9c"),
    VALUE: {
        attachment: {
            type: "template",
            payload: {
                "template_type": "generic",
                elements: [
                    {
                        title: "THỜI TIẾT",
                        subtitle: "Thời tiết",
                        "image_url": "https://d279m997dpfwgl.cloudfront.net/wp/2017/12/weather_album-art-1000x1000.jpg",
                        buttons: [
                            {
                                title: "CHI TIẾT",
                                type: "postback",
                                payload: "WEATHER"
                            }
                        ]
                    }
                ]
            }
        }
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("1"),
    "CREATED_DATE": ISODate("2019-07-17T06:46:16.828Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2ec11ac782c4001776946c"),
    "NODE__ID": ObjectId("5d2ea68d0349d20017501f9e"),
    VALUE: {
        text: "Bạn đã kích hoạt thành công chat với Trí tuệ nhân tạo 👌 Mời bạn chat để được tư vấn ✌️"
    },
    TYPE: "TEXT",
    ORDER: NumberInt("1"),
    "CREATED_DATE": ISODate("2019-07-17T06:32:58.021Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2ec3c1c782c4001776946e"),
    "NODE__ID": ObjectId("5d2ea63f0349d20017501f9a"),
    VALUE: {
        attachment: {
            type: "template",
            payload: {
                "template_type": "generic",
                elements: [
                    {
                        title: "TUÂN THỦ CHUNG",
                        subtitle: "Bộ chỉ tiêu tuân thủ chung",
                        "image_url": "https://bennelonglegal.com.au/images/2018/08/24/bennelong-legal-services.jpg",
                        buttons: [
                            {
                                title: "CHI TIẾT",
                                type: "postback",
                                payload: "GENERAL_COMPLIANCE"
                            }
                        ]
                    },
                    {
                        title: "TUÂN THỦ NGHIỆP VỤ",
                        subtitle: "Bộ chỉ tiêu thủ theo hoạt động nghiệp vụ",
                        "image_url": "http://www.irislink.com/Partners/test/Menu2018/img/Legal01.jpg",
                        buttons: [
                            {
                                title: "CHI TIẾT",
                                type: "postback",
                                payload: "BUSINESS_COMPLIANCE"
                            }
                        ]
                    },
                    {
                        title: "CẨM NANG TƯ VẤN",
                        subtitle: "Cẩm nang tư vấn",
                        "image_url": "https://cdn.lynda.com/course/724786/724786-636924146919841630-16x9.jpg",
                        buttons: [
                            {
                                title: "CHI TIẾT",
                                type: "postback",
                                payload: "HANDBOOK"
                            }
                        ]
                    }
                ]
            }
        }
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("1"),
    "CREATED_DATE": ISODate("2019-07-17T06:44:17.267Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": "2019-07-17T07:14:16.775Z",
    "UPDATED_BY": "5d2c5d9e72e61100084c0a7f"
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2ecbeec782c4001776949a"),
    "NODE__ID": ObjectId("5d2ecbeec782c40017769499"),
    VALUE: null,
    TYPE: null,
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-17T07:19:10.323Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2ecd92c782c4001776949b"),
    "NODE__ID": ObjectId("5d2ecbeec782c40017769499"),
    VALUE: {
        attachment: {
            type: "template",
            payload: {
                "template_type": "generic",
                elements: [
                    {
                        title: "CÔNG CỤ CHUYỂN NHƯỢNG",
                        subtitle: "Sổ tay tuân thủ về Công cụ chuyển nhượng",
                        "image_url": "http://www.northumberland.gov.uk/NorthumberlandCountyCouncil/media/About-the-Council-Media/policy-signing.jpg?ext=.jpg",
                        buttons: [
                            {
                                title: "QUY ĐỊNH CHUNG VỀ CÔNG CỤ CHUYỂN NHƯỢNG",
                                type: "postback",
                                payload: "QUYDINHCHUNGVECONGCUCHUYENNHUONG"
                            },
                            {
                                title: "HỒI PHIẾU",
                                type: "postback",
                                payload: "HOIPHIEU"
                            },
                            {
                                title: "SÉC",
                                type: "postback",
                                payload: "SEC"
                            }
                        ]
                    }
                ]
            }
        }
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("1"),
    "CREATED_DATE": ISODate("2019-07-17T07:26:10.442Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2ef8ec7cd18c0017afa7d0"),
    "NODE__ID": ObjectId("5d2ec5fbc782c4001776947a"),
    VALUE: {
        text: "ALO"
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-17T10:31:08.565Z"),
    "CREATED_BY": ObjectId("5d2c5d9e72e61100084c0a7f"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2f035b7cd18c0017afa7d3"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b43a"),
    VALUE: {
        text: "A. Điều khoản tham chiếu: Khoản 2 Điều 3, Luật các CCCN.\nB. Nội dung quy định: Người ký phát, người phát hành được phát hành công cụ chuyển nhượng trên cơ sở giao dịch mua bán hàng hoá, cung ứng dịch vụ, cho vay giữa các tổ chức, cá nhân với nhau; giao dịch cho vay của MSB với tổ chức, cá nhân; giao dịch thanh toán và giao dịch tặng cho theo quy định của pháp luật."
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-17T11:15:39.67Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": "2019-07-18T02:51:34.308Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2f04c37cd18c0017afa7d8"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b44f"),
    VALUE: {
        attachment: {
            type: "template",
            payload: {
                "template_type": "generic",
                elements: [
                    {
                        title: "QUY ĐỊNH CHUNG VỀ CÔNG CỤ CHUYỂN NHƯỢNG",
                        subtitle: "QUY ĐỊNH CHUNG VỀ CÔNG CỤ CHUYỂN NHƯỢNG",
                        "image_url": "https://thukyluat.vn/uploads/image/2018/09/07/100028781.jpg",
                        buttons: [
                            {
                                title: "Cơ sở phát hành công cụ chuyển nhượng",
                                type: "postback",
                                payload: "COSOPHATHANHCONGCUCHUYENNHUONG"
                            },
                            {
                                title: "Thanh toán công cụ chuyển nhượng ghi trả bằng ngoại tệ",
                                type: "postback",
                                payload: "THANHTOANCONGCUCHUYENNHUONGGHITRABANGNGOAITE"
                            },
                            {
                                title: "Ký công cụ chuyển nhượng",
                                type: "postback",
                                payload: "KYCONGCUCHUYENNHUONG"
                            }
                        ]
                    }
                ]
            }
        }
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-17T11:21:39.494Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": "2019-07-17T11:22:53.417Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2f05997cd18c0017afa7db"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b44f"),
    VALUE: {
        attachment: {
            type: "template",
            payload: {
                "template_type": "generic",
                elements: [
                    {
                        title: "QUY ĐỊNH CHUNG VỀ CÔNG CỤ CHUYỂN NHƯỢNG",
                        subtitle: "QUY ĐỊNH CHUNG VỀ CÔNG CỤ CHUYỂN NHƯỢNG",
                        "image_url": "https://thukyluat.vn/uploads/image/2018/09/07/100028781.jpg",
                        buttons: [
                            {
                                title: "Ngôn ngữ trên công cụ chuyển nhượng",
                                type: "postback",
                                payload: "NGONNGUTRENCONGCUCHUYENNHUONG"
                            },
                            {
                                title: "Hư hỏng công cụ chuyển nhượng",
                                type: "postback",
                                payload: "HUHONGCONGCUCHUYENNHUONG"
                            },
                            {
                                title: "Các hành vi bị cấm khi giao dịch công cụ chuyển nhượng",
                                type: "postback",
                                payload: "CACHANHVIBICAMKHIGIAODICHCONGCUCHUYENNHUONG"
                            }
                        ]
                    }
                ]
            }
        }
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("1"),
    "CREATED_DATE": ISODate("2019-07-17T11:25:13.431Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": "2019-07-17T11:28:10.041Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2f06b57cd18c0017afa7dd"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b43b"),
    VALUE: {
        text: "A. Điều khoản tham chiếu: Khoản 2 Điều 9, Luật các CCCN.\nB. Nội dung quy định: \n1.\tMSB chỉ thực hiện thanh toán bằng ngoại tệ các công cụ chuyển nhượng ghi trả bằng ngoại tệ khi người thụ hưởng cuối cùng được phép thu ngoại tệ theo quy định của pháp luật về quản lý ngoại hối.\n2.\tTrong trường hợp công cụ chuyển nhượng ghi trả bằng ngoại tệ nhưng người thụ hưởng cuối cùng là người không được phép thu ngoại tệ theo quy định của pháp luật về quản lý ngoại hối thì MSB phải thực hiện thanh toán số tiền trên công cụ chuyển nhượng bằng đồng Việt Nam theo tỷ giá hối đoái do Ngân hàng Nhà nước Việt Nam công bố tại thời điểm thanh toán hoặc theo tỷ giá kinh doanh ngoại tệ của MSB công bố tại thời điểm thanh toán.\nC. Chế tài theo khoản 2 Điều 24 Nghị định 96/2014/NĐ-CP: \n2. Phạt tiền từ 40.000.000 đồng đến 80.000.000 đồng đối với một trong các hành vi vi phạm sau đây:\nc) Thanh toán công cụ chuyển nhượng bằng ngoại tệ không đúng quy định về hoạt động ngoại hối quy định tại Điều 9 Luật Các công cụ chuyển nhượng và các quy định pháp luật có liên quan.\n"
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-17T11:29:57.87Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": "2019-07-18T02:55:51.768Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2fde0b1330ca0017dacc4d"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b43c"),
    VALUE: {
        text: "A. Điều khoản dẫn chiếu: Điều 11, Điều 12 và Khoản 3 Điều 15 Luật các CCCN.\nB. Nội dung quy định: \n1.\tChữ ký đủ ràng buộc nghĩa vụ:\na)\tCông cụ chuyển nhượng phải có chữ ký của người ký phát hoặc người phát hành.\nb)\tNgười có liên quan chỉ có nghĩa vụ theo công cụ chuyển nhượng khi trên công cụ chuyển nhượng hoặc tờ phụ đính kèm có chữ ký của người có liên quan hoặc của người được người có liên quan uỷ quyền với tư cách là người ký phát, người phát hành, người chấp nhận, người chuyển nhượng hoặc người bảo lãnh\".\n2.\tKhi trên công cụ chuyển nhượng có chữ ký giả mạo hoặc chữ ký của người không được uỷ quyền thì chữ ký đó không có giá trị; chữ ký của người có liên quan khác trên công cụ chuyển nhượng vẫn có giá trị.\n3.\tHành vi bị cấm: Ký công cụ chuyển nhượng không đúng thẩm quyền hoặc giả mạo chữ ký trên công cụ chuyển nhượng.\nC. Chế tài theo khoản 1, điểm a khoản 4, khoản 6 Điều 29 Nghị định 96/2014/NĐ-CP: \n1. Phạt tiền từ 15.000.000 đồng đến 20.000.000 đồng đối với hành vi ký vào công cụ chuyển nhượng không đúng thẩm quyền. \n4. Phạt tiền từ 60.000.000 đồng đến 120.000.000 đồng đối với một trong các hành vi vi phạm sau đây: \na)  Hành vi giả mạo chữ ký trên công cụ chuyển nhượng. \n6. Áp dụng biện pháp khắc phục hậu quả: Buộc nộp vào ngân sách nhà nước số lợi bất hợp pháp có được do thực hiện hành vi vi phạm.\n\n\n"
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-18T02:48:43.278Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": "2019-07-18T02:55:21.790Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2fde891330ca0017dacc4e"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b43d"),
    VALUE: {
        text: "A. Điều khoản tham chiếu: Điều 10 Luật các CCCN.\nB. Nội dung quy định: Công cụ chuyển nhượng phải được lập bằng tiếng Việt, trừ trường hợp quan hệ công cụ chuyển nhượng có yếu tố nước ngoài thì công cụ chuyển nhượng có thể được lập bằng tiếng nước ngoài theo thoả thuận của các bên."
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-18T02:50:49.108Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2fdeec1330ca0017dacc4f"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b43e"),
    VALUE: {
        text: "A. Điều khoản dẫn chiếu: Điều 14 Luật các CCCN.\nB. Nội dung quy định: Trường hợp MSB là người ký phát, người phát hành: MSB có nghĩa vụ phát hành lại công cụ chuyển nhượng, sau khi nhận được công cụ chuyển nhượng bị hư hỏng nếu công cụ chuyển nhượng này chưa đến hạn thanh toán và còn đủ thông tin hoặc có bằng chứng xác định người có công cụ bị hư hỏng là người thụ hưởng hợp pháp công cụ chuyển nhượng."
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-18T02:52:28.205Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2fdf5c1330ca0017dacc50"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b43f"),
    VALUE: {
        text: "A. Điều khoản dẫn chiếu: Điều 15 Luật các CCCN.\nB. Nội dung quy định: \n1.\tCác hành vi bị cấm:\na)\tLàm giả công cụ chuyển nhượng, sửa chữa hoặc tẩy xóa các yếu tố trên công cụ chuyển nhượng.\nb)\tCố ý chuyển nhượng hoặc nhận chuyển nhượng hoặc xuất trình để thanh toán công cụ chuyển nhượng bị làm giả, bị sửa chữa, bị tẩy xóa.\nc)\tKý công cụ chuyển nhượng không đúng thẩm quyền hoặc giả mạo chữ ký trên công cụ chuyển nhượng.\nd)\tChuyển nhượng công cụ chuyển nhượng khi đã biết công cụ chuyển nhượng này quá hạn thanh toán hoặc đã bị từ chối chấp nhận, bị từ chối thanh toán hoặc đã được thông báo bị mất.\ne)\tCố ý phát hành công cụ chuyển nhượng khi không đủ khả năng thanh toán.\nf)\tCố ý phát hành séc sau khi bị đình chỉ quyền phát hành séc.\nC. Chế tài theo điểm a khoản 3 Điều 29 Nghị định 96/2014/NĐ-CP: \n3. Phạt tiền từ 30.000.000 đồng đến 60.000.000 đồng đối với một trong các hành vi vi phạm sau đây:\na)   Chuyển nhượng công cụ chuyển nhượng khi đã biết công cụ chuyển nhượng này quá hạn thanh toán hoặc đã bị từ chối chấp nhận, bị từ chối thanh toán hoặc đã được thông báo bị mất quy định tại Khoản 4 Điều 15 Luật Các công cụ chuyển nhượng.\n\n"
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-18T02:54:20.936Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2fe2f01330ca0017dacc5c"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b451"),
    VALUE: {
        attachment: {
            type: "template",
            payload: {
                "template_type": "generic",
                elements: [
                    {
                        title: "SEC",
                        subtitle: "SEC",
                        "image_url": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRa-jmGDOQa7FXzSK44om5k8Bd4FerCVZOTA2elDQRoTjV39jEZeA",
                        buttons: [
                            {
                                title: "Các nội dung của séc",
                                type: "postback",
                                payload: "CACNOIDUNGCUASEC"
                            },
                            {
                                title: "In, giao nhận và bảo quản séc trắng",
                                type: "postback",
                                payload: "IN,GIAONHANVABAOQUANSECTRANG"
                            },
                            {
                                title: "Thanh toán séc tại người bị ký phát ",
                                type: "postback",
                                payload: "THANHTOANSECTAINGUOIBIKYPHAT "
                            }
                        ]
                    },
                    {
                        title: "SEC",
                        subtitle: "SEC",
                        "image_url": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRa-jmGDOQa7FXzSK44om5k8Bd4FerCVZOTA2elDQRoTjV39jEZeA",
                        buttons: [
                            {
                                title: "Xử lý séc không đủ khả năng thanh toán",
                                type: "postback",
                                payload: "XULYSECKHONGDUKHANANGTHANHTOAN"
                            },
                            {
                                title: "Quy định cung ứng, thanh toán, đình chỉnh thanh toán séc",
                                type: "postback",
                                payload: "QUYDINHCUNGUNG,THANHTOAN,ĐINHCHINHTHANHTOANSEC"
                            },
                            {
                                title: "Xử lý mất séc",
                                type: "postback",
                                payload: "XULYMATSEC"
                            }
                        ]
                    }
                ]
            }
        }
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-18T03:09:36.173Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": "2019-07-18T07:07:23.120Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2fe3e01330ca0017dacc62"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b450"),
    VALUE: {
        attachment: {
            type: "template",
            payload: {
                "template_type": "generic",
                elements: [
                    {
                        title: "HOIPHIEU",
                        subtitle: "HOIPHIEU",
                        "image_url": "https://www.google.com/url?sa=i&rct=j&q=&esrc=s&source=images&cd=&ved=2ahUKEwj72OGov73jAhVU7mEKHTMhA3YQjRx6BAgBEAU&url=https%3A%2F%2Fkienthucchuyennganh.com%2Fhoi-phieu-la-gi-cac-loai-hoi-phieu%2F&psig=AOvVaw18ZSZhAXG0yS03LVSoG9Sa&ust=1563505824974046",
                        buttons: [
                            {
                                title: "Nội dung của hối phiếu đòi nợ",
                                type: "postback",
                                payload: "NOIDUNGCUAHOIPHIEUDOINO"
                            },
                            {
                                title: "Nội dung của hối phiếu nhận nợ",
                                type: "postback",
                                payload: "NOIDUNGCUAHOIPHIEUNHANNO"
                            },
                            {
                                title: "Nghĩa vụ của người ký phát hối phiếu đòi nợ",
                                type: "postback",
                                payload: "NGHIAVUCUANGUOIKYPHATHOIPHIEUDOINO"
                            }
                        ]
                    },
                    {
                        title: "HOIPHIEU",
                        subtitle: "HOIPHIEU",
                        "image_url": "https://kienthucchuyennganh.com/wp-content/uploads/2019/01/mau-hoi-phieu-678x381.jpg",
                        buttons: [
                            {
                                title: "Nghĩa vụ của người phát hành hối phiếu nhận nợ",
                                type: "postback",
                                payload: "NGHIAVUCUANGUOIKYPHATHOIPHIEUNHANNO"
                            },
                            {
                                title: "Nghĩa vụ của người chấp nhận hối phiếu đòi nợ",
                                type: "postback",
                                payload: "NGHIAVUCUANGUOICHAPNHANHOIPHIEUDOINO"
                            },
                            {
                                title: "Nghĩa vụ của người thu hộ hối phiếu đòi nợ",
                                type: "postback",
                                payload: "NGHIAVUCUANGUOITHUHOHOIPHIEUDOINO"
                            }
                        ]
                    },
                    {
                        title: "HOIPHIEU",
                        subtitle: "HOIPHIEU",
                        "image_url": "https://kienthucchuyennganh.com/wp-content/uploads/2019/01/mau-hoi-phieu-678x381.jpg",
                        buttons: [
                            {
                                title: "Hướng dẫn thực hiện về nhờ thu hối phiếu qua người thu hộ",
                                type: "postback",
                                payload: "HUONGDANTHUCHIENVENHOTHUHOIPHIEUQUANGUOITHUHO"
                            },
                            {
                                title: "Kiểm soát, xử lý hối phiếu bị mất",
                                type: "postback",
                                payload: "KIEMSOAT,XULYHOIPHIEUBIMAT"
                            },
                            {
                                title: "Phí dịch vụ thu hộ hối phiếu",
                                type: "postback"
                            }
                        ]
                    }
                ]
            }
        }
    },
    TYPE: "GENERIC",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-18T03:13:36.589Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": "2019-07-18T06:39:43.644Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2febf41330ca0017dacc75"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b440"),
    VALUE: {
        text: "A. ĐIỀU KHOẢN DẪN CHIẾU: Khoản 1 Điều 16 Luật các CCCN.\nB. NỘI DUNG QUY ĐỊNH\n1.\tHối phiếu đòi nợ có các nội dung sau đây:\na)\tCụm từ “Hối phiếu đòi nợ” được ghi trên mặt trước của hối phiếu đòi nợ;\nb)\tYêu cầu thanh toán không điều kiện một số tiền xác định;\nc)\tThời hạn thanh toán;\nd)\tĐịa điểm thanh toán;\ne)\tTên đối với tổ chức hoặc họ, tên đối với cá nhân, địa chỉ của người bị ký phát;\nf)\tTên đối với tổ chức hoặc họ, tên đối với cá nhân của người thụ hưởng được người ký phát chỉ định hoặc yêu cầu thanh toán hối phiếu đòi nợ theo lệnh của người thụ hưởng hoặc yêu cầu thanh toán hối phiếu đòi nợ cho người cầm giữ;\ng)\tĐịa điểm và ngày ký phát;\nh)\tTên đối với tổ chức hoặc họ, tên đối với cá nhân, địa chỉ và chữ ký của người ký phát.\n2.\tHối phiếu đòi nợ không có giá trị nếu thiếu một trong các nội dung quy định tại khoản 1 Điều này, trừ các trường hợp sau đây:\na)\tThời hạn thanh toán không được ghi trên hối phiếu đòi nợ thì hối phiếu đòi nợ sẽ được thanh toán ngay khi xuất trình;\nb)\tĐịa điểm thanh toán không được ghi trên hối phiếu đòi nợ thì hối phiếu đòi nợ sẽ được thanh toán tại địa chỉ của người bị ký phát;\nc)\tĐịa điểm ký phát không được ghi cụ thể trên hối phiếu đòi nợ thì hối phiếu đòi nợ được coi là ký phát tại địa chỉ của người ký phát.\n3.\tKhi số tiền trên hối phiếu đòi nợ được ghi bằng số khác với số tiền ghi bằng chữ thì số tiền ghi bằng chữ có giá trị thanh toán. Trong trường hợp số tiền trên hối phiếu đòi nợ được ghi hai lần trở lên bằng chữ hoặc bằng số và có sự khác nhau thì số tiền có giá trị nhỏ nhất được ghi bằng chữ có giá trị thanh toán.\n4.\tTrong trường hợp hối phiếu đòi nợ không có đủ chỗ để viết, hối phiếu đòi nợ đó có thể có thêm tờ phụ đính kèm. Tờ phụ đính kèm được sử dụng để ghi nội dung bảo lãnh, chuyển nhượng, cầm cố, nhờ thu. Người đầu tiên lập tờ phụ phải gắn liền tờ phụ với hối phiếu đòi nợ và ký tên trên chỗ giáp lai giữa tờ phụ và hối phiếu đòi nợ.\n"
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-18T03:48:04.183Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2fec621330ca0017dacc76"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b441"),
    VALUE: {
        text: "A. ĐIỀU KHOẢN DẪN CHIẾU: Điều 53 Luật các CCCN.\nB. NỘI DUNG QUY ĐỊNH:\n1.\tHối phiếu nhận nợ có các nội dung sau đây:\na)\tCụm từ “Hối phiếu nhận nợ” được ghi trên mặt trước của hối phiếu nhận nợ;\nb)\tCam kết thanh toán không điều kiện một số tiền xác định;\nc)\tThời hạn thanh toán;\nd)\tĐịa điểm thanh toán;\ne)\tTên đối với tổ chức hoặc họ, tên đối với cá nhân của người thụ hưởng được người phát hành chỉ định hoặc yêu cầu thanh toán hối phiếu nhận nợ theo lệnh của người thụ hưởng hoặc yêu cầu thanh toán hối phiếu cho người cầm giữ;\nf)\tĐịa điểm và ngày ký phát hành;\ng)\tTên đối với tổ chức hoặc họ, tên đối với cá nhân, địa chỉ và chữ ký của người phát hành.\n2.\tHối phiếu nhận nợ không có giá trị nếu thiếu một trong các nội dung quy định tại khoản 1 Điều này, trừ các trường hợp sau đây:\na)\tTrường hợp địa điểm thanh toán không được ghi trên hối phiếu nhận nợ thì địa điểm thanh toán là địa chỉ của người phát hành.\nb)\tTrường hợp địa điểm phát hành không được ghi trên hối phiếu nhận nợ thì địa điểm phát hành là địa chỉ của người phát hành.\n3.\tKhi số tiền trên hối phiếu nhận nợ được ghi bằng số khác với số tiền ghi bằng chữ thì số tiền ghi bằng chữ có giá trị thanh toán. Trong trường hợp số tiền trên hối phiếu nhận nợ được ghi hai lần trở lên bằng chữ hoặc bằng số và có sự khác nhau thì số tiền có giá trị nhỏ nhất được ghi bằng chữ có giá trị thanh toán.\n4.\tTrong trường hợp hối phiếu nhận nợ không có đủ chỗ để viết, hối phiếu nhận nợ đó có thể có thêm tờ phụ đính kèm. Tờ phụ đính kèm được sử dụng để ghi nội dung bảo lãnh, chuyển nhượng, cầm cố, nhờ thu. Người đầu tiên lập tờ phụ phải gắn liền tờ phụ với hối phiếu nhận nợ và ký tên trên chỗ giáp lai giữa tờ phụ và hối phiếu nhận nợ."
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-18T03:49:54.366Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2fee0d1330ca0017dacc77"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b443"),
    VALUE: {
        text: "A. ĐIỀU KHOẢN DẪN CHIẾU: Điều 54 Luật các CCCN.\nB. NỘI DUNG QUY ĐỊNH: Trường hợp MSB là người phát hành: MSB có nghĩa vụ thanh toán số tiền ghi trên hối phiếu nhận nợ cho người thụ hưởng khi đến hạn thanh toán và có các nghĩa vụ khác như người chấp nhận hối phiếu đòi nợ theo quy định của Luật các CCCN"
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-18T03:57:01.36Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": "2019-07-18T04:41:26.528Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2ff5b61330ca0017dacc78"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b442"),
    VALUE: {
        text: "A. ĐIỀU KHOẢN DẪN CHIẾU: Điều 17 Luật các CCCN.\nB. NỘI DUNG QUY ĐỊNH: \n1.\tTrường hợp MSB là người ký phát:\na)\tMSB có nghĩa vụ thanh toán số tiền ghi trên hối phiếu đòi nợ cho người thụ hưởng khi hối phiếu đòi nợ bị từ chối chấp nhận hoặc bị từ chối thanh toán.\nb)\tTrường hợp người chuyển nhượng hoặc người bảo lãnh đã thanh toán hối phiếu đòi nợ cho người thụ hưởng sau khi hối phiếu đòi nợ bị từ chối chấp nhận hoặc bị từ chối thanh toán thì MSB có nghĩa vụ thanh toán cho người chuyển nhượng hoặc người bảo lãnh số tiền ghi trên hối phiếu đó.\n"
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-18T04:29:42.69Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d30106d733e7000173e99f6"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b444"),
    VALUE: {
        text: "A. ĐIỀU KHOẢN DẪN CHIẾU: Điều 22, Luật các CCCN.\nB. NỘI DUNG QUY ĐỊNH: Trường hợp MSB là người chấp nhận hối phiếu đòi nợ thì sau khi chấp nhận hối phiếu đòi nợ, MSB phải có nghĩa vụ thanh toán không điều kiện hối phiếu đòi nợ theo nội dung đã chấp nhận cho người thụ hưởng, người đã thanh toán hối phiếu đòi nợ theo quy định của Luật các CCCN.\nC. CHẾ TÀI theo điểm a khoản 2 Điều 29 Nghị định 96/2014/NĐ-CP: \n2. Phạt tiền từ 20.000.000 đồng đến 30.000.000 đồng đối với một trong các hành vi vi phạm sau đây:\na) Thực hiện không đúng quy định về nghĩa vụ của người chấp nhận quy định tại Điều 22 Luật Các công cụ chuyển nhượng.\n"
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-18T06:23:41.907Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d3010dc733e7000173e99f7"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b445"),
    VALUE: {
        text: "A. ĐIỀU KHOẢN DẪN CHIẾU: Khoản 2, 3 Điều 39 Luật các CCCN; Khoản 2 Điều 12 Quyết định 44/2006/QĐ-NHNN.\nB. NỘI DUNG QUY ĐỊNH: \n1.\tTrường hợp MSB là người thu hộ hối phiếu đòi nợ thì:\na)\tMSB không được thực hiện các quyền của người thụ hưởng theo hối phiếu đòi nợ ngoài quyền xuất trình hối phiếu đòi nợ để thanh toán, quyền nhận số tiền trên hối phiếu, quyền chuyển giao hối phiếu đòi nợ cho người thu hộ khác để nhờ thu hối phiếu đòi nợ.\nb)\tMSB phải xuất trình hối phiếu đòi nợ cho người bị ký phát để thanh toán theo quy định tại Điều 43 của Luật các CCCN. Trường hợp MSB không xuất trình hoặc xuất trình không đúng thời hạn hối phiếu đòi nợ để thanh toán dẫn đến hối phiếu đòi nợ không được thanh toán thì MSB có trách nhiệm bồi thường thiệt hại cho người thụ hưởng tối đa bằng số tiền ghi trên hối phiếu đòi nợ.\nC. CHẾ TÀI theo điểm b khoản 2 Điều 29 Nghị định 96/2014/NĐ-CP: \n2. Phạt tiền từ 20.000.000 đồng đến 30.000.000 đồng đối với một trong các hành vi vi phạm sau đây:\nb) Nhờ thu qua người thu hộ không đúng quy định tại các khoản 1, 2 và 3 Điều 39 Luật Các công cụ chuyển nhượng.\n\n"
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-18T06:25:32.823Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d301130733e7000173e99f8"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b446"),
    VALUE: {
        text: "A. ĐIỀU KHOẢN DẪN CHIẾU: Khoản 1 Điều 13 Quyết định 44/2006/QĐ-NHNN.\nB. NỘI DUNG QUY ĐỊNH: Trường hợp MSB là người thu hộ hối phiếu đòi nợ thì: Chủ tịch Hội đồng quản trị, Tổng giám đốc MSB chịu trách nhiệm hướng dẫn, tổ chức thực hiện đối với các đơn vị trực thuộc và khách hàng của mình, bảo đảm việc nhờ thu hối phiếu qua người thu hộ được kịp thời, nhanh chóng, chính xác và an toàn tài sản.\nC. CHẾ TÀI: theo điểm b khoản 2 Điều 29 Nghị định 96/2014/NĐ-CP: \n2. Phạt tiền từ 20.000.000 đồng đến 30.000.000 đồng đối với một trong các hành vi vi phạm sau đây:\nb) Nhờ thu qua người thu hộ không đúng quy định tại các khoản 1, 2 và 3 Điều 39 Luật Các công cụ chuyển nhượng.\n"
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-18T06:26:56.09Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d3011aa733e7000173e99f9"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b447"),
    VALUE: {
        text: "A. ĐIỀU KHOẢN DẪN CHIẾU: Điều 13 Luật các CCCN; Khoản 3 Điều 10 Quyết định 44/2006/QĐ-NHNN; Khoản 5 Điều 10 Quyết định 44/2006/QĐ-NHNN; Khoản 6 Điều 10 Quyết định 44/2006/QĐ-NHNN.\nB. NỘI DUNG QUY ĐỊNH:\n1.\tTrường hợp MSB là người phát hành: Khi nhận được thông báo về việc mất hối phiếu phải kiểm tra ngay các thông tin về tờ hối phiếu bị mất và vào sổ theo dõi hối phiếu đã được thông báo mất. MSB không được thanh toán tờ hối phiếu đã được thông báo bị mất. \nTrường hợp tờ hối phiếu đã báo mất được xuất trình yêu cầu thanh toán, MSB có trách nhiệm lập biên bản thu giữ tờ hối phiếu đó và thông báo cho người ra thông báo mất hối phiếu đến giải quyết.\n2.\tTrường hợp MSB là người phát hành: Phải lưu giữ thông tin về hối phiếu bị báo mất và thông báo bằng văn bản cho Trung tâm thông tin tín dụng của NHNN.\n3.\tTrường hợp MSB là người thu hộ: Phải thường xuyên tra cứu, cập nhật thông tin về hối phiếu đã có thông báo bị mất qua Trung tâm thông tin tín dụng của NHNN và không được nhận thu hộ đối với các hối phiếu đã có thông báo bị mất. Trường hợp sau khi nhận chuyển giao hối phiếu, người thu hộ bị mất hối phiếu trong quá trình xử lý nhờ thu thì MSB phải thông báo ngay cho người nhờ thu để người nhờ thu xử lý theo Khoản 1 Điều 10 Quyết định 44/2006/QĐ-NHNN.\nC. THAM CHIẾU: theo khoản 4 Điều 10 Quyết định 44/2006/QĐ-NHNN: \n4. Người bị ký phát, người phát hành không chịu trách nhiệm về những thiệt hại do việc lợi dụng tờ hối phiếu bị mất gây ra nếu trước khi nhận được thông báo mất, tờ hối phiếu đó đã được xuất trình và thanh toán theo đúng quy định của pháp luật.\nNếu sau khi có thông báo về hối phiếu bị mất mà người bị ký phát, người phát hành vẫn thanh toán cho tờ hối phiếu đó thì người bị ký phát, người phát hành chịu trách nhiệm bồi thường thiệt hại cho người thụ hưởng.\n\n"
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-18T06:28:58.394Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d301201733e7000173e99fa"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b448"),
    VALUE: {
        text: "A. ĐIỀU KHOẢN DẪN CHIẾU: Điều 4 Quyết định 44/2006/QĐ-NHNN.\nB. NỘI DUNG QUY ĐỊNH: Tổng giám đốc MSB được quy định mức phí dịch vụ thu hộ hối phiếu do đơn vị mình cung cấp nhưng phải đảm bảo phù hợp với quy định của Ngân hàng Nhà nước Việt Nam về thu phí dịch vụ thanh toán qua ngân hàng.\nC. CHẾ TÀI: theo điểm c khoản 1 và điểm a khoản 4 Điều 14 Nghị định 96/2014/NĐ-CP: \n1. Phạt tiền từ 10.000.000 đồng đến 20.000.000 đồng đối với một trong các hành vi vi phạm sau đây: \nc) Thu các loại phí cung ứng dịch vụ không đúng quy định pháp luật.\n4. Áp dụng biện pháp khắc phục hậu quả: \na) Buộc nộp vào ngân sách nhà nước số lợi bất hợp pháp có được do thực hiện hành vi vi phạm mức phí cung ứng dịch vụ.\n"
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-18T06:30:25.467Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d301b6e733e7000173e9a09"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b449"),
    VALUE: {
        text: "A. ĐIỀU KHOẢN DẪN CHIẾU: Điều 58, Luật các CCCN.\nB. NỘI DUNG QUY ĐỊNH:\n1.\tMặt trước séc có các nội dung sau đây:\na)\tTừ \"Séc\" được in phía trên séc;\nb)\tSố tiền xác định;\nc)\tTên của MSB (là người bị ký phát);\nd)\tTên đối với tổ chức hoặc họ, tên đối với cá nhân của người thụ hưởng được người ký phát chỉ định hoặc yêu cầu thanh toán séc theo lệnh của người thụ hưởng hoặc yêu cầu thanh toán séc cho người cầm giữ;\ne)\tĐịa điểm thanh toán;\nf)\tNgày ký phát;\ng)\tTên đối với tổ chức hoặc họ, tên đối với cá nhân và chữ ký của người ký phát.\n1.\tSéc thiếu một trong các nội dung quy định tại khoản 1 Điều này thì không có giá trị, trừ trường hợp địa điểm thanh toán không ghi trên séc thì séc được thanh toán tại địa điểm kinh doanh của người bị ký phát.\n2.\tTrường hợp séc được thanh toán qua Trung tâm thanh toán bù trừ séc thì trên séc phải có thêm các nội dung theo quy định của Trung tâm thanh toán bù trừ séc.\n3.\tMặt sau của séc được sử dụng để ghi các nội dung chuyển nhượng séc.\n4.\tSố tiền ghi bằng số trên séc phải bằng với số tiền ghi bằng chữ trên séc. Nếu số tiền ghi bằng số khác với số tiền ghi bằng chữ thì séc không có giá trị thanh toán."
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-18T07:10:38.38Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d3023d0733e7000173e9a18"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b44b"),
    VALUE: {
        text: "A. ĐIỀU KHOẢN DẪN CHIẾU: Điều 21 Thông tư 22/2015/TT-NHNN\nB. NỘI DUNG QUY ĐỊNH: Khi séc được xuất trình để thanh toán theo thời hạn và địa điểm xuất trình theo đúng quy định tại Điều 19 và Điều 20 của Thông tư 22/2015/TT-NHNN và người ký phát có đủ khả năng thanh toán để chi trả số tiền ghi trên séc, thì MSB kiểm tra tính hợp pháp, hợp lệ của tờ séc và có trách nhiệm thanh toán cho người thụ hưởng hoặc người được người thụ hưởng ủy quyền đối với tờ séc đảm bảo tính hợp pháp, hợp lệ ngay trong ngày xuất trình hoặc ngày làm việc tiếp theo sau ngày xuất trình đó. MSB thực hiện thanh toán séc theo đúng quy trình quy định tại khoản 1 và khoản 2 Điều 21 Thông tư 22/2015/TT-NHNN.\nC. THAM CHIẾU:"
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-18T07:46:24.031Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d30243e733e7000173e9a19"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b44c"),
    VALUE: {
        text: "A. ĐIỀU KHOẢN DẪN CHIẾU: Điều 22 Thông tư 22/2015/TT-NHNN.\nB. NỘI DUNG QUY ĐỊNH: MSB xử lý séc không đủ khả năng thanh toán như sau:\n1.\tMSB thông báo cho người ký phát về việc tờ séc không đủ khả năng thanh toán và có trách nhiệm lưu giữ thông tin về người ký phát séc không đủ khả năng thanh toán vào hồ sơ lưu của mình. Việc thông báo này có thể bằng điện thoại, điện tín hoặc phương tiện thông tin thích hợp khác.\n2.\tĐồng thời, MSB thông báo về việc tờ séc không đủ khả năng thanh toán cho người xuất trình séc (bao gồm người thụ hưởng hoặc người thu hộ) ngay trong ngày xuất trình hoặc ngày làm việc tiếp theo sau ngày xuất trình tờ séc đó bằng phương thức thông tin theo thỏa thuận giữa hai bên.\n3.\tTrong thời hạn 05 ngày làm việc kể từ ngày gửi thông báo cho người ký phát séc về tờ séc không đủ khả năng thanh toán, MSB thông báo cho Trung tâm thông tin tín dụng Quốc gia Việt Nam thông tin séc không đủ khả năng thanh toán theo Phụ lục 07 đính kèm Thông tư 22/2015/TT-NHNN. \n"
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-18T07:48:14.408Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": "2019-07-18T07:48:53.251Z",
    "UPDATED_BY": "5d2c5da072e61100084c0a84"
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d3024cf733e7000173e9a1a"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b44d"),
    VALUE: {
        text: "A. ĐIỀU KHOẢN DẪN CHIẾU: Điều 24 Thông tư 22/2015/TT-NHNN.\nB. NỘI DUNG QUY ĐỊNH: MSB là tổ chức cung ứng séc: Có trách nhiệm ban hành quy định nội bộ hướng dẫn về việc cung ứng séc, thanh toán séc, đình chỉ thanh toán séc tại MSB phù hợp với quy định của Thông tư 22/2015/TT-NHNN.\nC. THAM CHIẾU: Quy định Cung ứng và sử dụng Séc mã số QD DV 010 do Tổng Giám đốc MSB ban hành lần 3 ngày 18/5/2018 và văn bản sửa đổi, bổ sung trong từng thời kỳ."
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-18T07:50:39.065Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d30252a733e7000173e9a1b"),
    "NODE__ID": ObjectId("5d2d94a76fb49b001771b44e"),
    VALUE: {
        text: "A. ĐIỀU KHOẢN DẪN CHIẾU: Khoản 3, Khoản 5 Điều 26 Thông tư 22/2015/TT-NHNN.\nB. NỘI DUNG QUY ĐỊNH:\n1.\tMSB khi nhận được thông báo về việc tờ séc bị mất, phải kiểm tra ngay các thông tin về tờ séc bị mất, và theo dõi séc đã được thông báo mất. MSB không được thanh toán tờ séc đã được báo mất. Khi tờ séc đã được báo mất được xuất trình đòi thanh toán, MSB có trách nhiệm lập biên bản giữ lại tờ séc đó và thông báo cho người ra thông báo mất séc đến giải quyết.\n2.\tMSB có trách nhiệm lưu giữ thông tin về séc bị báo mất và thông báo bằng văn bản cho Trung tâm thông tin tín dụng Quốc gia Việt Nam.\nC. THAM CHIẾU: theo Khoản 4 Điều 26 Thông tư 22/2015/TT-NHNN: \n4. Người bị ký phát không chịu trách nhiệm về các thiệt hại do việc lợi dụng tờ séc bị mất gây ra, nếu trước khi nhận được thông báo mất séc, tờ séc đó đã được xuất trình và thanh toán đúng quy định của pháp luật.\nNếu sau khi có thông báo mất séc mà người bị ký phát vẫn thanh toán cho tờ séc đó thì người bị ký phát chịu trách nhiệm bồi thường thiệt hại cho người thụ hưởng.\n"
    },
    TYPE: "TEXT",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-18T07:52:10.662Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a84"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d358cfe1f65a10017041cd2"),
    "NODE__ID": ObjectId("5d358cfe1f65a10017041cd1"),
    VALUE: null,
    TYPE: null,
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-22T10:16:30.809Z"),
    "CREATED_BY": ObjectId("5d2c5da072e61100084c0a83"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_TEMPLATE").insert([ {
    _id: ObjectId("5d2c5da272e61100084c0ab1"),
    "NODE__ID": ObjectId("5d2c5da072e61100084c0a8e"),
    VALUE: {
        "persistent_menu": [
            {
                locale: "default",
                "composer_input_disabled": false,
                "call_to_actions": [
                    {
                        title: "Sổ tay tuân thủ",
                        type: "nested",
                        "call_to_actions": [
                            {
                                title: "Tuân thủ chung",
                                type: "postback",
                                payload: "GENERAL_COMPLIANCE"
                            },
                            {
                                title: "Tuân thủ nghiệp vụ",
                                type: "postback",
                                payload: "BUSINESS_COMPLIANCE"
                            }
                        ]
                    },
                    {
                        title: "Cẩm nang tư vấn",
                        type: "postback",
                        payload: "HANDBOOK"
                    },
                    {
                        title: "Thời tiết",
                        type: "postback",
                        payload: "WEATHER"
                    }
                ]
            }
        ]
    },
    TYPE: "PERSISTENT_MENU",
    ORDER: NumberInt("0"),
    "CREATED_DATE": ISODate("2019-07-15T11:04:02.155Z"),
    "CREATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a"),
    "UPDATED_AT": "2019-07-24T03:21:14.274Z",
    "UPDATED_BY": "5d2c5d9e72e61100084c0a7f"
} ]);

// ----------------------------
// Collection structure for MCL_USER
// ----------------------------
db.getCollection("MCL_USER").drop();
db.createCollection("MCL_USER");

// ----------------------------
// Documents of MCL_USER
// ----------------------------
db.getCollection("MCL_USER").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a87"),
    PSID: "2558001157566882",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.393Z"),
    "IS_ACTIVE": NumberInt("1"),
    FULLNAME: "Linh Tran Mai Thuy",
    "DISTINGUISHED_NAME": "Linh Tran Mai Thuy",
    "FIRST_NAME": "Linh",
    "LAST_NAME": "Tran Mai Thuy",
    "PHONE_NUMBER": "0975235032",
    EMAIL: "linhtmt1@msb.com.vn",
    PORTRAIT: null,
    GROUPS: [ ],
    STATUS: NumberInt("1"),
    "UPDATED_AT": ISODate("2019-07-15T11:04:01.313Z"),
    "UPDATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a")
} ]);
db.getCollection("MCL_USER").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a8b"),
    PSID: "3048424208504702",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.395Z"),
    "IS_ACTIVE": NumberInt("1"),
    FULLNAME: "Hung Nghiem Manh (CN-GD.PTNHDT)",
    "DISTINGUISHED_NAME": "Hung Nghiem Manh (CN-GD.PTNHDT)",
    "FIRST_NAME": "Hung",
    "LAST_NAME": "Nghiem Manh (CN-GD.PTNHDT)",
    "PHONE_NUMBER": "0983364369",
    EMAIL: "hungnm@msb.com.vn",
    PORTRAIT: null,
    GROUPS: [
        "5d36cf61229bc60017eaa12b"
    ],
    STATUS: NumberInt("1"),
    "UPDATED_AT": ISODate("2019-07-15T11:04:01.3Z"),
    "UPDATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a")
} ]);
db.getCollection("MCL_USER").insert([ {
    _id: ObjectId("5d37dba21131cc00178a451c"),
    PSID: "2584269004917663",
    "CREATED_AT": ISODate("2019-07-24T04:16:34.097Z"),
    "IS_ACTIVE": NumberInt("1"),
    FULLNAME: null,
    "DISTINGUISHED_NAME": null,
    "FIRST_NAME": null,
    "LAST_NAME": null,
    "PHONE_NUMBER": null,
    EMAIL: null,
    PORTRAIT: null,
    GROUPS: [ ],
    STATUS: NumberInt("0"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_USER").insert([ {
    _id: ObjectId("5d37dd3e1131cc00178a451d"),
    PSID: "2394866633878537",
    "CREATED_AT": ISODate("2019-07-24T04:23:26.261Z"),
    "IS_ACTIVE": NumberInt("1"),
    FULLNAME: null,
    "DISTINGUISHED_NAME": null,
    "FIRST_NAME": null,
    "LAST_NAME": null,
    "PHONE_NUMBER": null,
    EMAIL: null,
    PORTRAIT: null,
    GROUPS: [ ],
    STATUS: NumberInt("0"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_USER").insert([ {
    _id: ObjectId("5d37df1b1131cc00178a451e"),
    PSID: "2494020004011999",
    "CREATED_AT": ISODate("2019-07-24T04:31:23.111Z"),
    "IS_ACTIVE": NumberInt("1"),
    FULLNAME: null,
    "DISTINGUISHED_NAME": null,
    "FIRST_NAME": null,
    "LAST_NAME": null,
    "PHONE_NUMBER": null,
    EMAIL: null,
    PORTRAIT: null,
    GROUPS: [ ],
    STATUS: NumberInt("0"),
    "UPDATED_AT": null,
    "UPDATED_BY": null
} ]);
db.getCollection("MCL_USER").insert([ {
    _id: ObjectId("5d2c5da072e61100084c0a85"),
    PSID: "1984892361559396",
    "CREATED_AT": ISODate("2019-07-15T11:04:00.352Z"),
    "IS_ACTIVE": NumberInt("1"),
    FULLNAME: "Linh Nguyen Hoang (CTV)",
    "DISTINGUISHED_NAME": "Linh Nguyen Hoang (CTV)",
    "FIRST_NAME": "Linh",
    "LAST_NAME": "Nguyen Hoang (CTV)",
    "PHONE_NUMBER": "0365295389",
    EMAIL: "hlinh188@gmail.com",
    PORTRAIT: null,
    GROUPS: [
        "5d36cf61229bc60017eaa12b"
    ],
    STATUS: NumberInt("1"),
    "UPDATED_AT": ISODate("2019-07-15T11:04:01.327Z"),
    "UPDATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a")
} ]);

// ----------------------------
// Collection structure for MCL_USER_ACTIVITY
// ----------------------------
db.getCollection("MCL_USER_ACTIVITY").drop();
db.createCollection("MCL_USER_ACTIVITY");

// ----------------------------
// Documents of MCL_USER_ACTIVITY
// ----------------------------
db.getCollection("MCL_USER_ACTIVITY").insert([ {
    _id: ObjectId("5d37d309ba16dd0017910ee2"),
    PSID: "2394866633878537",
    "FUNCTION_ID": "HOME",
    "CREATED_DATE": ISODate("2019-07-24T03:39:53.012Z")
} ]);
db.getCollection("MCL_USER_ACTIVITY").insert([ {
    _id: ObjectId("5d37d310ba16dd0017910ee3"),
    PSID: "2394866633878537",
    "FUNCTION_ID": "TRACUU",
    "CREATED_DATE": ISODate("2019-07-24T03:40:00.653Z")
} ]);
db.getCollection("MCL_USER_ACTIVITY").insert([ {
    _id: ObjectId("5d37d315ba16dd0017910ee4"),
    PSID: "2394866633878537",
    "FUNCTION_ID": "HANDBOOK",
    "CREATED_DATE": ISODate("2019-07-24T03:40:05.011Z")
} ]);
db.getCollection("MCL_USER_ACTIVITY").insert([ {
    _id: ObjectId("5d37d54bba16dd0017910ee5"),
    PSID: "2394866633878537",
    "FUNCTION_ID": "WEATHER",
    "CREATED_DATE": ISODate("2019-07-24T03:49:31.307Z")
} ]);
db.getCollection("MCL_USER_ACTIVITY").insert([ {
    _id: ObjectId("5d37d590ba16dd0017910ee6"),
    PSID: "2394866633878537",
    "FUNCTION_ID": "HANDBOOK",
    "CREATED_DATE": ISODate("2019-07-24T03:50:40.713Z")
} ]);
db.getCollection("MCL_USER_ACTIVITY").insert([ {
    _id: ObjectId("5d37d5f9ba16dd0017910ee7"),
    PSID: "2394866633878537",
    "FUNCTION_ID": "GET_STARTED",
    "CREATED_DATE": ISODate("2019-07-24T03:52:25.614Z")
} ]);
db.getCollection("MCL_USER_ACTIVITY").insert([ {
    _id: ObjectId("5d37d6d9ba16dd0017910ee8"),
    PSID: "2394866633878537",
    "FUNCTION_ID": "WEATHER",
    "CREATED_DATE": ISODate("2019-07-24T03:56:09.377Z")
} ]);
db.getCollection("MCL_USER_ACTIVITY").insert([ {
    _id: ObjectId("5d37d84a1131cc00178a4519"),
    PSID: "2394866633878537",
    "FUNCTION_ID": "TRACUU",
    "CREATED_DATE": ISODate("2019-07-24T04:02:18.414Z")
} ]);
db.getCollection("MCL_USER_ACTIVITY").insert([ {
    _id: ObjectId("5d37d8861131cc00178a451a"),
    PSID: "2394866633878537",
    "FUNCTION_ID": "GET_STARTED",
    "CREATED_DATE": ISODate("2019-07-24T04:03:18.998Z")
} ]);
db.getCollection("MCL_USER_ACTIVITY").insert([ {
    _id: ObjectId("5d37d8aa1131cc00178a451b"),
    PSID: "2394866633878537",
    "FUNCTION_ID": "TRACUU",
    "CREATED_DATE": ISODate("2019-07-24T04:03:54.716Z")
} ]);

// ----------------------------
// Collection structure for MCL_USER_FB_INFO
// ----------------------------
db.getCollection("MCL_USER_FB_INFO").drop();
db.createCollection("MCL_USER_FB_INFO");

// ----------------------------
// Documents of MCL_USER_FB_INFO
// ----------------------------
db.getCollection("MCL_USER_FB_INFO").insert([ {
    _id: ObjectId("5d2c5da172e61100084c0aa3"),
    "FB_NAME": "Do Tien Dat",
    "FB_FIRST_NAME": "Do",
    "FB_LAST_NAME": "Dat",
    "FB_PROFILE_PIC": "https://platform-lookaside.fbsbx.com/platform/profilepic/?psid=2904348559575591&width=1024&ext=1566462601&hash=AeTTEqYuPO7iAeBV",
    "FB_LOCALE": "vi_VN",
    "FB_GENDER": "male",
    "FB_TIMEZONE": NumberInt("7"),
    PSID: "2904348559575591",
    "CREATED_AT": ISODate("2019-07-15T11:04:01.496Z"),
    "UPDATED_AT": ISODate("2019-07-23T08:30:01.223Z"),
    "UPDATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a")
} ]);
db.getCollection("MCL_USER_FB_INFO").insert([ {
    _id: ObjectId("5d2c5da172e61100084c0aa0"),
    "FB_NAME": "Manh Hung Nghiem",
    "FB_FIRST_NAME": "Manh Hung",
    "FB_LAST_NAME": "Nghiem",
    "FB_PROFILE_PIC": "https://platform-lookaside.fbsbx.com/platform/profilepic/?psid=3048424208504702&width=1024&ext=1566462601&hash=AeSbjrupMAAr19Ex",
    "FB_LOCALE": "vi_VN",
    "FB_GENDER": "male",
    "FB_TIMEZONE": NumberInt("7"),
    PSID: "3048424208504702",
    "CREATED_AT": ISODate("2019-07-15T11:04:01.484Z"),
    "UPDATED_AT": ISODate("2019-07-23T08:30:01.57Z"),
    "UPDATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a")
} ]);
db.getCollection("MCL_USER_FB_INFO").insert([ {
    _id: ObjectId("5d2c5da172e61100084c0aa4"),
    "FB_NAME": "Hoàng Linh",
    "FB_FIRST_NAME": "Hoàng",
    "FB_LAST_NAME": "Linh",
    "FB_PROFILE_PIC": "https://platform-lookaside.fbsbx.com/platform/profilepic/?psid=1984892361559396&width=1024&ext=1566462601&hash=AeQtVZMkXm-CopHD",
    "FB_LOCALE": "vi_VN",
    "FB_GENDER": "male",
    "FB_TIMEZONE": NumberInt("7"),
    PSID: "1984892361559396",
    "CREATED_AT": ISODate("2019-07-15T11:04:01.499Z"),
    "UPDATED_AT": ISODate("2019-07-23T08:30:01.736Z"),
    "UPDATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a")
} ]);
db.getCollection("MCL_USER_FB_INFO").insert([ {
    _id: ObjectId("5d2c5da172e61100084c0aa2"),
    "FB_NAME": "Thuỳ Linh Trần Mai",
    "FB_FIRST_NAME": "Thuỳ Linh",
    "FB_LAST_NAME": "Trần Mai",
    "FB_PROFILE_PIC": "https://platform-lookaside.fbsbx.com/platform/profilepic/?psid=2558001157566882&width=1024&ext=1566462601&hash=AeRznDTujCwX7DIV",
    "FB_LOCALE": "vi_VN",
    "FB_GENDER": "female",
    "FB_TIMEZONE": NumberInt("7"),
    PSID: "2558001157566882",
    "CREATED_AT": ISODate("2019-07-15T11:04:01.489Z"),
    "UPDATED_AT": ISODate("2019-07-23T08:30:01.917Z"),
    "UPDATED_BY": ObjectId("5d2c5d9c72e61100084c0a3a")
} ]);
