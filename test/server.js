var express = require('express'),
  passport = require('passport'),
  bodyParser = require('body-parser'),
  LdapStrategy = require('passport-ldapauth');

var OPTS = {
  server: {
    url: 'ldap://10.1.16.1:3268',
    bindDN: 'cn=root',
    bindCredentials: 'secret',
    searchBase: 'ou=passport-ldapauth',
    searchFilter: '(uid={{username}})'
  }
};

// var app = express();

passport.use(new LdapStrategy(OPTS));

// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
// app.use(passport.initialize());

// passport.authenticate('ldapauth', { session: false });
passport.authenticate('ldapauth', function(err, user, info) {
  if (err) { 
    console.log(err);
   }
  if (!user) {  
    console.log(err); 
  }
  req.logIn(user, function(err) {
    if (err) { console.log(err); }
    else {
      console.log(user);
    }
  });
  console.log(info);
});