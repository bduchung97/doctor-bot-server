const ObjectId = require('mongodb').ObjectID,
  MongoClient = require('mongodb').MongoClient,
  config = require('../src/mongodb/config');
exports.show_all_user_activities = async function show_all_user_activities() {
  let result = 'err';
  try {
    let client = await MongoClient.connect(config.url, {
      useNewUrlParser: true
    });
    let mgdb = client.db(config.dbName);
    result = await mgdb.collection(config.collection.USER_ACTIVITY).find().toArray();
    return result;
  } catch (e) {
    console.log(e);
  }
}

//show_all_user_activities()
