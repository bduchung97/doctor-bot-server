const XLSX = require('xlsx');

exports.read_file_excel = function read_file_excel(filename, callback) {
  var workbook = XLSX.readFile(filename);
  var sheet_name_list = workbook.SheetNames;
  var data = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]])
  // for (i in data) {
  //   data[i].ANSWER = data[i].ANSWER.replace(/\r\n/g, '<br/>');
  // }
  console.log(data)
  callback(null, data);
}

// read_file_excel('./QnA.xlsx')