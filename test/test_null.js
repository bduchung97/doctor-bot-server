const ObjectId = require('mongodb').ObjectID,
  MongoClient = require('mongodb').MongoClient,
  config = require('../src/mongodb/config');


function show_all_user_activities(template_id, data, callback) {
  MongoClient.connect(config.url, {
    useNewUrlParser: true
  }, function (err, client) {
    const mgdb = client.db(config.dbName);
    mgdb.collection(config.collection.TEMPLATE).updateOne({ _id: ObjectId(template_id) }, { $set: data }, function (err, result) {
      //db.get().collection(config.collection.USER_ACTIVITY).find(filter).toArray(function (err, result) {
      if (err) {
        callback(err, null);
      }
      else {
        if (result.modifiedCount == 1) {
          callback(null, result.modifiedCount);
        }
        else {
          callback(null, null);
        }
      }
    });
  });
}
let data = {
  VALUE: {
    "attachment": {
        "type": "template",
        "payload": {
            "template_type": "generic",
            "elements": [
                {
                    "title": "Tuân thủ chung",
                    "image_url": "https://i.imgur.com/QfNS9mS.jpg",
                    "subtitle": "Bộ chỉ tiêu tuân thủ chung",
                    "buttons": [
                        {
                            "title": "CHUNG",
                            "type": "postback",
                            "payload": "GENERAL"
                        }
                    ]
                }
            ]
        }
    }
},
  UPDATED_AT: '2019-06-10T02:03:07.046Z',
  UPDATED_BY: '5cfdba2821a89d0610037075'
}
show_all_user_activities('5cfdc03de1bc8216d45ef8e1', data);