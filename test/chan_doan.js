// let action = ['rest', 'normal', 'training'],
//  status1 = ['Bình thường', 'Không bình thường', 'Tiên lượng xấu'],
//  gender = ['male', 'female'];

exports.chan_doan = function chan_doan(age1, gender1, action1, heart_rate1){
	var status;
	if(gender1 == 'male'){
		if(age1 < 18){
			if(action1 == 'rest'){
				if(heart_rate1 > 50 && heart_rate1 <= 60){
					status = 'Bình thường';
				} else if (heart_rate1 > 60 & heart_rate1 <= 65){
					status = 'Không bình thường';
				} else if (heart_rate1 > 65){
					status = 'Tiên lượng xấu';
				}
			} else if(action1 == 'normal'){
				if(heart_rate1 > 65 && heart_rate1 <= 75){
					status = 'Bình thường';
				} else if (heart_rate1 > 75 & heart_rate1 <= 120){
					status = 'Không bình thường';
				} else if (heart_rate1 > 120){
					status = 'Tiên lượng xấu';
				}
			} else if(action == 'training'){
				if(heart_rate1 > 120 && heart_rate1 <= 142){
					status = 'Bình thường';
				} else if(heart_rate1 > 142){
					status = 'Tiên lượng xấu';
				}
			}
		} else if(age1 >= 18 && age1 < 36){
			if(action1 == 'rest'){
				if(heart_rate1 > 56 && heart_rate1 <= 65){
					status = 'Bình thường';
				} else if (heart_rate1 > 65 & heart_rate1 <= 75){
					status = 'Không bình thường';
				} else if (heart_rate1 > 75){
					status = 'Tiên lượng xấu';
				}
			} else if(action1 == 'normal'){
				if(heart_rate1 > 60 && heart_rate1 <= 80){
					status = 'Bình thường';
				} else if (heart_rate1 > 80 & heart_rate1 <= 130){
					status = 'Không bình thường';
				} else if (heart_rate1 > 130){
					status = 'Tiên lượng xấu';
				}
			} else if(action == 'training'){
				if(heart_rate1 > 130 && heart_rate1 <= 142){
					status = 'Bình thường';
				} else if(heart_rate1 > 142){
					status = 'Tiên lượng xấu';
				}
			}
		} else if(age1 >= 36 && age1 < 56){
			if(action1 == 'rest'){
				if(heart_rate1 > 57 && heart_rate1 <= 63){
					status = 'Bình thường';
				} else if (heart_rate1 > 63 & heart_rate1 <= 73){
					status = 'Không bình thường';
				} else if (heart_rate1 > 73){
					status = 'Tiên lượng xấu';
				}
			} else if(action1 == 'normal'){
				if(heart_rate1 > 60 && heart_rate1 <= 80){
					status = 'Bình thường';
				} else if (heart_rate1 > 80 & heart_rate1 <= 120){
					status = 'Không bình thường';
				} else if (heart_rate1 > 120){
					status = 'Tiên lượng xấu';
				}
			} else if(action == 'training'){
				if(heart_rate1 > 120 && heart_rate1 <= 130){
					status = 'Bình thường';
				} else if(heart_rate1 > 130){
					status = 'Tiên lượng xấu';
				}
			}
		} else if(age1 >= 56 && age1 < 65){
			if(action1 == 'rest'){
				if(heart_rate1 > 57 && heart_rate1 <= 67){
					status = 'Bình thường';
				} else if (heart_rate1 > 67 & heart_rate1 <= 76){
					status = 'Không bình thường';
				} else if (heart_rate1 > 76){
					status = 'Tiên lượng xấu';
				}
			} else if(action1 == 'normal'){
				if(heart_rate1 > 60 && heart_rate1 <= 80){
					status = 'Bình thường';
				} else if (heart_rate1 > 80 & heart_rate1 <= 120){
					status = 'Không bình thường';
				} else if (heart_rate1 > 120){
					status = 'Tiên lượng xấu';
				}
			} else if(action == 'training'){
				if(heart_rate1 > 120 && heart_rate1 <= 130){
					status = 'Bình thường';
				} else if(heart_rate1 > 130){
					status = 'Tiên lượng xấu';
				}
			}
		} else {
			if(action1 == 'rest'){
				if(heart_rate1 > 55 && heart_rate1 <= 65){
					status = 'Bình thường';
				} else if (heart_rate1 > 65 & heart_rate1 <= 76){
					status = 'Không bình thường';
				} else if (heart_rate1 > 76){
					status = 'Tiên lượng xấu';
				}
			} else if(action1 == 'normal'){
				if(heart_rate1 > 60 && heart_rate1 <= 80){
					status = 'Bình thường';
				} else if (heart_rate1 > 80 & heart_rate1 <= 120){
					status = 'Không bình thường';
				} else if (heart_rate1 > 120){
					status = 'Tiên lượng xấu';
				}
			} else if(action == 'training'){
				if(heart_rate1 > 120 && heart_rate1 <= 130){
					status = 'Bình thường';
				} else if(heart_rate1 > 130){
					status = 'Tiên lượng xấu';
				}
			}
		}
	} else{
		if(age1 < 18){
			if(action1 == 'rest'){
				if(heart_rate1 > 50 && heart_rate1 <= 60){
					status = 'Bình thường';
				} else if (heart_rate1 > 60 & heart_rate1 <= 65){
					status = 'Không bình thường';
				} else if (heart_rate1 > 65){
					status = 'Tiên lượng xấu';
				}
			} else if(action1 == 'normal'){
				if(heart_rate1 > 65 && heart_rate1 <= 75){
					status = 'Bình thường';
				} else if (heart_rate1 > 75 & heart_rate1 <= 120){
					status = 'Không bình thường';
				} else if (heart_rate1 > 120){
					status = 'Tiên lượng xấu';
				}
			} else if(action == 'training'){
				if(heart_rate1 > 120 && heart_rate1 <= 142){
					status = 'Bình thường';
				} else if(heart_rate1 > 142){
					status = 'Tiên lượng xấu';
				}
			}
		} else if(age1 >= 18 && age1 < 36){
			if(action1 == 'rest'){
				if(heart_rate1 > 56 && heart_rate1 <= 65){
					status = 'Bình thường';
				} else if (heart_rate1 > 65 & heart_rate1 <= 75){
					status = 'Không bình thường';
				} else if (heart_rate1 > 75){
					status = 'Tiên lượng xấu';
				}
			} else if(action1 == 'normal'){
				if(heart_rate1 > 60 && heart_rate1 <= 80){
					status = 'Bình thường';
				} else if (heart_rate1 > 80 & heart_rate1 <= 130){
					status = 'Không bình thường';
				} else if (heart_rate1 > 130){
					status = 'Tiên lượng xấu';
				}
			} else if(action == 'training'){
				if(heart_rate1 > 130 && heart_rate1 <= 142){
					status = 'Bình thường';
				} else if(heart_rate1 > 142){
					status = 'Tiên lượng xấu';
				}
			}
		} else if(age1 >= 36 && age1 < 56){
			if(action1 == 'rest'){
				if(heart_rate1 > 57 && heart_rate1 <= 63){
					status = 'Bình thường';
				} else if (heart_rate1 > 63 & heart_rate1 <= 73){
					status = 'Không bình thường';
				} else if (heart_rate1 > 73){
					status = 'Tiên lượng xấu';
				}
			} else if(action1 == 'normal'){
				if(heart_rate1 > 60 && heart_rate1 <= 80){
					status = 'Bình thường';
				} else if (heart_rate1 > 80 & heart_rate1 <= 120){
					status = 'Không bình thường';
				} else if (heart_rate1 > 120){
					status = 'Tiên lượng xấu';
				}
			} else if(action == 'training'){
				if(heart_rate1 > 120 && heart_rate1 <= 130){
					status = 'Bình thường';
				} else if(heart_rate1 > 130){
					status = 'Tiên lượng xấu';
				}
			}
		} else if(age1 >= 56 && age1 < 65){
			if(action1 == 'rest'){
				if(heart_rate1 > 57 && heart_rate1 <= 67){
					status = 'Bình thường';
				} else if (heart_rate1 > 67 & heart_rate1 <= 76){
					status = 'Không bình thường';
				} else if (heart_rate1 > 76){
					status = 'Tiên lượng xấu';
				}
			} else if(action1 == 'normal'){
				if(heart_rate1 > 60 && heart_rate1 <= 80){
					status = 'Bình thường';
				} else if (heart_rate1 > 80 & heart_rate1 <= 120){
					status = 'Không bình thường';
				} else if (heart_rate1 > 120){
					status = 'Tiên lượng xấu';
				}
			} else if(action == 'training'){
				if(heart_rate1 > 120 && heart_rate1 <= 130){
					status = 'Bình thường';
				} else if(heart_rate1 > 130){
					status = 'Tiên lượng xấu';
				}
			}
		} else {
			if(action1 == 'rest'){
				if(heart_rate1 > 55 && heart_rate1 <= 65){
					status = 'Bình thường';
				} else if (heart_rate1 > 65 & heart_rate1 <= 76){
					status = 'Không bình thường';
				} else if (heart_rate1 > 76){
					status = 'Tiên lượng xấu';
				}
			} else if(action1 == 'normal'){
				if(heart_rate1 > 60 && heart_rate1 <= 80){
					status = 'Bình thường';
				} else if (heart_rate1 > 80 & heart_rate1 <= 120){
					status = 'Không bình thường';
				} else if (heart_rate1 > 120){
					status = 'Tiên lượng xấu';
				}
			} else if(action == 'training'){
				if(heart_rate1 > 120 && heart_rate1 <= 130){
					status = 'Bình thường';
				} else if(heart_rate1 > 130){
					status = 'Tiên lượng xấu';
				}
			}
		}
	}
	
	return status;
}

// console.log(chan_doan(22, gender[0], action[1], 145));
// console.log(st);

// console.log(gender[0]);