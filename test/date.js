let ObjectId = require('mongodb').ObjectID;

let array = [
  ObjectId("5cf5518306242829f841e183"),
  ObjectId("5cf5518306242829f841e184"),
  ObjectId("5cf5518306242829f841e185")
]

let ceia = require('../src/utils/handle_array/check_object_in_object_array.js');

console.log(ceia.check_object_in_object_array(array,  ObjectId("5cf5518306242829f841e185")))